//
//  SMARewardedInterstitial+MediationAdapter.h
//  SmaatoSDKRewardedAds
//
//  Created by Smaato Inc on 01.03.19.
//  Copyright © 2019 Smaato Inc. All rights reserved.￼
//  Licensed under the Smaato SDK License Agreement￼
//  https://www.smaato.com/sdk-license-agreement/
//

#import <SmaatoSDKRewardedAds/SMARewardedInterstitial.h>

/**
 Helper category is required when SmaatoSDK provides functionality through RewardedVideo custom adapters for 3rd party AdNetworks
 */
@interface SMARewardedInterstitial (MediationAdapter)
/**
 An adapter class name from where ad request originates. Default is \c nil

 @warning       The parameter should only be passed when Smaato SDK is secondary.
 The parameter will serve as an additional information to Smaato if any prompt support should be required for your
 application.
 */
@property (nonatomic, nullable, class) NSString *mediationNetworkName;

/**
 An adapter version number from where ad request originates. Default is \c nil

 @warning       The parameter should only be passed when Smaato SDK is secondary.
 The parameter will serve as an additional information to Smaato if any prompt support should be required for your
 application.
 */
@property (nonatomic, nullable, class) NSString *mediationAdapterVersion;

/**
 Third party SDK version for mediated ads. Default is \c nil

 @warning       The parameter should only be passed when Smaato SDK is secondary.
 The parameter will serve as an additional information to Smaato if any prompt support should be required for your
 application.
 */
@property (nonatomic, nullable, class) NSString *mediationNetworkSDKVersion;

@end
