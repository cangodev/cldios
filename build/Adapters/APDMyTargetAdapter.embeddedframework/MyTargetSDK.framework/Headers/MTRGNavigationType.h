//
//  MTRGNavigationType.h
//  myTargetSDK 5.3.5
//
//  Created by Anton Bulankin on 30.05.16.
//  Copyright © 2016 Mail.ru. All rights reserved.
//

typedef enum
{
	MTRGNavigationTypeWeb,
	MTRGNavigationTypeStore
} MTRGNavigationType;
