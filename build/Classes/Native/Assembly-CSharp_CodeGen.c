﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.String App::get_Name()
extern void App_get_Name_mEF80A8B17155890E5EF98BC49FB19F48E3FAA7C2 ();
// 0x00000002 System.String App::get_FacebookAppId()
extern void App_get_FacebookAppId_m1F119D0B5B6B1EF782B0A521ACDA475A7D1AA219 ();
// 0x00000003 System.String App::get_VKontakteAppId()
extern void App_get_VKontakteAppId_mE695AF84FE1EAE2D83735ADAB688204E6AE252AC ();
// 0x00000004 System.String App::get_OdnoklassnikiAppId()
extern void App_get_OdnoklassnikiAppId_mD2A64B078C65A3EF3AAF04E6716E5D3C1CC44702 ();
// 0x00000005 System.String App::get_OdnoklassnikiSecretId()
extern void App_get_OdnoklassnikiSecretId_mFCED46D032B32CD5EB57B5966FD107C163B4C74C ();
// 0x00000006 System.Void App::.ctor()
extern void App__ctor_m90026EF974BA072CE606432C32AB44F8F18D4ECE ();
// 0x00000007 System.Void ShareController::Start()
extern void ShareController_Start_m2BEBEFA890259FBC6D0F4563A4968E3BCDEC7F48 ();
// 0x00000008 System.Void ShareController::OnMouseDown()
extern void ShareController_OnMouseDown_m2B1A3016F7E5E2AE410AF55025006FB8CEBE0E57 ();
// 0x00000009 System.Void ShareController::.ctor()
extern void ShareController__ctor_mEA9D4C5D64F44814BE0315B915EEC6D828A10286 ();
// 0x0000000A System.Void Sharing::shareVia(System.String,System.String,System.String,System.String)
extern void Sharing_shareVia_m54C1F772674A65836049EDD6BE94C933831F0170 ();
// 0x0000000B System.Void Sharing::ShareVia(System.String,System.String,System.String)
extern void Sharing_ShareVia_m92F6E1D072763CB924EF8471E459391968E52A16 ();
// 0x0000000C System.Void Sharing::OnShareError(System.String)
extern void Sharing_OnShareError_m7E1DEE6F27D4D2957CC42278B8BA7CF72E8BB24C ();
// 0x0000000D System.Void Sharing::.ctor()
extern void Sharing__ctor_m4FFB3F0250726B033EE6CAC273957D86F38B2AB2 ();
// 0x0000000E System.Void AppodealDemo::Awake()
extern void AppodealDemo_Awake_m417AAAA6866DBF6439AB08599CCF358849B95F4D ();
// 0x0000000F System.Void AppodealDemo::Init()
extern void AppodealDemo_Init_mD5FA0D05CAD251C72C9BD8002F7AEDF17CFD074C ();
// 0x00000010 System.Void AppodealDemo::OnGUI()
extern void AppodealDemo_OnGUI_mFD79DD9C6CC399A61D114581BC20B4AB336D3AE1 ();
// 0x00000011 System.Void AppodealDemo::InitStyles()
extern void AppodealDemo_InitStyles_mEB3920FC227BA58637A86ACF8398BD33BFF8CFBA ();
// 0x00000012 UnityEngine.Texture2D AppodealDemo::MakeTexure(System.Int32,System.Int32,UnityEngine.Color)
extern void AppodealDemo_MakeTexure_m55286AA85BEA14273DEF30978C80903A39ECDFAA ();
// 0x00000013 System.Void AppodealDemo::showInterstitial()
extern void AppodealDemo_showInterstitial_m4E218C752BB97D0BDAFABDF375E01CD7225DF35F ();
// 0x00000014 System.Void AppodealDemo::showRewardedVideo()
extern void AppodealDemo_showRewardedVideo_m490CFADBB16399F5BFD47400908D4E074ECFE060 ();
// 0x00000015 System.Void AppodealDemo::showBanner()
extern void AppodealDemo_showBanner_m18AE375DC86CA1453F50AA3A712D48ED94F7C1C1 ();
// 0x00000016 System.Void AppodealDemo::showBannerView()
extern void AppodealDemo_showBannerView_m5F293C923BA7F2972DD6B1BF5F0F555226FA4DEE ();
// 0x00000017 System.Void AppodealDemo::showMrecView()
extern void AppodealDemo_showMrecView_m489A0812A1E26A6C9519457207E83407C9709F1D ();
// 0x00000018 System.Void AppodealDemo::hideBanner()
extern void AppodealDemo_hideBanner_mE7859FC8CF19ED2C119DC75451AC53D0FBE2331D ();
// 0x00000019 System.Void AppodealDemo::hideBannerView()
extern void AppodealDemo_hideBannerView_m659C22C7BA8B2E7B0A882F3BF95F18F92A546006 ();
// 0x0000001A System.Void AppodealDemo::hideMrecView()
extern void AppodealDemo_hideMrecView_mA2082E1BD7DDE84C37ABD18CE3BEF81F86D7C6B3 ();
// 0x0000001B System.Void AppodealDemo::updateConsent()
extern void AppodealDemo_updateConsent_mCA4A11D69ADD817B433E7765DCC083D5A6D0C82C ();
// 0x0000001C System.Void AppodealDemo::OnApplicationFocus(System.Boolean)
extern void AppodealDemo_OnApplicationFocus_m45F0A04B5BCC1FC8CAD171B657663697C0A389F5 ();
// 0x0000001D System.Void AppodealDemo::onBannerLoaded(System.Boolean)
extern void AppodealDemo_onBannerLoaded_mE6012C1FE4BEACBBA299687D79DC2387DF641F6F ();
// 0x0000001E System.Void AppodealDemo::onBannerFailedToLoad()
extern void AppodealDemo_onBannerFailedToLoad_mE41F90E27DB5A163A7657EF75E64ACCA65E0EAFE ();
// 0x0000001F System.Void AppodealDemo::onBannerShown()
extern void AppodealDemo_onBannerShown_m51FCF055FB7B8119E2F6F7CAE18955A68463623C ();
// 0x00000020 System.Void AppodealDemo::onBannerClicked()
extern void AppodealDemo_onBannerClicked_m889F7299432E671F881B835CA516B5367563F58B ();
// 0x00000021 System.Void AppodealDemo::onBannerExpired()
extern void AppodealDemo_onBannerExpired_m2D2DF510A27B9FE0E6537499DC0CE3360B0498C4 ();
// 0x00000022 System.Void AppodealDemo::onMrecLoaded(System.Boolean)
extern void AppodealDemo_onMrecLoaded_m31C19F5FB2A3477C8B6399B133F603849EA8CF03 ();
// 0x00000023 System.Void AppodealDemo::onMrecFailedToLoad()
extern void AppodealDemo_onMrecFailedToLoad_m00FD3C8BC870FFB57E9565F978B1927E2182050D ();
// 0x00000024 System.Void AppodealDemo::onMrecShown()
extern void AppodealDemo_onMrecShown_mF18AFC67AF9405613E80AAFCA3A0BE948AA350EA ();
// 0x00000025 System.Void AppodealDemo::onMrecClicked()
extern void AppodealDemo_onMrecClicked_mDEF71272E8E95E5A60CA69CF406FE9581401D71C ();
// 0x00000026 System.Void AppodealDemo::onMrecExpired()
extern void AppodealDemo_onMrecExpired_m66D6B6E1F3841BA985B88BA3C65327FDFD0AF792 ();
// 0x00000027 System.Void AppodealDemo::onInterstitialLoaded(System.Boolean)
extern void AppodealDemo_onInterstitialLoaded_mC816FD18315FA64554A34C09ABC83DEC3D35A972 ();
// 0x00000028 System.Void AppodealDemo::onInterstitialFailedToLoad()
extern void AppodealDemo_onInterstitialFailedToLoad_m9FB8C09C3AE1CEAF36759D7C2517BBF353C1130C ();
// 0x00000029 System.Void AppodealDemo::onInterstitialShown()
extern void AppodealDemo_onInterstitialShown_mE3D4C0273521908C9995FB484257C2267DC98D67 ();
// 0x0000002A System.Void AppodealDemo::onInterstitialClosed()
extern void AppodealDemo_onInterstitialClosed_mD014736D651DFE9E057008F77F98E36088CECE75 ();
// 0x0000002B System.Void AppodealDemo::onInterstitialClicked()
extern void AppodealDemo_onInterstitialClicked_mEF69D501008386CBF419B75A2A0B857613BEB892 ();
// 0x0000002C System.Void AppodealDemo::onInterstitialExpired()
extern void AppodealDemo_onInterstitialExpired_m0FCA36EAF748536BEE6F6FED9A124BECCA4893AB ();
// 0x0000002D System.Void AppodealDemo::onNonSkippableVideoLoaded(System.Boolean)
extern void AppodealDemo_onNonSkippableVideoLoaded_m3226622D0AD489BE52DBC298F12258694590C3E2 ();
// 0x0000002E System.Void AppodealDemo::onNonSkippableVideoFailedToLoad()
extern void AppodealDemo_onNonSkippableVideoFailedToLoad_m0064A3D742703BCCFFD75257C867BDCCF0CC2094 ();
// 0x0000002F System.Void AppodealDemo::onNonSkippableVideoShown()
extern void AppodealDemo_onNonSkippableVideoShown_m17A2D884F016C0302321923751F0092C3558486B ();
// 0x00000030 System.Void AppodealDemo::onNonSkippableVideoClosed(System.Boolean)
extern void AppodealDemo_onNonSkippableVideoClosed_mBDE0A1066A23497B7684369C1B35C6B951C1A4B9 ();
// 0x00000031 System.Void AppodealDemo::onNonSkippableVideoFinished()
extern void AppodealDemo_onNonSkippableVideoFinished_mE68832B2D1DDFBFFC61A032EF3704C3169594509 ();
// 0x00000032 System.Void AppodealDemo::onNonSkippableVideoExpired()
extern void AppodealDemo_onNonSkippableVideoExpired_mA343C6DB3E880419BD413225DCF6DB0C2112110C ();
// 0x00000033 System.Void AppodealDemo::onRewardedVideoLoaded(System.Boolean)
extern void AppodealDemo_onRewardedVideoLoaded_m61BAC208371F613016DDB5A9898FE8ED16223B84 ();
// 0x00000034 System.Void AppodealDemo::onRewardedVideoFailedToLoad()
extern void AppodealDemo_onRewardedVideoFailedToLoad_mA30258C7746A8659CA854B2365EE4189F8266E4E ();
// 0x00000035 System.Void AppodealDemo::onRewardedVideoShown()
extern void AppodealDemo_onRewardedVideoShown_mBE08A0185DF724CF703347CE12DF4DC23DA31719 ();
// 0x00000036 System.Void AppodealDemo::onRewardedVideoClosed(System.Boolean)
extern void AppodealDemo_onRewardedVideoClosed_m6F27E30D89C0127DA2C39E44EEF88121557431E6 ();
// 0x00000037 System.Void AppodealDemo::onRewardedVideoFinished(System.Double,System.String)
extern void AppodealDemo_onRewardedVideoFinished_m034405FC6306160311D78BA9B39AB60A8B23881C ();
// 0x00000038 System.Void AppodealDemo::onRewardedVideoExpired()
extern void AppodealDemo_onRewardedVideoExpired_m16ACA2BEF34495F17C89E22B32C19C31FFA9746A ();
// 0x00000039 System.Void AppodealDemo::onRewardedVideoClicked()
extern void AppodealDemo_onRewardedVideoClicked_mB4E8B8613AB53B11DF9D495F1559FE244107A5B7 ();
// 0x0000003A System.Void AppodealDemo::writeExternalStorageResponse(System.Int32)
extern void AppodealDemo_writeExternalStorageResponse_m83652E782BE1A3425DD89F6863DAB59C8AE17173 ();
// 0x0000003B System.Void AppodealDemo::accessCoarseLocationResponse(System.Int32)
extern void AppodealDemo_accessCoarseLocationResponse_m4A90137A4B1166E29A4D6981F37D28F7BD119BCA ();
// 0x0000003C System.Void AppodealDemo::.ctor()
extern void AppodealDemo__ctor_m8D92B9A7684D9BD78CBF425EECBE43FC811763A5 ();
// 0x0000003D System.Void GDPR::Start()
extern void GDPR_Start_m3B1EA6A12ACBD53AF81ED3DE56261B7DDDD4158F ();
// 0x0000003E System.Void GDPR::onYesClick()
extern void GDPR_onYesClick_m120F1208A306961CEAA74D4A4556D68546E93A69 ();
// 0x0000003F System.Void GDPR::onNoClick()
extern void GDPR_onNoClick_m913602283382ABFDE14F40FE8BC33E10340C77CC ();
// 0x00000040 System.Void GDPR::onPLClick()
extern void GDPR_onPLClick_m978EB652B45D184376E68890F3D73C0CD7CE0884 ();
// 0x00000041 System.Void GDPR::onCloseClick()
extern void GDPR_onCloseClick_m12AC1CA6C44773E4BFCBA1801095F0D00CDDFE3C ();
// 0x00000042 System.Void GDPR::.ctor()
extern void GDPR__ctor_mD25800B726F72F65EC67B133F8545C9900C869D0 ();
// 0x00000043 System.Void loading::Start()
extern void loading_Start_mC8FF407F4A326E079E33A7241835358120B9E154 ();
// 0x00000044 System.Void loading::.ctor()
extern void loading__ctor_m62F43F895C4DEEA1F2F75702EFB4E3BAD1E706AC ();
// 0x00000045 System.Void activate::Start()
extern void activate_Start_m7F9CA5B78AA33E9ABC724F058BDE1B917002AAE5 ();
// 0x00000046 System.Void activate::FixedUpdate()
extern void activate_FixedUpdate_mDC38B5B2D49A7FB215058B8F0C1669EBD692D19D ();
// 0x00000047 System.Void activate::.ctor()
extern void activate__ctor_m12EB5E629E0BF677CD1AA8ABC105F70E55C364DF ();
// 0x00000048 System.Void actmoney::Start()
extern void actmoney_Start_m4F106FB4D6CDF42A9F6759B363F1055AAD0EBA60 ();
// 0x00000049 System.Void actmoney::Update()
extern void actmoney_Update_m66EDF1D311ECF0AC2A745BE05BD30297BB919265 ();
// 0x0000004A System.Void actmoney::.ctor()
extern void actmoney__ctor_m2200E33FDA7DB236EC95AE8AD92CB4013E024621 ();
// 0x0000004B System.Void audiocontrol::Start()
extern void audiocontrol_Start_m22DAFB425C348A74B8C9C0F9035D1BD04C804D7B ();
// 0x0000004C System.Void audiocontrol::Update()
extern void audiocontrol_Update_mA39E8268C70899D2F233BC682206D917C36E9AD2 ();
// 0x0000004D System.Void audiocontrol::.ctor()
extern void audiocontrol__ctor_mF4C937F5BCA330EF353F341222B5B075C75F6BCD ();
// 0x0000004E System.Void bannerload::Start()
extern void bannerload_Start_m979E4F01EFE9526DAF5DD2C047BAFEC06D30A474 ();
// 0x0000004F System.Void bannerload::FixedUpdate()
extern void bannerload_FixedUpdate_mB33C60BB074F02552E587766F545E74AAAE79D4E ();
// 0x00000050 System.Void bannerload::.ctor()
extern void bannerload__ctor_mFB3C9E1783262119191B39633D9E3A69CE712DF4 ();
// 0x00000051 System.Void calltip::Start()
extern void calltip_Start_mE34A140D2F6E66E26988FA0FF6B98BDBC8AC5DF1 ();
// 0x00000052 System.Void calltip::ShowRewardedAd()
extern void calltip_ShowRewardedAd_m6A6C9848ACAFB5E3EAFD7F5771AED2BEEC4F4E36 ();
// 0x00000053 System.Void calltip::FixedUpdate()
extern void calltip_FixedUpdate_mB8CAF1A5848C89F7829705E1E80F7015D4E22958 ();
// 0x00000054 System.Void calltip::OnMouseDown()
extern void calltip_OnMouseDown_mF230F3AFF8C01A21FD3BF760FF8C7A0EE952B1AB ();
// 0x00000055 System.Void calltip::.ctor()
extern void calltip__ctor_mDC4A925E88A3B1E40C4C7D38644670D4B362472A ();
// 0x00000056 System.Void cameracontrol::Start()
extern void cameracontrol_Start_m1BDF3A9E5DB3B685ECE1688DC5C17527512F6BE9 ();
// 0x00000057 System.Void cameracontrol::Update()
extern void cameracontrol_Update_m17AB36CD0708F7B567A7A233865624C8036EF6D9 ();
// 0x00000058 System.Void cameracontrol::changecam()
extern void cameracontrol_changecam_m47128F3FA13C4BBBE1E2020112A0C834641291D3 ();
// 0x00000059 System.Void cameracontrol::.ctor()
extern void cameracontrol__ctor_mF646030CE9536B36E3A1483E806E5C83EB1BA510 ();
// 0x0000005A System.Void cameracontrol::.cctor()
extern void cameracontrol__cctor_mE929C3103CAA277588F890AAB9BB2F705057E37E ();
// 0x0000005B System.Void changetip::Start()
extern void changetip_Start_m2FB64463989D04D567A2A04EA53668BA65CA5A15 ();
// 0x0000005C System.Void changetip::Update()
extern void changetip_Update_m147AA5E4727B96A29AE91B399076623222F24E5B ();
// 0x0000005D System.Void changetip::.ctor()
extern void changetip__ctor_m3C54CC01CBCC6483790625D00B4D07E561A59A85 ();
// 0x0000005E System.Void choosefon::Start()
extern void choosefon_Start_m91CBC54587C3D47ACABA1FE2E6F13A2C2077C774 ();
// 0x0000005F System.Void choosefon::Update()
extern void choosefon_Update_mCAEDB33D08176C9455A24FEB5F64689691FB881D ();
// 0x00000060 System.Void choosefon::OnMouseDown()
extern void choosefon_OnMouseDown_m50C1B8C4F2A7DE92DBDA97DB07BE2D1344DF339D ();
// 0x00000061 System.Void choosefon::.ctor()
extern void choosefon__ctor_m836E27C6308B9371327E69293E8A9B6CADB7E063 ();
// 0x00000062 System.Void chooselanguage::Start()
extern void chooselanguage_Start_mCC58FCBCFB8E57830ACA688736F1914568185E7F ();
// 0x00000063 System.Void chooselanguage::Update()
extern void chooselanguage_Update_mA37C858E3A3CA03451EF404E3156BBB687C71A88 ();
// 0x00000064 System.Void chooselanguage::OnMouseDown()
extern void chooselanguage_OnMouseDown_mADDD072155DCD5CD3EB23AF651AB2A792C857D2B ();
// 0x00000065 System.Void chooselanguage::.ctor()
extern void chooselanguage__ctor_m8EFF114ED91CA2A612CAD1D8503B48E1D30E0FF1 ();
// 0x00000066 System.Void chooseplash::Start()
extern void chooseplash_Start_m04960B57EE9F465E00D47D7A3A35947EB932DEA2 ();
// 0x00000067 System.Void chooseplash::Update()
extern void chooseplash_Update_mB09E29D67B74DDF969AB14EA4963EDFFD035DC8C ();
// 0x00000068 System.Void chooseplash::OnMouseDown()
extern void chooseplash_OnMouseDown_m30DB3B638BBEB365ED3C2D9D065D2090BC989F45 ();
// 0x00000069 System.Void chooseplash::.ctor()
extern void chooseplash__ctor_m223131C0EE8A4FAA6BF225944DE3C6C2F2ECBE8A ();
// 0x0000006A System.Void classic::Start()
extern void classic_Start_mC96F2B8ACC85912029853250DB196C750273BE15 ();
// 0x0000006B System.Void classic::FixedUpdate()
extern void classic_FixedUpdate_mB7F33E4A6F7B40E4D0BBAD6FA1FA9DA156284DC5 ();
// 0x0000006C System.Void classic::OnMouseDown()
extern void classic_OnMouseDown_m3981F3290B5D338C395B247DE24B27C737AEFF05 ();
// 0x0000006D System.Void classic::.ctor()
extern void classic__ctor_m543D547350C8176B79E328CB5F44F540560C628A ();
// 0x0000006E System.Void color::Start()
extern void color_Start_m5385DCBECBCCA316FAFAC3392D254DD7B6F674B7 ();
// 0x0000006F System.Void color::Update()
extern void color_Update_m9D322CB54A8B90946302F1871C09669A736FFDA4 ();
// 0x00000070 System.Void color::.ctor()
extern void color__ctor_m734650722AB12ED540321E2053DD74D4CFC80AB2 ();
// 0x00000071 System.Void color2::Start()
extern void color2_Start_mC48E9471CA18F8D9C05532B3F1942122280AFC48 ();
// 0x00000072 System.Void color2::Update()
extern void color2_Update_m7E07956791C12072AF3256B815DF7D1D3FF54569 ();
// 0x00000073 System.Void color2::.ctor()
extern void color2__ctor_m1F1652DA56A731F3D97D68A6A03DF83F70F293AC ();
// 0x00000074 System.Void depthc::Start()
extern void depthc_Start_m467C5994D55C964473DA00F4DE64E593A39D3FD9 ();
// 0x00000075 System.Void depthc::Update()
extern void depthc_Update_m48664E5C28611B77B86509B140EE2BA4861DF119 ();
// 0x00000076 System.Void depthc::.ctor()
extern void depthc__ctor_mD61BD9957E3A4D4C8FD6FB4170F888D7967C2E4A ();
// 0x00000077 System.Void disableonnoads::Start()
extern void disableonnoads_Start_m861873E99B4C5A2F273F6AA0A5E06F0FEA6969F9 ();
// 0x00000078 System.Void disableonnoads::Update()
extern void disableonnoads_Update_mE9B39D1193927834C43F0B07808C43627837A13F ();
// 0x00000079 System.Void disableonnoads::.ctor()
extern void disableonnoads__ctor_m442EFCE3190DF7405CEABA87A4275715634389D3 ();
// 0x0000007A System.Void generator::ChooseSobsna()
extern void generator_ChooseSobsna_mB8102DD89BC869AFA5DA1C17FFAD8185F176196B ();
// 0x0000007B System.Void generator::Update()
extern void generator_Update_mA9128DAA56B7E545ED0CAA11833F9970E6843859 ();
// 0x0000007C System.Void generator::ChooseQuest()
extern void generator_ChooseQuest_m28DBB45441C20922F3E8B8105305E2EFA7E77031 ();
// 0x0000007D System.Void generator::Vibor()
extern void generator_Vibor_m435B37822FCFA1120AEC26D6239B2878C0881A8D ();
// 0x0000007E System.Void generator::Analyze()
extern void generator_Analyze_m3E39C9195D25ED9B1F66818EB6925DF3D885E8F5 ();
// 0x0000007F System.Void generator::ChooseVars()
extern void generator_ChooseVars_m831D26439A7AB99B01B45CB66893EC922FD40EC1 ();
// 0x00000080 System.Void generator::ShakeAndNazn()
extern void generator_ShakeAndNazn_mBD3AF5C518A77A823AAC427CF08BEA4C790C749D ();
// 0x00000081 System.Void generator::.ctor()
extern void generator__ctor_m1CAA06B6F2954475CE2424BEAFDB589B793ADB0F ();
// 0x00000082 System.Void goback::Start()
extern void goback_Start_mAC2CF61AB5B86008235C91D2865E892B3BC76323 ();
// 0x00000083 System.Void goback::Update()
extern void goback_Update_mB7A903A6C1F6B5F4928CD189ED014472FE2E3A1E ();
// 0x00000084 System.Void goback::OnMouseDown()
extern void goback_OnMouseDown_mD75FAA6DCA98B3A33DE98A7CF80B8E14542A9AF5 ();
// 0x00000085 System.Void goback::.ctor()
extern void goback__ctor_m2A0F7EC8CFB0383D6CA2DC39B0DF2125232979A4 ();
// 0x00000086 System.Void gotomenu::Start()
extern void gotomenu_Start_m5E7A2D0999C9D2F8F941E76B4F49A5B99A7223C7 ();
// 0x00000087 System.Void gotomenu::Update()
extern void gotomenu_Update_m1B837374FAD0853BEB3D3846C404CB5C4D395F9F ();
// 0x00000088 System.Void gotomenu::OnMouseDown()
extern void gotomenu_OnMouseDown_m287C0CB8103FCD63667962A317CBC882305B787B ();
// 0x00000089 System.Void gotomenu::.ctor()
extern void gotomenu__ctor_m9CD6E2EC42C792903CE71CA06ABA469CB3B5B2BA ();
// 0x0000008A System.Void gotomenuff::Start()
extern void gotomenuff_Start_m0ACB8F1A0426EEC32710FA7B7B4A4CF8CF3C74AF ();
// 0x0000008B System.Void gotomenuff::Update()
extern void gotomenuff_Update_mBA298FE3C492F049580F926141D1FBDF313C131D ();
// 0x0000008C System.Void gotomenuff::.ctor()
extern void gotomenuff__ctor_mB1D6BA6B4DA7CA4553EB2ABEAB33BA1EFBB5773D ();
// 0x0000008D System.Void huy::Start()
extern void huy_Start_m4AF6C7EB6BDED5831D099EA3A6452A5046424F6B ();
// 0x0000008E System.Void huy::Update()
extern void huy_Update_mFDBBE8DB4F1C9644831821DDF81A004E05457F19 ();
// 0x0000008F System.Void huy::.ctor()
extern void huy__ctor_m4706A961CCEECF08DA47F968348D9636618E5134 ();
// 0x00000090 System.Void lang500::Start()
extern void lang500_Start_mFA6310CD041729E1793C99945CBF832FC5D1D9C7 ();
// 0x00000091 System.Void lang500::Update()
extern void lang500_Update_m35ED918C79CE957596BFA0B0F34A4CFF9D62A234 ();
// 0x00000092 System.Void lang500::.ctor()
extern void lang500__ctor_m661E38482BF44319EA05A28DDF611593DF4356EB ();
// 0x00000093 System.Void langgr::Start()
extern void langgr_Start_m2254141D0DBFEE76C68D8DDD5B94FEC6B50C960B ();
// 0x00000094 System.Void langgr::Update()
extern void langgr_Update_mE8479750DA4AA9DD23C0F9F97BE119752D5C6FC1 ();
// 0x00000095 System.Void langgr::.ctor()
extern void langgr__ctor_m1EBD4960F01E3FDB5C77BFA193079933E3114606 ();
// 0x00000096 System.Void langnaebka::Start()
extern void langnaebka_Start_m4AA9EC23F0E2140406D60A6C6A1CE46D5566EDB2 ();
// 0x00000097 System.Void langnaebka::Update()
extern void langnaebka_Update_m6778C26093F6CDA9E9A947707599651F3ED09EAA ();
// 0x00000098 System.Void langnaebka::.ctor()
extern void langnaebka__ctor_m5200F387FE1409D706E3D41ACC17D670BC622667 ();
// 0x00000099 System.Void langshit::Start()
extern void langshit_Start_m7368CD9844F957306ECB2688EF3631FE95C2BB21 ();
// 0x0000009A System.Void langshit::Update()
extern void langshit_Update_m0B5232E5EC2A29C6C5594AF40AFD83AD9BB685B1 ();
// 0x0000009B System.Void langshit::.ctor()
extern void langshit__ctor_mD164304A633460B1E24615224FCE24362790B661 ();
// 0x0000009C System.Void langtime::Start()
extern void langtime_Start_m8C106A228CBFC00B8C09D2564E58866B8F4D7036 ();
// 0x0000009D System.Void langtime::Update()
extern void langtime_Update_m01EBBE83E800A942542392015E714FF1AD8DFA35 ();
// 0x0000009E System.Void langtime::.ctor()
extern void langtime__ctor_m88F069579EC0C1820B16453EA8AD2C9B33421D2F ();
// 0x0000009F System.Void language::Start()
extern void language_Start_m77A32C7BD092B0DD63810385058A1C3314701BFA ();
// 0x000000A0 System.Void language::Update()
extern void language_Update_mDBA67BC92D4ADEA0F2B1FAED1FDE35E59B08F9C3 ();
// 0x000000A1 System.Void language::.ctor()
extern void language__ctor_m7684732E14C2A55250E3932CC3DA2A7A92F48C0C ();
// 0x000000A2 System.Void language10::Start()
extern void language10_Start_m1097A5B2E1264EB61D783D7CA71CBC68BBA21D3C ();
// 0x000000A3 System.Void language10::Update()
extern void language10_Update_m30B5A7F95DC16DCC3AACD70B7A90E102A9EC6A95 ();
// 0x000000A4 System.Void language10::.ctor()
extern void language10__ctor_m68E39DE545748088730C73CA2B7BEE95EB4D9EEC ();
// 0x000000A5 System.Void language2::Start()
extern void language2_Start_m4419272953205B08100D61BC8EC5BE5649452B2F ();
// 0x000000A6 System.Void language2::Update()
extern void language2_Update_mF41DBF0BC8801092A9EC7267645610426AB9D657 ();
// 0x000000A7 System.Void language2::.ctor()
extern void language2__ctor_m62EB09FB0C7F8AFF9ADEEAB7D54F98348617A141 ();
// 0x000000A8 System.Void language3::Start()
extern void language3_Start_mAF738655514F62DB5A0FDC1A5B3DD29566838024 ();
// 0x000000A9 System.Void language3::Update()
extern void language3_Update_m5E6B465ED8D5C1559F7F8B3648D7E8E46EC36F7E ();
// 0x000000AA System.Void language3::.ctor()
extern void language3__ctor_mAAC80FABB54C74E532748DDD2B05C5CBB0B1768C ();
// 0x000000AB System.Void language4::Start()
extern void language4_Start_m310141F7C66193C9C4A7F9AA306383321986612A ();
// 0x000000AC System.Void language4::Update()
extern void language4_Update_mD56C1A435DB281434A29CB085FBCFF86517E0073 ();
// 0x000000AD System.Void language4::.ctor()
extern void language4__ctor_mE1725E9CBDEFCE6D52F96B3C042BD707E009B062 ();
// 0x000000AE System.Void language5::Start()
extern void language5_Start_m226B24E82911AA19739345DB262AA0479FA2EBDB ();
// 0x000000AF System.Void language5::Update()
extern void language5_Update_mF69C4F6F4476D0AD89FBD4495BAD7D67380C3E09 ();
// 0x000000B0 System.Void language5::.ctor()
extern void language5__ctor_mD0E47C13040DB7A3BEF2A34E70EADB44D28FE68B ();
// 0x000000B1 System.Void language6::Start()
extern void language6_Start_m6A061B288B5108994E851C37C753EE3AA99EB976 ();
// 0x000000B2 System.Void language6::Update()
extern void language6_Update_m21DADBF6701C1D39AC211E9884664A45AB3B1006 ();
// 0x000000B3 System.Void language6::.ctor()
extern void language6__ctor_mE9904C6458751C137FBF7A47C80B5C192A886534 ();
// 0x000000B4 System.Void language7::Start()
extern void language7_Start_m2009F31CA0CEAE5FC78D227EA516F78007BA4FDD ();
// 0x000000B5 System.Void language7::Update()
extern void language7_Update_m658132B0537C63F29B59DDCE39BD644D5F9B37F9 ();
// 0x000000B6 System.Void language7::.ctor()
extern void language7__ctor_mE70887F5FF1064032A925129A8F4297A21AFE3A1 ();
// 0x000000B7 System.Void language8::Start()
extern void language8_Start_m8DE518C1C5651D266FFE3D557426BD7FB4C65741 ();
// 0x000000B8 System.Void language8::Update()
extern void language8_Update_m4DC6F42A9B207FEE775344978D1B378A2A2594F1 ();
// 0x000000B9 System.Void language8::.ctor()
extern void language8__ctor_m056B43630D78B78748561EB77E284F88B7367713 ();
// 0x000000BA System.Void language9::Start()
extern void language9_Start_mA385C4DBBA5BF9B2EF393919E0C0913BB2365034 ();
// 0x000000BB System.Void language9::Update()
extern void language9_Update_m899318B0D97183AD8840637EB834FEA83EE3FE18 ();
// 0x000000BC System.Void language9::.ctor()
extern void language9__ctor_mC81B20CEA5EA203E4BF7F9200DD6EF20AAE5E3C3 ();
// 0x000000BD System.Void languageads::Start()
extern void languageads_Start_m85210AFA36A43E90D7A7ED76859410364FA108A6 ();
// 0x000000BE System.Void languageads::Update()
extern void languageads_Update_m2DED4219CE9E939247D11F1E167E6DE432FFEC9A ();
// 0x000000BF System.Void languageads::.ctor()
extern void languageads__ctor_m3B6BE542BEC046B2668A30BDC16290FBE72BB284 ();
// 0x000000C0 System.Void languagefickk::Start()
extern void languagefickk_Start_m73CFE07CDF0DB77D0641141C5BE5F49922BAA586 ();
// 0x000000C1 System.Void languagefickk::Update()
extern void languagefickk_Update_m6DC0837791103CC6689A3BAA80E03ECAAF8F63DF ();
// 0x000000C2 System.Void languagefickk::.ctor()
extern void languagefickk__ctor_mD4D0D923D859C4B1E1268FDBD7F50BEDB8B7F09C ();
// 0x000000C3 System.Void languageplay::Start()
extern void languageplay_Start_m85F7DDEDE512EDEF700E77D026BCAA4DDC817881 ();
// 0x000000C4 System.Void languageplay::Update()
extern void languageplay_Update_m98566B5C3E5EE0651E191EA436897B0825603545 ();
// 0x000000C5 System.Void languageplay::.ctor()
extern void languageplay__ctor_mF5F3504969770114F1B6642AE7FB0DD45B1E9C3E ();
// 0x000000C6 System.Void languagerestore::Start()
extern void languagerestore_Start_mCC90AEB41EE64E19F7A15266300EAE73D06D2BFB ();
// 0x000000C7 System.Void languagerestore::Update()
extern void languagerestore_Update_m39B2B83A6DDDDDA5A325A9FBCF82DABF81666043 ();
// 0x000000C8 System.Void languagerestore::.ctor()
extern void languagerestore__ctor_m9A181BB594B48BFDFDEC336A9BB4A60936EA5252 ();
// 0x000000C9 System.Void load::Start()
extern void load_Start_m394C448EFBF249FAAC248BDE8E213C41A0C4F496 ();
// 0x000000CA System.Void load::Update()
extern void load_Update_m316770170C3AB3AECFCBFC096511699DD918A341 ();
// 0x000000CB System.Void load::.ctor()
extern void load__ctor_m86B8E8605695FD9C6DBAE72EE0D9A7E1C2E43263 ();
// 0x000000CC System.Void loadagain::OnMouseDown()
extern void loadagain_OnMouseDown_m7485ABCE2B4A02451F3EE03EF7E47529DAE1AAF5 ();
// 0x000000CD System.Void loadagain::.ctor()
extern void loadagain__ctor_m6B37556158A2BE9F549A45B982F233ED14AB2B2B ();
// 0x000000CE System.Void loadloader::Start()
extern void loadloader_Start_m1667B6828A68FD97AA1AC483118467688FCC7D4C ();
// 0x000000CF System.Void loadloader::.ctor()
extern void loadloader__ctor_mE5583059E0337797B5200A1939582CBE189BA0D3 ();
// 0x000000D0 System.Void magascontrol::Start()
extern void magascontrol_Start_m36B9A518A85E32D3881C9E6300390F7C49834710 ();
// 0x000000D1 System.Void magascontrol::Update()
extern void magascontrol_Update_mEA4343843DB57172A15C7B27F41E676BC0129B30 ();
// 0x000000D2 System.Void magascontrol::.ctor()
extern void magascontrol__ctor_m7D38AE41DD32CD3A9D127EC5FDCB48C7936C9246 ();
// 0x000000D3 System.Void magazlang::Start()
extern void magazlang_Start_mD479AD8662B1AD0F9AD88D44EBBD0FB8356EB090 ();
// 0x000000D4 System.Void magazlang::Update()
extern void magazlang_Update_m52E4616B35D409E35F2766565EF2CA89781F5335 ();
// 0x000000D5 System.Void magazlang::.ctor()
extern void magazlang__ctor_m708D77DD6959B3633C04692FCE86969C44D44301 ();
// 0x000000D6 System.Void medset::Start()
extern void medset_Start_m436C57FC98B0BD163AF43F69D85482BB0C54A8DE ();
// 0x000000D7 System.Void medset::Update()
extern void medset_Update_m2135A9E906291FB64541918A320EF43650AD19B9 ();
// 0x000000D8 System.Void medset::.ctor()
extern void medset__ctor_m05F34ECAC9DF0397F8E09DBC558D89D15369FC50 ();
// 0x000000D9 System.Void movescreen::Start()
extern void movescreen_Start_m11C6AA356F58DF5AF1F5C66F9B9986BAEDFF5CB1 ();
// 0x000000DA System.Void movescreen::FixedUpdate()
extern void movescreen_FixedUpdate_m8ABC090EDCEE2F86EA22B430756F932944D669F7 ();
// 0x000000DB System.Void movescreen::OnMouseDown()
extern void movescreen_OnMouseDown_m1D2A8EDCFA14CF465D2EDF952FA9BD8600F1527C ();
// 0x000000DC System.Void movescreen::.ctor()
extern void movescreen__ctor_mE08AAB82B10693FA349F8FD3E1FBCAF3A75C4DB3 ();
// 0x000000DD System.Void needmoney::Start()
extern void needmoney_Start_mE9D84CC34F1B9B6A8E1EA89DFA0278B386DE9F89 ();
// 0x000000DE System.Void needmoney::Update()
extern void needmoney_Update_m876806523F1432E9EB42D1F4F54E8C1084C1EED9 ();
// 0x000000DF System.Void needmoney::.ctor()
extern void needmoney__ctor_mA4E0BB2EDBCB88C797CA5C7FBE219A6A12F175DA ();
// 0x000000E0 System.Void nesgor::Start()
extern void nesgor_Start_m7831DBD8F79EA92C5144DD72E2D68903F5C133E5 ();
// 0x000000E1 System.Void nesgor::Update()
extern void nesgor_Update_mEE056E3BB073FCFAB1B0F154E92D8527C73F53D7 ();
// 0x000000E2 System.Void nesgor::OnMouseDown()
extern void nesgor_OnMouseDown_m23C2B069FCB5A42C557BA781DEFFDDDFFADA0B55 ();
// 0x000000E3 System.Void nesgor::.ctor()
extern void nesgor__ctor_m7D8B8D6E788572BA422C4C969A53B967C7897CBA ();
// 0x000000E4 System.Void no::Start()
extern void no_Start_mCB0FB33B75E1645862A2B6F0D0FD79FC65FBD53D ();
// 0x000000E5 System.Void no::Update()
extern void no_Update_mA44542DF4E74EBC091B083D92BFD6C92ED36E973 ();
// 0x000000E6 System.Void no::OnMouseDown()
extern void no_OnMouseDown_mD80B6FD70801736CA3E1FA6DA29363273A52D620 ();
// 0x000000E7 System.Void no::.ctor()
extern void no__ctor_m0D594C80057C9E0FCDF091A70B0AA90E17563C90 ();
// 0x000000E8 System.Void ocenka::Start()
extern void ocenka_Start_mA270ED84EAC3680E29A36021F181388A6D4F8EFD ();
// 0x000000E9 System.Void ocenka::Update()
extern void ocenka_Update_m4C34515A949B3423E8465D0CB0FDF0A35985233F ();
// 0x000000EA System.Void ocenka::OnMouseDown()
extern void ocenka_OnMouseDown_m4132494BEABE90D86A7CBAB792297EEE09C619A6 ();
// 0x000000EB System.Void ocenka::.ctor()
extern void ocenka__ctor_mABD84ECD5F809FF6C866496925139E1FFA4B1EE5 ();
// 0x000000EC System.Void ofblyad::Start()
extern void ofblyad_Start_m0229EADF54C9FC95F09109E765400E5E87EE1CEC ();
// 0x000000ED System.Void ofblyad::Update()
extern void ofblyad_Update_mE3587369E74FA3DCFC2F340495A697A38818E976 ();
// 0x000000EE System.Void ofblyad::.ctor()
extern void ofblyad__ctor_m8AF7C3CC3CB08303338CE0DFE49C80E13ACAA51E ();
// 0x000000EF System.Void offsprite::Start()
extern void offsprite_Start_m3439520AB0FA106E979D91310199D2920CD2C2D4 ();
// 0x000000F0 System.Void offsprite::FixedUpdate()
extern void offsprite_FixedUpdate_mF8314D49638995669D45C64B2A49EC715B6DED4D ();
// 0x000000F1 System.Void offsprite::.ctor()
extern void offsprite__ctor_m96DB40F1695656238AB26AC13FA3AD055F64B7B2 ();
// 0x000000F2 System.Void oidjiaso::Start()
extern void oidjiaso_Start_m821E3CFD39121E5B1280133151121978789AA0C9 ();
// 0x000000F3 System.Void oidjiaso::Update()
extern void oidjiaso_Update_mCFD4AC247D3BA9E0696FFF2399C21317D1868E12 ();
// 0x000000F4 System.Void oidjiaso::OnMouseDown()
extern void oidjiaso_OnMouseDown_m4321DB517FFC1766F96B7BB897BBA0592B9CEEF9 ();
// 0x000000F5 System.Void oidjiaso::.ctor()
extern void oidjiaso__ctor_m00AD5C75C4B5157CF6701A5034EA8D6EB49FB8F4 ();
// 0x000000F6 System.Void onclick::Start()
extern void onclick_Start_mA204DA5368CA9B753225395EB35B673CDC611216 ();
// 0x000000F7 System.Void onclick::Update()
extern void onclick_Update_m5A0990E149F6C9317188F16DA7ED72EA65B9E734 ();
// 0x000000F8 System.Void onclick::FixedUpdate()
extern void onclick_FixedUpdate_m3973D67B7B07877AC0010197EEF1831F0E33A694 ();
// 0x000000F9 System.Void onclick::Lost()
extern void onclick_Lost_m85FAC2B835528D8A8307ECDAC029751527BA164B ();
// 0x000000FA System.Void onclick::Won()
extern void onclick_Won_m2D8E85429A84FF381C3E935831B37917386766F2 ();
// 0x000000FB System.Void onclick::OnMouseUp()
extern void onclick_OnMouseUp_m22E83B8CF94D7755DE1C2960E779F438BCA3F745 ();
// 0x000000FC System.Void onclick::.ctor()
extern void onclick__ctor_m1706DD28064458ADB78A5F3EB31644C0B0E230B8 ();
// 0x000000FD System.Void onefiveplay::Start()
extern void onefiveplay_Start_m5693968EECF0AF81C55A72A6DBA30945A2B89D32 ();
// 0x000000FE System.Void onefiveplay::FixedUpdate()
extern void onefiveplay_FixedUpdate_mAE74315B2E7E1F76DAEEC7EF14D44CD478936F71 ();
// 0x000000FF System.Void onefiveplay::.ctor()
extern void onefiveplay__ctor_m0F7DD06973AD7AD447516DBA7D6B628315FDDF7F ();
// 0x00000100 System.Void onlights::Start()
extern void onlights_Start_m5F03C0241376222E3A6BBD3A95025B6C6D339D54 ();
// 0x00000101 System.Void onlights::Update()
extern void onlights_Update_m7BE812100BB23EB67CF7BD83E51FB9D011BF3A58 ();
// 0x00000102 System.Void onlights::OnMouseDown()
extern void onlights_OnMouseDown_m62ED15F59F8B2DBB4D415435711C7473F6B94D67 ();
// 0x00000103 System.Void onlights::.ctor()
extern void onlights__ctor_m88CEC9CD536A76C6E37B3CEC0A515E1405216D82 ();
// 0x00000104 System.Void ontime::Start()
extern void ontime_Start_m9BC5400722F07E887B1714E4F087E3C28D97268A ();
// 0x00000105 System.Void ontime::FixedUpdate()
extern void ontime_FixedUpdate_mA8BC15E460A7D5ABC1CE75B4EEBF916E3E65912E ();
// 0x00000106 System.Void ontime::OnMouseDown()
extern void ontime_OnMouseDown_mF704BA1DD8C5632E015641AFDA2B87D04F3A2FE4 ();
// 0x00000107 System.Void ontime::.ctor()
extern void ontime__ctor_m755FB7637EEE62C2379A1A367374592DCD47F39D ();
// 0x00000108 System.Void openfon::Start()
extern void openfon_Start_m708719617AD64711B0651B5FFD53F328031A36D4 ();
// 0x00000109 System.Void openfon::Update()
extern void openfon_Update_m9CBD432340C0863AA944A883626F4C45D0D49749 ();
// 0x0000010A System.Void openfon::OnMouseDown()
extern void openfon_OnMouseDown_mCEE6B506DD808197C5E5EB66A74A7BE02395A13B ();
// 0x0000010B System.Void openfon::.ctor()
extern void openfon__ctor_mF2F14DB6F03FEA751F3B11D389C500E67365BE84 ();
// 0x0000010C System.Void openmagazik::Start()
extern void openmagazik_Start_m80D623C7FF55FE7D445692EF4861080BB02D4B32 ();
// 0x0000010D System.Void openmagazik::Update()
extern void openmagazik_Update_m5799583887EFDFC22BAE772456CBFA1FD2074C03 ();
// 0x0000010E System.Void openmagazik::OnMouseDown()
extern void openmagazik_OnMouseDown_m99412FEFCB253DDD84C0E6B475930EE46603F359 ();
// 0x0000010F System.Void openmagazik::.ctor()
extern void openmagazik__ctor_m46D64C91D21046A1A934D7C357577DDEAF8B80DA ();
// 0x00000110 System.Void openmarket::Start()
extern void openmarket_Start_m1ACD24EB13172A943DEC471FB9BF9EAAB1CCDF9A ();
// 0x00000111 System.Void openmarket::Update()
extern void openmarket_Update_m8FBC79FCDA759B012C6D677EF7CCE49EF10B2F51 ();
// 0x00000112 System.Void openmarket::OnMouseDown()
extern void openmarket_OnMouseDown_m16664B46E5C16DE10F3AE4CD25EBECD9B5FF35C1 ();
// 0x00000113 System.Void openmarket::.ctor()
extern void openmarket__ctor_m161160724F82DF7F7D31C87DE5275E9DBCAEA38C ();
// 0x00000114 System.Void openoptions::Start()
extern void openoptions_Start_m79250E598C1C4ABC35C4CC065CEF5C247630637B ();
// 0x00000115 System.Void openoptions::Update()
extern void openoptions_Update_m17A2BC2894D0DD67B825EC9F46C8ADCABB55C20E ();
// 0x00000116 System.Void openoptions::OnMouseDown()
extern void openoptions_OnMouseDown_m7D2733CC495DDE7A6D53A04DA5C79FA139FA7E69 ();
// 0x00000117 System.Void openoptions::.ctor()
extern void openoptions__ctor_m7D973505CD86718204096E18405013EB40A6A6D9 ();
// 0x00000118 System.Void openplash::Start()
extern void openplash_Start_mF4FE21DBF720D4118216C6EFF98B09CEF2B321C1 ();
// 0x00000119 System.Void openplash::Update()
extern void openplash_Update_m740B24AB5DE7D3FA47B6A3F7D3AB29965A956644 ();
// 0x0000011A System.Void openplash::OnMouseDown()
extern void openplash_OnMouseDown_m426A2A61D677A99BA66406FFF2A12DF83D673E49 ();
// 0x0000011B System.Void openplash::.ctor()
extern void openplash__ctor_m1F19A8AD462812D44E56C629FA521D070E53C95C ();
// 0x0000011C System.Void openshop::Start()
extern void openshop_Start_m2B825551726BB999B006E5ABD87331B3B7E362B4 ();
// 0x0000011D System.Void openshop::Update()
extern void openshop_Update_mDE0C9F8E46D81F8D2AE239E04308947BB60F1BBA ();
// 0x0000011E System.Void openshop::OnMouseDown()
extern void openshop_OnMouseDown_mB83BE2D3B1FFE981C63BE8FFB379DF933474E8F8 ();
// 0x0000011F System.Void openshop::.ctor()
extern void openshop__ctor_m3D551825A16255DE1EDACE57919D4FD02050D3E0 ();
// 0x00000120 System.Void planshet::Start()
extern void planshet_Start_m0D9DDF5D7EE93FBB303334475DE18B15DB7889EA ();
// 0x00000121 System.Void planshet::FixedUpdate()
extern void planshet_FixedUpdate_m020CB85D8DA07E61AAF96DDC2F0BA03827694B95 ();
// 0x00000122 System.Void planshet::.ctor()
extern void planshet__ctor_mA7717F68E2AD6A0AD5B198BE896543F0306A6821 ();
// 0x00000123 System.Void playagain::Start()
extern void playagain_Start_m974AB4EA0F85A16162D3856FECFD8256E120DC8A ();
// 0x00000124 System.Void playagain::Update()
extern void playagain_Update_mF5B6335BD76CBBD9573261B89458C2E6D05DCBA7 ();
// 0x00000125 System.Void playagain::.ctor()
extern void playagain__ctor_m1CF61F40B3CA776F230C0D59E0BCF515DE2D2998 ();
// 0x00000126 System.Void plus15sec::Start()
extern void plus15sec_Start_mA341029AA81B59DC07AE4B9F79E1B5D5C90229F2 ();
// 0x00000127 System.Void plus15sec::Update()
extern void plus15sec_Update_m58CF81738930A9B833B7CB4452843E1568CFC67D ();
// 0x00000128 System.Void plus15sec::OnMouseDown()
extern void plus15sec_OnMouseDown_m6C11D8429EB069EB33E64D780CB103D1503B9B38 ();
// 0x00000129 System.Void plus15sec::.ctor()
extern void plus15sec__ctor_m110BF8288C4CC460C0FAB7D297A09D271277B2B2 ();
// 0x0000012A System.Void prefs::Start()
extern void prefs_Start_m48F6F2727A9E75D0D146BE0E41ECC965A00D5DC0 ();
// 0x0000012B System.Void prefs::Update()
extern void prefs_Update_m4B853B3A3877A01F720FCB161E3F21DCBEE242C3 ();
// 0x0000012C System.Void prefs::.ctor()
extern void prefs__ctor_m9E5C8A1913ABDEF5BE638AAA1CEF965952B1944B ();
// 0x0000012D System.Void preload::Start()
extern void preload_Start_mAD019C4D2DC3F04DF4CA20BE3C81371732953943 ();
// 0x0000012E System.Void preload::Update()
extern void preload_Update_m28C2957E58C84F36C17FB4FFCA228E71B6900EBA ();
// 0x0000012F System.Void preload::.ctor()
extern void preload__ctor_mC8E10F6DB1645C8C6D53B71C243E15B57EF224BB ();
// 0x00000130 System.Void ptzovik::Start()
extern void ptzovik_Start_m055E79EA03C5D9FB2139107817847AFE9951B921 ();
// 0x00000131 System.Void ptzovik::Update()
extern void ptzovik_Update_m3492D317EA078533314A1A9911778E74B23D37F3 ();
// 0x00000132 System.Void ptzovik::.ctor()
extern void ptzovik__ctor_m332BE8E7C5A359F8A9D61B488EEEAAC2BB98905A ();
// 0x00000133 System.Void randomanimka::Start()
extern void randomanimka_Start_m2F8765C45E299A7AA28DD9A45F2FC9EC8D31C4F7 ();
// 0x00000134 System.Void randomanimka::Update()
extern void randomanimka_Update_m4BABFED8C3C9F260B24597DDB65EDC28F2F5A8F7 ();
// 0x00000135 System.Void randomanimka::.ctor()
extern void randomanimka__ctor_m4EBCC42A198425DF5A2D4DD39E7F90B3C1D18281 ();
// 0x00000136 System.Void realplus::Start()
extern void realplus_Start_mF7A6AC72139507735B869EE07604578B000F79EE ();
// 0x00000137 System.Void realplus::Update()
extern void realplus_Update_m3025EC4CD323C79704FA44096CBC5D105DC691C7 ();
// 0x00000138 System.Void realplus::OnMouseDown()
extern void realplus_OnMouseDown_mF78EE22FD5C4D68C2949482389A2BF467AB55AD2 ();
// 0x00000139 System.Void realplus::.ctor()
extern void realplus__ctor_m144CB3BA2B1EDC2196DF65EBDC466F84891296EB ();
// 0x0000013A System.Void repliccontrol::Start()
extern void repliccontrol_Start_m9ACD6143B23E2DF07729A7ADCEFA10CBE8030258 ();
// 0x0000013B System.Void repliccontrol::Update()
extern void repliccontrol_Update_m5A85FDDF9E42E96F7E713B065EF618339A1AF86E ();
// 0x0000013C System.Void repliccontrol::ChooseRep()
extern void repliccontrol_ChooseRep_mEEBAD06227C63E617506CC88825A013D6CFA49C4 ();
// 0x0000013D System.Void repliccontrol::.ctor()
extern void repliccontrol__ctor_mF1A6DEFDB890F7536629DE6215661B6B279AF261 ();
// 0x0000013E System.Void repliccontrol::.cctor()
extern void repliccontrol__cctor_mF93678C23D9921F87A1AB7BB282ED211E19B365E ();
// 0x0000013F System.Void restart::Start()
extern void restart_Start_m81662DA5A311EDDED1E8A3814C7C6D7EA92F6CF4 ();
// 0x00000140 System.Void restart::Update()
extern void restart_Update_m15B21B18B2E4B46DB623A701C095F9C73FBC54F4 ();
// 0x00000141 System.Void restart::OnMouseDown()
extern void restart_OnMouseDown_m7C2FC9FD4052813C3B33839EAC332070E5BE5750 ();
// 0x00000142 System.Void restart::.ctor()
extern void restart__ctor_m23F5A4FE51F55C67834AF58E04B9390479D039D8 ();
// 0x00000143 System.Void score::Start()
extern void score_Start_m0A60A4EC7C7734144184FB54878249B7FB44670B ();
// 0x00000144 System.Void score::Update()
extern void score_Update_m1DEB4166FC8653F89D7D11A32F9184DA23AC1950 ();
// 0x00000145 System.Void score::.ctor()
extern void score__ctor_m99F8700D5ADB83DE3556E55E2F8475362FA947C2 ();
// 0x00000146 System.Void score::.cctor()
extern void score__cctor_mC4C70FC051769273460B8C81360339113D13846B ();
// 0x00000147 System.Void sdadas::Start()
extern void sdadas_Start_mC90B40A7AC6032756918A0501B6CA8E96E0FC41D ();
// 0x00000148 System.Void sdadas::Update()
extern void sdadas_Update_m281D76DD26815225F82966E21E433A356FE2CDB3 ();
// 0x00000149 System.Void sdadas::.ctor()
extern void sdadas__ctor_m87CB39A7937F203CD237377025499B0F0AAA7274 ();
// 0x0000014A System.Void secretnaebka::Start()
extern void secretnaebka_Start_mD7D0CC774A014AD3DE735385971695CC34AFBA34 ();
// 0x0000014B System.Void secretnaebka::Update()
extern void secretnaebka_Update_m3050CE6F420BCECC0E07643402B009F66E66B653 ();
// 0x0000014C System.Void secretnaebka::.ctor()
extern void secretnaebka__ctor_m5C7E768575CDAF86F51E891794F93C71FB9DB8CD ();
// 0x0000014D System.Void sovr::Start()
extern void sovr_Start_mD308E2B2CA52C780000633908ED72DDD3868C40C ();
// 0x0000014E System.Void sovr::FixedUpdate()
extern void sovr_FixedUpdate_mB05ED50D1A40FD4D7736CAE3E5027C1D7A222F30 ();
// 0x0000014F System.Void sovr::OnMouseDown()
extern void sovr_OnMouseDown_mA2EA2A3235478518DEF818DA85C7F69D57378CB0 ();
// 0x00000150 System.Void sovr::.ctor()
extern void sovr__ctor_m2EEBF8A422194C7135865BE6ECD8500183B0F68A ();
// 0x00000151 System.Void start::Start()
extern void start_Start_m958AA2D538728C57EB48F3C74396B1177D9281E4 ();
// 0x00000152 System.Void start::Awake()
extern void start_Awake_mEB7BA6FDEE2163E68950942636C87CCAB007D8C0 ();
// 0x00000153 System.Void start::Update()
extern void start_Update_m5B2B737B77629DFAB80EF26F3ACDA467E4618884 ();
// 0x00000154 System.Void start::OnMouseDown()
extern void start_OnMouseDown_m548FA9A8C26806E2D9E6F49A6F8A563F540CAA3A ();
// 0x00000155 System.Void start::.ctor()
extern void start__ctor_m5A8265BE093DB12BD82B0575AB80C2E9AB3DD4AC ();
// 0x00000156 System.Void textvrash::Start()
extern void textvrash_Start_m9C26ECD1AFA3FBED89E7A8320584C52F4C4EE741 ();
// 0x00000157 System.Void textvrash::FixedUpdate()
extern void textvrash_FixedUpdate_m757669898A9818DA46B12223EB0117F2A88A508F ();
// 0x00000158 System.Void textvrash::.ctor()
extern void textvrash__ctor_m1AAD8E1D06143517BEE691F74F0D7F65C8D1404F ();
// 0x00000159 System.Void vivod::Start()
extern void vivod_Start_m2B4F1F08ED95890BB9BCCD8E3605BD83E3FCD777 ();
// 0x0000015A System.Void vivod::Awake()
extern void vivod_Awake_m0AFECC536528FEE0E8BAAFA35884201DC9A25165 ();
// 0x0000015B System.Void vivod::Update()
extern void vivod_Update_mDF0842DB76C987E3798C4C009FF8041F02732BD2 ();
// 0x0000015C System.Void vivod::.ctor()
extern void vivod__ctor_m4DEE9058B360BDBD1CFC39CC58EBD439911F11E5 ();
// 0x0000015D System.Void vivod::.cctor()
extern void vivod__cctor_m004A659CD924E7086D206A8F47FFDECCDE55AF1C ();
// 0x0000015E System.Void vrash::Start()
extern void vrash_Start_mC5EB8C7343105D4B1C4888F2D32F8BFE09C93E78 ();
// 0x0000015F System.Void vrash::FixedUpdate()
extern void vrash_FixedUpdate_m36E5542C127F196A3590B44DAA1E9A4B105B4EAB ();
// 0x00000160 System.Void vrash::.ctor()
extern void vrash__ctor_mAD1DCBF5812CC0926BEF1DC97495A811D7F05126 ();
// 0x00000161 System.Void vrash2::Start()
extern void vrash2_Start_mB5D8563B40D329550A67408E88F45EE8569AEE99 ();
// 0x00000162 System.Void vrash2::FixedUpdate()
extern void vrash2_FixedUpdate_mA3EE1D437535E86442C096B4BE39C757C74A261A ();
// 0x00000163 System.Void vrash2::.ctor()
extern void vrash2__ctor_m4F87DA475B4893AFEC7D023448B2F9E6687D63AE ();
// 0x00000164 System.Void vrashkrug::Start()
extern void vrashkrug_Start_m131F263B0BB6CE453081E504AE4C17810A5F32AC ();
// 0x00000165 System.Void vrashkrug::FixedUpdate()
extern void vrashkrug_FixedUpdate_mE399B99AC54B860D55DA26928A7BA67FFFCB926A ();
// 0x00000166 System.Void vrashkrug::.ctor()
extern void vrashkrug__ctor_m7224864BB2D8543C23B1F04A8676EF5B8D95F0F7 ();
// 0x00000167 System.Void x2tip::Start()
extern void x2tip_Start_mAB78F0E29058BDF05E96A1F0408977B3CBB6F992 ();
// 0x00000168 System.Void x2tip::FixedUpdate()
extern void x2tip_FixedUpdate_mFFE5693DFAD7BF8EE35C6A8FCBBAB80B46CD29AD ();
// 0x00000169 System.Void x2tip::OnMouseDown()
extern void x2tip_OnMouseDown_mDB3FBD8B1FC9A31ADBDDBA5FE4B45456BB3F980B ();
// 0x0000016A System.Void x2tip::.ctor()
extern void x2tip__ctor_m083D3F2F77DEB7C0D43389ABADB32BA51792E8E3 ();
// 0x0000016B System.Void yes::Start()
extern void yes_Start_m2FFF5C7A490BA9768E43B268210104F0E371EC25 ();
// 0x0000016C System.Void yes::Update()
extern void yes_Update_m9D7619DFEABD9B64A6C1C35C4F187410F07E40CF ();
// 0x0000016D System.Void yes::OnMouseDown()
extern void yes_OnMouseDown_mBD3EAE21B5A9137CB7BE5D3A84A7425E9D927627 ();
// 0x0000016E System.Void yes::.ctor()
extern void yes__ctor_m38D6295FE568A2CC7B20B3F1E1E1D51743C41D17 ();
// 0x0000016F System.Void zabratmoney::Start()
extern void zabratmoney_Start_m8AE46FFBCF2E2E2E5235BFA9152568DEF9446832 ();
// 0x00000170 System.Void zabratmoney::Update()
extern void zabratmoney_Update_mD7F53A577260CC0D172FA63D659541376463A1E4 ();
// 0x00000171 System.Void zabratmoney::OnMouseDown()
extern void zabratmoney_OnMouseDown_m1220E6EE6FB81C86A773225E7C608BBF78D30EA6 ();
// 0x00000172 System.Void zabratmoney::.ctor()
extern void zabratmoney__ctor_m4B52AA36C4EBB005FE0B6B4C739E84D83FA2B44D ();
// 0x00000173 System.Void zaltip::Start()
extern void zaltip_Start_mF003BE496B7C2E221441F6312AADB74C1AAE755D ();
// 0x00000174 System.Void zaltip::ShowRewardedAd()
extern void zaltip_ShowRewardedAd_mB5E223BE079E093762E376AEED55F8D5D71F30D8 ();
// 0x00000175 System.Void zaltip::FixedUpdate()
extern void zaltip_FixedUpdate_m83E52C147D3345C561AACF32B0A9EFA8F6E789BE ();
// 0x00000176 System.Void zaltip::OnMouseDown()
extern void zaltip_OnMouseDown_mBCD4C20E5E7F3FBC38CF0CB3D676AC4B8C837356 ();
// 0x00000177 System.Void zaltip::Naznach()
extern void zaltip_Naznach_m32DAA727AB6914925B335901C65BF7325FD7B0DC ();
// 0x00000178 System.Void zaltip::.ctor()
extern void zaltip__ctor_m03972C1473ABCD6C0F445A82B6ACD2D0F3262DFD ();
// 0x00000179 System.Void ebal.fiftytip::Start()
extern void fiftytip_Start_m44268189463FD878BEE6802EF0BA3B03392D2C7B ();
// 0x0000017A System.Void ebal.fiftytip::Update()
extern void fiftytip_Update_mB3F6E918B335F5A034FD83A9A039D0F23964C18C ();
// 0x0000017B System.Void ebal.fiftytip::OnMouseDown()
extern void fiftytip_OnMouseDown_m3577EC94B2E0EE06EB32979AC1485D1AC89BA8FF ();
// 0x0000017C System.Void ebal.fiftytip::.ctor()
extern void fiftytip__ctor_m0424C33CC01EA8DBC6E11F1CC6D2C83B01825081 ();
// 0x0000017D System.Void SleekRender.changetolow::Start()
extern void changetolow_Start_m0F298CCBCEE4C81F84660E61E7A5BBEA502FB266 ();
// 0x0000017E System.Void SleekRender.changetolow::Update()
extern void changetolow_Update_m28E5160D92F2BFF387D3BA2D253B18404772286F ();
// 0x0000017F System.Void SleekRender.changetolow::OnMouseDown()
extern void changetolow_OnMouseDown_mF7A53F0396C16D313438D34064615ABBE142FD46 ();
// 0x00000180 System.Void SleekRender.changetolow::.ctor()
extern void changetolow__ctor_m979D682BCF513D893A89BB4B74A7CF40E49C0AFA ();
// 0x00000181 System.Void SleekRender.highfuck::Start()
extern void highfuck_Start_mC1BF6C09FFDDBE508DC43CD139C96A15D47682F5 ();
// 0x00000182 System.Void SleekRender.highfuck::Update()
extern void highfuck_Update_mDF17F44546EAACA5790559C5C16EE3F50B795429 ();
// 0x00000183 System.Void SleekRender.highfuck::OnMouseDown()
extern void highfuck_OnMouseDown_mB74592E8B9E644DBC788A2CADB74553FE5A367AC ();
// 0x00000184 System.Void SleekRender.highfuck::.ctor()
extern void highfuck__ctor_mBCE906C788BD4315FDA5E0ED27021C4C20EA0BE1 ();
// 0x00000185 System.Void SleekRender.medset::Update()
extern void medset_Update_m1861439C83385CF28075370DC942FB7F0ACB7C37 ();
// 0x00000186 System.Void SleekRender.medset::OnMouseDown()
extern void medset_OnMouseDown_m9EB8337C66541ACB948E6461963A53CFC842BAE5 ();
// 0x00000187 System.Void SleekRender.medset::.ctor()
extern void medset__ctor_mB49154CB2C238B149062513D55BC0ECA9A2E1146 ();
// 0x00000188 System.Void SleekRender.medhuy::Start()
extern void medhuy_Start_m362BDAACAAF9008A1DF97FBF16EAD7D557ED2218 ();
// 0x00000189 System.Void SleekRender.medhuy::Update()
extern void medhuy_Update_m3F31DBF9BF22CF9778011DD6F98F7AA5E171C92E ();
// 0x0000018A System.Void SleekRender.medhuy::OnMouseDown()
extern void medhuy_OnMouseDown_mC17EE4586A5AEFE9CAD6F26DEE3EE92D201ED761 ();
// 0x0000018B System.Void SleekRender.medhuy::.ctor()
extern void medhuy__ctor_m3114310476C1A2BB87A6946428191906D02B8583 ();
// 0x0000018C AppodealAds.Unity.Common.IAppodealAdsClient AppodealAds.Unity.AppodealAdsClientFactory::GetAppodealAdsClient()
extern void AppodealAdsClientFactory_GetAppodealAdsClient_mECEE5DDF222D3D255DBC8E9C1A0D87B8EF3F50F5 ();
// 0x0000018D System.Void AppodealAds.Unity.AppodealAdsClientFactory::.ctor()
extern void AppodealAdsClientFactory__ctor_m4C25D531F9EB7CED25D791EBF191E9CFAA2FEE09 ();
// 0x0000018E System.Void AppodealAds.Unity.iOS.AppodealAdsClient::.ctor()
extern void AppodealAdsClient__ctor_mD6097E8E16558304119D394C6DB47134AE4A0B6D ();
// 0x0000018F AppodealAds.Unity.iOS.AppodealAdsClient AppodealAds.Unity.iOS.AppodealAdsClient::get_Instance()
extern void AppodealAdsClient_get_Instance_mAFCF04478EF6813AD32A5163588C5848A568915C ();
// 0x00000190 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::requestAndroidMPermissions(AppodealAds.Unity.Common.IPermissionGrantedListener)
extern void AppodealAdsClient_requestAndroidMPermissions_m972159493CBB0F2C4FC897006E2F06ED646E9848 ();
// 0x00000191 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::interstitialDidLoad(System.Boolean)
extern void AppodealAdsClient_interstitialDidLoad_mF0BB510147ACC2ED6196D58C04BADCF7DD672804 ();
// 0x00000192 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::interstitialDidFailToLoad()
extern void AppodealAdsClient_interstitialDidFailToLoad_m02A0C2398B471B328B01CD9461F156F50CA86BE2 ();
// 0x00000193 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::interstitialDidClick()
extern void AppodealAdsClient_interstitialDidClick_m48EC441678FDACF1CE950F29F17D710C011F3372 ();
// 0x00000194 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::interstitialDidDismiss()
extern void AppodealAdsClient_interstitialDidDismiss_mE3C05776C18C88C21836E4FC194C7E978A559FB2 ();
// 0x00000195 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::interstitialWillPresent()
extern void AppodealAdsClient_interstitialWillPresent_mA144DC35E835CDDBC8272155DDE56B4D622DC9A8 ();
// 0x00000196 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::interstitialDidExpired()
extern void AppodealAdsClient_interstitialDidExpired_m70EBB4905AEE8AD1A710FF6D3E92FBE9822BBFCE ();
// 0x00000197 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::setInterstitialCallbacks(AppodealAds.Unity.Common.IInterstitialAdListener)
extern void AppodealAdsClient_setInterstitialCallbacks_mFBBC4C1EF912F4AFB9CD6B16877D5EE18EEF9F6D ();
// 0x00000198 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::nonSkippableVideoDidLoadAd(System.Boolean)
extern void AppodealAdsClient_nonSkippableVideoDidLoadAd_mE266D3DF979A3DE4663C5B2B9EA763E2CBD82063 ();
// 0x00000199 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::nonSkippableVideoDidFailToLoadAd()
extern void AppodealAdsClient_nonSkippableVideoDidFailToLoadAd_m0DB22D1F8F7471FD7614ACEF6602721D2AEBB4BD ();
// 0x0000019A System.Void AppodealAds.Unity.iOS.AppodealAdsClient::nonSkippableVideoWillDismiss(System.Boolean)
extern void AppodealAdsClient_nonSkippableVideoWillDismiss_m106A8F432A7B3624CB4C5952A0C81F4536C9AAB2 ();
// 0x0000019B System.Void AppodealAds.Unity.iOS.AppodealAdsClient::nonSkippableVideoDidFinish()
extern void AppodealAdsClient_nonSkippableVideoDidFinish_m1D97230B52B056EF2796FEC19736CC29697C1EE3 ();
// 0x0000019C System.Void AppodealAds.Unity.iOS.AppodealAdsClient::nonSkippableVideoDidPresent()
extern void AppodealAdsClient_nonSkippableVideoDidPresent_mC72B89A402A310456B7A3DCB13685282666E8E2C ();
// 0x0000019D System.Void AppodealAds.Unity.iOS.AppodealAdsClient::nonSkippableVideoDidExpired()
extern void AppodealAdsClient_nonSkippableVideoDidExpired_mE988DEBD05886F100F0D1118DDF3FEF1ADF12AA6 ();
// 0x0000019E System.Void AppodealAds.Unity.iOS.AppodealAdsClient::setNonSkippableVideoCallbacks(AppodealAds.Unity.Common.INonSkippableVideoAdListener)
extern void AppodealAdsClient_setNonSkippableVideoCallbacks_m50251C93B3920BFA02929F0C3BF2BD3CE4679B7A ();
// 0x0000019F System.Void AppodealAds.Unity.iOS.AppodealAdsClient::rewardedVideoDidLoadAd(System.Boolean)
extern void AppodealAdsClient_rewardedVideoDidLoadAd_m72EE5300ECEE4915958A5D119C2344DBEB3B5B4C ();
// 0x000001A0 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::rewardedVideoDidFailToLoadAd()
extern void AppodealAdsClient_rewardedVideoDidFailToLoadAd_m61E6B73608743C20063451294C9EAB5271F5AD5B ();
// 0x000001A1 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::rewardedVideoWillDismiss(System.Boolean)
extern void AppodealAdsClient_rewardedVideoWillDismiss_m1C28CC6A05D9ED2DE343F965A7E8FD5DC3810F96 ();
// 0x000001A2 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::rewardedVideoDidFinish(System.Double,System.String)
extern void AppodealAdsClient_rewardedVideoDidFinish_mE2F18E3F1C5311569E0BF7C3743B448A4BE34679 ();
// 0x000001A3 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::rewardedVideoDidPresent()
extern void AppodealAdsClient_rewardedVideoDidPresent_m5F0062152B6355C1497F3662B3128D172D3D4365 ();
// 0x000001A4 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::rewardedVideoDidExpired()
extern void AppodealAdsClient_rewardedVideoDidExpired_mAC5AD49EA77382D236F3CDDC6B4DE2E39ACB6976 ();
// 0x000001A5 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::rewardedVideoDidReceiveTap()
extern void AppodealAdsClient_rewardedVideoDidReceiveTap_m7B6FA93EE995BBF3E93B161F41AE8FDDF288B405 ();
// 0x000001A6 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::setRewardedVideoCallbacks(AppodealAds.Unity.Common.IRewardedVideoAdListener)
extern void AppodealAdsClient_setRewardedVideoCallbacks_mBA7DA11ED723FF86D7598F0F46B475430308EC74 ();
// 0x000001A7 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::bannerDidLoadAd(System.Boolean)
extern void AppodealAdsClient_bannerDidLoadAd_mF459BD51E103C268D0E8C786CB4C16CDBB334D58 ();
// 0x000001A8 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::bannerDidFailToLoadAd()
extern void AppodealAdsClient_bannerDidFailToLoadAd_mAC347D45866570913B08474E397C7A43D1AA62E9 ();
// 0x000001A9 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::bannerDidClick()
extern void AppodealAdsClient_bannerDidClick_m7772D454BF1EB04448DCE581018FE16BA3C987C3 ();
// 0x000001AA System.Void AppodealAds.Unity.iOS.AppodealAdsClient::bannerDidShow()
extern void AppodealAdsClient_bannerDidShow_mDE7E4BE941E82BBF12BFB4D8FBEA4D82ECEE57BB ();
// 0x000001AB System.Void AppodealAds.Unity.iOS.AppodealAdsClient::bannerViewDidLoadAd(System.Boolean)
extern void AppodealAdsClient_bannerViewDidLoadAd_m5A4291DDC3852D80673BE2A762E687D912B2F0B7 ();
// 0x000001AC System.Void AppodealAds.Unity.iOS.AppodealAdsClient::bannerViewDidFailToLoadAd()
extern void AppodealAdsClient_bannerViewDidFailToLoadAd_m500257DC50EEFC82C843799108C08D2E0A276A0D ();
// 0x000001AD System.Void AppodealAds.Unity.iOS.AppodealAdsClient::bannerViewDidClick()
extern void AppodealAdsClient_bannerViewDidClick_m2452AED8E93F782EB12889388BB54B826EC7B7DF ();
// 0x000001AE System.Void AppodealAds.Unity.iOS.AppodealAdsClient::bannerViewDidExpired()
extern void AppodealAdsClient_bannerViewDidExpired_mEEBFFAD759189DD5D2AC3D4A847ED132186BEAA3 ();
// 0x000001AF System.Void AppodealAds.Unity.iOS.AppodealAdsClient::setBannerCallbacks(AppodealAds.Unity.Common.IBannerAdListener)
extern void AppodealAdsClient_setBannerCallbacks_mE8A183D467CD173E5488A3AB214637AD10AD85FE ();
// 0x000001B0 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::mrecViewDidLoadAd(System.Boolean)
extern void AppodealAdsClient_mrecViewDidLoadAd_m59CE906506A9BD3D91F85E6CCC32727AEA8168B1 ();
// 0x000001B1 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::mrecViewDidFailToLoadAd()
extern void AppodealAdsClient_mrecViewDidFailToLoadAd_mD759E93CB8BCD76A19F87AE93E5220311FDE4E61 ();
// 0x000001B2 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::mrecViewDidClick()
extern void AppodealAdsClient_mrecViewDidClick_m23FD716FFE2FA0307D73478F09FA8C6A0378C2AB ();
// 0x000001B3 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::mrecViewDidExpired()
extern void AppodealAdsClient_mrecViewDidExpired_m1603F4DABFEDDEFDC12AE46961D8CFFEC0EC5A5A ();
// 0x000001B4 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::setMrecCallbacks(AppodealAds.Unity.Common.IMrecAdListener)
extern void AppodealAdsClient_setMrecCallbacks_mDAA52B5D406CA93283D81A6719D6C2AE30D0EF83 ();
// 0x000001B5 System.Int32 AppodealAds.Unity.iOS.AppodealAdsClient::nativeAdTypesForType(System.Int32)
extern void AppodealAdsClient_nativeAdTypesForType_m70D2BDF6A78928878F572B76B134DECEBC01920B ();
// 0x000001B6 System.Int32 AppodealAds.Unity.iOS.AppodealAdsClient::nativeShowStyleForType(System.Int32)
extern void AppodealAdsClient_nativeShowStyleForType_m35E8D58805C7E8FFAF53113D6CC1CE76F60B6A28 ();
// 0x000001B7 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::initialize(System.String,System.Int32)
extern void AppodealAdsClient_initialize_m9B1B4B6D20F7ADF85FCDF7E5375FD22ED1D82664 ();
// 0x000001B8 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::initialize(System.String,System.Int32,System.Boolean)
extern void AppodealAdsClient_initialize_m8A798D7F821AA2E81DC260A6BC03414D8CEEB12F ();
// 0x000001B9 System.Boolean AppodealAds.Unity.iOS.AppodealAdsClient::isInitialized(System.Int32)
extern void AppodealAdsClient_isInitialized_mF89DBFB99AB200874B8507FC091E5ACB4027875F ();
// 0x000001BA System.Boolean AppodealAds.Unity.iOS.AppodealAdsClient::show(System.Int32)
extern void AppodealAdsClient_show_m90DD22C710DF94258C887BCE3D7E39FD56A501C4 ();
// 0x000001BB System.Boolean AppodealAds.Unity.iOS.AppodealAdsClient::show(System.Int32,System.String)
extern void AppodealAdsClient_show_m97CC49AE8636AB3467688EB86FA69A7E0241E7A0 ();
// 0x000001BC System.Boolean AppodealAds.Unity.iOS.AppodealAdsClient::showBannerView(System.Int32,System.Int32,System.String)
extern void AppodealAdsClient_showBannerView_m40F33B6AC695C7D8DA9A310A9FC153B241ABD888 ();
// 0x000001BD System.Boolean AppodealAds.Unity.iOS.AppodealAdsClient::showMrecView(System.Int32,System.Int32,System.String)
extern void AppodealAdsClient_showMrecView_m88CA7C6A2C2A1C726636D4AD8C9251FA8B62561B ();
// 0x000001BE System.Boolean AppodealAds.Unity.iOS.AppodealAdsClient::isLoaded(System.Int32)
extern void AppodealAdsClient_isLoaded_m85DF071D5ABB2E34CA5D45CCCB8F9464A9A299C3 ();
// 0x000001BF System.Void AppodealAds.Unity.iOS.AppodealAdsClient::cache(System.Int32)
extern void AppodealAdsClient_cache_mC08BD7C6299C9DE64A8C048941B6C92775FF51CA ();
// 0x000001C0 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::setAutoCache(System.Int32,System.Boolean)
extern void AppodealAdsClient_setAutoCache_m7DD43CBC49FE466B649832A2D6F065021D629BD9 ();
// 0x000001C1 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::hide(System.Int32)
extern void AppodealAdsClient_hide_m78BB29B603AF3E5EE8DAEEAA8D089E2CCF7660EA ();
// 0x000001C2 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::hideBannerView()
extern void AppodealAdsClient_hideBannerView_mAEA52C43E94A5B63FEAF5D689847806AA375F8B4 ();
// 0x000001C3 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::hideMrecView()
extern void AppodealAdsClient_hideMrecView_m0D053E8923898518A193F9920E650C7FA4024A89 ();
// 0x000001C4 System.Boolean AppodealAds.Unity.iOS.AppodealAdsClient::isPrecache(System.Int32)
extern void AppodealAdsClient_isPrecache_m2E29ABA3F9DD13353E21AC1CFD624CCEA6A87041 ();
// 0x000001C5 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::onResume(System.Int32)
extern void AppodealAdsClient_onResume_mFD22C44C35C626E5BC7318C9D46CD1A15C148DB9 ();
// 0x000001C6 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::setSmartBanners(System.Boolean)
extern void AppodealAdsClient_setSmartBanners_mE92AE25D7DD059F5CE7ACAF5E7027AA89A460752 ();
// 0x000001C7 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::setBannerAnimation(System.Boolean)
extern void AppodealAdsClient_setBannerAnimation_m780BA8C40D2E90FE561B57F536A91184AFB28A51 ();
// 0x000001C8 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::setBannerBackground(System.Boolean)
extern void AppodealAdsClient_setBannerBackground_mDD5490A634EFC1EE528FAC0543B477560FE9A25B ();
// 0x000001C9 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::setTabletBanners(System.Boolean)
extern void AppodealAdsClient_setTabletBanners_mF40A60D8A5CA8ABF9119CD2ECFBD7AD070C3B9C0 ();
// 0x000001CA System.Void AppodealAds.Unity.iOS.AppodealAdsClient::setTesting(System.Boolean)
extern void AppodealAdsClient_setTesting_m4F73D156630D9E080F18372492C9E6D9105899D4 ();
// 0x000001CB System.Void AppodealAds.Unity.iOS.AppodealAdsClient::updateConsent(System.Boolean)
extern void AppodealAdsClient_updateConsent_mD9F89ED36DA5B2F0CFA76633C04B3312271759E7 ();
// 0x000001CC System.Void AppodealAds.Unity.iOS.AppodealAdsClient::resetFilterMatureContentFlag()
extern void AppodealAdsClient_resetFilterMatureContentFlag_mF008BA667DCF998439006538FDFFE03457280E72 ();
// 0x000001CD System.Void AppodealAds.Unity.iOS.AppodealAdsClient::setLogLevel(AppodealAds.Unity.Api.Appodeal_LogLevel)
extern void AppodealAdsClient_setLogLevel_mA3EA525B61F3B7B0F958C34AE585403ECA6B4EF9 ();
// 0x000001CE System.Void AppodealAds.Unity.iOS.AppodealAdsClient::setChildDirectedTreatment(System.Boolean)
extern void AppodealAdsClient_setChildDirectedTreatment_mB6C2752993D320C8BDC59B77929BF02B2D27BBE1 ();
// 0x000001CF System.Void AppodealAds.Unity.iOS.AppodealAdsClient::disableNetwork(System.String)
extern void AppodealAdsClient_disableNetwork_m6B22DC28E90CEDE92FAF618D4C776DDC02BE5B32 ();
// 0x000001D0 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::disableNetwork(System.String,System.Int32)
extern void AppodealAdsClient_disableNetwork_m69279F8697E0E71C280DFC76FC3D5519CD413124 ();
// 0x000001D1 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::disableLocationPermissionCheck()
extern void AppodealAdsClient_disableLocationPermissionCheck_mAC8CBEF6DDB08E560D8560BE7B754C2475E42FDD ();
// 0x000001D2 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::disableWriteExternalStoragePermissionCheck()
extern void AppodealAdsClient_disableWriteExternalStoragePermissionCheck_mC6C17830C52DD7DFFC4F2449F04D2867D9A916F2 ();
// 0x000001D3 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::muteVideosIfCallsMuted(System.Boolean)
extern void AppodealAdsClient_muteVideosIfCallsMuted_m5348046F0EE210FD8979759FED1B09DC1F4E7B84 ();
// 0x000001D4 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::showTestScreen()
extern void AppodealAdsClient_showTestScreen_m1A50EC72EDE20DFD83CAC43600765F658E585BFF ();
// 0x000001D5 System.String AppodealAds.Unity.iOS.AppodealAdsClient::getVersion()
extern void AppodealAdsClient_getVersion_m1C8BE17C5552F0CCD18D5284A6C97ED9A8675317 ();
// 0x000001D6 System.Boolean AppodealAds.Unity.iOS.AppodealAdsClient::canShow(System.Int32,System.String)
extern void AppodealAdsClient_canShow_m0488A1D132B9C7DAC9F5A7769610783B2C263BE3 ();
// 0x000001D7 System.Boolean AppodealAds.Unity.iOS.AppodealAdsClient::canShow(System.Int32)
extern void AppodealAdsClient_canShow_mD054EE483A5767B87DB4D535ACBDFEC1A1B88BEC ();
// 0x000001D8 System.String AppodealAds.Unity.iOS.AppodealAdsClient::getRewardCurrency(System.String)
extern void AppodealAdsClient_getRewardCurrency_m3ACF6D27140C3FB9A901FB9BA5E639B054DD8EC3 ();
// 0x000001D9 System.Double AppodealAds.Unity.iOS.AppodealAdsClient::getRewardAmount(System.String)
extern void AppodealAdsClient_getRewardAmount_m671CDEE3CE82D375F89C0A8691C15405F2C69CAD ();
// 0x000001DA System.String AppodealAds.Unity.iOS.AppodealAdsClient::getRewardCurrency()
extern void AppodealAdsClient_getRewardCurrency_mAD3B8764829E7FED1AD2F9B9719624A3948DC090 ();
// 0x000001DB System.Double AppodealAds.Unity.iOS.AppodealAdsClient::getRewardAmount()
extern void AppodealAdsClient_getRewardAmount_m2FBD9EB0E1B42C90B6D320AE576A265A2ED0FA43 ();
// 0x000001DC System.Double AppodealAds.Unity.iOS.AppodealAdsClient::getPredictedEcpm(System.Int32)
extern void AppodealAdsClient_getPredictedEcpm_mACAB8629473918218A470711AB28AA07158E81A7 ();
// 0x000001DD System.Void AppodealAds.Unity.iOS.AppodealAdsClient::setSegmentFilter(System.String,System.Boolean)
extern void AppodealAdsClient_setSegmentFilter_m15AE6AC661DEF50C66F1D6D6AA00A81331D13B73 ();
// 0x000001DE System.Void AppodealAds.Unity.iOS.AppodealAdsClient::setSegmentFilter(System.String,System.Int32)
extern void AppodealAdsClient_setSegmentFilter_mB0EC8F9A35CDE97760A5CF27CE474519B3CE2A0D ();
// 0x000001DF System.Void AppodealAds.Unity.iOS.AppodealAdsClient::setSegmentFilter(System.String,System.Double)
extern void AppodealAdsClient_setSegmentFilter_mD9CCDC82A3CAB311D02AAA0C526CAB11CF552129 ();
// 0x000001E0 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::setSegmentFilter(System.String,System.String)
extern void AppodealAdsClient_setSegmentFilter_m7AB9D03F7911E99C4E6DDE7AAA3EE6F22117A980 ();
// 0x000001E1 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::setExtraData(System.String,System.Boolean)
extern void AppodealAdsClient_setExtraData_mFCC85BA480A8C0B9DB548BCD7519029F95801868 ();
// 0x000001E2 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::setExtraData(System.String,System.Int32)
extern void AppodealAdsClient_setExtraData_mED48F07B62E1EE91F327BC5E06CC0F98D1D0EE5F ();
// 0x000001E3 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::setExtraData(System.String,System.Double)
extern void AppodealAdsClient_setExtraData_mF9C3F0A2898AB7CD4062BC13BAE2BBC4115D0466 ();
// 0x000001E4 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::setExtraData(System.String,System.String)
extern void AppodealAdsClient_setExtraData_mFEF9B0C81D5CEC0213A892869D12A357C9CE409E ();
// 0x000001E5 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::setTriggerOnLoadedOnPrecache(System.Int32,System.Boolean)
extern void AppodealAdsClient_setTriggerOnLoadedOnPrecache_mC219530EF4E2CA4A32E5599587B75D6B4E4765B0 ();
// 0x000001E6 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::destroy(System.Int32)
extern void AppodealAdsClient_destroy_m2F26EDF686D321F5D73B73B7D9ED0D2ABC3CD560 ();
// 0x000001E7 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::getUserSettings()
extern void AppodealAdsClient_getUserSettings_m8EF9C01B414B3661CB95A47542A62CF09CE3295A ();
// 0x000001E8 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::setUserId(System.String)
extern void AppodealAdsClient_setUserId_mCAF2D867D456AE4AB952FB63CC38438D2731B6F7 ();
// 0x000001E9 System.Void AppodealAds.Unity.iOS.AppodealAdsClient::setAge(System.Int32)
extern void AppodealAdsClient_setAge_m293C4FA9124AECD0A8C257CFD8252956E2DD7515 ();
// 0x000001EA System.Void AppodealAds.Unity.iOS.AppodealAdsClient::setGender(AppodealAds.Unity.Api.UserSettings_Gender)
extern void AppodealAdsClient_setGender_m8E47A4912B0607BA24C141B6312ADD07236894FC ();
// 0x000001EB System.Void AppodealAds.Unity.iOS.AppodealAdsClient::trackInAppPurchase(System.Double,System.String)
extern void AppodealAdsClient_trackInAppPurchase_mEA404DF89D504D020B1E691E860A2C3BE1185BA2 ();
// 0x000001EC System.Void AppodealAds.Unity.iOS.AppodealAdsClient::.cctor()
extern void AppodealAdsClient__cctor_m5F9DE7DC34AF8ED53FBED665DE45724E174E1758 ();
// 0x000001ED System.Void AppodealAds.Unity.iOS.AppodealInterstitialCallbacks::.ctor(System.Object,System.IntPtr)
extern void AppodealInterstitialCallbacks__ctor_m0EB6C09BD6D2F26F957E6FF3F1DDBC3A8E51C9D5 ();
// 0x000001EE System.Void AppodealAds.Unity.iOS.AppodealInterstitialCallbacks::Invoke()
extern void AppodealInterstitialCallbacks_Invoke_mA771AFF6C8FEEFECBD14C3A08F7233BA697738B5 ();
// 0x000001EF System.IAsyncResult AppodealAds.Unity.iOS.AppodealInterstitialCallbacks::BeginInvoke(System.AsyncCallback,System.Object)
extern void AppodealInterstitialCallbacks_BeginInvoke_m129A79B7EA4C42F8C8DB8BBB04D42EE0344B86D4 ();
// 0x000001F0 System.Void AppodealAds.Unity.iOS.AppodealInterstitialCallbacks::EndInvoke(System.IAsyncResult)
extern void AppodealInterstitialCallbacks_EndInvoke_m97FAB1F143C5E006E9832860B5B555182210CC31 ();
// 0x000001F1 System.Void AppodealAds.Unity.iOS.AppodealInterstitialDidLoadCallback::.ctor(System.Object,System.IntPtr)
extern void AppodealInterstitialDidLoadCallback__ctor_mFF10AAE357162F6433E840BF307CCD7294ADBB97 ();
// 0x000001F2 System.Void AppodealAds.Unity.iOS.AppodealInterstitialDidLoadCallback::Invoke(System.Boolean)
extern void AppodealInterstitialDidLoadCallback_Invoke_m303B048D2A29D868503C61A9F62D02FF3385C3D6 ();
// 0x000001F3 System.IAsyncResult AppodealAds.Unity.iOS.AppodealInterstitialDidLoadCallback::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern void AppodealInterstitialDidLoadCallback_BeginInvoke_mB9E372CC74519C8B86370BD29C96DE44D864AF87 ();
// 0x000001F4 System.Void AppodealAds.Unity.iOS.AppodealInterstitialDidLoadCallback::EndInvoke(System.IAsyncResult)
extern void AppodealInterstitialDidLoadCallback_EndInvoke_m90FB1272A905A0E6FE88CD1DFAA458619C3B4975 ();
// 0x000001F5 System.Void AppodealAds.Unity.iOS.AppodealNonSkippableVideoCallbacks::.ctor(System.Object,System.IntPtr)
extern void AppodealNonSkippableVideoCallbacks__ctor_mEA027C6B07EBE9F959910F1037A83F2A48B93E02 ();
// 0x000001F6 System.Void AppodealAds.Unity.iOS.AppodealNonSkippableVideoCallbacks::Invoke()
extern void AppodealNonSkippableVideoCallbacks_Invoke_m8FF85C4C9E74D2EEE23B9C87A873A666AF1145B0 ();
// 0x000001F7 System.IAsyncResult AppodealAds.Unity.iOS.AppodealNonSkippableVideoCallbacks::BeginInvoke(System.AsyncCallback,System.Object)
extern void AppodealNonSkippableVideoCallbacks_BeginInvoke_m09476DCEA33B039E87E11FB41858C09BF2699875 ();
// 0x000001F8 System.Void AppodealAds.Unity.iOS.AppodealNonSkippableVideoCallbacks::EndInvoke(System.IAsyncResult)
extern void AppodealNonSkippableVideoCallbacks_EndInvoke_m18D4D5270AFEF8B28DD439F36B51CD3CEE71E184 ();
// 0x000001F9 System.Void AppodealAds.Unity.iOS.AppodealNonSkippableVideoDidLoadCallback::.ctor(System.Object,System.IntPtr)
extern void AppodealNonSkippableVideoDidLoadCallback__ctor_m3A9FF89F13D5609B9ACBEB2A32E2C9ACFF1E664D ();
// 0x000001FA System.Void AppodealAds.Unity.iOS.AppodealNonSkippableVideoDidLoadCallback::Invoke(System.Boolean)
extern void AppodealNonSkippableVideoDidLoadCallback_Invoke_m02D3BBA56DB0E4D8DA532638E43B82AF84E06455 ();
// 0x000001FB System.IAsyncResult AppodealAds.Unity.iOS.AppodealNonSkippableVideoDidLoadCallback::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern void AppodealNonSkippableVideoDidLoadCallback_BeginInvoke_mABD3D1D02EB4A53EDB7C5E990FF1C259E929F9EB ();
// 0x000001FC System.Void AppodealAds.Unity.iOS.AppodealNonSkippableVideoDidLoadCallback::EndInvoke(System.IAsyncResult)
extern void AppodealNonSkippableVideoDidLoadCallback_EndInvoke_m613E033E63B33F26D11DF23F9FF501AD755EC3D7 ();
// 0x000001FD System.Void AppodealAds.Unity.iOS.AppodealNonSkippableVideoDidDismissCallback::.ctor(System.Object,System.IntPtr)
extern void AppodealNonSkippableVideoDidDismissCallback__ctor_m5A5F10413793FF0CDEF872A1EB0AC7EC91C7D78B ();
// 0x000001FE System.Void AppodealAds.Unity.iOS.AppodealNonSkippableVideoDidDismissCallback::Invoke(System.Boolean)
extern void AppodealNonSkippableVideoDidDismissCallback_Invoke_m1F5AC4B509F4B1F15E4B377AEDE968E699630C84 ();
// 0x000001FF System.IAsyncResult AppodealAds.Unity.iOS.AppodealNonSkippableVideoDidDismissCallback::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern void AppodealNonSkippableVideoDidDismissCallback_BeginInvoke_m2E529481D21077CD0F34F01687C162EBC75829A7 ();
// 0x00000200 System.Void AppodealAds.Unity.iOS.AppodealNonSkippableVideoDidDismissCallback::EndInvoke(System.IAsyncResult)
extern void AppodealNonSkippableVideoDidDismissCallback_EndInvoke_mB5EA593D4286E976D13AF81088BEF186B5AF98C5 ();
// 0x00000201 System.Void AppodealAds.Unity.iOS.AppodealBannerCallbacks::.ctor(System.Object,System.IntPtr)
extern void AppodealBannerCallbacks__ctor_mFE47B901B918522BFD9481C97E5D873080C33023 ();
// 0x00000202 System.Void AppodealAds.Unity.iOS.AppodealBannerCallbacks::Invoke()
extern void AppodealBannerCallbacks_Invoke_mF0E87F656A681B33E3C96229F528A334E9690266 ();
// 0x00000203 System.IAsyncResult AppodealAds.Unity.iOS.AppodealBannerCallbacks::BeginInvoke(System.AsyncCallback,System.Object)
extern void AppodealBannerCallbacks_BeginInvoke_m3CB1AA36D8378C2928B4E81CA121907119BFBF06 ();
// 0x00000204 System.Void AppodealAds.Unity.iOS.AppodealBannerCallbacks::EndInvoke(System.IAsyncResult)
extern void AppodealBannerCallbacks_EndInvoke_mC7384B03CD048338A8AD1C61F3FA29CECE1C2FEC ();
// 0x00000205 System.Void AppodealAds.Unity.iOS.AppodealBannerDidLoadCallback::.ctor(System.Object,System.IntPtr)
extern void AppodealBannerDidLoadCallback__ctor_m47B72793C3C9E9F2EC6AB19DCE94E9880061E6B8 ();
// 0x00000206 System.Void AppodealAds.Unity.iOS.AppodealBannerDidLoadCallback::Invoke(System.Boolean)
extern void AppodealBannerDidLoadCallback_Invoke_mC24960E2BF67216307A34EE2E4671FF99883C3E1 ();
// 0x00000207 System.IAsyncResult AppodealAds.Unity.iOS.AppodealBannerDidLoadCallback::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern void AppodealBannerDidLoadCallback_BeginInvoke_m8BB413F7FCA0204B6EEE07A39B217ACFC5EC7F9B ();
// 0x00000208 System.Void AppodealAds.Unity.iOS.AppodealBannerDidLoadCallback::EndInvoke(System.IAsyncResult)
extern void AppodealBannerDidLoadCallback_EndInvoke_mF45B4E30DFD25D2321CE3FED756DCC89C3DC5EFE ();
// 0x00000209 System.Void AppodealAds.Unity.iOS.AppodealBannerViewCallbacks::.ctor(System.Object,System.IntPtr)
extern void AppodealBannerViewCallbacks__ctor_m81BAA390743FE2A609D48CE1427CABD57B3C37AB ();
// 0x0000020A System.Void AppodealAds.Unity.iOS.AppodealBannerViewCallbacks::Invoke()
extern void AppodealBannerViewCallbacks_Invoke_mA0C93424617FB2ACEFEA051BBE2A3EC549603C74 ();
// 0x0000020B System.IAsyncResult AppodealAds.Unity.iOS.AppodealBannerViewCallbacks::BeginInvoke(System.AsyncCallback,System.Object)
extern void AppodealBannerViewCallbacks_BeginInvoke_m40A14BDD6FB6BFDAEFD26192A9F1C03F423EF9C5 ();
// 0x0000020C System.Void AppodealAds.Unity.iOS.AppodealBannerViewCallbacks::EndInvoke(System.IAsyncResult)
extern void AppodealBannerViewCallbacks_EndInvoke_m97D8C2C0203C22D36952868893F037C11F8AE126 ();
// 0x0000020D System.Void AppodealAds.Unity.iOS.AppodealBannerViewDidLoadCallback::.ctor(System.Object,System.IntPtr)
extern void AppodealBannerViewDidLoadCallback__ctor_mE39ACB3B6C798605B8382A3F323E439C1472F1CB ();
// 0x0000020E System.Void AppodealAds.Unity.iOS.AppodealBannerViewDidLoadCallback::Invoke(System.Boolean)
extern void AppodealBannerViewDidLoadCallback_Invoke_mA8A150C9741BAA68C16FF1E3D589280A651CE816 ();
// 0x0000020F System.IAsyncResult AppodealAds.Unity.iOS.AppodealBannerViewDidLoadCallback::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern void AppodealBannerViewDidLoadCallback_BeginInvoke_mE76BA79FC806B997F7E7C5D9A2FE9DF1E806FF30 ();
// 0x00000210 System.Void AppodealAds.Unity.iOS.AppodealBannerViewDidLoadCallback::EndInvoke(System.IAsyncResult)
extern void AppodealBannerViewDidLoadCallback_EndInvoke_m6CF0EF0DAEA974929C4C501174D8B8AD6431E17E ();
// 0x00000211 System.Void AppodealAds.Unity.iOS.AppodealMrecViewCallbacks::.ctor(System.Object,System.IntPtr)
extern void AppodealMrecViewCallbacks__ctor_mF9EEC9D7C5CEB555746AF6A34F6F42D308FDE627 ();
// 0x00000212 System.Void AppodealAds.Unity.iOS.AppodealMrecViewCallbacks::Invoke()
extern void AppodealMrecViewCallbacks_Invoke_m423B5ADCED3897DB72F38152767354B6C6636914 ();
// 0x00000213 System.IAsyncResult AppodealAds.Unity.iOS.AppodealMrecViewCallbacks::BeginInvoke(System.AsyncCallback,System.Object)
extern void AppodealMrecViewCallbacks_BeginInvoke_m1F7599E3018F1B9A27E39686AA26FD9D1A27E287 ();
// 0x00000214 System.Void AppodealAds.Unity.iOS.AppodealMrecViewCallbacks::EndInvoke(System.IAsyncResult)
extern void AppodealMrecViewCallbacks_EndInvoke_m4CBFE212FC482475CA1ED6C19AC4A23BADF14F75 ();
// 0x00000215 System.Void AppodealAds.Unity.iOS.AppodealMrecViewDidLoadCallback::.ctor(System.Object,System.IntPtr)
extern void AppodealMrecViewDidLoadCallback__ctor_m9BB4DED3C766D292B9CD2D56BEEBE8B051480BD6 ();
// 0x00000216 System.Void AppodealAds.Unity.iOS.AppodealMrecViewDidLoadCallback::Invoke(System.Boolean)
extern void AppodealMrecViewDidLoadCallback_Invoke_m84BD0E373B96DD4837F1AACB8300A14B79E0091C ();
// 0x00000217 System.IAsyncResult AppodealAds.Unity.iOS.AppodealMrecViewDidLoadCallback::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern void AppodealMrecViewDidLoadCallback_BeginInvoke_m124ECF74AF21FCA6D1E04D4AEF39D29D62862668 ();
// 0x00000218 System.Void AppodealAds.Unity.iOS.AppodealMrecViewDidLoadCallback::EndInvoke(System.IAsyncResult)
extern void AppodealMrecViewDidLoadCallback_EndInvoke_mA5146AC8EC12CC49479AE8C6CEEB444C212722ED ();
// 0x00000219 System.Void AppodealAds.Unity.iOS.AppodealRewardedVideoCallbacks::.ctor(System.Object,System.IntPtr)
extern void AppodealRewardedVideoCallbacks__ctor_m479161DF64DAAE184AFFBF77863E81D05C217A6E ();
// 0x0000021A System.Void AppodealAds.Unity.iOS.AppodealRewardedVideoCallbacks::Invoke()
extern void AppodealRewardedVideoCallbacks_Invoke_m56646D4B5EE3DF12C07B188537157045F6855488 ();
// 0x0000021B System.IAsyncResult AppodealAds.Unity.iOS.AppodealRewardedVideoCallbacks::BeginInvoke(System.AsyncCallback,System.Object)
extern void AppodealRewardedVideoCallbacks_BeginInvoke_m2AC51ADE64E8845521D3E8FA2BB12F318195468F ();
// 0x0000021C System.Void AppodealAds.Unity.iOS.AppodealRewardedVideoCallbacks::EndInvoke(System.IAsyncResult)
extern void AppodealRewardedVideoCallbacks_EndInvoke_mB53CE98A485AC9B29BEC9659D4E5DA987B097C6E ();
// 0x0000021D System.Void AppodealAds.Unity.iOS.AppodealRewardedVideoDidLoadCallback::.ctor(System.Object,System.IntPtr)
extern void AppodealRewardedVideoDidLoadCallback__ctor_mE28E39314D46D2072D8EB5DBED4463450EF08340 ();
// 0x0000021E System.Void AppodealAds.Unity.iOS.AppodealRewardedVideoDidLoadCallback::Invoke(System.Boolean)
extern void AppodealRewardedVideoDidLoadCallback_Invoke_m1A0833D6A1C1EECC4CFEB38ED92A1CC5CCE5CA03 ();
// 0x0000021F System.IAsyncResult AppodealAds.Unity.iOS.AppodealRewardedVideoDidLoadCallback::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern void AppodealRewardedVideoDidLoadCallback_BeginInvoke_mA16AC28790368B1A3BC3614CE17F868B7F89C903 ();
// 0x00000220 System.Void AppodealAds.Unity.iOS.AppodealRewardedVideoDidLoadCallback::EndInvoke(System.IAsyncResult)
extern void AppodealRewardedVideoDidLoadCallback_EndInvoke_mFC9D74C268AA3C2949B87960F9FEB2CBBEBF1341 ();
// 0x00000221 System.Void AppodealAds.Unity.iOS.AppodealRewardedVideoDidDismissCallback::.ctor(System.Object,System.IntPtr)
extern void AppodealRewardedVideoDidDismissCallback__ctor_m944767978BB67EE24C6D3C48538FF28C5DA23B37 ();
// 0x00000222 System.Void AppodealAds.Unity.iOS.AppodealRewardedVideoDidDismissCallback::Invoke(System.Boolean)
extern void AppodealRewardedVideoDidDismissCallback_Invoke_mDAED1151858E3EE4546EDEF7B8D745A7BAD63E35 ();
// 0x00000223 System.IAsyncResult AppodealAds.Unity.iOS.AppodealRewardedVideoDidDismissCallback::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern void AppodealRewardedVideoDidDismissCallback_BeginInvoke_m8A2C8A5E10C0E1A46F431CB6B2608241D9ABC50D ();
// 0x00000224 System.Void AppodealAds.Unity.iOS.AppodealRewardedVideoDidDismissCallback::EndInvoke(System.IAsyncResult)
extern void AppodealRewardedVideoDidDismissCallback_EndInvoke_m57FD711751639622D18F5294F032701EC24DDA05 ();
// 0x00000225 System.Void AppodealAds.Unity.iOS.AppodealRewardedVideoDidFinishCallback::.ctor(System.Object,System.IntPtr)
extern void AppodealRewardedVideoDidFinishCallback__ctor_mF36A39B39F918D609EDD21C8646EA622607FEB62 ();
// 0x00000226 System.Void AppodealAds.Unity.iOS.AppodealRewardedVideoDidFinishCallback::Invoke(System.Double,System.String)
extern void AppodealRewardedVideoDidFinishCallback_Invoke_m754F8184AE387B195F27CB2D4C9580723D79438B ();
// 0x00000227 System.IAsyncResult AppodealAds.Unity.iOS.AppodealRewardedVideoDidFinishCallback::BeginInvoke(System.Double,System.String,System.AsyncCallback,System.Object)
extern void AppodealRewardedVideoDidFinishCallback_BeginInvoke_m8BECECEDEFCDD22355D595D4118FEC4CE6381012 ();
// 0x00000228 System.Void AppodealAds.Unity.iOS.AppodealRewardedVideoDidFinishCallback::EndInvoke(System.IAsyncResult)
extern void AppodealRewardedVideoDidFinishCallback_EndInvoke_mECE3D78D46DC364B3F1FA0C91D6F13DB846411EB ();
// 0x00000229 System.Void AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealInitialize(System.String,System.Int32,System.Boolean,System.String,System.String)
extern void AppodealObjCBridge_AppodealInitialize_m64A0C59D0EC17C316396A23B1BB00E22C476EC91 ();
// 0x0000022A System.Boolean AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealIsInitialized(System.Int32)
extern void AppodealObjCBridge_AppodealIsInitialized_mB1AEDA7DDE9B53D1EA7DA0A2667F483926B69264 ();
// 0x0000022B System.Boolean AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealShowAd(System.Int32)
extern void AppodealObjCBridge_AppodealShowAd_m7091300D3C4E3D54B0A133EB040AB455C7127F05 ();
// 0x0000022C System.Boolean AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealShowAdforPlacement(System.Int32,System.String)
extern void AppodealObjCBridge_AppodealShowAdforPlacement_m112AD9F8B44D729BC481867C1A920EB03057BFDD ();
// 0x0000022D System.Boolean AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealShowBannerAdViewforPlacement(System.Int32,System.Int32,System.String)
extern void AppodealObjCBridge_AppodealShowBannerAdViewforPlacement_m8DA7A560C4AC8B68F6B70D7178F52E1130F208D3 ();
// 0x0000022E System.Boolean AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealShowMrecAdViewforPlacement(System.Int32,System.Int32,System.String)
extern void AppodealObjCBridge_AppodealShowMrecAdViewforPlacement_m4797EEBB8E9DB2B04E9533F98F447AE2CCD69AAB ();
// 0x0000022F System.Boolean AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealIsReadyWithStyle(System.Int32)
extern void AppodealObjCBridge_AppodealIsReadyWithStyle_mC4306AEEC053D04F90F0B63D85808CC6EF946073 ();
// 0x00000230 System.Void AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealHideBanner()
extern void AppodealObjCBridge_AppodealHideBanner_m41D557A508E16F902C60DE81404F4BDCFCDCB5AD ();
// 0x00000231 System.Void AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealHideBannerView()
extern void AppodealObjCBridge_AppodealHideBannerView_mA20769338E2DA62EEC0268F216582D575ECD37D5 ();
// 0x00000232 System.Void AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealHideMrecView()
extern void AppodealObjCBridge_AppodealHideMrecView_mBF02B27ABCACC9474FAF2560C68751D323CDE94D ();
// 0x00000233 System.Void AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealCacheAd(System.Int32)
extern void AppodealObjCBridge_AppodealCacheAd_m09A0879F2F6D596EBE3E653BAF39341321A209B7 ();
// 0x00000234 System.Void AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealSetAutocache(System.Boolean,System.Int32)
extern void AppodealObjCBridge_AppodealSetAutocache_m341770DD166DFEA7AAAA5E71A11A816CAFF37B8D ();
// 0x00000235 System.Void AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealSetSmartBanners(System.Boolean)
extern void AppodealObjCBridge_AppodealSetSmartBanners_mB7CACC2D3990315A89CD8E878C8EA4FB433D8131 ();
// 0x00000236 System.Void AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealSetTabletBanners(System.Boolean)
extern void AppodealObjCBridge_AppodealSetTabletBanners_m6C6B07C6D06D1F72135FD0151953949323E49694 ();
// 0x00000237 System.Void AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealSetBannerBackground(System.Boolean)
extern void AppodealObjCBridge_AppodealSetBannerBackground_m7D639AE490DAF207F8AF2BAACEA6758AD0A1D6B5 ();
// 0x00000238 System.Void AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealSetBannerAnimation(System.Boolean)
extern void AppodealObjCBridge_AppodealSetBannerAnimation_m99A4EB4FD7543E3339D4178B566DD45522DA66C6 ();
// 0x00000239 System.Void AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealSetLogLevel(System.Int32)
extern void AppodealObjCBridge_AppodealSetLogLevel_m1A04636F2450DB2F11672C2C9CAD2A762BDDA13A ();
// 0x0000023A System.Void AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealSetTestingEnabled(System.Boolean)
extern void AppodealObjCBridge_AppodealSetTestingEnabled_m0E0703EFA93C329028E3ADD98773967DA04A8FFA ();
// 0x0000023B System.Void AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealUpdateConsent(System.Boolean)
extern void AppodealObjCBridge_AppodealUpdateConsent_m2411F42C40409AB84BF41782AA8D97309A7D0600 ();
// 0x0000023C System.Void AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealSetChildDirectedTreatment(System.Boolean)
extern void AppodealObjCBridge_AppodealSetChildDirectedTreatment_mD9B227E5B9EA594F2F8BFDB3A8E02663414F54D9 ();
// 0x0000023D System.Void AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealDisableNetwork(System.String)
extern void AppodealObjCBridge_AppodealDisableNetwork_mA267C690A6533916C170D642FCA7C2A653A272BF ();
// 0x0000023E System.Void AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealSetTriggerPrecacheCallbacks(System.Boolean)
extern void AppodealObjCBridge_AppodealSetTriggerPrecacheCallbacks_mCBF761596102165094272B00A876268CD8B9F939 ();
// 0x0000023F System.Void AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealDisableNetworkForAdTypes(System.String,System.Int32)
extern void AppodealObjCBridge_AppodealDisableNetworkForAdTypes_m85C67FEF7B4040AF236BF604BCE525119AB53B18 ();
// 0x00000240 System.Void AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealDisableLocationPermissionCheck()
extern void AppodealObjCBridge_AppodealDisableLocationPermissionCheck_mDBB029130F2719851B948F8490280597BA6A08DE ();
// 0x00000241 System.String AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealGetVersion()
extern void AppodealObjCBridge_AppodealGetVersion_m0408771437F39D38757D43474A00A6847FC9D69F ();
// 0x00000242 System.Boolean AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealCanShow(System.Int32)
extern void AppodealObjCBridge_AppodealCanShow_m8AC6FF79E50A0609A042023E18CD842D6F34369F ();
// 0x00000243 System.Boolean AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealCanShowWithPlacement(System.Int32,System.String)
extern void AppodealObjCBridge_AppodealCanShowWithPlacement_m97BBF300DA5F70AF20ABCF69D2D0451AF705E37A ();
// 0x00000244 System.String AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealGetRewardCurrency(System.String)
extern void AppodealObjCBridge_AppodealGetRewardCurrency_mE7F7ABFD1DDA9314B6BE41BC02AD9F076A81168A ();
// 0x00000245 System.Double AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealGetRewardAmount(System.String)
extern void AppodealObjCBridge_AppodealGetRewardAmount_m32DB6D5831849E37751B458CF06449807E0C9083 ();
// 0x00000246 System.Double AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealGetPredictedEcpm(System.Int32)
extern void AppodealObjCBridge_AppodealGetPredictedEcpm_mE8E0A5EDB6BC7E6D7315D4E96F6A76385E5AF22C ();
// 0x00000247 System.Void AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealSetSegmentFilterString(System.String,System.String)
extern void AppodealObjCBridge_AppodealSetSegmentFilterString_mB635BDA66AE832D4FD6EEEC2F6699D51C27743E4 ();
// 0x00000248 System.Void AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealSetSegmentFilterDouble(System.String,System.Double)
extern void AppodealObjCBridge_AppodealSetSegmentFilterDouble_m76D4F75488581F1A0576D19F223A41CD542C413E ();
// 0x00000249 System.Void AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealSetSegmentFilterInt(System.String,System.Int32)
extern void AppodealObjCBridge_AppodealSetSegmentFilterInt_m0B31BC4D25599F7323096E6C8320DDCC65DDF7A4 ();
// 0x0000024A System.Void AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealSetSegmentFilterBool(System.String,System.Boolean)
extern void AppodealObjCBridge_AppodealSetSegmentFilterBool_m4C6ECC50E3D657C9E37F706230873377951AEE17 ();
// 0x0000024B System.Void AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealSetExtraDataBool(System.String,System.Boolean)
extern void AppodealObjCBridge_AppodealSetExtraDataBool_m42F23DFCB14C2C2C4BFA405849A361E801BD62A1 ();
// 0x0000024C System.Void AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealSetExtraDataInt(System.String,System.Int32)
extern void AppodealObjCBridge_AppodealSetExtraDataInt_mDD5291C6DB8F1C7F3BDB1FE6B3752B8473D5A6E2 ();
// 0x0000024D System.Void AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealSetExtraDataDouble(System.String,System.Double)
extern void AppodealObjCBridge_AppodealSetExtraDataDouble_m77E46C2E4934560F15DD8E173194CE2AC94E17FD ();
// 0x0000024E System.Void AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealSetExtraDataString(System.String,System.String)
extern void AppodealObjCBridge_AppodealSetExtraDataString_mAA0C17CFED77E71796EFED316528639F4AD4FF00 ();
// 0x0000024F System.Void AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealTrackInAppPurchase(System.Double,System.String)
extern void AppodealObjCBridge_AppodealTrackInAppPurchase_mFA25E4E5CDF6174B510257FB378DEB3C429A3832 ();
// 0x00000250 System.Void AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealSetUserId(System.String)
extern void AppodealObjCBridge_AppodealSetUserId_m083A3BE854CAFDC0FCEC8D83FE558A6EA54DD8D6 ();
// 0x00000251 System.Void AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealSetUserAge(System.Int32)
extern void AppodealObjCBridge_AppodealSetUserAge_m51C7841937692B567D376245045265C5A99173E7 ();
// 0x00000252 System.Void AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealSetUserGender(System.Int32)
extern void AppodealObjCBridge_AppodealSetUserGender_m7826358E279A4AB007B4ED95A580B587B9B6507B ();
// 0x00000253 System.Void AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealSetInterstitialDelegate(AppodealAds.Unity.iOS.AppodealInterstitialDidLoadCallback,AppodealAds.Unity.iOS.AppodealInterstitialCallbacks,AppodealAds.Unity.iOS.AppodealInterstitialCallbacks,AppodealAds.Unity.iOS.AppodealInterstitialCallbacks,AppodealAds.Unity.iOS.AppodealInterstitialCallbacks,AppodealAds.Unity.iOS.AppodealInterstitialCallbacks)
extern void AppodealObjCBridge_AppodealSetInterstitialDelegate_m0C7587D4E10FFA8956A6AB8BA70D3AA0042454FA ();
// 0x00000254 System.Void AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealSetNonSkippableVideoDelegate(AppodealAds.Unity.iOS.AppodealNonSkippableVideoDidLoadCallback,AppodealAds.Unity.iOS.AppodealNonSkippableVideoCallbacks,AppodealAds.Unity.iOS.AppodealNonSkippableVideoDidDismissCallback,AppodealAds.Unity.iOS.AppodealNonSkippableVideoCallbacks,AppodealAds.Unity.iOS.AppodealNonSkippableVideoCallbacks,AppodealAds.Unity.iOS.AppodealNonSkippableVideoCallbacks)
extern void AppodealObjCBridge_AppodealSetNonSkippableVideoDelegate_m49F3A2EDB8F600D09479D4D647E9CEEBBFE9D5A4 ();
// 0x00000255 System.Void AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealSetRewardedVideoDelegate(AppodealAds.Unity.iOS.AppodealRewardedVideoDidLoadCallback,AppodealAds.Unity.iOS.AppodealRewardedVideoCallbacks,AppodealAds.Unity.iOS.AppodealRewardedVideoDidDismissCallback,AppodealAds.Unity.iOS.AppodealRewardedVideoDidFinishCallback,AppodealAds.Unity.iOS.AppodealRewardedVideoCallbacks,AppodealAds.Unity.iOS.AppodealRewardedVideoCallbacks,AppodealAds.Unity.iOS.AppodealRewardedVideoCallbacks)
extern void AppodealObjCBridge_AppodealSetRewardedVideoDelegate_m760C64F53E76334942E6EC0781BD1D9E8A6EDB5F ();
// 0x00000256 System.Void AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealSetBannerDelegate(AppodealAds.Unity.iOS.AppodealBannerDidLoadCallback,AppodealAds.Unity.iOS.AppodealBannerCallbacks,AppodealAds.Unity.iOS.AppodealBannerCallbacks,AppodealAds.Unity.iOS.AppodealBannerCallbacks,AppodealAds.Unity.iOS.AppodealBannerCallbacks)
extern void AppodealObjCBridge_AppodealSetBannerDelegate_m83A5D18FD4D7E6D21C94201BAB51D8C25175718C ();
// 0x00000257 System.Void AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealSetBannerViewDelegate(AppodealAds.Unity.iOS.AppodealBannerDidLoadCallback,AppodealAds.Unity.iOS.AppodealBannerCallbacks,AppodealAds.Unity.iOS.AppodealBannerCallbacks,AppodealAds.Unity.iOS.AppodealBannerCallbacks)
extern void AppodealObjCBridge_AppodealSetBannerViewDelegate_m534C2108A33125C8B5B59106E670BC15566F965F ();
// 0x00000258 System.Void AppodealAds.Unity.iOS.AppodealObjCBridge::AppodealSetMrecViewDelegate(AppodealAds.Unity.iOS.AppodealBannerDidLoadCallback,AppodealAds.Unity.iOS.AppodealBannerCallbacks,AppodealAds.Unity.iOS.AppodealBannerCallbacks,AppodealAds.Unity.iOS.AppodealBannerCallbacks)
extern void AppodealObjCBridge_AppodealSetMrecViewDelegate_mE3421525EF2B16BBA82D6DFF810C522DB122BB31 ();
// 0x00000259 System.Void AppodealAds.Unity.iOS.AppodealObjCBridge::.ctor()
extern void AppodealObjCBridge__ctor_mF09BC1500B56E6BD5FB90F3ECB5C93872A9DAEB0 ();
// 0x0000025A System.Void AppodealAds.Unity.Dummy.DummyClient::initialize(System.String,System.Int32)
extern void DummyClient_initialize_mED36454B0DDCF36FD6D8596063D02CC326D0A192 ();
// 0x0000025B System.Void AppodealAds.Unity.Dummy.DummyClient::initialize(System.String,System.Int32,System.Boolean)
extern void DummyClient_initialize_m3F54759EEBC456FCEA2627008D3D709C71F40553 ();
// 0x0000025C System.Boolean AppodealAds.Unity.Dummy.DummyClient::isInitialized(System.Int32)
extern void DummyClient_isInitialized_m9D7C66FA8207A206E1231C67C7BE0347C57DC001 ();
// 0x0000025D System.Boolean AppodealAds.Unity.Dummy.DummyClient::show(System.Int32)
extern void DummyClient_show_mB69725D0B53FCE078B4556DC866159D3A717FCEE ();
// 0x0000025E System.Boolean AppodealAds.Unity.Dummy.DummyClient::show(System.Int32,System.String)
extern void DummyClient_show_m358E8649C8A2DEAD7D95BE625EBC90E01532B9F3 ();
// 0x0000025F System.Boolean AppodealAds.Unity.Dummy.DummyClient::showBannerView(System.Int32,System.Int32,System.String)
extern void DummyClient_showBannerView_m5A45A3D2C3B47B34655E260C40E1D7DFAF633AEB ();
// 0x00000260 System.Boolean AppodealAds.Unity.Dummy.DummyClient::showMrecView(System.Int32,System.Int32,System.String)
extern void DummyClient_showMrecView_m040F3DFA41DFBD1DA26981B2DDAD8BA58083F825 ();
// 0x00000261 System.Boolean AppodealAds.Unity.Dummy.DummyClient::isLoaded(System.Int32)
extern void DummyClient_isLoaded_mE21DEF1E2A4881482D82794219D22545612FC34E ();
// 0x00000262 System.Void AppodealAds.Unity.Dummy.DummyClient::cache(System.Int32)
extern void DummyClient_cache_mFD1C95CD1B530246450118FDEDDEA50897BCACBC ();
// 0x00000263 System.Void AppodealAds.Unity.Dummy.DummyClient::hide(System.Int32)
extern void DummyClient_hide_mCCF830721CAABC64F6F04648802F29AA7655DC91 ();
// 0x00000264 System.Void AppodealAds.Unity.Dummy.DummyClient::hideBannerView()
extern void DummyClient_hideBannerView_m21BDD9C2AFA198784159F03E725425EA44819220 ();
// 0x00000265 System.Void AppodealAds.Unity.Dummy.DummyClient::hideMrecView()
extern void DummyClient_hideMrecView_m9A3A00A7DF12B2EC2B26082DCB402E1B2AD59B8D ();
// 0x00000266 System.Boolean AppodealAds.Unity.Dummy.DummyClient::isPrecache(System.Int32)
extern void DummyClient_isPrecache_m6F7893DF1A184CBBC492F3B9D4A7776B566D3D6B ();
// 0x00000267 System.Void AppodealAds.Unity.Dummy.DummyClient::setAutoCache(System.Int32,System.Boolean)
extern void DummyClient_setAutoCache_mF85904CA0F5D0040EDA0588171F327F789465D4F ();
// 0x00000268 System.Void AppodealAds.Unity.Dummy.DummyClient::onResume(System.Int32)
extern void DummyClient_onResume_mF472088C9B73D9BE3543C55091812634CCC82948 ();
// 0x00000269 System.Void AppodealAds.Unity.Dummy.DummyClient::setSmartBanners(System.Boolean)
extern void DummyClient_setSmartBanners_m3852E430E97D9054280D7F404FDC7A8CFBD7899F ();
// 0x0000026A System.Void AppodealAds.Unity.Dummy.DummyClient::setBannerAnimation(System.Boolean)
extern void DummyClient_setBannerAnimation_m866D6A701DDCF867692AE924749F8C2BA8EAAB29 ();
// 0x0000026B System.Void AppodealAds.Unity.Dummy.DummyClient::setBannerBackground(System.Boolean)
extern void DummyClient_setBannerBackground_m4AFFCA1E9B4DD036DCCC83BBE26AE34BC9C00CDD ();
// 0x0000026C System.Void AppodealAds.Unity.Dummy.DummyClient::setTabletBanners(System.Boolean)
extern void DummyClient_setTabletBanners_m029D0AA76D4EFBEFF76E8E8807B45805FD386BED ();
// 0x0000026D System.Void AppodealAds.Unity.Dummy.DummyClient::setTesting(System.Boolean)
extern void DummyClient_setTesting_mE09FD3ABE1BA070B2EC831EE098C6CAA8DD826D5 ();
// 0x0000026E System.Void AppodealAds.Unity.Dummy.DummyClient::setLogLevel(AppodealAds.Unity.Api.Appodeal_LogLevel)
extern void DummyClient_setLogLevel_m525423F870A81F1E45C559D7D1ED5BEFDBD3607A ();
// 0x0000026F System.Void AppodealAds.Unity.Dummy.DummyClient::setChildDirectedTreatment(System.Boolean)
extern void DummyClient_setChildDirectedTreatment_m941BDCA91FF615D889700F855C649969E50C2732 ();
// 0x00000270 System.Void AppodealAds.Unity.Dummy.DummyClient::updateConsent(System.Boolean)
extern void DummyClient_updateConsent_m9D04CBC6199CD091144915A71B1B3B3BB1011083 ();
// 0x00000271 System.Void AppodealAds.Unity.Dummy.DummyClient::resetFilterMatureContentFlag()
extern void DummyClient_resetFilterMatureContentFlag_m2A4A916A72D4E093DF530EC7DA8BC6745D133EBE ();
// 0x00000272 System.Void AppodealAds.Unity.Dummy.DummyClient::disableNetwork(System.String)
extern void DummyClient_disableNetwork_m469B0A2521346F6217928CF46E1E2E78CD588ABB ();
// 0x00000273 System.Void AppodealAds.Unity.Dummy.DummyClient::disableNetwork(System.String,System.Int32)
extern void DummyClient_disableNetwork_m6DEED150795038667742D96D8F94F92F52942BCF ();
// 0x00000274 System.Void AppodealAds.Unity.Dummy.DummyClient::disableLocationPermissionCheck()
extern void DummyClient_disableLocationPermissionCheck_mEFB4FC7E5AE8233A09E960C018BDF704CC38ACEF ();
// 0x00000275 System.Void AppodealAds.Unity.Dummy.DummyClient::disableWriteExternalStoragePermissionCheck()
extern void DummyClient_disableWriteExternalStoragePermissionCheck_m5D2E94C5922BDB091C5364FBA33DF5BA7CBC8EA3 ();
// 0x00000276 System.Void AppodealAds.Unity.Dummy.DummyClient::setTriggerOnLoadedOnPrecache(System.Int32,System.Boolean)
extern void DummyClient_setTriggerOnLoadedOnPrecache_mF38DB99067FF396A7D87D6078452D5BAA9FC6D8F ();
// 0x00000277 System.Void AppodealAds.Unity.Dummy.DummyClient::muteVideosIfCallsMuted(System.Boolean)
extern void DummyClient_muteVideosIfCallsMuted_m8AAF1A2BD0E3F251929B1508C4623AC4DD7BF260 ();
// 0x00000278 System.Void AppodealAds.Unity.Dummy.DummyClient::showTestScreen()
extern void DummyClient_showTestScreen_mFA408BBDF1F9128C7B6BF8BB74FB5EB17C112B63 ();
// 0x00000279 System.String AppodealAds.Unity.Dummy.DummyClient::getVersion()
extern void DummyClient_getVersion_m4EF33B1DFCE3C98C5AF485CBA2E542876B96D394 ();
// 0x0000027A System.Boolean AppodealAds.Unity.Dummy.DummyClient::canShow(System.Int32)
extern void DummyClient_canShow_m81F8D49A96FF71C012D2C5B48E4CF8AF228716CB ();
// 0x0000027B System.Boolean AppodealAds.Unity.Dummy.DummyClient::canShow(System.Int32,System.String)
extern void DummyClient_canShow_m288C0BB8238A3531A26185E4DB699C69CC371995 ();
// 0x0000027C System.Void AppodealAds.Unity.Dummy.DummyClient::setSegmentFilter(System.String,System.Boolean)
extern void DummyClient_setSegmentFilter_mE9F68F7D71CBFE301B16B1F4290A1FD9C9BB1729 ();
// 0x0000027D System.Void AppodealAds.Unity.Dummy.DummyClient::setSegmentFilter(System.String,System.Int32)
extern void DummyClient_setSegmentFilter_mB54C09F34BEF12F953AC4ADFD8990F8383E9083B ();
// 0x0000027E System.Void AppodealAds.Unity.Dummy.DummyClient::setSegmentFilter(System.String,System.Double)
extern void DummyClient_setSegmentFilter_m7B6329DCC595FD58C34846EDE66138C40298065C ();
// 0x0000027F System.Void AppodealAds.Unity.Dummy.DummyClient::setSegmentFilter(System.String,System.String)
extern void DummyClient_setSegmentFilter_m9785613145237971ADEB0303F791528BE8C191BA ();
// 0x00000280 System.Void AppodealAds.Unity.Dummy.DummyClient::trackInAppPurchase(System.Double,System.String)
extern void DummyClient_trackInAppPurchase_mFEDEA7279C2E41CF44E2CCD08DBC8CFA45DA2A74 ();
// 0x00000281 System.String AppodealAds.Unity.Dummy.DummyClient::getRewardCurrency(System.String)
extern void DummyClient_getRewardCurrency_m3917F65A2F60D4D85C078D4B558C9520D7A3C28F ();
// 0x00000282 System.Double AppodealAds.Unity.Dummy.DummyClient::getRewardAmount(System.String)
extern void DummyClient_getRewardAmount_m43722C33F25A334EA2456E4A2C3B05D9BE2DC288 ();
// 0x00000283 System.String AppodealAds.Unity.Dummy.DummyClient::getRewardCurrency()
extern void DummyClient_getRewardCurrency_m0BDA9CE9A315E56B9E41D54818F5F8E21BE3D273 ();
// 0x00000284 System.Double AppodealAds.Unity.Dummy.DummyClient::getRewardAmount()
extern void DummyClient_getRewardAmount_m93692E73E4F8C40916C1EEEF031F8BBB802C02D7 ();
// 0x00000285 System.Double AppodealAds.Unity.Dummy.DummyClient::getPredictedEcpm(System.Int32)
extern void DummyClient_getPredictedEcpm_m4D7EDD776C2ADADB5101DB801289D122D12DE360 ();
// 0x00000286 System.Void AppodealAds.Unity.Dummy.DummyClient::setExtraData(System.String,System.Boolean)
extern void DummyClient_setExtraData_mE2AD6114AD2633DD5800D50CA574FA8430CDA81A ();
// 0x00000287 System.Void AppodealAds.Unity.Dummy.DummyClient::setExtraData(System.String,System.Int32)
extern void DummyClient_setExtraData_m02BE872D96ACAA76A1E4B952E6DBBF3C1B81F2A7 ();
// 0x00000288 System.Void AppodealAds.Unity.Dummy.DummyClient::setExtraData(System.String,System.Double)
extern void DummyClient_setExtraData_m77F1E95371DEE476A393C5DD2964E21FED8A71DE ();
// 0x00000289 System.Void AppodealAds.Unity.Dummy.DummyClient::setExtraData(System.String,System.String)
extern void DummyClient_setExtraData_mF1C47104D7E31C0B767A5582284683E130DD68DF ();
// 0x0000028A System.Void AppodealAds.Unity.Dummy.DummyClient::setInterstitialCallbacks(AppodealAds.Unity.Common.IInterstitialAdListener)
extern void DummyClient_setInterstitialCallbacks_m1B4963E62CD5A334F9B7383DF05BE485BBAF2551 ();
// 0x0000028B System.Void AppodealAds.Unity.Dummy.DummyClient::setNonSkippableVideoCallbacks(AppodealAds.Unity.Common.INonSkippableVideoAdListener)
extern void DummyClient_setNonSkippableVideoCallbacks_m17AA57899F5F8273CD27A46BDBC4A332FFC8E72F ();
// 0x0000028C System.Void AppodealAds.Unity.Dummy.DummyClient::setRewardedVideoCallbacks(AppodealAds.Unity.Common.IRewardedVideoAdListener)
extern void DummyClient_setRewardedVideoCallbacks_m3F04DC83744EE45E1C705DB0E2D533DEE8DD2359 ();
// 0x0000028D System.Void AppodealAds.Unity.Dummy.DummyClient::setBannerCallbacks(AppodealAds.Unity.Common.IBannerAdListener)
extern void DummyClient_setBannerCallbacks_m3153FDAFD1C066513ED7A27C7589D24C21FCE2D3 ();
// 0x0000028E System.Void AppodealAds.Unity.Dummy.DummyClient::setMrecCallbacks(AppodealAds.Unity.Common.IMrecAdListener)
extern void DummyClient_setMrecCallbacks_m5A01F0A28D891EAF9FEC029278AAD4E9634AA605 ();
// 0x0000028F System.Void AppodealAds.Unity.Dummy.DummyClient::requestAndroidMPermissions(AppodealAds.Unity.Common.IPermissionGrantedListener)
extern void DummyClient_requestAndroidMPermissions_m98B1E99DB97D3F452F1B1FF7E4DD7953DEABD821 ();
// 0x00000290 System.Void AppodealAds.Unity.Dummy.DummyClient::getUserSettings()
extern void DummyClient_getUserSettings_m10CF86B6EB2C0F43875DD46CD0FC843B95CAA1D1 ();
// 0x00000291 System.Void AppodealAds.Unity.Dummy.DummyClient::setUserId(System.String)
extern void DummyClient_setUserId_mEB60754914608A7EF0C4A621D918946395740F4C ();
// 0x00000292 System.Void AppodealAds.Unity.Dummy.DummyClient::setAge(System.Int32)
extern void DummyClient_setAge_m1466EA6951284675CEE1AB546730777F48DCF343 ();
// 0x00000293 System.Void AppodealAds.Unity.Dummy.DummyClient::setGender(AppodealAds.Unity.Api.UserSettings_Gender)
extern void DummyClient_setGender_mA4720911F86560FA55B5521D6EA90F00BE314BDB ();
// 0x00000294 System.Void AppodealAds.Unity.Dummy.DummyClient::requestAndroidMPermissions()
extern void DummyClient_requestAndroidMPermissions_m577A4EFFCAE5B6D04061ED8614DA2993C655CAEE ();
// 0x00000295 System.Void AppodealAds.Unity.Dummy.DummyClient::destroy(System.Int32)
extern void DummyClient_destroy_mC1154E31C7B12462E379D144B64FD28007B8EBBB ();
// 0x00000296 System.Void AppodealAds.Unity.Dummy.DummyClient::.ctor()
extern void DummyClient__ctor_m5958F4B3C4112BEB0E20BBACB0D00FDABD9B832E ();
// 0x00000297 System.Void AppodealAds.Unity.Android.AppodealBannerCallbacks::.ctor(AppodealAds.Unity.Common.IBannerAdListener)
extern void AppodealBannerCallbacks__ctor_m5DB0E31211ECBF8841A927D99C6FCA60C6EDC65A ();
// 0x00000298 System.Void AppodealAds.Unity.Android.AppodealInterstitialCallbacks::.ctor(AppodealAds.Unity.Common.IInterstitialAdListener)
extern void AppodealInterstitialCallbacks__ctor_m08F45C7F6B5662C7AB29C3174ED5B35F0E367A36 ();
// 0x00000299 System.Void AppodealAds.Unity.Android.AppodealMrecCallbacks::.ctor(AppodealAds.Unity.Common.IMrecAdListener)
extern void AppodealMrecCallbacks__ctor_m8878BB0FFAB8539F715AE86CDEB1603B87A61C28 ();
// 0x0000029A System.Void AppodealAds.Unity.Android.AppodealNonSkippableVideoCallbacks::.ctor(AppodealAds.Unity.Common.INonSkippableVideoAdListener)
extern void AppodealNonSkippableVideoCallbacks__ctor_mFCC48EBE61E65C1BEB7C880DB694FDD64EBE1784 ();
// 0x0000029B System.Void AppodealAds.Unity.Android.AppodealPermissionCallbacks::.ctor(AppodealAds.Unity.Common.IPermissionGrantedListener)
extern void AppodealPermissionCallbacks__ctor_m709F71A7A19557F80B1A0AA9276510309898A23E ();
// 0x0000029C System.Void AppodealAds.Unity.Android.AppodealRewardedVideoCallbacks::.ctor(AppodealAds.Unity.Common.IRewardedVideoAdListener)
extern void AppodealRewardedVideoCallbacks__ctor_m673E7BB39AF4B426A4686AF7F1BC41BB69A3FB18 ();
// 0x0000029D System.Void AppodealAds.Unity.Common.IAppodealAdsClient::initialize(System.String,System.Int32)
// 0x0000029E System.Void AppodealAds.Unity.Common.IAppodealAdsClient::initialize(System.String,System.Int32,System.Boolean)
// 0x0000029F System.Boolean AppodealAds.Unity.Common.IAppodealAdsClient::isInitialized(System.Int32)
// 0x000002A0 System.Boolean AppodealAds.Unity.Common.IAppodealAdsClient::show(System.Int32)
// 0x000002A1 System.Boolean AppodealAds.Unity.Common.IAppodealAdsClient::show(System.Int32,System.String)
// 0x000002A2 System.Boolean AppodealAds.Unity.Common.IAppodealAdsClient::isLoaded(System.Int32)
// 0x000002A3 System.Void AppodealAds.Unity.Common.IAppodealAdsClient::cache(System.Int32)
// 0x000002A4 System.Void AppodealAds.Unity.Common.IAppodealAdsClient::hide(System.Int32)
// 0x000002A5 System.Void AppodealAds.Unity.Common.IAppodealAdsClient::setAutoCache(System.Int32,System.Boolean)
// 0x000002A6 System.Boolean AppodealAds.Unity.Common.IAppodealAdsClient::isPrecache(System.Int32)
// 0x000002A7 System.Void AppodealAds.Unity.Common.IAppodealAdsClient::onResume(System.Int32)
// 0x000002A8 System.Boolean AppodealAds.Unity.Common.IAppodealAdsClient::showBannerView(System.Int32,System.Int32,System.String)
// 0x000002A9 System.Boolean AppodealAds.Unity.Common.IAppodealAdsClient::showMrecView(System.Int32,System.Int32,System.String)
// 0x000002AA System.Void AppodealAds.Unity.Common.IAppodealAdsClient::hideBannerView()
// 0x000002AB System.Void AppodealAds.Unity.Common.IAppodealAdsClient::hideMrecView()
// 0x000002AC System.Void AppodealAds.Unity.Common.IAppodealAdsClient::setSmartBanners(System.Boolean)
// 0x000002AD System.Void AppodealAds.Unity.Common.IAppodealAdsClient::setBannerAnimation(System.Boolean)
// 0x000002AE System.Void AppodealAds.Unity.Common.IAppodealAdsClient::setBannerBackground(System.Boolean)
// 0x000002AF System.Void AppodealAds.Unity.Common.IAppodealAdsClient::setTabletBanners(System.Boolean)
// 0x000002B0 System.Void AppodealAds.Unity.Common.IAppodealAdsClient::setTesting(System.Boolean)
// 0x000002B1 System.Void AppodealAds.Unity.Common.IAppodealAdsClient::setLogLevel(AppodealAds.Unity.Api.Appodeal_LogLevel)
// 0x000002B2 System.Void AppodealAds.Unity.Common.IAppodealAdsClient::setChildDirectedTreatment(System.Boolean)
// 0x000002B3 System.Void AppodealAds.Unity.Common.IAppodealAdsClient::updateConsent(System.Boolean)
// 0x000002B4 System.Void AppodealAds.Unity.Common.IAppodealAdsClient::resetFilterMatureContentFlag()
// 0x000002B5 System.Void AppodealAds.Unity.Common.IAppodealAdsClient::disableNetwork(System.String)
// 0x000002B6 System.Void AppodealAds.Unity.Common.IAppodealAdsClient::disableNetwork(System.String,System.Int32)
// 0x000002B7 System.Void AppodealAds.Unity.Common.IAppodealAdsClient::disableLocationPermissionCheck()
// 0x000002B8 System.Void AppodealAds.Unity.Common.IAppodealAdsClient::disableWriteExternalStoragePermissionCheck()
// 0x000002B9 System.Void AppodealAds.Unity.Common.IAppodealAdsClient::muteVideosIfCallsMuted(System.Boolean)
// 0x000002BA System.Void AppodealAds.Unity.Common.IAppodealAdsClient::showTestScreen()
// 0x000002BB System.String AppodealAds.Unity.Common.IAppodealAdsClient::getVersion()
// 0x000002BC System.Boolean AppodealAds.Unity.Common.IAppodealAdsClient::canShow(System.Int32)
// 0x000002BD System.Boolean AppodealAds.Unity.Common.IAppodealAdsClient::canShow(System.Int32,System.String)
// 0x000002BE System.Void AppodealAds.Unity.Common.IAppodealAdsClient::setSegmentFilter(System.String,System.Boolean)
// 0x000002BF System.Void AppodealAds.Unity.Common.IAppodealAdsClient::setSegmentFilter(System.String,System.Int32)
// 0x000002C0 System.Void AppodealAds.Unity.Common.IAppodealAdsClient::setSegmentFilter(System.String,System.Double)
// 0x000002C1 System.Void AppodealAds.Unity.Common.IAppodealAdsClient::setSegmentFilter(System.String,System.String)
// 0x000002C2 System.Void AppodealAds.Unity.Common.IAppodealAdsClient::setExtraData(System.String,System.Boolean)
// 0x000002C3 System.Void AppodealAds.Unity.Common.IAppodealAdsClient::setExtraData(System.String,System.Int32)
// 0x000002C4 System.Void AppodealAds.Unity.Common.IAppodealAdsClient::setExtraData(System.String,System.Double)
// 0x000002C5 System.Void AppodealAds.Unity.Common.IAppodealAdsClient::setExtraData(System.String,System.String)
// 0x000002C6 System.String AppodealAds.Unity.Common.IAppodealAdsClient::getRewardCurrency(System.String)
// 0x000002C7 System.Double AppodealAds.Unity.Common.IAppodealAdsClient::getRewardAmount(System.String)
// 0x000002C8 System.String AppodealAds.Unity.Common.IAppodealAdsClient::getRewardCurrency()
// 0x000002C9 System.Double AppodealAds.Unity.Common.IAppodealAdsClient::getRewardAmount()
// 0x000002CA System.Double AppodealAds.Unity.Common.IAppodealAdsClient::getPredictedEcpm(System.Int32)
// 0x000002CB System.Void AppodealAds.Unity.Common.IAppodealAdsClient::setTriggerOnLoadedOnPrecache(System.Int32,System.Boolean)
// 0x000002CC System.Void AppodealAds.Unity.Common.IAppodealAdsClient::getUserSettings()
// 0x000002CD System.Void AppodealAds.Unity.Common.IAppodealAdsClient::setAge(System.Int32)
// 0x000002CE System.Void AppodealAds.Unity.Common.IAppodealAdsClient::setGender(AppodealAds.Unity.Api.UserSettings_Gender)
// 0x000002CF System.Void AppodealAds.Unity.Common.IAppodealAdsClient::setUserId(System.String)
// 0x000002D0 System.Void AppodealAds.Unity.Common.IAppodealAdsClient::trackInAppPurchase(System.Double,System.String)
// 0x000002D1 System.Void AppodealAds.Unity.Common.IAppodealAdsClient::setInterstitialCallbacks(AppodealAds.Unity.Common.IInterstitialAdListener)
// 0x000002D2 System.Void AppodealAds.Unity.Common.IAppodealAdsClient::setNonSkippableVideoCallbacks(AppodealAds.Unity.Common.INonSkippableVideoAdListener)
// 0x000002D3 System.Void AppodealAds.Unity.Common.IAppodealAdsClient::setRewardedVideoCallbacks(AppodealAds.Unity.Common.IRewardedVideoAdListener)
// 0x000002D4 System.Void AppodealAds.Unity.Common.IAppodealAdsClient::setBannerCallbacks(AppodealAds.Unity.Common.IBannerAdListener)
// 0x000002D5 System.Void AppodealAds.Unity.Common.IAppodealAdsClient::setMrecCallbacks(AppodealAds.Unity.Common.IMrecAdListener)
// 0x000002D6 System.Void AppodealAds.Unity.Common.IAppodealAdsClient::requestAndroidMPermissions(AppodealAds.Unity.Common.IPermissionGrantedListener)
// 0x000002D7 System.Void AppodealAds.Unity.Common.IAppodealAdsClient::destroy(System.Int32)
// 0x000002D8 System.Void AppodealAds.Unity.Common.IBannerAdListener::onBannerLoaded(System.Boolean)
// 0x000002D9 System.Void AppodealAds.Unity.Common.IBannerAdListener::onBannerFailedToLoad()
// 0x000002DA System.Void AppodealAds.Unity.Common.IBannerAdListener::onBannerShown()
// 0x000002DB System.Void AppodealAds.Unity.Common.IBannerAdListener::onBannerClicked()
// 0x000002DC System.Void AppodealAds.Unity.Common.IBannerAdListener::onBannerExpired()
// 0x000002DD System.Void AppodealAds.Unity.Common.IInterstitialAdListener::onInterstitialLoaded(System.Boolean)
// 0x000002DE System.Void AppodealAds.Unity.Common.IInterstitialAdListener::onInterstitialFailedToLoad()
// 0x000002DF System.Void AppodealAds.Unity.Common.IInterstitialAdListener::onInterstitialShown()
// 0x000002E0 System.Void AppodealAds.Unity.Common.IInterstitialAdListener::onInterstitialClosed()
// 0x000002E1 System.Void AppodealAds.Unity.Common.IInterstitialAdListener::onInterstitialClicked()
// 0x000002E2 System.Void AppodealAds.Unity.Common.IInterstitialAdListener::onInterstitialExpired()
// 0x000002E3 System.Void AppodealAds.Unity.Common.IMrecAdListener::onMrecLoaded(System.Boolean)
// 0x000002E4 System.Void AppodealAds.Unity.Common.IMrecAdListener::onMrecFailedToLoad()
// 0x000002E5 System.Void AppodealAds.Unity.Common.IMrecAdListener::onMrecShown()
// 0x000002E6 System.Void AppodealAds.Unity.Common.IMrecAdListener::onMrecClicked()
// 0x000002E7 System.Void AppodealAds.Unity.Common.IMrecAdListener::onMrecExpired()
// 0x000002E8 System.Void AppodealAds.Unity.Common.INonSkippableVideoAdListener::onNonSkippableVideoLoaded(System.Boolean)
// 0x000002E9 System.Void AppodealAds.Unity.Common.INonSkippableVideoAdListener::onNonSkippableVideoFailedToLoad()
// 0x000002EA System.Void AppodealAds.Unity.Common.INonSkippableVideoAdListener::onNonSkippableVideoShown()
// 0x000002EB System.Void AppodealAds.Unity.Common.INonSkippableVideoAdListener::onNonSkippableVideoFinished()
// 0x000002EC System.Void AppodealAds.Unity.Common.INonSkippableVideoAdListener::onNonSkippableVideoClosed(System.Boolean)
// 0x000002ED System.Void AppodealAds.Unity.Common.INonSkippableVideoAdListener::onNonSkippableVideoExpired()
// 0x000002EE System.Void AppodealAds.Unity.Common.IPermissionGrantedListener::writeExternalStorageResponse(System.Int32)
// 0x000002EF System.Void AppodealAds.Unity.Common.IPermissionGrantedListener::accessCoarseLocationResponse(System.Int32)
// 0x000002F0 System.Void AppodealAds.Unity.Common.IRewardedVideoAdListener::onRewardedVideoLoaded(System.Boolean)
// 0x000002F1 System.Void AppodealAds.Unity.Common.IRewardedVideoAdListener::onRewardedVideoFailedToLoad()
// 0x000002F2 System.Void AppodealAds.Unity.Common.IRewardedVideoAdListener::onRewardedVideoShown()
// 0x000002F3 System.Void AppodealAds.Unity.Common.IRewardedVideoAdListener::onRewardedVideoFinished(System.Double,System.String)
// 0x000002F4 System.Void AppodealAds.Unity.Common.IRewardedVideoAdListener::onRewardedVideoClosed(System.Boolean)
// 0x000002F5 System.Void AppodealAds.Unity.Common.IRewardedVideoAdListener::onRewardedVideoExpired()
// 0x000002F6 System.Void AppodealAds.Unity.Common.IRewardedVideoAdListener::onRewardedVideoClicked()
// 0x000002F7 System.Void AppodealAds.Unity.Api.AppodealNetworks::.ctor()
extern void AppodealNetworks__ctor_mF016B355BF6DC14D6461F69573BFC6FC79E5F5DA ();
// 0x000002F8 AppodealAds.Unity.Common.IAppodealAdsClient AppodealAds.Unity.Api.Appodeal::getInstance()
extern void Appodeal_getInstance_m3EDAC1E5E60D296119245C57F7AF5534CF57C83F ();
// 0x000002F9 System.Void AppodealAds.Unity.Api.Appodeal::initialize(System.String,System.Int32)
extern void Appodeal_initialize_m7C4300E7BC284EB8331749D97B4FD25174A1244C ();
// 0x000002FA System.Void AppodealAds.Unity.Api.Appodeal::initialize(System.String,System.Int32,System.Boolean)
extern void Appodeal_initialize_m891EAFF98230D571C1779B75EE6CFECD08122D44 ();
// 0x000002FB System.Boolean AppodealAds.Unity.Api.Appodeal::show(System.Int32)
extern void Appodeal_show_m154AD7C996073738388B655CEDE1E99F891FF694 ();
// 0x000002FC System.Boolean AppodealAds.Unity.Api.Appodeal::show(System.Int32,System.String)
extern void Appodeal_show_m2B130634646478FCB73FED49BED6C1BC34C6BE76 ();
// 0x000002FD System.Boolean AppodealAds.Unity.Api.Appodeal::showBannerView(System.Int32,System.Int32,System.String)
extern void Appodeal_showBannerView_mA34389D2A52B2EBA2649DAE144AFAE386C63E13D ();
// 0x000002FE System.Boolean AppodealAds.Unity.Api.Appodeal::showMrecView(System.Int32,System.Int32,System.String)
extern void Appodeal_showMrecView_m67ACE4D33B82B2AC22DD5E9BFB3429178DBE1FA1 ();
// 0x000002FF System.Boolean AppodealAds.Unity.Api.Appodeal::isLoaded(System.Int32)
extern void Appodeal_isLoaded_m8F9485BCC1B4B7D802865FBEB0255C2316BACD0A ();
// 0x00000300 System.Void AppodealAds.Unity.Api.Appodeal::cache(System.Int32)
extern void Appodeal_cache_m75A7C718B5F2AA5A3B8C8E83FAB6E17C1204B121 ();
// 0x00000301 System.Void AppodealAds.Unity.Api.Appodeal::hide(System.Int32)
extern void Appodeal_hide_mD9E7FEFA7FC4A7BDC9364B283DF880333997DBE8 ();
// 0x00000302 System.Void AppodealAds.Unity.Api.Appodeal::hideBannerView()
extern void Appodeal_hideBannerView_m92F47F6D21220F4AD3FCD678F4E721E63C1058FC ();
// 0x00000303 System.Void AppodealAds.Unity.Api.Appodeal::hideMrecView()
extern void Appodeal_hideMrecView_m93EE8C897506E17BB4FF2A18E01A1C99D19B829C ();
// 0x00000304 System.Void AppodealAds.Unity.Api.Appodeal::setAutoCache(System.Int32,System.Boolean)
extern void Appodeal_setAutoCache_mA47ED58601FB6CB1528D2B69277213DD3080700C ();
// 0x00000305 System.Boolean AppodealAds.Unity.Api.Appodeal::isPrecache(System.Int32)
extern void Appodeal_isPrecache_mE2DB643591648A3E5F84D2A3D43F17C19EA01F33 ();
// 0x00000306 System.Void AppodealAds.Unity.Api.Appodeal::onResume(System.Int32)
extern void Appodeal_onResume_m8C4418962F2923F397B6CAE43E7D37C4340B601B ();
// 0x00000307 System.Void AppodealAds.Unity.Api.Appodeal::setSmartBanners(System.Boolean)
extern void Appodeal_setSmartBanners_m7E71C3492824B24092278BD04B04780F6F86F525 ();
// 0x00000308 System.Void AppodealAds.Unity.Api.Appodeal::setBannerBackground(System.Boolean)
extern void Appodeal_setBannerBackground_m9B7348AAB8B9AF46BC0879131C6129842FD73315 ();
// 0x00000309 System.Void AppodealAds.Unity.Api.Appodeal::setBannerAnimation(System.Boolean)
extern void Appodeal_setBannerAnimation_m82352DD5388D89B0ECF2B3B40D8431F1F6A7ADEE ();
// 0x0000030A System.Void AppodealAds.Unity.Api.Appodeal::setTabletBanners(System.Boolean)
extern void Appodeal_setTabletBanners_mCCD8AD116114A8833FB308E25B6E0E7C4727F7A0 ();
// 0x0000030B System.Void AppodealAds.Unity.Api.Appodeal::setTesting(System.Boolean)
extern void Appodeal_setTesting_m9F150DDB4BE203C91CB8850D4358D5EDF42FC67C ();
// 0x0000030C System.Void AppodealAds.Unity.Api.Appodeal::setLogLevel(AppodealAds.Unity.Api.Appodeal_LogLevel)
extern void Appodeal_setLogLevel_mC80AAB79F61849E3531F6D932210A57454322D95 ();
// 0x0000030D System.Void AppodealAds.Unity.Api.Appodeal::setChildDirectedTreatment(System.Boolean)
extern void Appodeal_setChildDirectedTreatment_m606E8B67EB02F982C00E68E1E7174EDBBF577023 ();
// 0x0000030E System.Void AppodealAds.Unity.Api.Appodeal::updateConsent(System.Boolean)
extern void Appodeal_updateConsent_m52906D2F2C8DFD7410BF33CED4E0A8602EFF198C ();
// 0x0000030F System.Void AppodealAds.Unity.Api.Appodeal::resetFilterMatureContetnFlag()
extern void Appodeal_resetFilterMatureContetnFlag_mB1D29A60F2A58AA599DB3AD18FEAF22BB4552A07 ();
// 0x00000310 System.Void AppodealAds.Unity.Api.Appodeal::disableNetwork(System.String)
extern void Appodeal_disableNetwork_m38A57EA2C0DBDA37B3792C3AC63DF2F047143CAF ();
// 0x00000311 System.Void AppodealAds.Unity.Api.Appodeal::disableNetwork(System.String,System.Int32)
extern void Appodeal_disableNetwork_mA1F4966CE70C381371A4F39E707073A0898EF6CA ();
// 0x00000312 System.Void AppodealAds.Unity.Api.Appodeal::disableLocationPermissionCheck()
extern void Appodeal_disableLocationPermissionCheck_m6A920CF41E1A0F34F82206ABDA29A3184AF7DD51 ();
// 0x00000313 System.Void AppodealAds.Unity.Api.Appodeal::disableWriteExternalStoragePermissionCheck()
extern void Appodeal_disableWriteExternalStoragePermissionCheck_m47099D01C5FF8AF985963A9B05ABE4BFBF0A4F65 ();
// 0x00000314 System.Void AppodealAds.Unity.Api.Appodeal::setTriggerOnLoadedOnPrecache(System.Int32,System.Boolean)
extern void Appodeal_setTriggerOnLoadedOnPrecache_m3701020D074582C957BBCE84DC56C97B50D3D09F ();
// 0x00000315 System.Void AppodealAds.Unity.Api.Appodeal::muteVideosIfCallsMuted(System.Boolean)
extern void Appodeal_muteVideosIfCallsMuted_m0392965E424A2291CB2B010D0BEB63D19ED1DEEF ();
// 0x00000316 System.Void AppodealAds.Unity.Api.Appodeal::showTestScreen()
extern void Appodeal_showTestScreen_m32C96AA60A9FD654564164C86D88CBE1ADDF2F96 ();
// 0x00000317 System.Boolean AppodealAds.Unity.Api.Appodeal::canShow(System.Int32)
extern void Appodeal_canShow_m1181D969409AAB6291182955338BC307E24C5D67 ();
// 0x00000318 System.Boolean AppodealAds.Unity.Api.Appodeal::canShow(System.Int32,System.String)
extern void Appodeal_canShow_m1F5A45E264353D38F120785A4A36504E300C5A65 ();
// 0x00000319 System.Void AppodealAds.Unity.Api.Appodeal::setSegmentFilter(System.String,System.Boolean)
extern void Appodeal_setSegmentFilter_mC90C3C6DE67668B03F0B1846A3E4D3241B82E91C ();
// 0x0000031A System.Void AppodealAds.Unity.Api.Appodeal::setSegmentFilter(System.String,System.Int32)
extern void Appodeal_setSegmentFilter_m229E1BD9325512A47F7B44F7FE942337E98AF418 ();
// 0x0000031B System.Void AppodealAds.Unity.Api.Appodeal::setSegmentFilter(System.String,System.Double)
extern void Appodeal_setSegmentFilter_m49B3259D7EA6911306EACAD67F7D09DF20E32843 ();
// 0x0000031C System.Void AppodealAds.Unity.Api.Appodeal::setSegmentFilter(System.String,System.String)
extern void Appodeal_setSegmentFilter_m77CD82D83FDE4D2BB59D918D98FEB82E49E35B3C ();
// 0x0000031D System.Void AppodealAds.Unity.Api.Appodeal::setExtraData(System.String,System.Boolean)
extern void Appodeal_setExtraData_m071D334EE0239ED94844B262F1E0443F8951D6C3 ();
// 0x0000031E System.Void AppodealAds.Unity.Api.Appodeal::setExtraData(System.String,System.Int32)
extern void Appodeal_setExtraData_mC12062C41D559A08C8986E15EE83B216D34F4031 ();
// 0x0000031F System.Void AppodealAds.Unity.Api.Appodeal::setExtraData(System.String,System.Double)
extern void Appodeal_setExtraData_m7526E13382630E6B2338EBBAF922971DD650730F ();
// 0x00000320 System.Void AppodealAds.Unity.Api.Appodeal::setExtraData(System.String,System.String)
extern void Appodeal_setExtraData_mAD861FEBAE1F76A3F321F2F76A105323771A9447 ();
// 0x00000321 System.Void AppodealAds.Unity.Api.Appodeal::trackInAppPurchase(System.Double,System.String)
extern void Appodeal_trackInAppPurchase_mF8B5AA45B9C4277E21940EB745AB532207494B88 ();
// 0x00000322 System.String AppodealAds.Unity.Api.Appodeal::getNativeSDKVersion()
extern void Appodeal_getNativeSDKVersion_m9E6D595B7B786527981A3A518A0ABB1DCA04C858 ();
// 0x00000323 System.String AppodealAds.Unity.Api.Appodeal::getPluginVersion()
extern void Appodeal_getPluginVersion_mAB459CB8AC9C9D1390BE4F8C53A72952E78B388B ();
// 0x00000324 System.Void AppodealAds.Unity.Api.Appodeal::setInterstitialCallbacks(AppodealAds.Unity.Common.IInterstitialAdListener)
extern void Appodeal_setInterstitialCallbacks_m2611C0B16B1C0CF3677B71F25566EE07631F8618 ();
// 0x00000325 System.Void AppodealAds.Unity.Api.Appodeal::setNonSkippableVideoCallbacks(AppodealAds.Unity.Common.INonSkippableVideoAdListener)
extern void Appodeal_setNonSkippableVideoCallbacks_m8C7E5F3628BF5F04C7A6EA963A6D0D1C103BF4D4 ();
// 0x00000326 System.Void AppodealAds.Unity.Api.Appodeal::setRewardedVideoCallbacks(AppodealAds.Unity.Common.IRewardedVideoAdListener)
extern void Appodeal_setRewardedVideoCallbacks_m1BEB94190C06B3D0BD8D72474EDB84290DA25B5B ();
// 0x00000327 System.Void AppodealAds.Unity.Api.Appodeal::setBannerCallbacks(AppodealAds.Unity.Common.IBannerAdListener)
extern void Appodeal_setBannerCallbacks_m2FE7741E2DA1F01817B580612D1779C1A90A4CAA ();
// 0x00000328 System.Void AppodealAds.Unity.Api.Appodeal::setMrecCallbacks(AppodealAds.Unity.Common.IMrecAdListener)
extern void Appodeal_setMrecCallbacks_m3BCBE8EA0B4F38BFF1F7CE3DB4CB0900D0E1176E ();
// 0x00000329 System.Void AppodealAds.Unity.Api.Appodeal::requestAndroidMPermissions(AppodealAds.Unity.Common.IPermissionGrantedListener)
extern void Appodeal_requestAndroidMPermissions_mEF733BAEF5C7DB36713CEF85F524E6C148719403 ();
// 0x0000032A System.Collections.Generic.KeyValuePair`2<System.String,System.Double> AppodealAds.Unity.Api.Appodeal::getRewardParameters()
extern void Appodeal_getRewardParameters_m4AA5454867BBC0B4877CC2508705E862572AF63C ();
// 0x0000032B System.Collections.Generic.KeyValuePair`2<System.String,System.Double> AppodealAds.Unity.Api.Appodeal::getRewardParameters(System.String)
extern void Appodeal_getRewardParameters_m1E9F17A89297A2F3293F2B6DCC64111A2D7E6ACA ();
// 0x0000032C System.Double AppodealAds.Unity.Api.Appodeal::getPredictedEcpm(System.Int32)
extern void Appodeal_getPredictedEcpm_m4D8A52231772E58C4624FD346DBF1150E69FB34A ();
// 0x0000032D System.Void AppodealAds.Unity.Api.Appodeal::destroy(System.Int32)
extern void Appodeal_destroy_mF836AA886F32FB76A90C816D2AF2607D96A960B7 ();
// 0x0000032E System.String AppodealAds.Unity.Api.Appodeal::getUnityVersion()
extern void Appodeal_getUnityVersion_mF98E5F91E57AF4EEB9909A6BC4DC264628BEB972 ();
// 0x0000032F System.Void AppodealAds.Unity.Api.Appodeal::.ctor()
extern void Appodeal__ctor_m2EE6BAA818E3166228BD5EBCD308088CB5538F0E ();
// 0x00000330 System.Void AppodealAds.Unity.Api.ExtraData::.ctor()
extern void ExtraData__ctor_m65C00658CA4754C04064A6683670AA96B619C0A9 ();
// 0x00000331 System.Void AppodealAds.Unity.Api.ExtraData::.cctor()
extern void ExtraData__cctor_m01F9D0776186BCE46FBC828CA55AE13904E35695 ();
// 0x00000332 AppodealAds.Unity.Common.IAppodealAdsClient AppodealAds.Unity.Api.UserSettings::getInstance()
extern void UserSettings_getInstance_m5FDDD1523E5B225C583A16A887EEEBEBE224E6F6 ();
// 0x00000333 System.Void AppodealAds.Unity.Api.UserSettings::.ctor()
extern void UserSettings__ctor_m28CB46C621C4DF9894CCAA26C0260301D6FF084E ();
// 0x00000334 AppodealAds.Unity.Api.UserSettings AppodealAds.Unity.Api.UserSettings::setUserId(System.String)
extern void UserSettings_setUserId_m6DD2BC91A0FFDAADD3339BC96E1747B87AB80A42 ();
// 0x00000335 AppodealAds.Unity.Api.UserSettings AppodealAds.Unity.Api.UserSettings::setAge(System.Int32)
extern void UserSettings_setAge_m42D3DD7359F90528E28C0815872B6B5F6FA14FA1 ();
// 0x00000336 AppodealAds.Unity.Api.UserSettings AppodealAds.Unity.Api.UserSettings::setGender(AppodealAds.Unity.Api.UserSettings_Gender)
extern void UserSettings_setGender_m8849CCB829A590114A59839815D436BCE058C70B ();
static Il2CppMethodPointer s_methodPointers[822] = 
{
	App_get_Name_mEF80A8B17155890E5EF98BC49FB19F48E3FAA7C2,
	App_get_FacebookAppId_m1F119D0B5B6B1EF782B0A521ACDA475A7D1AA219,
	App_get_VKontakteAppId_mE695AF84FE1EAE2D83735ADAB688204E6AE252AC,
	App_get_OdnoklassnikiAppId_mD2A64B078C65A3EF3AAF04E6716E5D3C1CC44702,
	App_get_OdnoklassnikiSecretId_mFCED46D032B32CD5EB57B5966FD107C163B4C74C,
	App__ctor_m90026EF974BA072CE606432C32AB44F8F18D4ECE,
	ShareController_Start_m2BEBEFA890259FBC6D0F4563A4968E3BCDEC7F48,
	ShareController_OnMouseDown_m2B1A3016F7E5E2AE410AF55025006FB8CEBE0E57,
	ShareController__ctor_mEA9D4C5D64F44814BE0315B915EEC6D828A10286,
	Sharing_shareVia_m54C1F772674A65836049EDD6BE94C933831F0170,
	Sharing_ShareVia_m92F6E1D072763CB924EF8471E459391968E52A16,
	Sharing_OnShareError_m7E1DEE6F27D4D2957CC42278B8BA7CF72E8BB24C,
	Sharing__ctor_m4FFB3F0250726B033EE6CAC273957D86F38B2AB2,
	AppodealDemo_Awake_m417AAAA6866DBF6439AB08599CCF358849B95F4D,
	AppodealDemo_Init_mD5FA0D05CAD251C72C9BD8002F7AEDF17CFD074C,
	AppodealDemo_OnGUI_mFD79DD9C6CC399A61D114581BC20B4AB336D3AE1,
	AppodealDemo_InitStyles_mEB3920FC227BA58637A86ACF8398BD33BFF8CFBA,
	AppodealDemo_MakeTexure_m55286AA85BEA14273DEF30978C80903A39ECDFAA,
	AppodealDemo_showInterstitial_m4E218C752BB97D0BDAFABDF375E01CD7225DF35F,
	AppodealDemo_showRewardedVideo_m490CFADBB16399F5BFD47400908D4E074ECFE060,
	AppodealDemo_showBanner_m18AE375DC86CA1453F50AA3A712D48ED94F7C1C1,
	AppodealDemo_showBannerView_m5F293C923BA7F2972DD6B1BF5F0F555226FA4DEE,
	AppodealDemo_showMrecView_m489A0812A1E26A6C9519457207E83407C9709F1D,
	AppodealDemo_hideBanner_mE7859FC8CF19ED2C119DC75451AC53D0FBE2331D,
	AppodealDemo_hideBannerView_m659C22C7BA8B2E7B0A882F3BF95F18F92A546006,
	AppodealDemo_hideMrecView_mA2082E1BD7DDE84C37ABD18CE3BEF81F86D7C6B3,
	AppodealDemo_updateConsent_mCA4A11D69ADD817B433E7765DCC083D5A6D0C82C,
	AppodealDemo_OnApplicationFocus_m45F0A04B5BCC1FC8CAD171B657663697C0A389F5,
	AppodealDemo_onBannerLoaded_mE6012C1FE4BEACBBA299687D79DC2387DF641F6F,
	AppodealDemo_onBannerFailedToLoad_mE41F90E27DB5A163A7657EF75E64ACCA65E0EAFE,
	AppodealDemo_onBannerShown_m51FCF055FB7B8119E2F6F7CAE18955A68463623C,
	AppodealDemo_onBannerClicked_m889F7299432E671F881B835CA516B5367563F58B,
	AppodealDemo_onBannerExpired_m2D2DF510A27B9FE0E6537499DC0CE3360B0498C4,
	AppodealDemo_onMrecLoaded_m31C19F5FB2A3477C8B6399B133F603849EA8CF03,
	AppodealDemo_onMrecFailedToLoad_m00FD3C8BC870FFB57E9565F978B1927E2182050D,
	AppodealDemo_onMrecShown_mF18AFC67AF9405613E80AAFCA3A0BE948AA350EA,
	AppodealDemo_onMrecClicked_mDEF71272E8E95E5A60CA69CF406FE9581401D71C,
	AppodealDemo_onMrecExpired_m66D6B6E1F3841BA985B88BA3C65327FDFD0AF792,
	AppodealDemo_onInterstitialLoaded_mC816FD18315FA64554A34C09ABC83DEC3D35A972,
	AppodealDemo_onInterstitialFailedToLoad_m9FB8C09C3AE1CEAF36759D7C2517BBF353C1130C,
	AppodealDemo_onInterstitialShown_mE3D4C0273521908C9995FB484257C2267DC98D67,
	AppodealDemo_onInterstitialClosed_mD014736D651DFE9E057008F77F98E36088CECE75,
	AppodealDemo_onInterstitialClicked_mEF69D501008386CBF419B75A2A0B857613BEB892,
	AppodealDemo_onInterstitialExpired_m0FCA36EAF748536BEE6F6FED9A124BECCA4893AB,
	AppodealDemo_onNonSkippableVideoLoaded_m3226622D0AD489BE52DBC298F12258694590C3E2,
	AppodealDemo_onNonSkippableVideoFailedToLoad_m0064A3D742703BCCFFD75257C867BDCCF0CC2094,
	AppodealDemo_onNonSkippableVideoShown_m17A2D884F016C0302321923751F0092C3558486B,
	AppodealDemo_onNonSkippableVideoClosed_mBDE0A1066A23497B7684369C1B35C6B951C1A4B9,
	AppodealDemo_onNonSkippableVideoFinished_mE68832B2D1DDFBFFC61A032EF3704C3169594509,
	AppodealDemo_onNonSkippableVideoExpired_mA343C6DB3E880419BD413225DCF6DB0C2112110C,
	AppodealDemo_onRewardedVideoLoaded_m61BAC208371F613016DDB5A9898FE8ED16223B84,
	AppodealDemo_onRewardedVideoFailedToLoad_mA30258C7746A8659CA854B2365EE4189F8266E4E,
	AppodealDemo_onRewardedVideoShown_mBE08A0185DF724CF703347CE12DF4DC23DA31719,
	AppodealDemo_onRewardedVideoClosed_m6F27E30D89C0127DA2C39E44EEF88121557431E6,
	AppodealDemo_onRewardedVideoFinished_m034405FC6306160311D78BA9B39AB60A8B23881C,
	AppodealDemo_onRewardedVideoExpired_m16ACA2BEF34495F17C89E22B32C19C31FFA9746A,
	AppodealDemo_onRewardedVideoClicked_mB4E8B8613AB53B11DF9D495F1559FE244107A5B7,
	AppodealDemo_writeExternalStorageResponse_m83652E782BE1A3425DD89F6863DAB59C8AE17173,
	AppodealDemo_accessCoarseLocationResponse_m4A90137A4B1166E29A4D6981F37D28F7BD119BCA,
	AppodealDemo__ctor_m8D92B9A7684D9BD78CBF425EECBE43FC811763A5,
	GDPR_Start_m3B1EA6A12ACBD53AF81ED3DE56261B7DDDD4158F,
	GDPR_onYesClick_m120F1208A306961CEAA74D4A4556D68546E93A69,
	GDPR_onNoClick_m913602283382ABFDE14F40FE8BC33E10340C77CC,
	GDPR_onPLClick_m978EB652B45D184376E68890F3D73C0CD7CE0884,
	GDPR_onCloseClick_m12AC1CA6C44773E4BFCBA1801095F0D00CDDFE3C,
	GDPR__ctor_mD25800B726F72F65EC67B133F8545C9900C869D0,
	loading_Start_mC8FF407F4A326E079E33A7241835358120B9E154,
	loading__ctor_m62F43F895C4DEEA1F2F75702EFB4E3BAD1E706AC,
	activate_Start_m7F9CA5B78AA33E9ABC724F058BDE1B917002AAE5,
	activate_FixedUpdate_mDC38B5B2D49A7FB215058B8F0C1669EBD692D19D,
	activate__ctor_m12EB5E629E0BF677CD1AA8ABC105F70E55C364DF,
	actmoney_Start_m4F106FB4D6CDF42A9F6759B363F1055AAD0EBA60,
	actmoney_Update_m66EDF1D311ECF0AC2A745BE05BD30297BB919265,
	actmoney__ctor_m2200E33FDA7DB236EC95AE8AD92CB4013E024621,
	audiocontrol_Start_m22DAFB425C348A74B8C9C0F9035D1BD04C804D7B,
	audiocontrol_Update_mA39E8268C70899D2F233BC682206D917C36E9AD2,
	audiocontrol__ctor_mF4C937F5BCA330EF353F341222B5B075C75F6BCD,
	bannerload_Start_m979E4F01EFE9526DAF5DD2C047BAFEC06D30A474,
	bannerload_FixedUpdate_mB33C60BB074F02552E587766F545E74AAAE79D4E,
	bannerload__ctor_mFB3C9E1783262119191B39633D9E3A69CE712DF4,
	calltip_Start_mE34A140D2F6E66E26988FA0FF6B98BDBC8AC5DF1,
	calltip_ShowRewardedAd_m6A6C9848ACAFB5E3EAFD7F5771AED2BEEC4F4E36,
	calltip_FixedUpdate_mB8CAF1A5848C89F7829705E1E80F7015D4E22958,
	calltip_OnMouseDown_mF230F3AFF8C01A21FD3BF760FF8C7A0EE952B1AB,
	calltip__ctor_mDC4A925E88A3B1E40C4C7D38644670D4B362472A,
	cameracontrol_Start_m1BDF3A9E5DB3B685ECE1688DC5C17527512F6BE9,
	cameracontrol_Update_m17AB36CD0708F7B567A7A233865624C8036EF6D9,
	cameracontrol_changecam_m47128F3FA13C4BBBE1E2020112A0C834641291D3,
	cameracontrol__ctor_mF646030CE9536B36E3A1483E806E5C83EB1BA510,
	cameracontrol__cctor_mE929C3103CAA277588F890AAB9BB2F705057E37E,
	changetip_Start_m2FB64463989D04D567A2A04EA53668BA65CA5A15,
	changetip_Update_m147AA5E4727B96A29AE91B399076623222F24E5B,
	changetip__ctor_m3C54CC01CBCC6483790625D00B4D07E561A59A85,
	choosefon_Start_m91CBC54587C3D47ACABA1FE2E6F13A2C2077C774,
	choosefon_Update_mCAEDB33D08176C9455A24FEB5F64689691FB881D,
	choosefon_OnMouseDown_m50C1B8C4F2A7DE92DBDA97DB07BE2D1344DF339D,
	choosefon__ctor_m836E27C6308B9371327E69293E8A9B6CADB7E063,
	chooselanguage_Start_mCC58FCBCFB8E57830ACA688736F1914568185E7F,
	chooselanguage_Update_mA37C858E3A3CA03451EF404E3156BBB687C71A88,
	chooselanguage_OnMouseDown_mADDD072155DCD5CD3EB23AF651AB2A792C857D2B,
	chooselanguage__ctor_m8EFF114ED91CA2A612CAD1D8503B48E1D30E0FF1,
	chooseplash_Start_m04960B57EE9F465E00D47D7A3A35947EB932DEA2,
	chooseplash_Update_mB09E29D67B74DDF969AB14EA4963EDFFD035DC8C,
	chooseplash_OnMouseDown_m30DB3B638BBEB365ED3C2D9D065D2090BC989F45,
	chooseplash__ctor_m223131C0EE8A4FAA6BF225944DE3C6C2F2ECBE8A,
	classic_Start_mC96F2B8ACC85912029853250DB196C750273BE15,
	classic_FixedUpdate_mB7F33E4A6F7B40E4D0BBAD6FA1FA9DA156284DC5,
	classic_OnMouseDown_m3981F3290B5D338C395B247DE24B27C737AEFF05,
	classic__ctor_m543D547350C8176B79E328CB5F44F540560C628A,
	color_Start_m5385DCBECBCCA316FAFAC3392D254DD7B6F674B7,
	color_Update_m9D322CB54A8B90946302F1871C09669A736FFDA4,
	color__ctor_m734650722AB12ED540321E2053DD74D4CFC80AB2,
	color2_Start_mC48E9471CA18F8D9C05532B3F1942122280AFC48,
	color2_Update_m7E07956791C12072AF3256B815DF7D1D3FF54569,
	color2__ctor_m1F1652DA56A731F3D97D68A6A03DF83F70F293AC,
	depthc_Start_m467C5994D55C964473DA00F4DE64E593A39D3FD9,
	depthc_Update_m48664E5C28611B77B86509B140EE2BA4861DF119,
	depthc__ctor_mD61BD9957E3A4D4C8FD6FB4170F888D7967C2E4A,
	disableonnoads_Start_m861873E99B4C5A2F273F6AA0A5E06F0FEA6969F9,
	disableonnoads_Update_mE9B39D1193927834C43F0B07808C43627837A13F,
	disableonnoads__ctor_m442EFCE3190DF7405CEABA87A4275715634389D3,
	generator_ChooseSobsna_mB8102DD89BC869AFA5DA1C17FFAD8185F176196B,
	generator_Update_mA9128DAA56B7E545ED0CAA11833F9970E6843859,
	generator_ChooseQuest_m28DBB45441C20922F3E8B8105305E2EFA7E77031,
	generator_Vibor_m435B37822FCFA1120AEC26D6239B2878C0881A8D,
	generator_Analyze_m3E39C9195D25ED9B1F66818EB6925DF3D885E8F5,
	generator_ChooseVars_m831D26439A7AB99B01B45CB66893EC922FD40EC1,
	generator_ShakeAndNazn_mBD3AF5C518A77A823AAC427CF08BEA4C790C749D,
	generator__ctor_m1CAA06B6F2954475CE2424BEAFDB589B793ADB0F,
	goback_Start_mAC2CF61AB5B86008235C91D2865E892B3BC76323,
	goback_Update_mB7A903A6C1F6B5F4928CD189ED014472FE2E3A1E,
	goback_OnMouseDown_mD75FAA6DCA98B3A33DE98A7CF80B8E14542A9AF5,
	goback__ctor_m2A0F7EC8CFB0383D6CA2DC39B0DF2125232979A4,
	gotomenu_Start_m5E7A2D0999C9D2F8F941E76B4F49A5B99A7223C7,
	gotomenu_Update_m1B837374FAD0853BEB3D3846C404CB5C4D395F9F,
	gotomenu_OnMouseDown_m287C0CB8103FCD63667962A317CBC882305B787B,
	gotomenu__ctor_m9CD6E2EC42C792903CE71CA06ABA469CB3B5B2BA,
	gotomenuff_Start_m0ACB8F1A0426EEC32710FA7B7B4A4CF8CF3C74AF,
	gotomenuff_Update_mBA298FE3C492F049580F926141D1FBDF313C131D,
	gotomenuff__ctor_mB1D6BA6B4DA7CA4553EB2ABEAB33BA1EFBB5773D,
	huy_Start_m4AF6C7EB6BDED5831D099EA3A6452A5046424F6B,
	huy_Update_mFDBBE8DB4F1C9644831821DDF81A004E05457F19,
	huy__ctor_m4706A961CCEECF08DA47F968348D9636618E5134,
	lang500_Start_mFA6310CD041729E1793C99945CBF832FC5D1D9C7,
	lang500_Update_m35ED918C79CE957596BFA0B0F34A4CFF9D62A234,
	lang500__ctor_m661E38482BF44319EA05A28DDF611593DF4356EB,
	langgr_Start_m2254141D0DBFEE76C68D8DDD5B94FEC6B50C960B,
	langgr_Update_mE8479750DA4AA9DD23C0F9F97BE119752D5C6FC1,
	langgr__ctor_m1EBD4960F01E3FDB5C77BFA193079933E3114606,
	langnaebka_Start_m4AA9EC23F0E2140406D60A6C6A1CE46D5566EDB2,
	langnaebka_Update_m6778C26093F6CDA9E9A947707599651F3ED09EAA,
	langnaebka__ctor_m5200F387FE1409D706E3D41ACC17D670BC622667,
	langshit_Start_m7368CD9844F957306ECB2688EF3631FE95C2BB21,
	langshit_Update_m0B5232E5EC2A29C6C5594AF40AFD83AD9BB685B1,
	langshit__ctor_mD164304A633460B1E24615224FCE24362790B661,
	langtime_Start_m8C106A228CBFC00B8C09D2564E58866B8F4D7036,
	langtime_Update_m01EBBE83E800A942542392015E714FF1AD8DFA35,
	langtime__ctor_m88F069579EC0C1820B16453EA8AD2C9B33421D2F,
	language_Start_m77A32C7BD092B0DD63810385058A1C3314701BFA,
	language_Update_mDBA67BC92D4ADEA0F2B1FAED1FDE35E59B08F9C3,
	language__ctor_m7684732E14C2A55250E3932CC3DA2A7A92F48C0C,
	language10_Start_m1097A5B2E1264EB61D783D7CA71CBC68BBA21D3C,
	language10_Update_m30B5A7F95DC16DCC3AACD70B7A90E102A9EC6A95,
	language10__ctor_m68E39DE545748088730C73CA2B7BEE95EB4D9EEC,
	language2_Start_m4419272953205B08100D61BC8EC5BE5649452B2F,
	language2_Update_mF41DBF0BC8801092A9EC7267645610426AB9D657,
	language2__ctor_m62EB09FB0C7F8AFF9ADEEAB7D54F98348617A141,
	language3_Start_mAF738655514F62DB5A0FDC1A5B3DD29566838024,
	language3_Update_m5E6B465ED8D5C1559F7F8B3648D7E8E46EC36F7E,
	language3__ctor_mAAC80FABB54C74E532748DDD2B05C5CBB0B1768C,
	language4_Start_m310141F7C66193C9C4A7F9AA306383321986612A,
	language4_Update_mD56C1A435DB281434A29CB085FBCFF86517E0073,
	language4__ctor_mE1725E9CBDEFCE6D52F96B3C042BD707E009B062,
	language5_Start_m226B24E82911AA19739345DB262AA0479FA2EBDB,
	language5_Update_mF69C4F6F4476D0AD89FBD4495BAD7D67380C3E09,
	language5__ctor_mD0E47C13040DB7A3BEF2A34E70EADB44D28FE68B,
	language6_Start_m6A061B288B5108994E851C37C753EE3AA99EB976,
	language6_Update_m21DADBF6701C1D39AC211E9884664A45AB3B1006,
	language6__ctor_mE9904C6458751C137FBF7A47C80B5C192A886534,
	language7_Start_m2009F31CA0CEAE5FC78D227EA516F78007BA4FDD,
	language7_Update_m658132B0537C63F29B59DDCE39BD644D5F9B37F9,
	language7__ctor_mE70887F5FF1064032A925129A8F4297A21AFE3A1,
	language8_Start_m8DE518C1C5651D266FFE3D557426BD7FB4C65741,
	language8_Update_m4DC6F42A9B207FEE775344978D1B378A2A2594F1,
	language8__ctor_m056B43630D78B78748561EB77E284F88B7367713,
	language9_Start_mA385C4DBBA5BF9B2EF393919E0C0913BB2365034,
	language9_Update_m899318B0D97183AD8840637EB834FEA83EE3FE18,
	language9__ctor_mC81B20CEA5EA203E4BF7F9200DD6EF20AAE5E3C3,
	languageads_Start_m85210AFA36A43E90D7A7ED76859410364FA108A6,
	languageads_Update_m2DED4219CE9E939247D11F1E167E6DE432FFEC9A,
	languageads__ctor_m3B6BE542BEC046B2668A30BDC16290FBE72BB284,
	languagefickk_Start_m73CFE07CDF0DB77D0641141C5BE5F49922BAA586,
	languagefickk_Update_m6DC0837791103CC6689A3BAA80E03ECAAF8F63DF,
	languagefickk__ctor_mD4D0D923D859C4B1E1268FDBD7F50BEDB8B7F09C,
	languageplay_Start_m85F7DDEDE512EDEF700E77D026BCAA4DDC817881,
	languageplay_Update_m98566B5C3E5EE0651E191EA436897B0825603545,
	languageplay__ctor_mF5F3504969770114F1B6642AE7FB0DD45B1E9C3E,
	languagerestore_Start_mCC90AEB41EE64E19F7A15266300EAE73D06D2BFB,
	languagerestore_Update_m39B2B83A6DDDDDA5A325A9FBCF82DABF81666043,
	languagerestore__ctor_m9A181BB594B48BFDFDEC336A9BB4A60936EA5252,
	load_Start_m394C448EFBF249FAAC248BDE8E213C41A0C4F496,
	load_Update_m316770170C3AB3AECFCBFC096511699DD918A341,
	load__ctor_m86B8E8605695FD9C6DBAE72EE0D9A7E1C2E43263,
	loadagain_OnMouseDown_m7485ABCE2B4A02451F3EE03EF7E47529DAE1AAF5,
	loadagain__ctor_m6B37556158A2BE9F549A45B982F233ED14AB2B2B,
	loadloader_Start_m1667B6828A68FD97AA1AC483118467688FCC7D4C,
	loadloader__ctor_mE5583059E0337797B5200A1939582CBE189BA0D3,
	magascontrol_Start_m36B9A518A85E32D3881C9E6300390F7C49834710,
	magascontrol_Update_mEA4343843DB57172A15C7B27F41E676BC0129B30,
	magascontrol__ctor_m7D38AE41DD32CD3A9D127EC5FDCB48C7936C9246,
	magazlang_Start_mD479AD8662B1AD0F9AD88D44EBBD0FB8356EB090,
	magazlang_Update_m52E4616B35D409E35F2766565EF2CA89781F5335,
	magazlang__ctor_m708D77DD6959B3633C04692FCE86969C44D44301,
	medset_Start_m436C57FC98B0BD163AF43F69D85482BB0C54A8DE,
	medset_Update_m2135A9E906291FB64541918A320EF43650AD19B9,
	medset__ctor_m05F34ECAC9DF0397F8E09DBC558D89D15369FC50,
	movescreen_Start_m11C6AA356F58DF5AF1F5C66F9B9986BAEDFF5CB1,
	movescreen_FixedUpdate_m8ABC090EDCEE2F86EA22B430756F932944D669F7,
	movescreen_OnMouseDown_m1D2A8EDCFA14CF465D2EDF952FA9BD8600F1527C,
	movescreen__ctor_mE08AAB82B10693FA349F8FD3E1FBCAF3A75C4DB3,
	needmoney_Start_mE9D84CC34F1B9B6A8E1EA89DFA0278B386DE9F89,
	needmoney_Update_m876806523F1432E9EB42D1F4F54E8C1084C1EED9,
	needmoney__ctor_mA4E0BB2EDBCB88C797CA5C7FBE219A6A12F175DA,
	nesgor_Start_m7831DBD8F79EA92C5144DD72E2D68903F5C133E5,
	nesgor_Update_mEE056E3BB073FCFAB1B0F154E92D8527C73F53D7,
	nesgor_OnMouseDown_m23C2B069FCB5A42C557BA781DEFFDDDFFADA0B55,
	nesgor__ctor_m7D8B8D6E788572BA422C4C969A53B967C7897CBA,
	no_Start_mCB0FB33B75E1645862A2B6F0D0FD79FC65FBD53D,
	no_Update_mA44542DF4E74EBC091B083D92BFD6C92ED36E973,
	no_OnMouseDown_mD80B6FD70801736CA3E1FA6DA29363273A52D620,
	no__ctor_m0D594C80057C9E0FCDF091A70B0AA90E17563C90,
	ocenka_Start_mA270ED84EAC3680E29A36021F181388A6D4F8EFD,
	ocenka_Update_m4C34515A949B3423E8465D0CB0FDF0A35985233F,
	ocenka_OnMouseDown_m4132494BEABE90D86A7CBAB792297EEE09C619A6,
	ocenka__ctor_mABD84ECD5F809FF6C866496925139E1FFA4B1EE5,
	ofblyad_Start_m0229EADF54C9FC95F09109E765400E5E87EE1CEC,
	ofblyad_Update_mE3587369E74FA3DCFC2F340495A697A38818E976,
	ofblyad__ctor_m8AF7C3CC3CB08303338CE0DFE49C80E13ACAA51E,
	offsprite_Start_m3439520AB0FA106E979D91310199D2920CD2C2D4,
	offsprite_FixedUpdate_mF8314D49638995669D45C64B2A49EC715B6DED4D,
	offsprite__ctor_m96DB40F1695656238AB26AC13FA3AD055F64B7B2,
	oidjiaso_Start_m821E3CFD39121E5B1280133151121978789AA0C9,
	oidjiaso_Update_mCFD4AC247D3BA9E0696FFF2399C21317D1868E12,
	oidjiaso_OnMouseDown_m4321DB517FFC1766F96B7BB897BBA0592B9CEEF9,
	oidjiaso__ctor_m00AD5C75C4B5157CF6701A5034EA8D6EB49FB8F4,
	onclick_Start_mA204DA5368CA9B753225395EB35B673CDC611216,
	onclick_Update_m5A0990E149F6C9317188F16DA7ED72EA65B9E734,
	onclick_FixedUpdate_m3973D67B7B07877AC0010197EEF1831F0E33A694,
	onclick_Lost_m85FAC2B835528D8A8307ECDAC029751527BA164B,
	onclick_Won_m2D8E85429A84FF381C3E935831B37917386766F2,
	onclick_OnMouseUp_m22E83B8CF94D7755DE1C2960E779F438BCA3F745,
	onclick__ctor_m1706DD28064458ADB78A5F3EB31644C0B0E230B8,
	onefiveplay_Start_m5693968EECF0AF81C55A72A6DBA30945A2B89D32,
	onefiveplay_FixedUpdate_mAE74315B2E7E1F76DAEEC7EF14D44CD478936F71,
	onefiveplay__ctor_m0F7DD06973AD7AD447516DBA7D6B628315FDDF7F,
	onlights_Start_m5F03C0241376222E3A6BBD3A95025B6C6D339D54,
	onlights_Update_m7BE812100BB23EB67CF7BD83E51FB9D011BF3A58,
	onlights_OnMouseDown_m62ED15F59F8B2DBB4D415435711C7473F6B94D67,
	onlights__ctor_m88CEC9CD536A76C6E37B3CEC0A515E1405216D82,
	ontime_Start_m9BC5400722F07E887B1714E4F087E3C28D97268A,
	ontime_FixedUpdate_mA8BC15E460A7D5ABC1CE75B4EEBF916E3E65912E,
	ontime_OnMouseDown_mF704BA1DD8C5632E015641AFDA2B87D04F3A2FE4,
	ontime__ctor_m755FB7637EEE62C2379A1A367374592DCD47F39D,
	openfon_Start_m708719617AD64711B0651B5FFD53F328031A36D4,
	openfon_Update_m9CBD432340C0863AA944A883626F4C45D0D49749,
	openfon_OnMouseDown_mCEE6B506DD808197C5E5EB66A74A7BE02395A13B,
	openfon__ctor_mF2F14DB6F03FEA751F3B11D389C500E67365BE84,
	openmagazik_Start_m80D623C7FF55FE7D445692EF4861080BB02D4B32,
	openmagazik_Update_m5799583887EFDFC22BAE772456CBFA1FD2074C03,
	openmagazik_OnMouseDown_m99412FEFCB253DDD84C0E6B475930EE46603F359,
	openmagazik__ctor_m46D64C91D21046A1A934D7C357577DDEAF8B80DA,
	openmarket_Start_m1ACD24EB13172A943DEC471FB9BF9EAAB1CCDF9A,
	openmarket_Update_m8FBC79FCDA759B012C6D677EF7CCE49EF10B2F51,
	openmarket_OnMouseDown_m16664B46E5C16DE10F3AE4CD25EBECD9B5FF35C1,
	openmarket__ctor_m161160724F82DF7F7D31C87DE5275E9DBCAEA38C,
	openoptions_Start_m79250E598C1C4ABC35C4CC065CEF5C247630637B,
	openoptions_Update_m17A2BC2894D0DD67B825EC9F46C8ADCABB55C20E,
	openoptions_OnMouseDown_m7D2733CC495DDE7A6D53A04DA5C79FA139FA7E69,
	openoptions__ctor_m7D973505CD86718204096E18405013EB40A6A6D9,
	openplash_Start_mF4FE21DBF720D4118216C6EFF98B09CEF2B321C1,
	openplash_Update_m740B24AB5DE7D3FA47B6A3F7D3AB29965A956644,
	openplash_OnMouseDown_m426A2A61D677A99BA66406FFF2A12DF83D673E49,
	openplash__ctor_m1F19A8AD462812D44E56C629FA521D070E53C95C,
	openshop_Start_m2B825551726BB999B006E5ABD87331B3B7E362B4,
	openshop_Update_mDE0C9F8E46D81F8D2AE239E04308947BB60F1BBA,
	openshop_OnMouseDown_mB83BE2D3B1FFE981C63BE8FFB379DF933474E8F8,
	openshop__ctor_m3D551825A16255DE1EDACE57919D4FD02050D3E0,
	planshet_Start_m0D9DDF5D7EE93FBB303334475DE18B15DB7889EA,
	planshet_FixedUpdate_m020CB85D8DA07E61AAF96DDC2F0BA03827694B95,
	planshet__ctor_mA7717F68E2AD6A0AD5B198BE896543F0306A6821,
	playagain_Start_m974AB4EA0F85A16162D3856FECFD8256E120DC8A,
	playagain_Update_mF5B6335BD76CBBD9573261B89458C2E6D05DCBA7,
	playagain__ctor_m1CF61F40B3CA776F230C0D59E0BCF515DE2D2998,
	plus15sec_Start_mA341029AA81B59DC07AE4B9F79E1B5D5C90229F2,
	plus15sec_Update_m58CF81738930A9B833B7CB4452843E1568CFC67D,
	plus15sec_OnMouseDown_m6C11D8429EB069EB33E64D780CB103D1503B9B38,
	plus15sec__ctor_m110BF8288C4CC460C0FAB7D297A09D271277B2B2,
	prefs_Start_m48F6F2727A9E75D0D146BE0E41ECC965A00D5DC0,
	prefs_Update_m4B853B3A3877A01F720FCB161E3F21DCBEE242C3,
	prefs__ctor_m9E5C8A1913ABDEF5BE638AAA1CEF965952B1944B,
	preload_Start_mAD019C4D2DC3F04DF4CA20BE3C81371732953943,
	preload_Update_m28C2957E58C84F36C17FB4FFCA228E71B6900EBA,
	preload__ctor_mC8E10F6DB1645C8C6D53B71C243E15B57EF224BB,
	ptzovik_Start_m055E79EA03C5D9FB2139107817847AFE9951B921,
	ptzovik_Update_m3492D317EA078533314A1A9911778E74B23D37F3,
	ptzovik__ctor_m332BE8E7C5A359F8A9D61B488EEEAAC2BB98905A,
	randomanimka_Start_m2F8765C45E299A7AA28DD9A45F2FC9EC8D31C4F7,
	randomanimka_Update_m4BABFED8C3C9F260B24597DDB65EDC28F2F5A8F7,
	randomanimka__ctor_m4EBCC42A198425DF5A2D4DD39E7F90B3C1D18281,
	realplus_Start_mF7A6AC72139507735B869EE07604578B000F79EE,
	realplus_Update_m3025EC4CD323C79704FA44096CBC5D105DC691C7,
	realplus_OnMouseDown_mF78EE22FD5C4D68C2949482389A2BF467AB55AD2,
	realplus__ctor_m144CB3BA2B1EDC2196DF65EBDC466F84891296EB,
	repliccontrol_Start_m9ACD6143B23E2DF07729A7ADCEFA10CBE8030258,
	repliccontrol_Update_m5A85FDDF9E42E96F7E713B065EF618339A1AF86E,
	repliccontrol_ChooseRep_mEEBAD06227C63E617506CC88825A013D6CFA49C4,
	repliccontrol__ctor_mF1A6DEFDB890F7536629DE6215661B6B279AF261,
	repliccontrol__cctor_mF93678C23D9921F87A1AB7BB282ED211E19B365E,
	restart_Start_m81662DA5A311EDDED1E8A3814C7C6D7EA92F6CF4,
	restart_Update_m15B21B18B2E4B46DB623A701C095F9C73FBC54F4,
	restart_OnMouseDown_m7C2FC9FD4052813C3B33839EAC332070E5BE5750,
	restart__ctor_m23F5A4FE51F55C67834AF58E04B9390479D039D8,
	score_Start_m0A60A4EC7C7734144184FB54878249B7FB44670B,
	score_Update_m1DEB4166FC8653F89D7D11A32F9184DA23AC1950,
	score__ctor_m99F8700D5ADB83DE3556E55E2F8475362FA947C2,
	score__cctor_mC4C70FC051769273460B8C81360339113D13846B,
	sdadas_Start_mC90B40A7AC6032756918A0501B6CA8E96E0FC41D,
	sdadas_Update_m281D76DD26815225F82966E21E433A356FE2CDB3,
	sdadas__ctor_m87CB39A7937F203CD237377025499B0F0AAA7274,
	secretnaebka_Start_mD7D0CC774A014AD3DE735385971695CC34AFBA34,
	secretnaebka_Update_m3050CE6F420BCECC0E07643402B009F66E66B653,
	secretnaebka__ctor_m5C7E768575CDAF86F51E891794F93C71FB9DB8CD,
	sovr_Start_mD308E2B2CA52C780000633908ED72DDD3868C40C,
	sovr_FixedUpdate_mB05ED50D1A40FD4D7736CAE3E5027C1D7A222F30,
	sovr_OnMouseDown_mA2EA2A3235478518DEF818DA85C7F69D57378CB0,
	sovr__ctor_m2EEBF8A422194C7135865BE6ECD8500183B0F68A,
	start_Start_m958AA2D538728C57EB48F3C74396B1177D9281E4,
	start_Awake_mEB7BA6FDEE2163E68950942636C87CCAB007D8C0,
	start_Update_m5B2B737B77629DFAB80EF26F3ACDA467E4618884,
	start_OnMouseDown_m548FA9A8C26806E2D9E6F49A6F8A563F540CAA3A,
	start__ctor_m5A8265BE093DB12BD82B0575AB80C2E9AB3DD4AC,
	textvrash_Start_m9C26ECD1AFA3FBED89E7A8320584C52F4C4EE741,
	textvrash_FixedUpdate_m757669898A9818DA46B12223EB0117F2A88A508F,
	textvrash__ctor_m1AAD8E1D06143517BEE691F74F0D7F65C8D1404F,
	vivod_Start_m2B4F1F08ED95890BB9BCCD8E3605BD83E3FCD777,
	vivod_Awake_m0AFECC536528FEE0E8BAAFA35884201DC9A25165,
	vivod_Update_mDF0842DB76C987E3798C4C009FF8041F02732BD2,
	vivod__ctor_m4DEE9058B360BDBD1CFC39CC58EBD439911F11E5,
	vivod__cctor_m004A659CD924E7086D206A8F47FFDECCDE55AF1C,
	vrash_Start_mC5EB8C7343105D4B1C4888F2D32F8BFE09C93E78,
	vrash_FixedUpdate_m36E5542C127F196A3590B44DAA1E9A4B105B4EAB,
	vrash__ctor_mAD1DCBF5812CC0926BEF1DC97495A811D7F05126,
	vrash2_Start_mB5D8563B40D329550A67408E88F45EE8569AEE99,
	vrash2_FixedUpdate_mA3EE1D437535E86442C096B4BE39C757C74A261A,
	vrash2__ctor_m4F87DA475B4893AFEC7D023448B2F9E6687D63AE,
	vrashkrug_Start_m131F263B0BB6CE453081E504AE4C17810A5F32AC,
	vrashkrug_FixedUpdate_mE399B99AC54B860D55DA26928A7BA67FFFCB926A,
	vrashkrug__ctor_m7224864BB2D8543C23B1F04A8676EF5B8D95F0F7,
	x2tip_Start_mAB78F0E29058BDF05E96A1F0408977B3CBB6F992,
	x2tip_FixedUpdate_mFFE5693DFAD7BF8EE35C6A8FCBBAB80B46CD29AD,
	x2tip_OnMouseDown_mDB3FBD8B1FC9A31ADBDDBA5FE4B45456BB3F980B,
	x2tip__ctor_m083D3F2F77DEB7C0D43389ABADB32BA51792E8E3,
	yes_Start_m2FFF5C7A490BA9768E43B268210104F0E371EC25,
	yes_Update_m9D7619DFEABD9B64A6C1C35C4F187410F07E40CF,
	yes_OnMouseDown_mBD3EAE21B5A9137CB7BE5D3A84A7425E9D927627,
	yes__ctor_m38D6295FE568A2CC7B20B3F1E1E1D51743C41D17,
	zabratmoney_Start_m8AE46FFBCF2E2E2E5235BFA9152568DEF9446832,
	zabratmoney_Update_mD7F53A577260CC0D172FA63D659541376463A1E4,
	zabratmoney_OnMouseDown_m1220E6EE6FB81C86A773225E7C608BBF78D30EA6,
	zabratmoney__ctor_m4B52AA36C4EBB005FE0B6B4C739E84D83FA2B44D,
	zaltip_Start_mF003BE496B7C2E221441F6312AADB74C1AAE755D,
	zaltip_ShowRewardedAd_mB5E223BE079E093762E376AEED55F8D5D71F30D8,
	zaltip_FixedUpdate_m83E52C147D3345C561AACF32B0A9EFA8F6E789BE,
	zaltip_OnMouseDown_mBCD4C20E5E7F3FBC38CF0CB3D676AC4B8C837356,
	zaltip_Naznach_m32DAA727AB6914925B335901C65BF7325FD7B0DC,
	zaltip__ctor_m03972C1473ABCD6C0F445A82B6ACD2D0F3262DFD,
	fiftytip_Start_m44268189463FD878BEE6802EF0BA3B03392D2C7B,
	fiftytip_Update_mB3F6E918B335F5A034FD83A9A039D0F23964C18C,
	fiftytip_OnMouseDown_m3577EC94B2E0EE06EB32979AC1485D1AC89BA8FF,
	fiftytip__ctor_m0424C33CC01EA8DBC6E11F1CC6D2C83B01825081,
	changetolow_Start_m0F298CCBCEE4C81F84660E61E7A5BBEA502FB266,
	changetolow_Update_m28E5160D92F2BFF387D3BA2D253B18404772286F,
	changetolow_OnMouseDown_mF7A53F0396C16D313438D34064615ABBE142FD46,
	changetolow__ctor_m979D682BCF513D893A89BB4B74A7CF40E49C0AFA,
	highfuck_Start_mC1BF6C09FFDDBE508DC43CD139C96A15D47682F5,
	highfuck_Update_mDF17F44546EAACA5790559C5C16EE3F50B795429,
	highfuck_OnMouseDown_mB74592E8B9E644DBC788A2CADB74553FE5A367AC,
	highfuck__ctor_mBCE906C788BD4315FDA5E0ED27021C4C20EA0BE1,
	medset_Update_m1861439C83385CF28075370DC942FB7F0ACB7C37,
	medset_OnMouseDown_m9EB8337C66541ACB948E6461963A53CFC842BAE5,
	medset__ctor_mB49154CB2C238B149062513D55BC0ECA9A2E1146,
	medhuy_Start_m362BDAACAAF9008A1DF97FBF16EAD7D557ED2218,
	medhuy_Update_m3F31DBF9BF22CF9778011DD6F98F7AA5E171C92E,
	medhuy_OnMouseDown_mC17EE4586A5AEFE9CAD6F26DEE3EE92D201ED761,
	medhuy__ctor_m3114310476C1A2BB87A6946428191906D02B8583,
	AppodealAdsClientFactory_GetAppodealAdsClient_mECEE5DDF222D3D255DBC8E9C1A0D87B8EF3F50F5,
	AppodealAdsClientFactory__ctor_m4C25D531F9EB7CED25D791EBF191E9CFAA2FEE09,
	AppodealAdsClient__ctor_mD6097E8E16558304119D394C6DB47134AE4A0B6D,
	AppodealAdsClient_get_Instance_mAFCF04478EF6813AD32A5163588C5848A568915C,
	AppodealAdsClient_requestAndroidMPermissions_m972159493CBB0F2C4FC897006E2F06ED646E9848,
	AppodealAdsClient_interstitialDidLoad_mF0BB510147ACC2ED6196D58C04BADCF7DD672804,
	AppodealAdsClient_interstitialDidFailToLoad_m02A0C2398B471B328B01CD9461F156F50CA86BE2,
	AppodealAdsClient_interstitialDidClick_m48EC441678FDACF1CE950F29F17D710C011F3372,
	AppodealAdsClient_interstitialDidDismiss_mE3C05776C18C88C21836E4FC194C7E978A559FB2,
	AppodealAdsClient_interstitialWillPresent_mA144DC35E835CDDBC8272155DDE56B4D622DC9A8,
	AppodealAdsClient_interstitialDidExpired_m70EBB4905AEE8AD1A710FF6D3E92FBE9822BBFCE,
	AppodealAdsClient_setInterstitialCallbacks_mFBBC4C1EF912F4AFB9CD6B16877D5EE18EEF9F6D,
	AppodealAdsClient_nonSkippableVideoDidLoadAd_mE266D3DF979A3DE4663C5B2B9EA763E2CBD82063,
	AppodealAdsClient_nonSkippableVideoDidFailToLoadAd_m0DB22D1F8F7471FD7614ACEF6602721D2AEBB4BD,
	AppodealAdsClient_nonSkippableVideoWillDismiss_m106A8F432A7B3624CB4C5952A0C81F4536C9AAB2,
	AppodealAdsClient_nonSkippableVideoDidFinish_m1D97230B52B056EF2796FEC19736CC29697C1EE3,
	AppodealAdsClient_nonSkippableVideoDidPresent_mC72B89A402A310456B7A3DCB13685282666E8E2C,
	AppodealAdsClient_nonSkippableVideoDidExpired_mE988DEBD05886F100F0D1118DDF3FEF1ADF12AA6,
	AppodealAdsClient_setNonSkippableVideoCallbacks_m50251C93B3920BFA02929F0C3BF2BD3CE4679B7A,
	AppodealAdsClient_rewardedVideoDidLoadAd_m72EE5300ECEE4915958A5D119C2344DBEB3B5B4C,
	AppodealAdsClient_rewardedVideoDidFailToLoadAd_m61E6B73608743C20063451294C9EAB5271F5AD5B,
	AppodealAdsClient_rewardedVideoWillDismiss_m1C28CC6A05D9ED2DE343F965A7E8FD5DC3810F96,
	AppodealAdsClient_rewardedVideoDidFinish_mE2F18E3F1C5311569E0BF7C3743B448A4BE34679,
	AppodealAdsClient_rewardedVideoDidPresent_m5F0062152B6355C1497F3662B3128D172D3D4365,
	AppodealAdsClient_rewardedVideoDidExpired_mAC5AD49EA77382D236F3CDDC6B4DE2E39ACB6976,
	AppodealAdsClient_rewardedVideoDidReceiveTap_m7B6FA93EE995BBF3E93B161F41AE8FDDF288B405,
	AppodealAdsClient_setRewardedVideoCallbacks_mBA7DA11ED723FF86D7598F0F46B475430308EC74,
	AppodealAdsClient_bannerDidLoadAd_mF459BD51E103C268D0E8C786CB4C16CDBB334D58,
	AppodealAdsClient_bannerDidFailToLoadAd_mAC347D45866570913B08474E397C7A43D1AA62E9,
	AppodealAdsClient_bannerDidClick_m7772D454BF1EB04448DCE581018FE16BA3C987C3,
	AppodealAdsClient_bannerDidShow_mDE7E4BE941E82BBF12BFB4D8FBEA4D82ECEE57BB,
	AppodealAdsClient_bannerViewDidLoadAd_m5A4291DDC3852D80673BE2A762E687D912B2F0B7,
	AppodealAdsClient_bannerViewDidFailToLoadAd_m500257DC50EEFC82C843799108C08D2E0A276A0D,
	AppodealAdsClient_bannerViewDidClick_m2452AED8E93F782EB12889388BB54B826EC7B7DF,
	AppodealAdsClient_bannerViewDidExpired_mEEBFFAD759189DD5D2AC3D4A847ED132186BEAA3,
	AppodealAdsClient_setBannerCallbacks_mE8A183D467CD173E5488A3AB214637AD10AD85FE,
	AppodealAdsClient_mrecViewDidLoadAd_m59CE906506A9BD3D91F85E6CCC32727AEA8168B1,
	AppodealAdsClient_mrecViewDidFailToLoadAd_mD759E93CB8BCD76A19F87AE93E5220311FDE4E61,
	AppodealAdsClient_mrecViewDidClick_m23FD716FFE2FA0307D73478F09FA8C6A0378C2AB,
	AppodealAdsClient_mrecViewDidExpired_m1603F4DABFEDDEFDC12AE46961D8CFFEC0EC5A5A,
	AppodealAdsClient_setMrecCallbacks_mDAA52B5D406CA93283D81A6719D6C2AE30D0EF83,
	AppodealAdsClient_nativeAdTypesForType_m70D2BDF6A78928878F572B76B134DECEBC01920B,
	AppodealAdsClient_nativeShowStyleForType_m35E8D58805C7E8FFAF53113D6CC1CE76F60B6A28,
	AppodealAdsClient_initialize_m9B1B4B6D20F7ADF85FCDF7E5375FD22ED1D82664,
	AppodealAdsClient_initialize_m8A798D7F821AA2E81DC260A6BC03414D8CEEB12F,
	AppodealAdsClient_isInitialized_mF89DBFB99AB200874B8507FC091E5ACB4027875F,
	AppodealAdsClient_show_m90DD22C710DF94258C887BCE3D7E39FD56A501C4,
	AppodealAdsClient_show_m97CC49AE8636AB3467688EB86FA69A7E0241E7A0,
	AppodealAdsClient_showBannerView_m40F33B6AC695C7D8DA9A310A9FC153B241ABD888,
	AppodealAdsClient_showMrecView_m88CA7C6A2C2A1C726636D4AD8C9251FA8B62561B,
	AppodealAdsClient_isLoaded_m85DF071D5ABB2E34CA5D45CCCB8F9464A9A299C3,
	AppodealAdsClient_cache_mC08BD7C6299C9DE64A8C048941B6C92775FF51CA,
	AppodealAdsClient_setAutoCache_m7DD43CBC49FE466B649832A2D6F065021D629BD9,
	AppodealAdsClient_hide_m78BB29B603AF3E5EE8DAEEAA8D089E2CCF7660EA,
	AppodealAdsClient_hideBannerView_mAEA52C43E94A5B63FEAF5D689847806AA375F8B4,
	AppodealAdsClient_hideMrecView_m0D053E8923898518A193F9920E650C7FA4024A89,
	AppodealAdsClient_isPrecache_m2E29ABA3F9DD13353E21AC1CFD624CCEA6A87041,
	AppodealAdsClient_onResume_mFD22C44C35C626E5BC7318C9D46CD1A15C148DB9,
	AppodealAdsClient_setSmartBanners_mE92AE25D7DD059F5CE7ACAF5E7027AA89A460752,
	AppodealAdsClient_setBannerAnimation_m780BA8C40D2E90FE561B57F536A91184AFB28A51,
	AppodealAdsClient_setBannerBackground_mDD5490A634EFC1EE528FAC0543B477560FE9A25B,
	AppodealAdsClient_setTabletBanners_mF40A60D8A5CA8ABF9119CD2ECFBD7AD070C3B9C0,
	AppodealAdsClient_setTesting_m4F73D156630D9E080F18372492C9E6D9105899D4,
	AppodealAdsClient_updateConsent_mD9F89ED36DA5B2F0CFA76633C04B3312271759E7,
	AppodealAdsClient_resetFilterMatureContentFlag_mF008BA667DCF998439006538FDFFE03457280E72,
	AppodealAdsClient_setLogLevel_mA3EA525B61F3B7B0F958C34AE585403ECA6B4EF9,
	AppodealAdsClient_setChildDirectedTreatment_mB6C2752993D320C8BDC59B77929BF02B2D27BBE1,
	AppodealAdsClient_disableNetwork_m6B22DC28E90CEDE92FAF618D4C776DDC02BE5B32,
	AppodealAdsClient_disableNetwork_m69279F8697E0E71C280DFC76FC3D5519CD413124,
	AppodealAdsClient_disableLocationPermissionCheck_mAC8CBEF6DDB08E560D8560BE7B754C2475E42FDD,
	AppodealAdsClient_disableWriteExternalStoragePermissionCheck_mC6C17830C52DD7DFFC4F2449F04D2867D9A916F2,
	AppodealAdsClient_muteVideosIfCallsMuted_m5348046F0EE210FD8979759FED1B09DC1F4E7B84,
	AppodealAdsClient_showTestScreen_m1A50EC72EDE20DFD83CAC43600765F658E585BFF,
	AppodealAdsClient_getVersion_m1C8BE17C5552F0CCD18D5284A6C97ED9A8675317,
	AppodealAdsClient_canShow_m0488A1D132B9C7DAC9F5A7769610783B2C263BE3,
	AppodealAdsClient_canShow_mD054EE483A5767B87DB4D535ACBDFEC1A1B88BEC,
	AppodealAdsClient_getRewardCurrency_m3ACF6D27140C3FB9A901FB9BA5E639B054DD8EC3,
	AppodealAdsClient_getRewardAmount_m671CDEE3CE82D375F89C0A8691C15405F2C69CAD,
	AppodealAdsClient_getRewardCurrency_mAD3B8764829E7FED1AD2F9B9719624A3948DC090,
	AppodealAdsClient_getRewardAmount_m2FBD9EB0E1B42C90B6D320AE576A265A2ED0FA43,
	AppodealAdsClient_getPredictedEcpm_mACAB8629473918218A470711AB28AA07158E81A7,
	AppodealAdsClient_setSegmentFilter_m15AE6AC661DEF50C66F1D6D6AA00A81331D13B73,
	AppodealAdsClient_setSegmentFilter_mB0EC8F9A35CDE97760A5CF27CE474519B3CE2A0D,
	AppodealAdsClient_setSegmentFilter_mD9CCDC82A3CAB311D02AAA0C526CAB11CF552129,
	AppodealAdsClient_setSegmentFilter_m7AB9D03F7911E99C4E6DDE7AAA3EE6F22117A980,
	AppodealAdsClient_setExtraData_mFCC85BA480A8C0B9DB548BCD7519029F95801868,
	AppodealAdsClient_setExtraData_mED48F07B62E1EE91F327BC5E06CC0F98D1D0EE5F,
	AppodealAdsClient_setExtraData_mF9C3F0A2898AB7CD4062BC13BAE2BBC4115D0466,
	AppodealAdsClient_setExtraData_mFEF9B0C81D5CEC0213A892869D12A357C9CE409E,
	AppodealAdsClient_setTriggerOnLoadedOnPrecache_mC219530EF4E2CA4A32E5599587B75D6B4E4765B0,
	AppodealAdsClient_destroy_m2F26EDF686D321F5D73B73B7D9ED0D2ABC3CD560,
	AppodealAdsClient_getUserSettings_m8EF9C01B414B3661CB95A47542A62CF09CE3295A,
	AppodealAdsClient_setUserId_mCAF2D867D456AE4AB952FB63CC38438D2731B6F7,
	AppodealAdsClient_setAge_m293C4FA9124AECD0A8C257CFD8252956E2DD7515,
	AppodealAdsClient_setGender_m8E47A4912B0607BA24C141B6312ADD07236894FC,
	AppodealAdsClient_trackInAppPurchase_mEA404DF89D504D020B1E691E860A2C3BE1185BA2,
	AppodealAdsClient__cctor_m5F9DE7DC34AF8ED53FBED665DE45724E174E1758,
	AppodealInterstitialCallbacks__ctor_m0EB6C09BD6D2F26F957E6FF3F1DDBC3A8E51C9D5,
	AppodealInterstitialCallbacks_Invoke_mA771AFF6C8FEEFECBD14C3A08F7233BA697738B5,
	AppodealInterstitialCallbacks_BeginInvoke_m129A79B7EA4C42F8C8DB8BBB04D42EE0344B86D4,
	AppodealInterstitialCallbacks_EndInvoke_m97FAB1F143C5E006E9832860B5B555182210CC31,
	AppodealInterstitialDidLoadCallback__ctor_mFF10AAE357162F6433E840BF307CCD7294ADBB97,
	AppodealInterstitialDidLoadCallback_Invoke_m303B048D2A29D868503C61A9F62D02FF3385C3D6,
	AppodealInterstitialDidLoadCallback_BeginInvoke_mB9E372CC74519C8B86370BD29C96DE44D864AF87,
	AppodealInterstitialDidLoadCallback_EndInvoke_m90FB1272A905A0E6FE88CD1DFAA458619C3B4975,
	AppodealNonSkippableVideoCallbacks__ctor_mEA027C6B07EBE9F959910F1037A83F2A48B93E02,
	AppodealNonSkippableVideoCallbacks_Invoke_m8FF85C4C9E74D2EEE23B9C87A873A666AF1145B0,
	AppodealNonSkippableVideoCallbacks_BeginInvoke_m09476DCEA33B039E87E11FB41858C09BF2699875,
	AppodealNonSkippableVideoCallbacks_EndInvoke_m18D4D5270AFEF8B28DD439F36B51CD3CEE71E184,
	AppodealNonSkippableVideoDidLoadCallback__ctor_m3A9FF89F13D5609B9ACBEB2A32E2C9ACFF1E664D,
	AppodealNonSkippableVideoDidLoadCallback_Invoke_m02D3BBA56DB0E4D8DA532638E43B82AF84E06455,
	AppodealNonSkippableVideoDidLoadCallback_BeginInvoke_mABD3D1D02EB4A53EDB7C5E990FF1C259E929F9EB,
	AppodealNonSkippableVideoDidLoadCallback_EndInvoke_m613E033E63B33F26D11DF23F9FF501AD755EC3D7,
	AppodealNonSkippableVideoDidDismissCallback__ctor_m5A5F10413793FF0CDEF872A1EB0AC7EC91C7D78B,
	AppodealNonSkippableVideoDidDismissCallback_Invoke_m1F5AC4B509F4B1F15E4B377AEDE968E699630C84,
	AppodealNonSkippableVideoDidDismissCallback_BeginInvoke_m2E529481D21077CD0F34F01687C162EBC75829A7,
	AppodealNonSkippableVideoDidDismissCallback_EndInvoke_mB5EA593D4286E976D13AF81088BEF186B5AF98C5,
	AppodealBannerCallbacks__ctor_mFE47B901B918522BFD9481C97E5D873080C33023,
	AppodealBannerCallbacks_Invoke_mF0E87F656A681B33E3C96229F528A334E9690266,
	AppodealBannerCallbacks_BeginInvoke_m3CB1AA36D8378C2928B4E81CA121907119BFBF06,
	AppodealBannerCallbacks_EndInvoke_mC7384B03CD048338A8AD1C61F3FA29CECE1C2FEC,
	AppodealBannerDidLoadCallback__ctor_m47B72793C3C9E9F2EC6AB19DCE94E9880061E6B8,
	AppodealBannerDidLoadCallback_Invoke_mC24960E2BF67216307A34EE2E4671FF99883C3E1,
	AppodealBannerDidLoadCallback_BeginInvoke_m8BB413F7FCA0204B6EEE07A39B217ACFC5EC7F9B,
	AppodealBannerDidLoadCallback_EndInvoke_mF45B4E30DFD25D2321CE3FED756DCC89C3DC5EFE,
	AppodealBannerViewCallbacks__ctor_m81BAA390743FE2A609D48CE1427CABD57B3C37AB,
	AppodealBannerViewCallbacks_Invoke_mA0C93424617FB2ACEFEA051BBE2A3EC549603C74,
	AppodealBannerViewCallbacks_BeginInvoke_m40A14BDD6FB6BFDAEFD26192A9F1C03F423EF9C5,
	AppodealBannerViewCallbacks_EndInvoke_m97D8C2C0203C22D36952868893F037C11F8AE126,
	AppodealBannerViewDidLoadCallback__ctor_mE39ACB3B6C798605B8382A3F323E439C1472F1CB,
	AppodealBannerViewDidLoadCallback_Invoke_mA8A150C9741BAA68C16FF1E3D589280A651CE816,
	AppodealBannerViewDidLoadCallback_BeginInvoke_mE76BA79FC806B997F7E7C5D9A2FE9DF1E806FF30,
	AppodealBannerViewDidLoadCallback_EndInvoke_m6CF0EF0DAEA974929C4C501174D8B8AD6431E17E,
	AppodealMrecViewCallbacks__ctor_mF9EEC9D7C5CEB555746AF6A34F6F42D308FDE627,
	AppodealMrecViewCallbacks_Invoke_m423B5ADCED3897DB72F38152767354B6C6636914,
	AppodealMrecViewCallbacks_BeginInvoke_m1F7599E3018F1B9A27E39686AA26FD9D1A27E287,
	AppodealMrecViewCallbacks_EndInvoke_m4CBFE212FC482475CA1ED6C19AC4A23BADF14F75,
	AppodealMrecViewDidLoadCallback__ctor_m9BB4DED3C766D292B9CD2D56BEEBE8B051480BD6,
	AppodealMrecViewDidLoadCallback_Invoke_m84BD0E373B96DD4837F1AACB8300A14B79E0091C,
	AppodealMrecViewDidLoadCallback_BeginInvoke_m124ECF74AF21FCA6D1E04D4AEF39D29D62862668,
	AppodealMrecViewDidLoadCallback_EndInvoke_mA5146AC8EC12CC49479AE8C6CEEB444C212722ED,
	AppodealRewardedVideoCallbacks__ctor_m479161DF64DAAE184AFFBF77863E81D05C217A6E,
	AppodealRewardedVideoCallbacks_Invoke_m56646D4B5EE3DF12C07B188537157045F6855488,
	AppodealRewardedVideoCallbacks_BeginInvoke_m2AC51ADE64E8845521D3E8FA2BB12F318195468F,
	AppodealRewardedVideoCallbacks_EndInvoke_mB53CE98A485AC9B29BEC9659D4E5DA987B097C6E,
	AppodealRewardedVideoDidLoadCallback__ctor_mE28E39314D46D2072D8EB5DBED4463450EF08340,
	AppodealRewardedVideoDidLoadCallback_Invoke_m1A0833D6A1C1EECC4CFEB38ED92A1CC5CCE5CA03,
	AppodealRewardedVideoDidLoadCallback_BeginInvoke_mA16AC28790368B1A3BC3614CE17F868B7F89C903,
	AppodealRewardedVideoDidLoadCallback_EndInvoke_mFC9D74C268AA3C2949B87960F9FEB2CBBEBF1341,
	AppodealRewardedVideoDidDismissCallback__ctor_m944767978BB67EE24C6D3C48538FF28C5DA23B37,
	AppodealRewardedVideoDidDismissCallback_Invoke_mDAED1151858E3EE4546EDEF7B8D745A7BAD63E35,
	AppodealRewardedVideoDidDismissCallback_BeginInvoke_m8A2C8A5E10C0E1A46F431CB6B2608241D9ABC50D,
	AppodealRewardedVideoDidDismissCallback_EndInvoke_m57FD711751639622D18F5294F032701EC24DDA05,
	AppodealRewardedVideoDidFinishCallback__ctor_mF36A39B39F918D609EDD21C8646EA622607FEB62,
	AppodealRewardedVideoDidFinishCallback_Invoke_m754F8184AE387B195F27CB2D4C9580723D79438B,
	AppodealRewardedVideoDidFinishCallback_BeginInvoke_m8BECECEDEFCDD22355D595D4118FEC4CE6381012,
	AppodealRewardedVideoDidFinishCallback_EndInvoke_mECE3D78D46DC364B3F1FA0C91D6F13DB846411EB,
	AppodealObjCBridge_AppodealInitialize_m64A0C59D0EC17C316396A23B1BB00E22C476EC91,
	AppodealObjCBridge_AppodealIsInitialized_mB1AEDA7DDE9B53D1EA7DA0A2667F483926B69264,
	AppodealObjCBridge_AppodealShowAd_m7091300D3C4E3D54B0A133EB040AB455C7127F05,
	AppodealObjCBridge_AppodealShowAdforPlacement_m112AD9F8B44D729BC481867C1A920EB03057BFDD,
	AppodealObjCBridge_AppodealShowBannerAdViewforPlacement_m8DA7A560C4AC8B68F6B70D7178F52E1130F208D3,
	AppodealObjCBridge_AppodealShowMrecAdViewforPlacement_m4797EEBB8E9DB2B04E9533F98F447AE2CCD69AAB,
	AppodealObjCBridge_AppodealIsReadyWithStyle_mC4306AEEC053D04F90F0B63D85808CC6EF946073,
	AppodealObjCBridge_AppodealHideBanner_m41D557A508E16F902C60DE81404F4BDCFCDCB5AD,
	AppodealObjCBridge_AppodealHideBannerView_mA20769338E2DA62EEC0268F216582D575ECD37D5,
	AppodealObjCBridge_AppodealHideMrecView_mBF02B27ABCACC9474FAF2560C68751D323CDE94D,
	AppodealObjCBridge_AppodealCacheAd_m09A0879F2F6D596EBE3E653BAF39341321A209B7,
	AppodealObjCBridge_AppodealSetAutocache_m341770DD166DFEA7AAAA5E71A11A816CAFF37B8D,
	AppodealObjCBridge_AppodealSetSmartBanners_mB7CACC2D3990315A89CD8E878C8EA4FB433D8131,
	AppodealObjCBridge_AppodealSetTabletBanners_m6C6B07C6D06D1F72135FD0151953949323E49694,
	AppodealObjCBridge_AppodealSetBannerBackground_m7D639AE490DAF207F8AF2BAACEA6758AD0A1D6B5,
	AppodealObjCBridge_AppodealSetBannerAnimation_m99A4EB4FD7543E3339D4178B566DD45522DA66C6,
	AppodealObjCBridge_AppodealSetLogLevel_m1A04636F2450DB2F11672C2C9CAD2A762BDDA13A,
	AppodealObjCBridge_AppodealSetTestingEnabled_m0E0703EFA93C329028E3ADD98773967DA04A8FFA,
	AppodealObjCBridge_AppodealUpdateConsent_m2411F42C40409AB84BF41782AA8D97309A7D0600,
	AppodealObjCBridge_AppodealSetChildDirectedTreatment_mD9B227E5B9EA594F2F8BFDB3A8E02663414F54D9,
	AppodealObjCBridge_AppodealDisableNetwork_mA267C690A6533916C170D642FCA7C2A653A272BF,
	AppodealObjCBridge_AppodealSetTriggerPrecacheCallbacks_mCBF761596102165094272B00A876268CD8B9F939,
	AppodealObjCBridge_AppodealDisableNetworkForAdTypes_m85C67FEF7B4040AF236BF604BCE525119AB53B18,
	AppodealObjCBridge_AppodealDisableLocationPermissionCheck_mDBB029130F2719851B948F8490280597BA6A08DE,
	AppodealObjCBridge_AppodealGetVersion_m0408771437F39D38757D43474A00A6847FC9D69F,
	AppodealObjCBridge_AppodealCanShow_m8AC6FF79E50A0609A042023E18CD842D6F34369F,
	AppodealObjCBridge_AppodealCanShowWithPlacement_m97BBF300DA5F70AF20ABCF69D2D0451AF705E37A,
	AppodealObjCBridge_AppodealGetRewardCurrency_mE7F7ABFD1DDA9314B6BE41BC02AD9F076A81168A,
	AppodealObjCBridge_AppodealGetRewardAmount_m32DB6D5831849E37751B458CF06449807E0C9083,
	AppodealObjCBridge_AppodealGetPredictedEcpm_mE8E0A5EDB6BC7E6D7315D4E96F6A76385E5AF22C,
	AppodealObjCBridge_AppodealSetSegmentFilterString_mB635BDA66AE832D4FD6EEEC2F6699D51C27743E4,
	AppodealObjCBridge_AppodealSetSegmentFilterDouble_m76D4F75488581F1A0576D19F223A41CD542C413E,
	AppodealObjCBridge_AppodealSetSegmentFilterInt_m0B31BC4D25599F7323096E6C8320DDCC65DDF7A4,
	AppodealObjCBridge_AppodealSetSegmentFilterBool_m4C6ECC50E3D657C9E37F706230873377951AEE17,
	AppodealObjCBridge_AppodealSetExtraDataBool_m42F23DFCB14C2C2C4BFA405849A361E801BD62A1,
	AppodealObjCBridge_AppodealSetExtraDataInt_mDD5291C6DB8F1C7F3BDB1FE6B3752B8473D5A6E2,
	AppodealObjCBridge_AppodealSetExtraDataDouble_m77E46C2E4934560F15DD8E173194CE2AC94E17FD,
	AppodealObjCBridge_AppodealSetExtraDataString_mAA0C17CFED77E71796EFED316528639F4AD4FF00,
	AppodealObjCBridge_AppodealTrackInAppPurchase_mFA25E4E5CDF6174B510257FB378DEB3C429A3832,
	AppodealObjCBridge_AppodealSetUserId_m083A3BE854CAFDC0FCEC8D83FE558A6EA54DD8D6,
	AppodealObjCBridge_AppodealSetUserAge_m51C7841937692B567D376245045265C5A99173E7,
	AppodealObjCBridge_AppodealSetUserGender_m7826358E279A4AB007B4ED95A580B587B9B6507B,
	AppodealObjCBridge_AppodealSetInterstitialDelegate_m0C7587D4E10FFA8956A6AB8BA70D3AA0042454FA,
	AppodealObjCBridge_AppodealSetNonSkippableVideoDelegate_m49F3A2EDB8F600D09479D4D647E9CEEBBFE9D5A4,
	AppodealObjCBridge_AppodealSetRewardedVideoDelegate_m760C64F53E76334942E6EC0781BD1D9E8A6EDB5F,
	AppodealObjCBridge_AppodealSetBannerDelegate_m83A5D18FD4D7E6D21C94201BAB51D8C25175718C,
	AppodealObjCBridge_AppodealSetBannerViewDelegate_m534C2108A33125C8B5B59106E670BC15566F965F,
	AppodealObjCBridge_AppodealSetMrecViewDelegate_mE3421525EF2B16BBA82D6DFF810C522DB122BB31,
	AppodealObjCBridge__ctor_mF09BC1500B56E6BD5FB90F3ECB5C93872A9DAEB0,
	DummyClient_initialize_mED36454B0DDCF36FD6D8596063D02CC326D0A192,
	DummyClient_initialize_m3F54759EEBC456FCEA2627008D3D709C71F40553,
	DummyClient_isInitialized_m9D7C66FA8207A206E1231C67C7BE0347C57DC001,
	DummyClient_show_mB69725D0B53FCE078B4556DC866159D3A717FCEE,
	DummyClient_show_m358E8649C8A2DEAD7D95BE625EBC90E01532B9F3,
	DummyClient_showBannerView_m5A45A3D2C3B47B34655E260C40E1D7DFAF633AEB,
	DummyClient_showMrecView_m040F3DFA41DFBD1DA26981B2DDAD8BA58083F825,
	DummyClient_isLoaded_mE21DEF1E2A4881482D82794219D22545612FC34E,
	DummyClient_cache_mFD1C95CD1B530246450118FDEDDEA50897BCACBC,
	DummyClient_hide_mCCF830721CAABC64F6F04648802F29AA7655DC91,
	DummyClient_hideBannerView_m21BDD9C2AFA198784159F03E725425EA44819220,
	DummyClient_hideMrecView_m9A3A00A7DF12B2EC2B26082DCB402E1B2AD59B8D,
	DummyClient_isPrecache_m6F7893DF1A184CBBC492F3B9D4A7776B566D3D6B,
	DummyClient_setAutoCache_mF85904CA0F5D0040EDA0588171F327F789465D4F,
	DummyClient_onResume_mF472088C9B73D9BE3543C55091812634CCC82948,
	DummyClient_setSmartBanners_m3852E430E97D9054280D7F404FDC7A8CFBD7899F,
	DummyClient_setBannerAnimation_m866D6A701DDCF867692AE924749F8C2BA8EAAB29,
	DummyClient_setBannerBackground_m4AFFCA1E9B4DD036DCCC83BBE26AE34BC9C00CDD,
	DummyClient_setTabletBanners_m029D0AA76D4EFBEFF76E8E8807B45805FD386BED,
	DummyClient_setTesting_mE09FD3ABE1BA070B2EC831EE098C6CAA8DD826D5,
	DummyClient_setLogLevel_m525423F870A81F1E45C559D7D1ED5BEFDBD3607A,
	DummyClient_setChildDirectedTreatment_m941BDCA91FF615D889700F855C649969E50C2732,
	DummyClient_updateConsent_m9D04CBC6199CD091144915A71B1B3B3BB1011083,
	DummyClient_resetFilterMatureContentFlag_m2A4A916A72D4E093DF530EC7DA8BC6745D133EBE,
	DummyClient_disableNetwork_m469B0A2521346F6217928CF46E1E2E78CD588ABB,
	DummyClient_disableNetwork_m6DEED150795038667742D96D8F94F92F52942BCF,
	DummyClient_disableLocationPermissionCheck_mEFB4FC7E5AE8233A09E960C018BDF704CC38ACEF,
	DummyClient_disableWriteExternalStoragePermissionCheck_m5D2E94C5922BDB091C5364FBA33DF5BA7CBC8EA3,
	DummyClient_setTriggerOnLoadedOnPrecache_mF38DB99067FF396A7D87D6078452D5BAA9FC6D8F,
	DummyClient_muteVideosIfCallsMuted_m8AAF1A2BD0E3F251929B1508C4623AC4DD7BF260,
	DummyClient_showTestScreen_mFA408BBDF1F9128C7B6BF8BB74FB5EB17C112B63,
	DummyClient_getVersion_m4EF33B1DFCE3C98C5AF485CBA2E542876B96D394,
	DummyClient_canShow_m81F8D49A96FF71C012D2C5B48E4CF8AF228716CB,
	DummyClient_canShow_m288C0BB8238A3531A26185E4DB699C69CC371995,
	DummyClient_setSegmentFilter_mE9F68F7D71CBFE301B16B1F4290A1FD9C9BB1729,
	DummyClient_setSegmentFilter_mB54C09F34BEF12F953AC4ADFD8990F8383E9083B,
	DummyClient_setSegmentFilter_m7B6329DCC595FD58C34846EDE66138C40298065C,
	DummyClient_setSegmentFilter_m9785613145237971ADEB0303F791528BE8C191BA,
	DummyClient_trackInAppPurchase_mFEDEA7279C2E41CF44E2CCD08DBC8CFA45DA2A74,
	DummyClient_getRewardCurrency_m3917F65A2F60D4D85C078D4B558C9520D7A3C28F,
	DummyClient_getRewardAmount_m43722C33F25A334EA2456E4A2C3B05D9BE2DC288,
	DummyClient_getRewardCurrency_m0BDA9CE9A315E56B9E41D54818F5F8E21BE3D273,
	DummyClient_getRewardAmount_m93692E73E4F8C40916C1EEEF031F8BBB802C02D7,
	DummyClient_getPredictedEcpm_m4D7EDD776C2ADADB5101DB801289D122D12DE360,
	DummyClient_setExtraData_mE2AD6114AD2633DD5800D50CA574FA8430CDA81A,
	DummyClient_setExtraData_m02BE872D96ACAA76A1E4B952E6DBBF3C1B81F2A7,
	DummyClient_setExtraData_m77F1E95371DEE476A393C5DD2964E21FED8A71DE,
	DummyClient_setExtraData_mF1C47104D7E31C0B767A5582284683E130DD68DF,
	DummyClient_setInterstitialCallbacks_m1B4963E62CD5A334F9B7383DF05BE485BBAF2551,
	DummyClient_setNonSkippableVideoCallbacks_m17AA57899F5F8273CD27A46BDBC4A332FFC8E72F,
	DummyClient_setRewardedVideoCallbacks_m3F04DC83744EE45E1C705DB0E2D533DEE8DD2359,
	DummyClient_setBannerCallbacks_m3153FDAFD1C066513ED7A27C7589D24C21FCE2D3,
	DummyClient_setMrecCallbacks_m5A01F0A28D891EAF9FEC029278AAD4E9634AA605,
	DummyClient_requestAndroidMPermissions_m98B1E99DB97D3F452F1B1FF7E4DD7953DEABD821,
	DummyClient_getUserSettings_m10CF86B6EB2C0F43875DD46CD0FC843B95CAA1D1,
	DummyClient_setUserId_mEB60754914608A7EF0C4A621D918946395740F4C,
	DummyClient_setAge_m1466EA6951284675CEE1AB546730777F48DCF343,
	DummyClient_setGender_mA4720911F86560FA55B5521D6EA90F00BE314BDB,
	DummyClient_requestAndroidMPermissions_m577A4EFFCAE5B6D04061ED8614DA2993C655CAEE,
	DummyClient_destroy_mC1154E31C7B12462E379D144B64FD28007B8EBBB,
	DummyClient__ctor_m5958F4B3C4112BEB0E20BBACB0D00FDABD9B832E,
	AppodealBannerCallbacks__ctor_m5DB0E31211ECBF8841A927D99C6FCA60C6EDC65A,
	AppodealInterstitialCallbacks__ctor_m08F45C7F6B5662C7AB29C3174ED5B35F0E367A36,
	AppodealMrecCallbacks__ctor_m8878BB0FFAB8539F715AE86CDEB1603B87A61C28,
	AppodealNonSkippableVideoCallbacks__ctor_mFCC48EBE61E65C1BEB7C880DB694FDD64EBE1784,
	AppodealPermissionCallbacks__ctor_m709F71A7A19557F80B1A0AA9276510309898A23E,
	AppodealRewardedVideoCallbacks__ctor_m673E7BB39AF4B426A4686AF7F1BC41BB69A3FB18,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	AppodealNetworks__ctor_mF016B355BF6DC14D6461F69573BFC6FC79E5F5DA,
	Appodeal_getInstance_m3EDAC1E5E60D296119245C57F7AF5534CF57C83F,
	Appodeal_initialize_m7C4300E7BC284EB8331749D97B4FD25174A1244C,
	Appodeal_initialize_m891EAFF98230D571C1779B75EE6CFECD08122D44,
	Appodeal_show_m154AD7C996073738388B655CEDE1E99F891FF694,
	Appodeal_show_m2B130634646478FCB73FED49BED6C1BC34C6BE76,
	Appodeal_showBannerView_mA34389D2A52B2EBA2649DAE144AFAE386C63E13D,
	Appodeal_showMrecView_m67ACE4D33B82B2AC22DD5E9BFB3429178DBE1FA1,
	Appodeal_isLoaded_m8F9485BCC1B4B7D802865FBEB0255C2316BACD0A,
	Appodeal_cache_m75A7C718B5F2AA5A3B8C8E83FAB6E17C1204B121,
	Appodeal_hide_mD9E7FEFA7FC4A7BDC9364B283DF880333997DBE8,
	Appodeal_hideBannerView_m92F47F6D21220F4AD3FCD678F4E721E63C1058FC,
	Appodeal_hideMrecView_m93EE8C897506E17BB4FF2A18E01A1C99D19B829C,
	Appodeal_setAutoCache_mA47ED58601FB6CB1528D2B69277213DD3080700C,
	Appodeal_isPrecache_mE2DB643591648A3E5F84D2A3D43F17C19EA01F33,
	Appodeal_onResume_m8C4418962F2923F397B6CAE43E7D37C4340B601B,
	Appodeal_setSmartBanners_m7E71C3492824B24092278BD04B04780F6F86F525,
	Appodeal_setBannerBackground_m9B7348AAB8B9AF46BC0879131C6129842FD73315,
	Appodeal_setBannerAnimation_m82352DD5388D89B0ECF2B3B40D8431F1F6A7ADEE,
	Appodeal_setTabletBanners_mCCD8AD116114A8833FB308E25B6E0E7C4727F7A0,
	Appodeal_setTesting_m9F150DDB4BE203C91CB8850D4358D5EDF42FC67C,
	Appodeal_setLogLevel_mC80AAB79F61849E3531F6D932210A57454322D95,
	Appodeal_setChildDirectedTreatment_m606E8B67EB02F982C00E68E1E7174EDBBF577023,
	Appodeal_updateConsent_m52906D2F2C8DFD7410BF33CED4E0A8602EFF198C,
	Appodeal_resetFilterMatureContetnFlag_mB1D29A60F2A58AA599DB3AD18FEAF22BB4552A07,
	Appodeal_disableNetwork_m38A57EA2C0DBDA37B3792C3AC63DF2F047143CAF,
	Appodeal_disableNetwork_mA1F4966CE70C381371A4F39E707073A0898EF6CA,
	Appodeal_disableLocationPermissionCheck_m6A920CF41E1A0F34F82206ABDA29A3184AF7DD51,
	Appodeal_disableWriteExternalStoragePermissionCheck_m47099D01C5FF8AF985963A9B05ABE4BFBF0A4F65,
	Appodeal_setTriggerOnLoadedOnPrecache_m3701020D074582C957BBCE84DC56C97B50D3D09F,
	Appodeal_muteVideosIfCallsMuted_m0392965E424A2291CB2B010D0BEB63D19ED1DEEF,
	Appodeal_showTestScreen_m32C96AA60A9FD654564164C86D88CBE1ADDF2F96,
	Appodeal_canShow_m1181D969409AAB6291182955338BC307E24C5D67,
	Appodeal_canShow_m1F5A45E264353D38F120785A4A36504E300C5A65,
	Appodeal_setSegmentFilter_mC90C3C6DE67668B03F0B1846A3E4D3241B82E91C,
	Appodeal_setSegmentFilter_m229E1BD9325512A47F7B44F7FE942337E98AF418,
	Appodeal_setSegmentFilter_m49B3259D7EA6911306EACAD67F7D09DF20E32843,
	Appodeal_setSegmentFilter_m77CD82D83FDE4D2BB59D918D98FEB82E49E35B3C,
	Appodeal_setExtraData_m071D334EE0239ED94844B262F1E0443F8951D6C3,
	Appodeal_setExtraData_mC12062C41D559A08C8986E15EE83B216D34F4031,
	Appodeal_setExtraData_m7526E13382630E6B2338EBBAF922971DD650730F,
	Appodeal_setExtraData_mAD861FEBAE1F76A3F321F2F76A105323771A9447,
	Appodeal_trackInAppPurchase_mF8B5AA45B9C4277E21940EB745AB532207494B88,
	Appodeal_getNativeSDKVersion_m9E6D595B7B786527981A3A518A0ABB1DCA04C858,
	Appodeal_getPluginVersion_mAB459CB8AC9C9D1390BE4F8C53A72952E78B388B,
	Appodeal_setInterstitialCallbacks_m2611C0B16B1C0CF3677B71F25566EE07631F8618,
	Appodeal_setNonSkippableVideoCallbacks_m8C7E5F3628BF5F04C7A6EA963A6D0D1C103BF4D4,
	Appodeal_setRewardedVideoCallbacks_m1BEB94190C06B3D0BD8D72474EDB84290DA25B5B,
	Appodeal_setBannerCallbacks_m2FE7741E2DA1F01817B580612D1779C1A90A4CAA,
	Appodeal_setMrecCallbacks_m3BCBE8EA0B4F38BFF1F7CE3DB4CB0900D0E1176E,
	Appodeal_requestAndroidMPermissions_mEF733BAEF5C7DB36713CEF85F524E6C148719403,
	Appodeal_getRewardParameters_m4AA5454867BBC0B4877CC2508705E862572AF63C,
	Appodeal_getRewardParameters_m1E9F17A89297A2F3293F2B6DCC64111A2D7E6ACA,
	Appodeal_getPredictedEcpm_m4D8A52231772E58C4624FD346DBF1150E69FB34A,
	Appodeal_destroy_mF836AA886F32FB76A90C816D2AF2607D96A960B7,
	Appodeal_getUnityVersion_mF98E5F91E57AF4EEB9909A6BC4DC264628BEB972,
	Appodeal__ctor_m2EE6BAA818E3166228BD5EBCD308088CB5538F0E,
	ExtraData__ctor_m65C00658CA4754C04064A6683670AA96B619C0A9,
	ExtraData__cctor_m01F9D0776186BCE46FBC828CA55AE13904E35695,
	UserSettings_getInstance_m5FDDD1523E5B225C583A16A887EEEBEBE224E6F6,
	UserSettings__ctor_m28CB46C621C4DF9894CCAA26C0260301D6FF084E,
	UserSettings_setUserId_m6DD2BC91A0FFDAADD3339BC96E1747B87AB80A42,
	UserSettings_setAge_m42D3DD7359F90528E28C0815872B6B5F6FA14FA1,
	UserSettings_setGender_m8849CCB829A590114A59839815D436BCE058C70B,
};
static const int32_t s_InvokerIndices[822] = 
{
	19,
	19,
	19,
	19,
	19,
	13,
	13,
	13,
	13,
	1687,
	144,
	4,
	13,
	13,
	13,
	13,
	13,
	1688,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	44,
	44,
	13,
	13,
	13,
	13,
	44,
	13,
	13,
	13,
	13,
	44,
	13,
	13,
	13,
	13,
	13,
	44,
	13,
	13,
	44,
	13,
	13,
	44,
	13,
	13,
	44,
	1689,
	13,
	13,
	9,
	9,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	8,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	8,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	8,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	8,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	19,
	13,
	13,
	19,
	4,
	769,
	8,
	8,
	8,
	8,
	8,
	4,
	769,
	8,
	769,
	8,
	8,
	8,
	4,
	769,
	8,
	769,
	1690,
	8,
	8,
	8,
	4,
	769,
	8,
	8,
	8,
	769,
	8,
	8,
	8,
	4,
	769,
	8,
	8,
	8,
	4,
	66,
	66,
	124,
	296,
	61,
	61,
	1612,
	1691,
	1691,
	61,
	9,
	571,
	9,
	13,
	13,
	61,
	9,
	44,
	44,
	44,
	44,
	44,
	44,
	13,
	9,
	44,
	4,
	124,
	13,
	13,
	44,
	13,
	14,
	1612,
	61,
	6,
	182,
	14,
	389,
	1692,
	385,
	124,
	1438,
	23,
	385,
	124,
	1438,
	23,
	571,
	9,
	13,
	4,
	9,
	9,
	1689,
	8,
	163,
	13,
	16,
	4,
	163,
	44,
	1269,
	4,
	163,
	13,
	16,
	4,
	163,
	44,
	1269,
	4,
	163,
	44,
	1269,
	4,
	163,
	13,
	16,
	4,
	163,
	44,
	1269,
	4,
	163,
	13,
	16,
	4,
	163,
	44,
	1269,
	4,
	163,
	13,
	16,
	4,
	163,
	44,
	1269,
	4,
	163,
	13,
	16,
	4,
	163,
	44,
	1269,
	4,
	163,
	44,
	1269,
	4,
	163,
	1689,
	1693,
	4,
	1694,
	74,
	74,
	1695,
	1696,
	1696,
	74,
	8,
	8,
	8,
	121,
	1697,
	769,
	769,
	769,
	769,
	121,
	769,
	769,
	769,
	30,
	769,
	309,
	8,
	19,
	74,
	1695,
	0,
	299,
	238,
	122,
	1698,
	309,
	548,
	548,
	309,
	1698,
	122,
	1690,
	30,
	121,
	121,
	1699,
	1699,
	1700,
	1701,
	1687,
	1687,
	13,
	124,
	296,
	61,
	61,
	1612,
	1691,
	1691,
	61,
	9,
	9,
	13,
	13,
	61,
	571,
	9,
	44,
	44,
	44,
	44,
	44,
	9,
	44,
	44,
	13,
	4,
	124,
	13,
	13,
	571,
	44,
	13,
	14,
	61,
	1612,
	385,
	124,
	1438,
	23,
	1689,
	6,
	182,
	14,
	389,
	1692,
	385,
	124,
	1438,
	23,
	4,
	4,
	4,
	4,
	4,
	4,
	13,
	4,
	9,
	9,
	13,
	9,
	13,
	4,
	4,
	4,
	4,
	4,
	4,
	124,
	296,
	61,
	61,
	1612,
	61,
	9,
	9,
	571,
	61,
	9,
	1691,
	1691,
	13,
	13,
	44,
	44,
	44,
	44,
	44,
	9,
	44,
	44,
	13,
	4,
	124,
	13,
	13,
	44,
	13,
	14,
	61,
	1612,
	385,
	124,
	1438,
	23,
	385,
	124,
	1438,
	23,
	6,
	182,
	14,
	389,
	1692,
	571,
	13,
	9,
	9,
	4,
	1689,
	4,
	4,
	4,
	4,
	4,
	4,
	9,
	44,
	13,
	13,
	13,
	13,
	44,
	13,
	13,
	13,
	13,
	13,
	44,
	13,
	13,
	13,
	13,
	44,
	13,
	13,
	13,
	44,
	13,
	9,
	9,
	44,
	13,
	13,
	1689,
	44,
	13,
	13,
	13,
	19,
	309,
	1702,
	74,
	1695,
	1696,
	1696,
	74,
	121,
	121,
	8,
	8,
	1703,
	74,
	121,
	769,
	769,
	769,
	769,
	769,
	121,
	769,
	769,
	8,
	30,
	309,
	8,
	8,
	1703,
	769,
	8,
	74,
	1695,
	548,
	309,
	1698,
	122,
	548,
	309,
	1698,
	122,
	1690,
	19,
	19,
	30,
	30,
	30,
	30,
	30,
	30,
	1704,
	1705,
	238,
	121,
	19,
	13,
	13,
	8,
	19,
	13,
	6,
	63,
	63,
};
static const Il2CppTokenIndexPair s_reversePInvokeIndices[31] = 
{
	{ 0x06000191, 1 },
	{ 0x06000192, 2 },
	{ 0x06000193, 3 },
	{ 0x06000194, 4 },
	{ 0x06000195, 5 },
	{ 0x06000196, 6 },
	{ 0x06000198, 7 },
	{ 0x06000199, 8 },
	{ 0x0600019A, 9 },
	{ 0x0600019B, 10 },
	{ 0x0600019C, 11 },
	{ 0x0600019D, 12 },
	{ 0x0600019F, 13 },
	{ 0x060001A0, 14 },
	{ 0x060001A1, 15 },
	{ 0x060001A2, 16 },
	{ 0x060001A3, 17 },
	{ 0x060001A4, 18 },
	{ 0x060001A5, 19 },
	{ 0x060001A7, 20 },
	{ 0x060001A8, 21 },
	{ 0x060001A9, 22 },
	{ 0x060001AA, 23 },
	{ 0x060001AB, 24 },
	{ 0x060001AC, 25 },
	{ 0x060001AD, 26 },
	{ 0x060001AE, 27 },
	{ 0x060001B0, 28 },
	{ 0x060001B1, 29 },
	{ 0x060001B2, 30 },
	{ 0x060001B3, 31 },
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	822,
	s_methodPointers,
	s_InvokerIndices,
	31,
	s_reversePInvokeIndices,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
