﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// SleekRender.SleekRenderPostProcess
struct SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94;
// SleekRender.SleekRenderSettings
struct SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.List`1<UnityEngine.ParticleCollisionEvent>
struct List_1_t2762C811E470D336E31761384C6E5382164DA4C7;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IEnumerator
struct IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.NotSupportedException
struct NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.AnimationCurve
struct AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C;
// UnityEngine.AudioClip
struct AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051;
// UnityEngine.AudioClip/PCMReaderCallback
struct PCMReaderCallback_t9B87AB13DCD37957B045554BF28A57697E6B8EFB;
// UnityEngine.AudioClip/PCMSetPositionCallback
struct PCMSetPositionCallback_t092ED33043C0279B5E4D343EBCBD516CEF260801;
// UnityEngine.AudioClip[]
struct AudioClipU5BU5D_t03931BD44BC339329210676E452B8ECD3EC171C2;
// UnityEngine.AudioSource
struct AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C;
// UnityEngine.Behaviour
struct Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0;
// UnityEngine.Collider
struct Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF;
// UnityEngine.Collider[]
struct ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252;
// UnityEngine.Color[]
struct ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399;
// UnityEngine.Component
struct Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Light
struct Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Mesh
struct Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.ParticleCollisionEvent[]
struct ParticleCollisionEventU5BU5D_t771CB4A499CC97F3340C673D227DD1F5969EB9B9;
// UnityEngine.ParticleSystem
struct ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D;
// UnityEngine.ParticleSystem[]
struct ParticleSystemU5BU5D_t58EE604F685D8CBA8EFC9353456969F5A1B2FBB9;
// UnityEngine.RenderTexture
struct RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6;
// UnityEngine.Renderer
struct Renderer_t0556D67DD582620D1F495627EDE30D03284151F4;
// UnityEngine.Rigidbody
struct Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5;
// UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734;
// UnityEngine.Shader
struct Shader_tE2731FF351B74AB4186897484FB01E000C1160CA;
// UnityEngine.SphereCollider
struct SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F;
// UnityEngine.Texture
struct Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.Transform[]
struct TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;
// UnityStandardAssets.Effects.AfterburnerPhysicsForce
struct AfterburnerPhysicsForce_t753048E9DC6DB55FA797EA775B76014E1E7F204B;
// UnityStandardAssets.Effects.ExplosionFireAndDebris
struct ExplosionFireAndDebris_tACCFCC1184BB9A8C3A0B39D99FFC831E76F9EECB;
// UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>d__4
struct U3CStartU3Ed__4_t945A707CD8C01DA1C37AE2DD9D0A904C80F58139;
// UnityStandardAssets.Effects.ExtinguishableParticleSystem
struct ExtinguishableParticleSystem_tD91348BD8771EB84F5369DA844F390BE915ADB64;
// UnityStandardAssets.Effects.FireLight
struct FireLight_tBDF3DBBD1B6288E05ECBDCDB787FBB8ABD8572C5;
// UnityStandardAssets.Effects.Hose
struct Hose_t411AEC20AB5A70AEFCF03FCB2418F0C8EB14C2D2;
// UnityStandardAssets.Effects.ParticleSystemMultiplier
struct ParticleSystemMultiplier_t8EBFEF83EBA48DD7F6517ABFFBE2CD051CB2C748;
// UnityStandardAssets.Effects.SmokeParticles
struct SmokeParticles_t556E08200E5E25126D51BFF297C7522998E3D793;
// UnityStandardAssets.Effects.WaterHoseParticles
struct WaterHoseParticles_t4C0EC93AEAF5B1623A7F04AF7D912D6B6438E92B;

IL2CPP_EXTERN_C RuntimeClass* ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Graphics_t6FB7A5D4561F3AB3C34BF334BB0BD8061BE763B1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t2762C811E470D336E31761384C6E5382164DA4C7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CStartU3Ed__4_t945A707CD8C01DA1C37AE2DD9D0A904C80F58139_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WaterHoseParticles_t4C0EC93AEAF5B1623A7F04AF7D912D6B6438E92B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_t1462DEA80E3C0F6D30E594F33C4F5283DF75F28F____23447D2BC3FF6984AB09F575BC63CFE460337394_0_FieldInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral07C17D8026434E7F6CC9CE35B102F6869FB838FB;
IL2CPP_EXTERN_C String_t* _stringLiteral088472395B2697109252AA04C2F12D53CF051D62;
IL2CPP_EXTERN_C String_t* _stringLiteral1B1401C3041E707DF5717E7BB94159B6B4F887E8;
IL2CPP_EXTERN_C String_t* _stringLiteral30271936C7B32B89A14938BB7CD12CADA14C4986;
IL2CPP_EXTERN_C String_t* _stringLiteral3DEF32960CD3BA7431114E6BD565613B2E9B64BF;
IL2CPP_EXTERN_C String_t* _stringLiteral445809F402DAF60282A49D8CDDAA3FA7DCD75127;
IL2CPP_EXTERN_C String_t* _stringLiteral4609CF47A2EA4974A5D110F1FC15122D047ED134;
IL2CPP_EXTERN_C String_t* _stringLiteral4796C2FBF2E7B58822864E6AA158FDAF94586F93;
IL2CPP_EXTERN_C String_t* _stringLiteral49509FA907B8AD7BE4933B9CB7CBCD7B386D7165;
IL2CPP_EXTERN_C String_t* _stringLiteral4CA0AAB8D31E2D73CA72A054532184CDBCD32F53;
IL2CPP_EXTERN_C String_t* _stringLiteral6574DEA2D7262D7E5060CA7786488B77C79BA130;
IL2CPP_EXTERN_C String_t* _stringLiteral6CCAA9D4E4A7F56B1FEDDFBAC85B689B84422E0E;
IL2CPP_EXTERN_C String_t* _stringLiteral745D7F7582276839A0CDACA165D0880010C3B0CB;
IL2CPP_EXTERN_C String_t* _stringLiteral77831B254E3A6816BFF569221912AD410B829E3B;
IL2CPP_EXTERN_C String_t* _stringLiteral7F4FC3F227306B55CD4604D3B2D0C6753FAB7410;
IL2CPP_EXTERN_C String_t* _stringLiteral8042151345307136E9BF2C874EF9884446C75512;
IL2CPP_EXTERN_C String_t* _stringLiteral8C3FB1B5EB4296D3AF847E3A575580A61D698AF4;
IL2CPP_EXTERN_C String_t* _stringLiteral8D30A1632F91DD50F6A0FEC47EAA8ADB8D922D60;
IL2CPP_EXTERN_C String_t* _stringLiteralA86D57D17CF0F3781405F49FDEF380455D496A5E;
IL2CPP_EXTERN_C String_t* _stringLiteralAC523897F9F4D00B244564D81D5C14603DF6A69C;
IL2CPP_EXTERN_C String_t* _stringLiteralC510EA100EEE1C261FE63B56E1F3390BFB85F481;
IL2CPP_EXTERN_C String_t* _stringLiteralCBE31FA694878583D3DEB5EDC1C5A8F38AB53857;
IL2CPP_EXTERN_C String_t* _stringLiteralCD96BB6E91576D4BDBEC8D699ACCA61156855FBA;
IL2CPP_EXTERN_C String_t* _stringLiteralD53BF922AC5FC0414F5AFDC2F09EDC5F766B3C86;
IL2CPP_EXTERN_C String_t* _stringLiteralE0780C0EF7B1666605E71B814D8AD13B2A650ABE;
IL2CPP_EXTERN_C String_t* _stringLiteralF234EE02A8E22EC8F4118B8395D4992E644FE853;
IL2CPP_EXTERN_C String_t* _stringLiteralF6DC242E6953FE7235423EAAA611B8770BA13F2C;
IL2CPP_EXTERN_C String_t* _stringLiteralFEEDCD9FACABFCEDB19D47D699FA78BBEC70CAC1;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisAudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C_m04C8E98F2393C77979C9D8F6DE1D98343EF025E8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_mB090F51A34716700C0F4F1B08F9330C6F503DB9E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisCollider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF_m6B8E8E0E0AF6B08652B81B7950FC5AF63EAD40C6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisLight_tFDE490EADBC7E080F74CA804929513AF07C31A6C_m1DCED5DB1934151FC68A8E7CAECF7986359D7107_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisParticleSystemMultiplier_t8EBFEF83EBA48DD7F6517ABFFBE2CD051CB2C748_m77EDAEF51740299BC986B0DA36AB7282BB8DDD45_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D_mA6EBF435A59643B674086A2F309F6A4DCB263452_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisRigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5_m3F58A77E3F62D9C80D7B49BA04E3D4809264FD5C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponentsInChildren_TisParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D_mD5AEF673286CB17B689F765BB71E83509548DC41_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m377714278A7FAB4D7514D181355D61B01B3427B0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m095B750642DEE222CA241EE9D2392CA6E200FA8D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_Instantiate_TisTransform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_m8BCC51A95F42E99B9033022D8BF0EABA04FED8F0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ScriptableObject_CreateInstance_TisSleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142_mF36470B34704D69B08AB64E76E7987E6BF28E215_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m886FA065B7C64EA3A746502C9F6BF104B9801F07_RuntimeMethod_var;
IL2CPP_EXTERN_C const uint32_t AfterburnerPhysicsForce_FixedUpdate_m90286B7CDABBED931E9D13E5D879E6EC41BF6E8A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AfterburnerPhysicsForce_OnDrawGizmosSelected_m8E75C894A4544C40EB1FD3DA8883E3358BAF8AA3_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AfterburnerPhysicsForce_OnEnable_m31ABCDDA8E500224F25244C9D3FE44CBDD45D7CE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ExplosionFireAndDebris_AddFire_m781F3E400896C4F2E2B4A24FAEFE52D01ED26C0B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ExplosionFireAndDebris_Start_m85654B2EA9348AE14094A4CDCA1ECD714E4CBC22_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ExtinguishableParticleSystem_Start_m442E9165E311C7DC5AE322C5CF8B8D6A2325DC39_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FireLight_Start_mBC43BC2A9EDAD47A5D43C96C04E1AECC4405B81D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FireLight_Update_mE6D64BEF14D90624104BC092626B738B75D8D0F7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Hose_Update_mF09247CA136AF92D5586DD9C95823C7E9A6AE939_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ParticleSystemMultiplier_Start_m9629829804A66FA8E1BA25039F0BCD4447D1785D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SleekRenderPostProcess_CheckScreenSizeAndRecreateTexturesIfNeeded_m8FE59E4480A6A09382775B46448C5784B79029E4_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SleekRenderPostProcess_Compose_m76162AF19A38CE8168DB6ED4B2E89E56110D02BA_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SleekRenderPostProcess_CreateDefaultSettingsIfNoneLinked_mFAFCBC9B80CB0CAE18DB94C102CD3D0FAEFA83A9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SleekRenderPostProcess_CreateMainRenderTexture_mA21F6ABA9A57B0BD2A2ACBCCEBD12CA09A16838D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SleekRenderPostProcess_CreateResources_m88233905ED6473C25EA3C8A4B3757879C201D6DA_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SleekRenderPostProcess_CreateScreenSpaceQuadMesh_m2D59ABE6B083BEF9A9DE056AF5725199DD9FCB0C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SleekRenderPostProcess_CreateTransientRenderTexture_m5896F414C30F00DE545EF638447324CC3D1DE67F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SleekRenderPostProcess_DestroyImmediateIfNotNull_m0E4B29743210245443999A680294C1DDBF0140DC_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SleekRenderPostProcess_Downsample_m939DB30198E9BB4BEFF3884A283D450C8704EE96_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SleekRenderPostProcess_DrawFullscreenQuad_m77F12B59A976F77BFC5A8BF270AD5E5D30408DD0_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SleekRenderPostProcess_Precompose_mE5C8A6B77C24F3B90CDFC17D60895195BDD20965_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SmokeParticles_Start_m1A4CBEB9EDF7F792FCF7F96D9435B6C0B0A94DD6_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CStartU3Ed__4_MoveNext_m4846C74D5F4A4FF39164BCFD48198D976AA0F953_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m886FA065B7C64EA3A746502C9F6BF104B9801F07_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Uniforms__cctor_m7573D90B8FF2BCE1B1C8A02911B76BB8C140F972_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WaterHoseParticles_OnParticleCollision_m8E8399822017058F9AE64E3D9F7B47208553ED70_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WaterHoseParticles_Start_m6CFC5FEBF60CBD00CDE17E0A1EEC12163EC9A792_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WaterHoseParticles__ctor_m9E63BF105FF8511932CBF7FAA1C664713744DCA3_MetadataUsageId;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
struct AudioClipU5BU5D_t03931BD44BC339329210676E452B8ECD3EC171C2;
struct ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252;
struct ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399;
struct ParticleCollisionEventU5BU5D_t771CB4A499CC97F3340C673D227DD1F5969EB9B9;
struct ParticleSystemU5BU5D_t58EE604F685D8CBA8EFC9353456969F5A1B2FBB9;
struct TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804;
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6;
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tF157A75827DFDE1F9E89CA3CBB54B07FA9E227FC 
{
public:

public:
};


// System.Object


// SleekRender.SleekRenderPostProcess_Keywords
struct  Keywords_t4EED8C6C937B2D0F9B5FF4EE5C1EFF224B31CBF3  : public RuntimeObject
{
public:

public:
};


// SleekRender.SleekRenderPostProcess_Uniforms
struct  Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_StaticFields
{
public:
	// System.Int32 SleekRender.SleekRenderPostProcess_Uniforms::_LuminanceConst
	int32_t ____LuminanceConst_0;
	// System.Int32 SleekRender.SleekRenderPostProcess_Uniforms::_BloomIntencity
	int32_t ____BloomIntencity_1;
	// System.Int32 SleekRender.SleekRenderPostProcess_Uniforms::_BloomTint
	int32_t ____BloomTint_2;
	// System.Int32 SleekRender.SleekRenderPostProcess_Uniforms::_MainTex
	int32_t ____MainTex_3;
	// System.Int32 SleekRender.SleekRenderPostProcess_Uniforms::_BloomTex
	int32_t ____BloomTex_4;
	// System.Int32 SleekRender.SleekRenderPostProcess_Uniforms::_PreComposeTex
	int32_t ____PreComposeTex_5;
	// System.Int32 SleekRender.SleekRenderPostProcess_Uniforms::_TexelSize
	int32_t ____TexelSize_6;
	// System.Int32 SleekRender.SleekRenderPostProcess_Uniforms::_Colorize
	int32_t ____Colorize_7;
	// System.Int32 SleekRender.SleekRenderPostProcess_Uniforms::_VignetteShape
	int32_t ____VignetteShape_8;
	// System.Int32 SleekRender.SleekRenderPostProcess_Uniforms::_VignetteColor
	int32_t ____VignetteColor_9;
	// System.Int32 SleekRender.SleekRenderPostProcess_Uniforms::_BrightnessContrast
	int32_t ____BrightnessContrast_10;

public:
	inline static int32_t get_offset_of__LuminanceConst_0() { return static_cast<int32_t>(offsetof(Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_StaticFields, ____LuminanceConst_0)); }
	inline int32_t get__LuminanceConst_0() const { return ____LuminanceConst_0; }
	inline int32_t* get_address_of__LuminanceConst_0() { return &____LuminanceConst_0; }
	inline void set__LuminanceConst_0(int32_t value)
	{
		____LuminanceConst_0 = value;
	}

	inline static int32_t get_offset_of__BloomIntencity_1() { return static_cast<int32_t>(offsetof(Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_StaticFields, ____BloomIntencity_1)); }
	inline int32_t get__BloomIntencity_1() const { return ____BloomIntencity_1; }
	inline int32_t* get_address_of__BloomIntencity_1() { return &____BloomIntencity_1; }
	inline void set__BloomIntencity_1(int32_t value)
	{
		____BloomIntencity_1 = value;
	}

	inline static int32_t get_offset_of__BloomTint_2() { return static_cast<int32_t>(offsetof(Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_StaticFields, ____BloomTint_2)); }
	inline int32_t get__BloomTint_2() const { return ____BloomTint_2; }
	inline int32_t* get_address_of__BloomTint_2() { return &____BloomTint_2; }
	inline void set__BloomTint_2(int32_t value)
	{
		____BloomTint_2 = value;
	}

	inline static int32_t get_offset_of__MainTex_3() { return static_cast<int32_t>(offsetof(Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_StaticFields, ____MainTex_3)); }
	inline int32_t get__MainTex_3() const { return ____MainTex_3; }
	inline int32_t* get_address_of__MainTex_3() { return &____MainTex_3; }
	inline void set__MainTex_3(int32_t value)
	{
		____MainTex_3 = value;
	}

	inline static int32_t get_offset_of__BloomTex_4() { return static_cast<int32_t>(offsetof(Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_StaticFields, ____BloomTex_4)); }
	inline int32_t get__BloomTex_4() const { return ____BloomTex_4; }
	inline int32_t* get_address_of__BloomTex_4() { return &____BloomTex_4; }
	inline void set__BloomTex_4(int32_t value)
	{
		____BloomTex_4 = value;
	}

	inline static int32_t get_offset_of__PreComposeTex_5() { return static_cast<int32_t>(offsetof(Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_StaticFields, ____PreComposeTex_5)); }
	inline int32_t get__PreComposeTex_5() const { return ____PreComposeTex_5; }
	inline int32_t* get_address_of__PreComposeTex_5() { return &____PreComposeTex_5; }
	inline void set__PreComposeTex_5(int32_t value)
	{
		____PreComposeTex_5 = value;
	}

	inline static int32_t get_offset_of__TexelSize_6() { return static_cast<int32_t>(offsetof(Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_StaticFields, ____TexelSize_6)); }
	inline int32_t get__TexelSize_6() const { return ____TexelSize_6; }
	inline int32_t* get_address_of__TexelSize_6() { return &____TexelSize_6; }
	inline void set__TexelSize_6(int32_t value)
	{
		____TexelSize_6 = value;
	}

	inline static int32_t get_offset_of__Colorize_7() { return static_cast<int32_t>(offsetof(Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_StaticFields, ____Colorize_7)); }
	inline int32_t get__Colorize_7() const { return ____Colorize_7; }
	inline int32_t* get_address_of__Colorize_7() { return &____Colorize_7; }
	inline void set__Colorize_7(int32_t value)
	{
		____Colorize_7 = value;
	}

	inline static int32_t get_offset_of__VignetteShape_8() { return static_cast<int32_t>(offsetof(Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_StaticFields, ____VignetteShape_8)); }
	inline int32_t get__VignetteShape_8() const { return ____VignetteShape_8; }
	inline int32_t* get_address_of__VignetteShape_8() { return &____VignetteShape_8; }
	inline void set__VignetteShape_8(int32_t value)
	{
		____VignetteShape_8 = value;
	}

	inline static int32_t get_offset_of__VignetteColor_9() { return static_cast<int32_t>(offsetof(Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_StaticFields, ____VignetteColor_9)); }
	inline int32_t get__VignetteColor_9() const { return ____VignetteColor_9; }
	inline int32_t* get_address_of__VignetteColor_9() { return &____VignetteColor_9; }
	inline void set__VignetteColor_9(int32_t value)
	{
		____VignetteColor_9 = value;
	}

	inline static int32_t get_offset_of__BrightnessContrast_10() { return static_cast<int32_t>(offsetof(Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_StaticFields, ____BrightnessContrast_10)); }
	inline int32_t get__BrightnessContrast_10() const { return ____BrightnessContrast_10; }
	inline int32_t* get_address_of__BrightnessContrast_10() { return &____BrightnessContrast_10; }
	inline void set__BrightnessContrast_10(int32_t value)
	{
		____BrightnessContrast_10 = value;
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.Collections.Generic.List`1<UnityEngine.ParticleCollisionEvent>
struct  List_1_t2762C811E470D336E31761384C6E5382164DA4C7  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ParticleCollisionEventU5BU5D_t771CB4A499CC97F3340C673D227DD1F5969EB9B9* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2762C811E470D336E31761384C6E5382164DA4C7, ____items_1)); }
	inline ParticleCollisionEventU5BU5D_t771CB4A499CC97F3340C673D227DD1F5969EB9B9* get__items_1() const { return ____items_1; }
	inline ParticleCollisionEventU5BU5D_t771CB4A499CC97F3340C673D227DD1F5969EB9B9** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ParticleCollisionEventU5BU5D_t771CB4A499CC97F3340C673D227DD1F5969EB9B9* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2762C811E470D336E31761384C6E5382164DA4C7, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2762C811E470D336E31761384C6E5382164DA4C7, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t2762C811E470D336E31761384C6E5382164DA4C7, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t2762C811E470D336E31761384C6E5382164DA4C7_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ParticleCollisionEventU5BU5D_t771CB4A499CC97F3340C673D227DD1F5969EB9B9* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t2762C811E470D336E31761384C6E5382164DA4C7_StaticFields, ____emptyArray_5)); }
	inline ParticleCollisionEventU5BU5D_t771CB4A499CC97F3340C673D227DD1F5969EB9B9* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ParticleCollisionEventU5BU5D_t771CB4A499CC97F3340C673D227DD1F5969EB9B9** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ParticleCollisionEventU5BU5D_t771CB4A499CC97F3340C673D227DD1F5969EB9B9* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// UnityStandardAssets.Effects.ExplosionFireAndDebris_<Start>d__4
struct  U3CStartU3Ed__4_t945A707CD8C01DA1C37AE2DD9D0A904C80F58139  : public RuntimeObject
{
public:
	// System.Int32 UnityStandardAssets.Effects.ExplosionFireAndDebris_<Start>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityStandardAssets.Effects.ExplosionFireAndDebris_<Start>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityStandardAssets.Effects.ExplosionFireAndDebris UnityStandardAssets.Effects.ExplosionFireAndDebris_<Start>d__4::<>4__this
	ExplosionFireAndDebris_tACCFCC1184BB9A8C3A0B39D99FFC831E76F9EECB * ___U3CU3E4__this_2;
	// System.Single UnityStandardAssets.Effects.ExplosionFireAndDebris_<Start>d__4::<multiplier>5__2
	float ___U3CmultiplierU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t945A707CD8C01DA1C37AE2DD9D0A904C80F58139, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t945A707CD8C01DA1C37AE2DD9D0A904C80F58139, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t945A707CD8C01DA1C37AE2DD9D0A904C80F58139, ___U3CU3E4__this_2)); }
	inline ExplosionFireAndDebris_tACCFCC1184BB9A8C3A0B39D99FFC831E76F9EECB * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ExplosionFireAndDebris_tACCFCC1184BB9A8C3A0B39D99FFC831E76F9EECB ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ExplosionFireAndDebris_tACCFCC1184BB9A8C3A0B39D99FFC831E76F9EECB * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CmultiplierU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t945A707CD8C01DA1C37AE2DD9D0A904C80F58139, ___U3CmultiplierU3E5__2_3)); }
	inline float get_U3CmultiplierU3E5__2_3() const { return ___U3CmultiplierU3E5__2_3; }
	inline float* get_address_of_U3CmultiplierU3E5__2_3() { return &___U3CmultiplierU3E5__2_3; }
	inline void set_U3CmultiplierU3E5__2_3(float value)
	{
		___U3CmultiplierU3E5__2_3 = value;
	}
};


// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D24
struct  __StaticArrayInitTypeSizeU3D24_t58F71E5673095A266DDE698326B19494A677B10D 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D24_t58F71E5673095A266DDE698326B19494A677B10D__padding[24];
	};

public:
};


// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// UnityEngine.Color32
struct  Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};


// UnityEngine.Matrix4x4
struct  Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___identityMatrix_17 = value;
	}
};


// UnityEngine.ParticleSystem_EmissionModule
struct  EmissionModule_t35028C3DE5EFDCE49E8A9732460617A56BD1D3F1 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem_EmissionModule::m_ParticleSystem
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(EmissionModule_t35028C3DE5EFDCE49E8A9732460617A56BD1D3F1, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParticleSystem_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/EmissionModule
struct EmissionModule_t35028C3DE5EFDCE49E8A9732460617A56BD1D3F1_marshaled_pinvoke
{
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/EmissionModule
struct EmissionModule_t35028C3DE5EFDCE49E8A9732460617A56BD1D3F1_marshaled_com
{
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___m_ParticleSystem_0;
};

// UnityEngine.ParticleSystem_MainModule
struct  MainModule_t99C675667E0A363368324132DFA34B27FFEE6FC7 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem_MainModule::m_ParticleSystem
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(MainModule_t99C675667E0A363368324132DFA34B27FFEE6FC7, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParticleSystem_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/MainModule
struct MainModule_t99C675667E0A363368324132DFA34B27FFEE6FC7_marshaled_pinvoke
{
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/MainModule
struct MainModule_t99C675667E0A363368324132DFA34B27FFEE6FC7_marshaled_com
{
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___m_ParticleSystem_0;
};

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};


// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.Vector4
struct  Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___zeroVector_5)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___oneVector_6)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___negativeInfinityVector_8 = value;
	}
};


// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t1462DEA80E3C0F6D30E594F33C4F5283DF75F28F  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t1462DEA80E3C0F6D30E594F33C4F5283DF75F28F_StaticFields
{
public:
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D24 <PrivateImplementationDetails>::23447D2BC3FF6984AB09F575BC63CFE460337394
	__StaticArrayInitTypeSizeU3D24_t58F71E5673095A266DDE698326B19494A677B10D  ___23447D2BC3FF6984AB09F575BC63CFE460337394_0;

public:
	inline static int32_t get_offset_of_U323447D2BC3FF6984AB09F575BC63CFE460337394_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1462DEA80E3C0F6D30E594F33C4F5283DF75F28F_StaticFields, ___23447D2BC3FF6984AB09F575BC63CFE460337394_0)); }
	inline __StaticArrayInitTypeSizeU3D24_t58F71E5673095A266DDE698326B19494A677B10D  get_U323447D2BC3FF6984AB09F575BC63CFE460337394_0() const { return ___23447D2BC3FF6984AB09F575BC63CFE460337394_0; }
	inline __StaticArrayInitTypeSizeU3D24_t58F71E5673095A266DDE698326B19494A677B10D * get_address_of_U323447D2BC3FF6984AB09F575BC63CFE460337394_0() { return &___23447D2BC3FF6984AB09F575BC63CFE460337394_0; }
	inline void set_U323447D2BC3FF6984AB09F575BC63CFE460337394_0(__StaticArrayInitTypeSizeU3D24_t58F71E5673095A266DDE698326B19494A677B10D  value)
	{
		___23447D2BC3FF6984AB09F575BC63CFE460337394_0 = value;
	}
};


// SleekRender.LumaVectorType
struct  LumaVectorType_t804437D73AE9CDFBFF642902BE3E346FC9A965F4 
{
public:
	// System.Int32 SleekRender.LumaVectorType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LumaVectorType_t804437D73AE9CDFBFF642902BE3E346FC9A965F4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	Il2CppSafeArray/*INT*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	Il2CppSafeArray/*INT*/* ___native_trace_ips_15;
};

// System.RuntimeFieldHandle
struct  RuntimeFieldHandle_t844BDF00E8E6FE69D9AEAA7657F09018B864F4EF 
{
public:
	// System.IntPtr System.RuntimeFieldHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeFieldHandle_t844BDF00E8E6FE69D9AEAA7657F09018B864F4EF, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.FilterMode
struct  FilterMode_t6590B4B0BAE2BBBCABA8E1E93FA07A052B3261AF 
{
public:
	// System.Int32 UnityEngine.FilterMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FilterMode_t6590B4B0BAE2BBBCABA8E1E93FA07A052B3261AF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.ForceMode
struct  ForceMode_t76188FF14D0038E184106555207A81218E97D0A5 
{
public:
	// System.Int32 UnityEngine.ForceMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ForceMode_t76188FF14D0038E184106555207A81218E97D0A5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.KeyCode
struct  KeyCode_tC93EA87C5A6901160B583ADFCD3EF6726570DC3C 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyCode_tC93EA87C5A6901160B583ADFCD3EF6726570DC3C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.ParticleCollisionEvent
struct  ParticleCollisionEvent_t7F4D7D5ACF8521671549D9328D4E2DDE480D8D47 
{
public:
	// UnityEngine.Vector3 UnityEngine.ParticleCollisionEvent::m_Intersection
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Intersection_0;
	// UnityEngine.Vector3 UnityEngine.ParticleCollisionEvent::m_Normal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Normal_1;
	// UnityEngine.Vector3 UnityEngine.ParticleCollisionEvent::m_Velocity
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Velocity_2;
	// System.Int32 UnityEngine.ParticleCollisionEvent::m_ColliderInstanceID
	int32_t ___m_ColliderInstanceID_3;

public:
	inline static int32_t get_offset_of_m_Intersection_0() { return static_cast<int32_t>(offsetof(ParticleCollisionEvent_t7F4D7D5ACF8521671549D9328D4E2DDE480D8D47, ___m_Intersection_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Intersection_0() const { return ___m_Intersection_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Intersection_0() { return &___m_Intersection_0; }
	inline void set_m_Intersection_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Intersection_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(ParticleCollisionEvent_t7F4D7D5ACF8521671549D9328D4E2DDE480D8D47, ___m_Normal_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_Velocity_2() { return static_cast<int32_t>(offsetof(ParticleCollisionEvent_t7F4D7D5ACF8521671549D9328D4E2DDE480D8D47, ___m_Velocity_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Velocity_2() const { return ___m_Velocity_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Velocity_2() { return &___m_Velocity_2; }
	inline void set_m_Velocity_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Velocity_2 = value;
	}

	inline static int32_t get_offset_of_m_ColliderInstanceID_3() { return static_cast<int32_t>(offsetof(ParticleCollisionEvent_t7F4D7D5ACF8521671549D9328D4E2DDE480D8D47, ___m_ColliderInstanceID_3)); }
	inline int32_t get_m_ColliderInstanceID_3() const { return ___m_ColliderInstanceID_3; }
	inline int32_t* get_address_of_m_ColliderInstanceID_3() { return &___m_ColliderInstanceID_3; }
	inline void set_m_ColliderInstanceID_3(int32_t value)
	{
		___m_ColliderInstanceID_3 = value;
	}
};


// UnityEngine.ParticleSystemCurveMode
struct  ParticleSystemCurveMode_tD8A2390BB482B39C0C0714F3DDE715386BC7D48D 
{
public:
	// System.Int32 UnityEngine.ParticleSystemCurveMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParticleSystemCurveMode_tD8A2390BB482B39C0C0714F3DDE715386BC7D48D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Ray
struct  Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2, ___m_Origin_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2, ___m_Direction_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Direction_1 = value;
	}
};


// UnityEngine.RaycastHit
struct  RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Normal_1;
	// System.UInt32 UnityEngine.RaycastHit::m_FaceID
	uint32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Point_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Normal_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_FaceID_2)); }
	inline uint32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline uint32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(uint32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_UV_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};


// UnityEngine.RenderTextureFormat
struct  RenderTextureFormat_t2AB1B77FBD247648292FBBE1182F12B5FC47AF85 
{
public:
	// System.Int32 UnityEngine.RenderTextureFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RenderTextureFormat_t2AB1B77FBD247648292FBBE1182F12B5FC47AF85, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Rendering.GraphicsDeviceType
struct  GraphicsDeviceType_tA87720B0E7A15371E70249FC9F4EAE7644015552 
{
public:
	// System.Int32 UnityEngine.Rendering.GraphicsDeviceType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GraphicsDeviceType_tA87720B0E7A15371E70249FC9F4EAE7644015552, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.SendMessageOptions
struct  SendMessageOptions_t4EA4645A7D0C4E0186BD7A984CDF4EE2C8F26250 
{
public:
	// System.Int32 UnityEngine.SendMessageOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SendMessageOptions_t4EA4645A7D0C4E0186BD7A984CDF4EE2C8F26250, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.TextureWrapMode
struct  TextureWrapMode_t8AC763BD80806A9175C6AA8D33D6BABAD83E950F 
{
public:
	// System.Int32 UnityEngine.TextureWrapMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureWrapMode_t8AC763BD80806A9175C6AA8D33D6BABAD83E950F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.SystemException
struct  SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782  : public Exception_t
{
public:

public:
};


// UnityEngine.AudioClip
struct  AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:
	// UnityEngine.AudioClip_PCMReaderCallback UnityEngine.AudioClip::m_PCMReaderCallback
	PCMReaderCallback_t9B87AB13DCD37957B045554BF28A57697E6B8EFB * ___m_PCMReaderCallback_4;
	// UnityEngine.AudioClip_PCMSetPositionCallback UnityEngine.AudioClip::m_PCMSetPositionCallback
	PCMSetPositionCallback_t092ED33043C0279B5E4D343EBCBD516CEF260801 * ___m_PCMSetPositionCallback_5;

public:
	inline static int32_t get_offset_of_m_PCMReaderCallback_4() { return static_cast<int32_t>(offsetof(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051, ___m_PCMReaderCallback_4)); }
	inline PCMReaderCallback_t9B87AB13DCD37957B045554BF28A57697E6B8EFB * get_m_PCMReaderCallback_4() const { return ___m_PCMReaderCallback_4; }
	inline PCMReaderCallback_t9B87AB13DCD37957B045554BF28A57697E6B8EFB ** get_address_of_m_PCMReaderCallback_4() { return &___m_PCMReaderCallback_4; }
	inline void set_m_PCMReaderCallback_4(PCMReaderCallback_t9B87AB13DCD37957B045554BF28A57697E6B8EFB * value)
	{
		___m_PCMReaderCallback_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PCMReaderCallback_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_PCMSetPositionCallback_5() { return static_cast<int32_t>(offsetof(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051, ___m_PCMSetPositionCallback_5)); }
	inline PCMSetPositionCallback_t092ED33043C0279B5E4D343EBCBD516CEF260801 * get_m_PCMSetPositionCallback_5() const { return ___m_PCMSetPositionCallback_5; }
	inline PCMSetPositionCallback_t092ED33043C0279B5E4D343EBCBD516CEF260801 ** get_address_of_m_PCMSetPositionCallback_5() { return &___m_PCMSetPositionCallback_5; }
	inline void set_m_PCMSetPositionCallback_5(PCMSetPositionCallback_t092ED33043C0279B5E4D343EBCBD516CEF260801 * value)
	{
		___m_PCMSetPositionCallback_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PCMSetPositionCallback_5), (void*)value);
	}
};


// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.GameObject
struct  GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.Material
struct  Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.Mesh
struct  Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.ParticleSystem_MinMaxCurve
struct  MinMaxCurve_tDB335EDEBEBD4CFA753081D7C3A2FE2EECFA6D71 
{
public:
	// UnityEngine.ParticleSystemCurveMode UnityEngine.ParticleSystem_MinMaxCurve::m_Mode
	int32_t ___m_Mode_0;
	// System.Single UnityEngine.ParticleSystem_MinMaxCurve::m_CurveMultiplier
	float ___m_CurveMultiplier_1;
	// UnityEngine.AnimationCurve UnityEngine.ParticleSystem_MinMaxCurve::m_CurveMin
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___m_CurveMin_2;
	// UnityEngine.AnimationCurve UnityEngine.ParticleSystem_MinMaxCurve::m_CurveMax
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___m_CurveMax_3;
	// System.Single UnityEngine.ParticleSystem_MinMaxCurve::m_ConstantMin
	float ___m_ConstantMin_4;
	// System.Single UnityEngine.ParticleSystem_MinMaxCurve::m_ConstantMax
	float ___m_ConstantMax_5;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(MinMaxCurve_tDB335EDEBEBD4CFA753081D7C3A2FE2EECFA6D71, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_CurveMultiplier_1() { return static_cast<int32_t>(offsetof(MinMaxCurve_tDB335EDEBEBD4CFA753081D7C3A2FE2EECFA6D71, ___m_CurveMultiplier_1)); }
	inline float get_m_CurveMultiplier_1() const { return ___m_CurveMultiplier_1; }
	inline float* get_address_of_m_CurveMultiplier_1() { return &___m_CurveMultiplier_1; }
	inline void set_m_CurveMultiplier_1(float value)
	{
		___m_CurveMultiplier_1 = value;
	}

	inline static int32_t get_offset_of_m_CurveMin_2() { return static_cast<int32_t>(offsetof(MinMaxCurve_tDB335EDEBEBD4CFA753081D7C3A2FE2EECFA6D71, ___m_CurveMin_2)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_m_CurveMin_2() const { return ___m_CurveMin_2; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_m_CurveMin_2() { return &___m_CurveMin_2; }
	inline void set_m_CurveMin_2(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___m_CurveMin_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CurveMin_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_CurveMax_3() { return static_cast<int32_t>(offsetof(MinMaxCurve_tDB335EDEBEBD4CFA753081D7C3A2FE2EECFA6D71, ___m_CurveMax_3)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_m_CurveMax_3() const { return ___m_CurveMax_3; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_m_CurveMax_3() { return &___m_CurveMax_3; }
	inline void set_m_CurveMax_3(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___m_CurveMax_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CurveMax_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_ConstantMin_4() { return static_cast<int32_t>(offsetof(MinMaxCurve_tDB335EDEBEBD4CFA753081D7C3A2FE2EECFA6D71, ___m_ConstantMin_4)); }
	inline float get_m_ConstantMin_4() const { return ___m_ConstantMin_4; }
	inline float* get_address_of_m_ConstantMin_4() { return &___m_ConstantMin_4; }
	inline void set_m_ConstantMin_4(float value)
	{
		___m_ConstantMin_4 = value;
	}

	inline static int32_t get_offset_of_m_ConstantMax_5() { return static_cast<int32_t>(offsetof(MinMaxCurve_tDB335EDEBEBD4CFA753081D7C3A2FE2EECFA6D71, ___m_ConstantMax_5)); }
	inline float get_m_ConstantMax_5() const { return ___m_ConstantMax_5; }
	inline float* get_address_of_m_ConstantMax_5() { return &___m_ConstantMax_5; }
	inline void set_m_ConstantMax_5(float value)
	{
		___m_ConstantMax_5 = value;
	}
};


// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};

// UnityEngine.Shader
struct  Shader_tE2731FF351B74AB4186897484FB01E000C1160CA  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.Texture
struct  Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

struct Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4_StaticFields
{
public:
	// System.Int32 UnityEngine.Texture::GenerateAllMips
	int32_t ___GenerateAllMips_4;

public:
	inline static int32_t get_offset_of_GenerateAllMips_4() { return static_cast<int32_t>(offsetof(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4_StaticFields, ___GenerateAllMips_4)); }
	inline int32_t get_GenerateAllMips_4() const { return ___GenerateAllMips_4; }
	inline int32_t* get_address_of_GenerateAllMips_4() { return &___GenerateAllMips_4; }
	inline void set_GenerateAllMips_4(int32_t value)
	{
		___GenerateAllMips_4 = value;
	}
};


// SleekRender.SleekRenderSettings
struct  SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Boolean SleekRender.SleekRenderSettings::bloomExpanded
	bool ___bloomExpanded_4;
	// System.Boolean SleekRender.SleekRenderSettings::bloomEnabled
	bool ___bloomEnabled_5;
	// System.Single SleekRender.SleekRenderSettings::bloomThreshold
	float ___bloomThreshold_6;
	// System.Single SleekRender.SleekRenderSettings::bloomIntensity
	float ___bloomIntensity_7;
	// UnityEngine.Color SleekRender.SleekRenderSettings::bloomTint
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___bloomTint_8;
	// System.Boolean SleekRender.SleekRenderSettings::preserveAspectRatio
	bool ___preserveAspectRatio_9;
	// System.Int32 SleekRender.SleekRenderSettings::bloomTextureWidth
	int32_t ___bloomTextureWidth_10;
	// System.Int32 SleekRender.SleekRenderSettings::bloomTextureHeight
	int32_t ___bloomTextureHeight_11;
	// SleekRender.LumaVectorType SleekRender.SleekRenderSettings::bloomLumaCalculationType
	int32_t ___bloomLumaCalculationType_12;
	// UnityEngine.Vector3 SleekRender.SleekRenderSettings::bloomLumaVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bloomLumaVector_13;
	// System.Boolean SleekRender.SleekRenderSettings::colorizeExpanded
	bool ___colorizeExpanded_14;
	// System.Boolean SleekRender.SleekRenderSettings::colorizeEnabled
	bool ___colorizeEnabled_15;
	// UnityEngine.Color32 SleekRender.SleekRenderSettings::colorize
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___colorize_16;
	// System.Boolean SleekRender.SleekRenderSettings::vignetteExpanded
	bool ___vignetteExpanded_17;
	// System.Boolean SleekRender.SleekRenderSettings::vignetteEnabled
	bool ___vignetteEnabled_18;
	// System.Single SleekRender.SleekRenderSettings::vignetteBeginRadius
	float ___vignetteBeginRadius_19;
	// System.Single SleekRender.SleekRenderSettings::vignetteExpandRadius
	float ___vignetteExpandRadius_20;
	// UnityEngine.Color SleekRender.SleekRenderSettings::vignetteColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___vignetteColor_21;
	// System.Boolean SleekRender.SleekRenderSettings::brightnessContrastExpanded
	bool ___brightnessContrastExpanded_22;
	// System.Boolean SleekRender.SleekRenderSettings::brightnessContrastEnabled
	bool ___brightnessContrastEnabled_23;
	// System.Single SleekRender.SleekRenderSettings::contrast
	float ___contrast_24;
	// System.Single SleekRender.SleekRenderSettings::brightness
	float ___brightness_25;

public:
	inline static int32_t get_offset_of_bloomExpanded_4() { return static_cast<int32_t>(offsetof(SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142, ___bloomExpanded_4)); }
	inline bool get_bloomExpanded_4() const { return ___bloomExpanded_4; }
	inline bool* get_address_of_bloomExpanded_4() { return &___bloomExpanded_4; }
	inline void set_bloomExpanded_4(bool value)
	{
		___bloomExpanded_4 = value;
	}

	inline static int32_t get_offset_of_bloomEnabled_5() { return static_cast<int32_t>(offsetof(SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142, ___bloomEnabled_5)); }
	inline bool get_bloomEnabled_5() const { return ___bloomEnabled_5; }
	inline bool* get_address_of_bloomEnabled_5() { return &___bloomEnabled_5; }
	inline void set_bloomEnabled_5(bool value)
	{
		___bloomEnabled_5 = value;
	}

	inline static int32_t get_offset_of_bloomThreshold_6() { return static_cast<int32_t>(offsetof(SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142, ___bloomThreshold_6)); }
	inline float get_bloomThreshold_6() const { return ___bloomThreshold_6; }
	inline float* get_address_of_bloomThreshold_6() { return &___bloomThreshold_6; }
	inline void set_bloomThreshold_6(float value)
	{
		___bloomThreshold_6 = value;
	}

	inline static int32_t get_offset_of_bloomIntensity_7() { return static_cast<int32_t>(offsetof(SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142, ___bloomIntensity_7)); }
	inline float get_bloomIntensity_7() const { return ___bloomIntensity_7; }
	inline float* get_address_of_bloomIntensity_7() { return &___bloomIntensity_7; }
	inline void set_bloomIntensity_7(float value)
	{
		___bloomIntensity_7 = value;
	}

	inline static int32_t get_offset_of_bloomTint_8() { return static_cast<int32_t>(offsetof(SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142, ___bloomTint_8)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_bloomTint_8() const { return ___bloomTint_8; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_bloomTint_8() { return &___bloomTint_8; }
	inline void set_bloomTint_8(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___bloomTint_8 = value;
	}

	inline static int32_t get_offset_of_preserveAspectRatio_9() { return static_cast<int32_t>(offsetof(SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142, ___preserveAspectRatio_9)); }
	inline bool get_preserveAspectRatio_9() const { return ___preserveAspectRatio_9; }
	inline bool* get_address_of_preserveAspectRatio_9() { return &___preserveAspectRatio_9; }
	inline void set_preserveAspectRatio_9(bool value)
	{
		___preserveAspectRatio_9 = value;
	}

	inline static int32_t get_offset_of_bloomTextureWidth_10() { return static_cast<int32_t>(offsetof(SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142, ___bloomTextureWidth_10)); }
	inline int32_t get_bloomTextureWidth_10() const { return ___bloomTextureWidth_10; }
	inline int32_t* get_address_of_bloomTextureWidth_10() { return &___bloomTextureWidth_10; }
	inline void set_bloomTextureWidth_10(int32_t value)
	{
		___bloomTextureWidth_10 = value;
	}

	inline static int32_t get_offset_of_bloomTextureHeight_11() { return static_cast<int32_t>(offsetof(SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142, ___bloomTextureHeight_11)); }
	inline int32_t get_bloomTextureHeight_11() const { return ___bloomTextureHeight_11; }
	inline int32_t* get_address_of_bloomTextureHeight_11() { return &___bloomTextureHeight_11; }
	inline void set_bloomTextureHeight_11(int32_t value)
	{
		___bloomTextureHeight_11 = value;
	}

	inline static int32_t get_offset_of_bloomLumaCalculationType_12() { return static_cast<int32_t>(offsetof(SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142, ___bloomLumaCalculationType_12)); }
	inline int32_t get_bloomLumaCalculationType_12() const { return ___bloomLumaCalculationType_12; }
	inline int32_t* get_address_of_bloomLumaCalculationType_12() { return &___bloomLumaCalculationType_12; }
	inline void set_bloomLumaCalculationType_12(int32_t value)
	{
		___bloomLumaCalculationType_12 = value;
	}

	inline static int32_t get_offset_of_bloomLumaVector_13() { return static_cast<int32_t>(offsetof(SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142, ___bloomLumaVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_bloomLumaVector_13() const { return ___bloomLumaVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_bloomLumaVector_13() { return &___bloomLumaVector_13; }
	inline void set_bloomLumaVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___bloomLumaVector_13 = value;
	}

	inline static int32_t get_offset_of_colorizeExpanded_14() { return static_cast<int32_t>(offsetof(SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142, ___colorizeExpanded_14)); }
	inline bool get_colorizeExpanded_14() const { return ___colorizeExpanded_14; }
	inline bool* get_address_of_colorizeExpanded_14() { return &___colorizeExpanded_14; }
	inline void set_colorizeExpanded_14(bool value)
	{
		___colorizeExpanded_14 = value;
	}

	inline static int32_t get_offset_of_colorizeEnabled_15() { return static_cast<int32_t>(offsetof(SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142, ___colorizeEnabled_15)); }
	inline bool get_colorizeEnabled_15() const { return ___colorizeEnabled_15; }
	inline bool* get_address_of_colorizeEnabled_15() { return &___colorizeEnabled_15; }
	inline void set_colorizeEnabled_15(bool value)
	{
		___colorizeEnabled_15 = value;
	}

	inline static int32_t get_offset_of_colorize_16() { return static_cast<int32_t>(offsetof(SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142, ___colorize_16)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_colorize_16() const { return ___colorize_16; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_colorize_16() { return &___colorize_16; }
	inline void set_colorize_16(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___colorize_16 = value;
	}

	inline static int32_t get_offset_of_vignetteExpanded_17() { return static_cast<int32_t>(offsetof(SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142, ___vignetteExpanded_17)); }
	inline bool get_vignetteExpanded_17() const { return ___vignetteExpanded_17; }
	inline bool* get_address_of_vignetteExpanded_17() { return &___vignetteExpanded_17; }
	inline void set_vignetteExpanded_17(bool value)
	{
		___vignetteExpanded_17 = value;
	}

	inline static int32_t get_offset_of_vignetteEnabled_18() { return static_cast<int32_t>(offsetof(SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142, ___vignetteEnabled_18)); }
	inline bool get_vignetteEnabled_18() const { return ___vignetteEnabled_18; }
	inline bool* get_address_of_vignetteEnabled_18() { return &___vignetteEnabled_18; }
	inline void set_vignetteEnabled_18(bool value)
	{
		___vignetteEnabled_18 = value;
	}

	inline static int32_t get_offset_of_vignetteBeginRadius_19() { return static_cast<int32_t>(offsetof(SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142, ___vignetteBeginRadius_19)); }
	inline float get_vignetteBeginRadius_19() const { return ___vignetteBeginRadius_19; }
	inline float* get_address_of_vignetteBeginRadius_19() { return &___vignetteBeginRadius_19; }
	inline void set_vignetteBeginRadius_19(float value)
	{
		___vignetteBeginRadius_19 = value;
	}

	inline static int32_t get_offset_of_vignetteExpandRadius_20() { return static_cast<int32_t>(offsetof(SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142, ___vignetteExpandRadius_20)); }
	inline float get_vignetteExpandRadius_20() const { return ___vignetteExpandRadius_20; }
	inline float* get_address_of_vignetteExpandRadius_20() { return &___vignetteExpandRadius_20; }
	inline void set_vignetteExpandRadius_20(float value)
	{
		___vignetteExpandRadius_20 = value;
	}

	inline static int32_t get_offset_of_vignetteColor_21() { return static_cast<int32_t>(offsetof(SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142, ___vignetteColor_21)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_vignetteColor_21() const { return ___vignetteColor_21; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_vignetteColor_21() { return &___vignetteColor_21; }
	inline void set_vignetteColor_21(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___vignetteColor_21 = value;
	}

	inline static int32_t get_offset_of_brightnessContrastExpanded_22() { return static_cast<int32_t>(offsetof(SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142, ___brightnessContrastExpanded_22)); }
	inline bool get_brightnessContrastExpanded_22() const { return ___brightnessContrastExpanded_22; }
	inline bool* get_address_of_brightnessContrastExpanded_22() { return &___brightnessContrastExpanded_22; }
	inline void set_brightnessContrastExpanded_22(bool value)
	{
		___brightnessContrastExpanded_22 = value;
	}

	inline static int32_t get_offset_of_brightnessContrastEnabled_23() { return static_cast<int32_t>(offsetof(SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142, ___brightnessContrastEnabled_23)); }
	inline bool get_brightnessContrastEnabled_23() const { return ___brightnessContrastEnabled_23; }
	inline bool* get_address_of_brightnessContrastEnabled_23() { return &___brightnessContrastEnabled_23; }
	inline void set_brightnessContrastEnabled_23(bool value)
	{
		___brightnessContrastEnabled_23 = value;
	}

	inline static int32_t get_offset_of_contrast_24() { return static_cast<int32_t>(offsetof(SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142, ___contrast_24)); }
	inline float get_contrast_24() const { return ___contrast_24; }
	inline float* get_address_of_contrast_24() { return &___contrast_24; }
	inline void set_contrast_24(float value)
	{
		___contrast_24 = value;
	}

	inline static int32_t get_offset_of_brightness_25() { return static_cast<int32_t>(offsetof(SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142, ___brightness_25)); }
	inline float get_brightness_25() const { return ___brightness_25; }
	inline float* get_address_of_brightness_25() { return &___brightness_25; }
	inline void set_brightness_25(float value)
	{
		___brightness_25 = value;
	}
};


// System.NotSupportedException
struct  NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:

public:
};


// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Collider
struct  Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.ParticleSystem
struct  ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.RenderTexture
struct  RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6  : public Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4
{
public:

public:
};


// UnityEngine.Renderer
struct  Renderer_t0556D67DD582620D1F495627EDE30D03284151F4  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Rigidbody
struct  Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Transform
struct  Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.AudioBehaviour
struct  AudioBehaviour_tC612EC4E17A648A5C568621F3FBF1DBD773C71C7  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};


// UnityEngine.Camera
struct  Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields
{
public:
	// UnityEngine.Camera_CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * ___onPreCull_4;
	// UnityEngine.Camera_CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * ___onPreRender_5;
	// UnityEngine.Camera_CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * ___onPostRender_6;

public:
	inline static int32_t get_offset_of_onPreCull_4() { return static_cast<int32_t>(offsetof(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields, ___onPreCull_4)); }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * get_onPreCull_4() const { return ___onPreCull_4; }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 ** get_address_of_onPreCull_4() { return &___onPreCull_4; }
	inline void set_onPreCull_4(CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * value)
	{
		___onPreCull_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreCull_4), (void*)value);
	}

	inline static int32_t get_offset_of_onPreRender_5() { return static_cast<int32_t>(offsetof(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields, ___onPreRender_5)); }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * get_onPreRender_5() const { return ___onPreRender_5; }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 ** get_address_of_onPreRender_5() { return &___onPreRender_5; }
	inline void set_onPreRender_5(CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * value)
	{
		___onPreRender_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreRender_5), (void*)value);
	}

	inline static int32_t get_offset_of_onPostRender_6() { return static_cast<int32_t>(offsetof(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields, ___onPostRender_6)); }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * get_onPostRender_6() const { return ___onPostRender_6; }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 ** get_address_of_onPostRender_6() { return &___onPostRender_6; }
	inline void set_onPostRender_6(CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * value)
	{
		___onPostRender_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPostRender_6), (void*)value);
	}
};


// UnityEngine.Light
struct  Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:
	// System.Int32 UnityEngine.Light::m_BakedIndex
	int32_t ___m_BakedIndex_4;

public:
	inline static int32_t get_offset_of_m_BakedIndex_4() { return static_cast<int32_t>(offsetof(Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C, ___m_BakedIndex_4)); }
	inline int32_t get_m_BakedIndex_4() const { return ___m_BakedIndex_4; }
	inline int32_t* get_address_of_m_BakedIndex_4() { return &___m_BakedIndex_4; }
	inline void set_m_BakedIndex_4(int32_t value)
	{
		___m_BakedIndex_4 = value;
	}
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};


// UnityEngine.SphereCollider
struct  SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F  : public Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF
{
public:

public:
};


// SleekRender.SleekRenderPostProcess
struct  SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// SleekRender.SleekRenderSettings SleekRender.SleekRenderPostProcess::settings
	SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142 * ___settings_4;
	// UnityEngine.Material SleekRender.SleekRenderPostProcess::_downsampleMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ____downsampleMaterial_5;
	// UnityEngine.Material SleekRender.SleekRenderPostProcess::_horizontalBlurMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ____horizontalBlurMaterial_6;
	// UnityEngine.Material SleekRender.SleekRenderPostProcess::_verticalBlurMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ____verticalBlurMaterial_7;
	// UnityEngine.Material SleekRender.SleekRenderPostProcess::_preComposeMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ____preComposeMaterial_8;
	// UnityEngine.Material SleekRender.SleekRenderPostProcess::_composeMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ____composeMaterial_9;
	// UnityEngine.RenderTexture SleekRender.SleekRenderPostProcess::_downsampledBrightpassTexture
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ____downsampledBrightpassTexture_10;
	// UnityEngine.RenderTexture SleekRender.SleekRenderPostProcess::_brightPassBlurTexture
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ____brightPassBlurTexture_11;
	// UnityEngine.RenderTexture SleekRender.SleekRenderPostProcess::_horizontalBlurTexture
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ____horizontalBlurTexture_12;
	// UnityEngine.RenderTexture SleekRender.SleekRenderPostProcess::_verticalBlurTexture
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ____verticalBlurTexture_13;
	// UnityEngine.RenderTexture SleekRender.SleekRenderPostProcess::_preComposeTexture
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ____preComposeTexture_14;
	// UnityEngine.Camera SleekRender.SleekRenderPostProcess::_mainCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ____mainCamera_15;
	// UnityEngine.Mesh SleekRender.SleekRenderPostProcess::_fullscreenQuadMesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ____fullscreenQuadMesh_16;
	// System.Int32 SleekRender.SleekRenderPostProcess::_currentCameraPixelWidth
	int32_t ____currentCameraPixelWidth_17;
	// System.Int32 SleekRender.SleekRenderPostProcess::_currentCameraPixelHeight
	int32_t ____currentCameraPixelHeight_18;
	// System.Boolean SleekRender.SleekRenderPostProcess::_isColorizeAlreadyEnabled
	bool ____isColorizeAlreadyEnabled_19;
	// System.Boolean SleekRender.SleekRenderPostProcess::_isBloomAlreadyEnabled
	bool ____isBloomAlreadyEnabled_20;
	// System.Boolean SleekRender.SleekRenderPostProcess::_isVignetteAlreadyEnabled
	bool ____isVignetteAlreadyEnabled_21;
	// System.Boolean SleekRender.SleekRenderPostProcess::_isAlreadyPreservingAspectRatio
	bool ____isAlreadyPreservingAspectRatio_22;
	// System.Boolean SleekRender.SleekRenderPostProcess::_isContrastAndBrightnessAlreadyEnabled
	bool ____isContrastAndBrightnessAlreadyEnabled_23;
	// UnityEngine.RenderTexture SleekRender.SleekRenderPostProcess::_rt
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ____rt_24;
	// UnityEngine.RenderTexture SleekRender.SleekRenderPostProcess::_rtTarget
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ____rtTarget_25;

public:
	inline static int32_t get_offset_of_settings_4() { return static_cast<int32_t>(offsetof(SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94, ___settings_4)); }
	inline SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142 * get_settings_4() const { return ___settings_4; }
	inline SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142 ** get_address_of_settings_4() { return &___settings_4; }
	inline void set_settings_4(SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142 * value)
	{
		___settings_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___settings_4), (void*)value);
	}

	inline static int32_t get_offset_of__downsampleMaterial_5() { return static_cast<int32_t>(offsetof(SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94, ____downsampleMaterial_5)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get__downsampleMaterial_5() const { return ____downsampleMaterial_5; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of__downsampleMaterial_5() { return &____downsampleMaterial_5; }
	inline void set__downsampleMaterial_5(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		____downsampleMaterial_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____downsampleMaterial_5), (void*)value);
	}

	inline static int32_t get_offset_of__horizontalBlurMaterial_6() { return static_cast<int32_t>(offsetof(SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94, ____horizontalBlurMaterial_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get__horizontalBlurMaterial_6() const { return ____horizontalBlurMaterial_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of__horizontalBlurMaterial_6() { return &____horizontalBlurMaterial_6; }
	inline void set__horizontalBlurMaterial_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		____horizontalBlurMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____horizontalBlurMaterial_6), (void*)value);
	}

	inline static int32_t get_offset_of__verticalBlurMaterial_7() { return static_cast<int32_t>(offsetof(SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94, ____verticalBlurMaterial_7)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get__verticalBlurMaterial_7() const { return ____verticalBlurMaterial_7; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of__verticalBlurMaterial_7() { return &____verticalBlurMaterial_7; }
	inline void set__verticalBlurMaterial_7(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		____verticalBlurMaterial_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____verticalBlurMaterial_7), (void*)value);
	}

	inline static int32_t get_offset_of__preComposeMaterial_8() { return static_cast<int32_t>(offsetof(SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94, ____preComposeMaterial_8)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get__preComposeMaterial_8() const { return ____preComposeMaterial_8; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of__preComposeMaterial_8() { return &____preComposeMaterial_8; }
	inline void set__preComposeMaterial_8(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		____preComposeMaterial_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____preComposeMaterial_8), (void*)value);
	}

	inline static int32_t get_offset_of__composeMaterial_9() { return static_cast<int32_t>(offsetof(SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94, ____composeMaterial_9)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get__composeMaterial_9() const { return ____composeMaterial_9; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of__composeMaterial_9() { return &____composeMaterial_9; }
	inline void set__composeMaterial_9(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		____composeMaterial_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____composeMaterial_9), (void*)value);
	}

	inline static int32_t get_offset_of__downsampledBrightpassTexture_10() { return static_cast<int32_t>(offsetof(SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94, ____downsampledBrightpassTexture_10)); }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * get__downsampledBrightpassTexture_10() const { return ____downsampledBrightpassTexture_10; }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 ** get_address_of__downsampledBrightpassTexture_10() { return &____downsampledBrightpassTexture_10; }
	inline void set__downsampledBrightpassTexture_10(RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * value)
	{
		____downsampledBrightpassTexture_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____downsampledBrightpassTexture_10), (void*)value);
	}

	inline static int32_t get_offset_of__brightPassBlurTexture_11() { return static_cast<int32_t>(offsetof(SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94, ____brightPassBlurTexture_11)); }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * get__brightPassBlurTexture_11() const { return ____brightPassBlurTexture_11; }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 ** get_address_of__brightPassBlurTexture_11() { return &____brightPassBlurTexture_11; }
	inline void set__brightPassBlurTexture_11(RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * value)
	{
		____brightPassBlurTexture_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____brightPassBlurTexture_11), (void*)value);
	}

	inline static int32_t get_offset_of__horizontalBlurTexture_12() { return static_cast<int32_t>(offsetof(SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94, ____horizontalBlurTexture_12)); }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * get__horizontalBlurTexture_12() const { return ____horizontalBlurTexture_12; }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 ** get_address_of__horizontalBlurTexture_12() { return &____horizontalBlurTexture_12; }
	inline void set__horizontalBlurTexture_12(RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * value)
	{
		____horizontalBlurTexture_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____horizontalBlurTexture_12), (void*)value);
	}

	inline static int32_t get_offset_of__verticalBlurTexture_13() { return static_cast<int32_t>(offsetof(SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94, ____verticalBlurTexture_13)); }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * get__verticalBlurTexture_13() const { return ____verticalBlurTexture_13; }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 ** get_address_of__verticalBlurTexture_13() { return &____verticalBlurTexture_13; }
	inline void set__verticalBlurTexture_13(RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * value)
	{
		____verticalBlurTexture_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____verticalBlurTexture_13), (void*)value);
	}

	inline static int32_t get_offset_of__preComposeTexture_14() { return static_cast<int32_t>(offsetof(SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94, ____preComposeTexture_14)); }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * get__preComposeTexture_14() const { return ____preComposeTexture_14; }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 ** get_address_of__preComposeTexture_14() { return &____preComposeTexture_14; }
	inline void set__preComposeTexture_14(RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * value)
	{
		____preComposeTexture_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____preComposeTexture_14), (void*)value);
	}

	inline static int32_t get_offset_of__mainCamera_15() { return static_cast<int32_t>(offsetof(SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94, ____mainCamera_15)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get__mainCamera_15() const { return ____mainCamera_15; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of__mainCamera_15() { return &____mainCamera_15; }
	inline void set__mainCamera_15(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		____mainCamera_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____mainCamera_15), (void*)value);
	}

	inline static int32_t get_offset_of__fullscreenQuadMesh_16() { return static_cast<int32_t>(offsetof(SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94, ____fullscreenQuadMesh_16)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get__fullscreenQuadMesh_16() const { return ____fullscreenQuadMesh_16; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of__fullscreenQuadMesh_16() { return &____fullscreenQuadMesh_16; }
	inline void set__fullscreenQuadMesh_16(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		____fullscreenQuadMesh_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____fullscreenQuadMesh_16), (void*)value);
	}

	inline static int32_t get_offset_of__currentCameraPixelWidth_17() { return static_cast<int32_t>(offsetof(SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94, ____currentCameraPixelWidth_17)); }
	inline int32_t get__currentCameraPixelWidth_17() const { return ____currentCameraPixelWidth_17; }
	inline int32_t* get_address_of__currentCameraPixelWidth_17() { return &____currentCameraPixelWidth_17; }
	inline void set__currentCameraPixelWidth_17(int32_t value)
	{
		____currentCameraPixelWidth_17 = value;
	}

	inline static int32_t get_offset_of__currentCameraPixelHeight_18() { return static_cast<int32_t>(offsetof(SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94, ____currentCameraPixelHeight_18)); }
	inline int32_t get__currentCameraPixelHeight_18() const { return ____currentCameraPixelHeight_18; }
	inline int32_t* get_address_of__currentCameraPixelHeight_18() { return &____currentCameraPixelHeight_18; }
	inline void set__currentCameraPixelHeight_18(int32_t value)
	{
		____currentCameraPixelHeight_18 = value;
	}

	inline static int32_t get_offset_of__isColorizeAlreadyEnabled_19() { return static_cast<int32_t>(offsetof(SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94, ____isColorizeAlreadyEnabled_19)); }
	inline bool get__isColorizeAlreadyEnabled_19() const { return ____isColorizeAlreadyEnabled_19; }
	inline bool* get_address_of__isColorizeAlreadyEnabled_19() { return &____isColorizeAlreadyEnabled_19; }
	inline void set__isColorizeAlreadyEnabled_19(bool value)
	{
		____isColorizeAlreadyEnabled_19 = value;
	}

	inline static int32_t get_offset_of__isBloomAlreadyEnabled_20() { return static_cast<int32_t>(offsetof(SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94, ____isBloomAlreadyEnabled_20)); }
	inline bool get__isBloomAlreadyEnabled_20() const { return ____isBloomAlreadyEnabled_20; }
	inline bool* get_address_of__isBloomAlreadyEnabled_20() { return &____isBloomAlreadyEnabled_20; }
	inline void set__isBloomAlreadyEnabled_20(bool value)
	{
		____isBloomAlreadyEnabled_20 = value;
	}

	inline static int32_t get_offset_of__isVignetteAlreadyEnabled_21() { return static_cast<int32_t>(offsetof(SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94, ____isVignetteAlreadyEnabled_21)); }
	inline bool get__isVignetteAlreadyEnabled_21() const { return ____isVignetteAlreadyEnabled_21; }
	inline bool* get_address_of__isVignetteAlreadyEnabled_21() { return &____isVignetteAlreadyEnabled_21; }
	inline void set__isVignetteAlreadyEnabled_21(bool value)
	{
		____isVignetteAlreadyEnabled_21 = value;
	}

	inline static int32_t get_offset_of__isAlreadyPreservingAspectRatio_22() { return static_cast<int32_t>(offsetof(SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94, ____isAlreadyPreservingAspectRatio_22)); }
	inline bool get__isAlreadyPreservingAspectRatio_22() const { return ____isAlreadyPreservingAspectRatio_22; }
	inline bool* get_address_of__isAlreadyPreservingAspectRatio_22() { return &____isAlreadyPreservingAspectRatio_22; }
	inline void set__isAlreadyPreservingAspectRatio_22(bool value)
	{
		____isAlreadyPreservingAspectRatio_22 = value;
	}

	inline static int32_t get_offset_of__isContrastAndBrightnessAlreadyEnabled_23() { return static_cast<int32_t>(offsetof(SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94, ____isContrastAndBrightnessAlreadyEnabled_23)); }
	inline bool get__isContrastAndBrightnessAlreadyEnabled_23() const { return ____isContrastAndBrightnessAlreadyEnabled_23; }
	inline bool* get_address_of__isContrastAndBrightnessAlreadyEnabled_23() { return &____isContrastAndBrightnessAlreadyEnabled_23; }
	inline void set__isContrastAndBrightnessAlreadyEnabled_23(bool value)
	{
		____isContrastAndBrightnessAlreadyEnabled_23 = value;
	}

	inline static int32_t get_offset_of__rt_24() { return static_cast<int32_t>(offsetof(SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94, ____rt_24)); }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * get__rt_24() const { return ____rt_24; }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 ** get_address_of__rt_24() { return &____rt_24; }
	inline void set__rt_24(RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * value)
	{
		____rt_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rt_24), (void*)value);
	}

	inline static int32_t get_offset_of__rtTarget_25() { return static_cast<int32_t>(offsetof(SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94, ____rtTarget_25)); }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * get__rtTarget_25() const { return ____rtTarget_25; }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 ** get_address_of__rtTarget_25() { return &____rtTarget_25; }
	inline void set__rtTarget_25(RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * value)
	{
		____rtTarget_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rtTarget_25), (void*)value);
	}
};


// UnityEngine.AudioSource
struct  AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C  : public AudioBehaviour_tC612EC4E17A648A5C568621F3FBF1DBD773C71C7
{
public:

public:
};


// UnityStandardAssets.Effects.AfterburnerPhysicsForce
struct  AfterburnerPhysicsForce_t753048E9DC6DB55FA797EA775B76014E1E7F204B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single UnityStandardAssets.Effects.AfterburnerPhysicsForce::effectAngle
	float ___effectAngle_4;
	// System.Single UnityStandardAssets.Effects.AfterburnerPhysicsForce::effectWidth
	float ___effectWidth_5;
	// System.Single UnityStandardAssets.Effects.AfterburnerPhysicsForce::effectDistance
	float ___effectDistance_6;
	// System.Single UnityStandardAssets.Effects.AfterburnerPhysicsForce::force
	float ___force_7;
	// UnityEngine.Collider[] UnityStandardAssets.Effects.AfterburnerPhysicsForce::m_Cols
	ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* ___m_Cols_8;
	// UnityEngine.SphereCollider UnityStandardAssets.Effects.AfterburnerPhysicsForce::m_Sphere
	SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * ___m_Sphere_9;

public:
	inline static int32_t get_offset_of_effectAngle_4() { return static_cast<int32_t>(offsetof(AfterburnerPhysicsForce_t753048E9DC6DB55FA797EA775B76014E1E7F204B, ___effectAngle_4)); }
	inline float get_effectAngle_4() const { return ___effectAngle_4; }
	inline float* get_address_of_effectAngle_4() { return &___effectAngle_4; }
	inline void set_effectAngle_4(float value)
	{
		___effectAngle_4 = value;
	}

	inline static int32_t get_offset_of_effectWidth_5() { return static_cast<int32_t>(offsetof(AfterburnerPhysicsForce_t753048E9DC6DB55FA797EA775B76014E1E7F204B, ___effectWidth_5)); }
	inline float get_effectWidth_5() const { return ___effectWidth_5; }
	inline float* get_address_of_effectWidth_5() { return &___effectWidth_5; }
	inline void set_effectWidth_5(float value)
	{
		___effectWidth_5 = value;
	}

	inline static int32_t get_offset_of_effectDistance_6() { return static_cast<int32_t>(offsetof(AfterburnerPhysicsForce_t753048E9DC6DB55FA797EA775B76014E1E7F204B, ___effectDistance_6)); }
	inline float get_effectDistance_6() const { return ___effectDistance_6; }
	inline float* get_address_of_effectDistance_6() { return &___effectDistance_6; }
	inline void set_effectDistance_6(float value)
	{
		___effectDistance_6 = value;
	}

	inline static int32_t get_offset_of_force_7() { return static_cast<int32_t>(offsetof(AfterburnerPhysicsForce_t753048E9DC6DB55FA797EA775B76014E1E7F204B, ___force_7)); }
	inline float get_force_7() const { return ___force_7; }
	inline float* get_address_of_force_7() { return &___force_7; }
	inline void set_force_7(float value)
	{
		___force_7 = value;
	}

	inline static int32_t get_offset_of_m_Cols_8() { return static_cast<int32_t>(offsetof(AfterburnerPhysicsForce_t753048E9DC6DB55FA797EA775B76014E1E7F204B, ___m_Cols_8)); }
	inline ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* get_m_Cols_8() const { return ___m_Cols_8; }
	inline ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252** get_address_of_m_Cols_8() { return &___m_Cols_8; }
	inline void set_m_Cols_8(ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* value)
	{
		___m_Cols_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Cols_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_Sphere_9() { return static_cast<int32_t>(offsetof(AfterburnerPhysicsForce_t753048E9DC6DB55FA797EA775B76014E1E7F204B, ___m_Sphere_9)); }
	inline SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * get_m_Sphere_9() const { return ___m_Sphere_9; }
	inline SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F ** get_address_of_m_Sphere_9() { return &___m_Sphere_9; }
	inline void set_m_Sphere_9(SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * value)
	{
		___m_Sphere_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Sphere_9), (void*)value);
	}
};


// UnityStandardAssets.Effects.ExplosionFireAndDebris
struct  ExplosionFireAndDebris_tACCFCC1184BB9A8C3A0B39D99FFC831E76F9EECB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Transform[] UnityStandardAssets.Effects.ExplosionFireAndDebris::debrisPrefabs
	TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* ___debrisPrefabs_4;
	// UnityEngine.Transform UnityStandardAssets.Effects.ExplosionFireAndDebris::firePrefab
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___firePrefab_5;
	// System.Int32 UnityStandardAssets.Effects.ExplosionFireAndDebris::numDebrisPieces
	int32_t ___numDebrisPieces_6;
	// System.Int32 UnityStandardAssets.Effects.ExplosionFireAndDebris::numFires
	int32_t ___numFires_7;

public:
	inline static int32_t get_offset_of_debrisPrefabs_4() { return static_cast<int32_t>(offsetof(ExplosionFireAndDebris_tACCFCC1184BB9A8C3A0B39D99FFC831E76F9EECB, ___debrisPrefabs_4)); }
	inline TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* get_debrisPrefabs_4() const { return ___debrisPrefabs_4; }
	inline TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804** get_address_of_debrisPrefabs_4() { return &___debrisPrefabs_4; }
	inline void set_debrisPrefabs_4(TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* value)
	{
		___debrisPrefabs_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___debrisPrefabs_4), (void*)value);
	}

	inline static int32_t get_offset_of_firePrefab_5() { return static_cast<int32_t>(offsetof(ExplosionFireAndDebris_tACCFCC1184BB9A8C3A0B39D99FFC831E76F9EECB, ___firePrefab_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_firePrefab_5() const { return ___firePrefab_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_firePrefab_5() { return &___firePrefab_5; }
	inline void set_firePrefab_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___firePrefab_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___firePrefab_5), (void*)value);
	}

	inline static int32_t get_offset_of_numDebrisPieces_6() { return static_cast<int32_t>(offsetof(ExplosionFireAndDebris_tACCFCC1184BB9A8C3A0B39D99FFC831E76F9EECB, ___numDebrisPieces_6)); }
	inline int32_t get_numDebrisPieces_6() const { return ___numDebrisPieces_6; }
	inline int32_t* get_address_of_numDebrisPieces_6() { return &___numDebrisPieces_6; }
	inline void set_numDebrisPieces_6(int32_t value)
	{
		___numDebrisPieces_6 = value;
	}

	inline static int32_t get_offset_of_numFires_7() { return static_cast<int32_t>(offsetof(ExplosionFireAndDebris_tACCFCC1184BB9A8C3A0B39D99FFC831E76F9EECB, ___numFires_7)); }
	inline int32_t get_numFires_7() const { return ___numFires_7; }
	inline int32_t* get_address_of_numFires_7() { return &___numFires_7; }
	inline void set_numFires_7(int32_t value)
	{
		___numFires_7 = value;
	}
};


// UnityStandardAssets.Effects.ExtinguishableParticleSystem
struct  ExtinguishableParticleSystem_tD91348BD8771EB84F5369DA844F390BE915ADB64  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single UnityStandardAssets.Effects.ExtinguishableParticleSystem::multiplier
	float ___multiplier_4;
	// UnityEngine.ParticleSystem[] UnityStandardAssets.Effects.ExtinguishableParticleSystem::m_Systems
	ParticleSystemU5BU5D_t58EE604F685D8CBA8EFC9353456969F5A1B2FBB9* ___m_Systems_5;

public:
	inline static int32_t get_offset_of_multiplier_4() { return static_cast<int32_t>(offsetof(ExtinguishableParticleSystem_tD91348BD8771EB84F5369DA844F390BE915ADB64, ___multiplier_4)); }
	inline float get_multiplier_4() const { return ___multiplier_4; }
	inline float* get_address_of_multiplier_4() { return &___multiplier_4; }
	inline void set_multiplier_4(float value)
	{
		___multiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_Systems_5() { return static_cast<int32_t>(offsetof(ExtinguishableParticleSystem_tD91348BD8771EB84F5369DA844F390BE915ADB64, ___m_Systems_5)); }
	inline ParticleSystemU5BU5D_t58EE604F685D8CBA8EFC9353456969F5A1B2FBB9* get_m_Systems_5() const { return ___m_Systems_5; }
	inline ParticleSystemU5BU5D_t58EE604F685D8CBA8EFC9353456969F5A1B2FBB9** get_address_of_m_Systems_5() { return &___m_Systems_5; }
	inline void set_m_Systems_5(ParticleSystemU5BU5D_t58EE604F685D8CBA8EFC9353456969F5A1B2FBB9* value)
	{
		___m_Systems_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Systems_5), (void*)value);
	}
};


// UnityStandardAssets.Effects.FireLight
struct  FireLight_tBDF3DBBD1B6288E05ECBDCDB787FBB8ABD8572C5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single UnityStandardAssets.Effects.FireLight::m_Rnd
	float ___m_Rnd_4;
	// System.Boolean UnityStandardAssets.Effects.FireLight::m_Burning
	bool ___m_Burning_5;
	// UnityEngine.Light UnityStandardAssets.Effects.FireLight::m_Light
	Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C * ___m_Light_6;

public:
	inline static int32_t get_offset_of_m_Rnd_4() { return static_cast<int32_t>(offsetof(FireLight_tBDF3DBBD1B6288E05ECBDCDB787FBB8ABD8572C5, ___m_Rnd_4)); }
	inline float get_m_Rnd_4() const { return ___m_Rnd_4; }
	inline float* get_address_of_m_Rnd_4() { return &___m_Rnd_4; }
	inline void set_m_Rnd_4(float value)
	{
		___m_Rnd_4 = value;
	}

	inline static int32_t get_offset_of_m_Burning_5() { return static_cast<int32_t>(offsetof(FireLight_tBDF3DBBD1B6288E05ECBDCDB787FBB8ABD8572C5, ___m_Burning_5)); }
	inline bool get_m_Burning_5() const { return ___m_Burning_5; }
	inline bool* get_address_of_m_Burning_5() { return &___m_Burning_5; }
	inline void set_m_Burning_5(bool value)
	{
		___m_Burning_5 = value;
	}

	inline static int32_t get_offset_of_m_Light_6() { return static_cast<int32_t>(offsetof(FireLight_tBDF3DBBD1B6288E05ECBDCDB787FBB8ABD8572C5, ___m_Light_6)); }
	inline Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C * get_m_Light_6() const { return ___m_Light_6; }
	inline Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C ** get_address_of_m_Light_6() { return &___m_Light_6; }
	inline void set_m_Light_6(Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C * value)
	{
		___m_Light_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Light_6), (void*)value);
	}
};


// UnityStandardAssets.Effects.Hose
struct  Hose_t411AEC20AB5A70AEFCF03FCB2418F0C8EB14C2D2  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single UnityStandardAssets.Effects.Hose::maxPower
	float ___maxPower_4;
	// System.Single UnityStandardAssets.Effects.Hose::minPower
	float ___minPower_5;
	// System.Single UnityStandardAssets.Effects.Hose::changeSpeed
	float ___changeSpeed_6;
	// UnityEngine.ParticleSystem[] UnityStandardAssets.Effects.Hose::hoseWaterSystems
	ParticleSystemU5BU5D_t58EE604F685D8CBA8EFC9353456969F5A1B2FBB9* ___hoseWaterSystems_7;
	// UnityEngine.Renderer UnityStandardAssets.Effects.Hose::systemRenderer
	Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * ___systemRenderer_8;
	// System.Single UnityStandardAssets.Effects.Hose::m_Power
	float ___m_Power_9;

public:
	inline static int32_t get_offset_of_maxPower_4() { return static_cast<int32_t>(offsetof(Hose_t411AEC20AB5A70AEFCF03FCB2418F0C8EB14C2D2, ___maxPower_4)); }
	inline float get_maxPower_4() const { return ___maxPower_4; }
	inline float* get_address_of_maxPower_4() { return &___maxPower_4; }
	inline void set_maxPower_4(float value)
	{
		___maxPower_4 = value;
	}

	inline static int32_t get_offset_of_minPower_5() { return static_cast<int32_t>(offsetof(Hose_t411AEC20AB5A70AEFCF03FCB2418F0C8EB14C2D2, ___minPower_5)); }
	inline float get_minPower_5() const { return ___minPower_5; }
	inline float* get_address_of_minPower_5() { return &___minPower_5; }
	inline void set_minPower_5(float value)
	{
		___minPower_5 = value;
	}

	inline static int32_t get_offset_of_changeSpeed_6() { return static_cast<int32_t>(offsetof(Hose_t411AEC20AB5A70AEFCF03FCB2418F0C8EB14C2D2, ___changeSpeed_6)); }
	inline float get_changeSpeed_6() const { return ___changeSpeed_6; }
	inline float* get_address_of_changeSpeed_6() { return &___changeSpeed_6; }
	inline void set_changeSpeed_6(float value)
	{
		___changeSpeed_6 = value;
	}

	inline static int32_t get_offset_of_hoseWaterSystems_7() { return static_cast<int32_t>(offsetof(Hose_t411AEC20AB5A70AEFCF03FCB2418F0C8EB14C2D2, ___hoseWaterSystems_7)); }
	inline ParticleSystemU5BU5D_t58EE604F685D8CBA8EFC9353456969F5A1B2FBB9* get_hoseWaterSystems_7() const { return ___hoseWaterSystems_7; }
	inline ParticleSystemU5BU5D_t58EE604F685D8CBA8EFC9353456969F5A1B2FBB9** get_address_of_hoseWaterSystems_7() { return &___hoseWaterSystems_7; }
	inline void set_hoseWaterSystems_7(ParticleSystemU5BU5D_t58EE604F685D8CBA8EFC9353456969F5A1B2FBB9* value)
	{
		___hoseWaterSystems_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hoseWaterSystems_7), (void*)value);
	}

	inline static int32_t get_offset_of_systemRenderer_8() { return static_cast<int32_t>(offsetof(Hose_t411AEC20AB5A70AEFCF03FCB2418F0C8EB14C2D2, ___systemRenderer_8)); }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * get_systemRenderer_8() const { return ___systemRenderer_8; }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 ** get_address_of_systemRenderer_8() { return &___systemRenderer_8; }
	inline void set_systemRenderer_8(Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * value)
	{
		___systemRenderer_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___systemRenderer_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_Power_9() { return static_cast<int32_t>(offsetof(Hose_t411AEC20AB5A70AEFCF03FCB2418F0C8EB14C2D2, ___m_Power_9)); }
	inline float get_m_Power_9() const { return ___m_Power_9; }
	inline float* get_address_of_m_Power_9() { return &___m_Power_9; }
	inline void set_m_Power_9(float value)
	{
		___m_Power_9 = value;
	}
};


// UnityStandardAssets.Effects.ParticleSystemMultiplier
struct  ParticleSystemMultiplier_t8EBFEF83EBA48DD7F6517ABFFBE2CD051CB2C748  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single UnityStandardAssets.Effects.ParticleSystemMultiplier::multiplier
	float ___multiplier_4;

public:
	inline static int32_t get_offset_of_multiplier_4() { return static_cast<int32_t>(offsetof(ParticleSystemMultiplier_t8EBFEF83EBA48DD7F6517ABFFBE2CD051CB2C748, ___multiplier_4)); }
	inline float get_multiplier_4() const { return ___multiplier_4; }
	inline float* get_address_of_multiplier_4() { return &___multiplier_4; }
	inline void set_multiplier_4(float value)
	{
		___multiplier_4 = value;
	}
};


// UnityStandardAssets.Effects.SmokeParticles
struct  SmokeParticles_t556E08200E5E25126D51BFF297C7522998E3D793  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.AudioClip[] UnityStandardAssets.Effects.SmokeParticles::extinguishSounds
	AudioClipU5BU5D_t03931BD44BC339329210676E452B8ECD3EC171C2* ___extinguishSounds_4;

public:
	inline static int32_t get_offset_of_extinguishSounds_4() { return static_cast<int32_t>(offsetof(SmokeParticles_t556E08200E5E25126D51BFF297C7522998E3D793, ___extinguishSounds_4)); }
	inline AudioClipU5BU5D_t03931BD44BC339329210676E452B8ECD3EC171C2* get_extinguishSounds_4() const { return ___extinguishSounds_4; }
	inline AudioClipU5BU5D_t03931BD44BC339329210676E452B8ECD3EC171C2** get_address_of_extinguishSounds_4() { return &___extinguishSounds_4; }
	inline void set_extinguishSounds_4(AudioClipU5BU5D_t03931BD44BC339329210676E452B8ECD3EC171C2* value)
	{
		___extinguishSounds_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extinguishSounds_4), (void*)value);
	}
};


// UnityStandardAssets.Effects.WaterHoseParticles
struct  WaterHoseParticles_t4C0EC93AEAF5B1623A7F04AF7D912D6B6438E92B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single UnityStandardAssets.Effects.WaterHoseParticles::force
	float ___force_5;
	// System.Collections.Generic.List`1<UnityEngine.ParticleCollisionEvent> UnityStandardAssets.Effects.WaterHoseParticles::m_CollisionEvents
	List_1_t2762C811E470D336E31761384C6E5382164DA4C7 * ___m_CollisionEvents_6;
	// UnityEngine.ParticleSystem UnityStandardAssets.Effects.WaterHoseParticles::m_ParticleSystem
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___m_ParticleSystem_7;

public:
	inline static int32_t get_offset_of_force_5() { return static_cast<int32_t>(offsetof(WaterHoseParticles_t4C0EC93AEAF5B1623A7F04AF7D912D6B6438E92B, ___force_5)); }
	inline float get_force_5() const { return ___force_5; }
	inline float* get_address_of_force_5() { return &___force_5; }
	inline void set_force_5(float value)
	{
		___force_5 = value;
	}

	inline static int32_t get_offset_of_m_CollisionEvents_6() { return static_cast<int32_t>(offsetof(WaterHoseParticles_t4C0EC93AEAF5B1623A7F04AF7D912D6B6438E92B, ___m_CollisionEvents_6)); }
	inline List_1_t2762C811E470D336E31761384C6E5382164DA4C7 * get_m_CollisionEvents_6() const { return ___m_CollisionEvents_6; }
	inline List_1_t2762C811E470D336E31761384C6E5382164DA4C7 ** get_address_of_m_CollisionEvents_6() { return &___m_CollisionEvents_6; }
	inline void set_m_CollisionEvents_6(List_1_t2762C811E470D336E31761384C6E5382164DA4C7 * value)
	{
		___m_CollisionEvents_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CollisionEvents_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_ParticleSystem_7() { return static_cast<int32_t>(offsetof(WaterHoseParticles_t4C0EC93AEAF5B1623A7F04AF7D912D6B6438E92B, ___m_ParticleSystem_7)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_m_ParticleSystem_7() const { return ___m_ParticleSystem_7; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_m_ParticleSystem_7() { return &___m_ParticleSystem_7; }
	inline void set_m_ParticleSystem_7(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___m_ParticleSystem_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParticleSystem_7), (void*)value);
	}
};

struct WaterHoseParticles_t4C0EC93AEAF5B1623A7F04AF7D912D6B6438E92B_StaticFields
{
public:
	// System.Single UnityStandardAssets.Effects.WaterHoseParticles::lastSoundTime
	float ___lastSoundTime_4;

public:
	inline static int32_t get_offset_of_lastSoundTime_4() { return static_cast<int32_t>(offsetof(WaterHoseParticles_t4C0EC93AEAF5B1623A7F04AF7D912D6B6438E92B_StaticFields, ___lastSoundTime_4)); }
	inline float get_lastSoundTime_4() const { return ___lastSoundTime_4; }
	inline float* get_address_of_lastSoundTime_4() { return &___lastSoundTime_4; }
	inline void set_lastSoundTime_4(float value)
	{
		___lastSoundTime_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  m_Items[1];

public:
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  m_Items[1];

public:
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Color[]
struct ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  m_Items[1];

public:
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		m_Items[index] = value;
	}
};
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Collider[]
struct ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * m_Items[1];

public:
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Transform[]
struct TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * m_Items[1];

public:
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.ParticleSystem[]
struct ParticleSystemU5BU5D_t58EE604F685D8CBA8EFC9353456969F5A1B2FBB9  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * m_Items[1];

public:
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.AudioClip[]
struct AudioClipU5BU5D_t03931BD44BC339329210676E452B8ECD3EC171C2  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * m_Items[1];

public:
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.ParticleCollisionEvent[]
struct ParticleCollisionEventU5BU5D_t771CB4A499CC97F3340C673D227DD1F5969EB9B9  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ParticleCollisionEvent_t7F4D7D5ACF8521671549D9328D4E2DDE480D8D47  m_Items[1];

public:
	inline ParticleCollisionEvent_t7F4D7D5ACF8521671549D9328D4E2DDE480D8D47  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ParticleCollisionEvent_t7F4D7D5ACF8521671549D9328D4E2DDE480D8D47 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ParticleCollisionEvent_t7F4D7D5ACF8521671549D9328D4E2DDE480D8D47  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline ParticleCollisionEvent_t7F4D7D5ACF8521671549D9328D4E2DDE480D8D47  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ParticleCollisionEvent_t7F4D7D5ACF8521671549D9328D4E2DDE480D8D47 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ParticleCollisionEvent_t7F4D7D5ACF8521671549D9328D4E2DDE480D8D47  value)
	{
		m_Items[index] = value;
	}
};


// !!0 UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m129DEF8A66683189ED44B21496135824743EF617_gshared (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.ScriptableObject::CreateInstance<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ScriptableObject_CreateInstance_TisRuntimeObject_m7A8F75139352BA04C2EEC1D72D430FAC94C753DE_gshared (const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Object_Instantiate_TisRuntimeObject_m352D452C728667C9C76C942525CDE26444568ECD_gshared (RuntimeObject * ___original0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position1, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___rotation2, const RuntimeMethod* method);
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* Component_GetComponentsInChildren_TisRuntimeObject_m6D4C38C330FCFD3C323B34031A7E877A5E2D453A_gshared (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<UnityEngine.ParticleCollisionEvent>::get_Item(System.Int32)
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR ParticleCollisionEvent_t7F4D7D5ACF8521671549D9328D4E2DDE480D8D47  List_1_get_Item_m095B750642DEE222CA241EE9D2392CA6E200FA8D_gshared_inline (List_1_t2762C811E470D336E31761384C6E5382164DA4C7 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.ParticleCollisionEvent>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m377714278A7FAB4D7514D181355D61B01B3427B0_gshared (List_1_t2762C811E470D336E31761384C6E5382164DA4C7 * __this, const RuntimeMethod* method);

// System.Void SleekRender.SleekRenderPostProcess::CreateDefaultSettingsIfNoneLinked()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SleekRenderPostProcess_CreateDefaultSettingsIfNoneLinked_mFAFCBC9B80CB0CAE18DB94C102CD3D0FAEFA83A9 (SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94 * __this, const RuntimeMethod* method);
// System.Void SleekRender.SleekRenderPostProcess::CreateResources()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SleekRenderPostProcess_CreateResources_m88233905ED6473C25EA3C8A4B3757879C201D6DA (SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94 * __this, const RuntimeMethod* method);
// System.Void SleekRender.SleekRenderPostProcess::ReleaseResources()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SleekRenderPostProcess_ReleaseResources_mDFBCB6AF7DB534D25370ED70E8C21A4D8E85F165 (SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94 * __this, const RuntimeMethod* method);
// System.Void SleekRender.SleekRenderPostProcess::ApplyPostProcess(UnityEngine.RenderTexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SleekRenderPostProcess_ApplyPostProcess_m7228BB4785430B68E38460258D93189559260835 (SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94 * __this, RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ___source0, const RuntimeMethod* method);
// System.Void SleekRender.SleekRenderPostProcess::Compose(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SleekRenderPostProcess_Compose_m76162AF19A38CE8168DB6ED4B2E89E56110D02BA (SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94 * __this, RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ___source0, RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ___target1, const RuntimeMethod* method);
// System.Void SleekRender.SleekRenderPostProcess::Downsample(UnityEngine.RenderTexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SleekRenderPostProcess_Downsample_m939DB30198E9BB4BEFF3884A283D450C8704EE96 (SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94 * __this, RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ___source0, const RuntimeMethod* method);
// System.Void SleekRender.SleekRenderPostProcess::Bloom(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SleekRenderPostProcess_Bloom_m38FA596CFD5659B6F02915F17199F0DF0ADE16FC (SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94 * __this, bool ___isBloomEnabled0, const RuntimeMethod* method);
// System.Void SleekRender.SleekRenderPostProcess::Precompose(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SleekRenderPostProcess_Precompose_mE5C8A6B77C24F3B90CDFC17D60895195BDD20965 (SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94 * __this, bool ___isBloomEnabled0, const RuntimeMethod* method);
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector4__ctor_m545458525879607A5392A10B175D0C19B2BC715D (Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method);
// System.Void UnityEngine.Material::SetVector(System.Int32,UnityEngine.Vector4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material_SetVector_m95B7CB07B91F004B4DD9DB5DFA5146472737B8EA (Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * __this, int32_t ___nameID0, Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___value1, const RuntimeMethod* method);
// System.Void SleekRender.SleekRenderPostProcess::Blit(UnityEngine.Texture,UnityEngine.RenderTexture,UnityEngine.Material,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SleekRenderPostProcess_Blit_mEDE1D6060C22F747323C93F8ABECC7C879413CBE (SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94 * __this, Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___source0, RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ___destination1, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material2, int32_t ___materialPass3, const RuntimeMethod* method);
// System.Void UnityEngine.Material::EnableKeyword(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material_EnableKeyword_m7466758182CBBC40134C9048CDF682DF46F32FA9 (Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * __this, String_t* ___keyword0, const RuntimeMethod* method);
// System.Void UnityEngine.Material::DisableKeyword(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material_DisableKeyword_m2ACBFC5D28ED46FF2CF5532F00D702FF62C02ED3 (Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * __this, String_t* ___keyword0, const RuntimeMethod* method);
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector4__ctor_mFADAC87CAF1F57AD3FE715E6930D78BF7D303D81 (Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * __this, float ___x0, float ___y1, const RuntimeMethod* method);
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Color__ctor_m20DF490CEB364C4FC36D7EE392640DF5B7420D7C (Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method);
// System.Void UnityEngine.Material::SetColor(System.Int32,UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material_SetColor_m48FBB701F6177B367EDFAC6BE896D183EF640725 (Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * __this, int32_t ___nameID0, Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.Material::SetFloat(System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material_SetFloat_mC2FDDF0798373DEE6BBA9B9FFFE03EC3CFB9BF47 (Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * __this, int32_t ___nameID0, float ___value1, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color32::op_Implicit(UnityEngine.Color32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  Color32_op_Implicit_mA89CAD76E78975F51DF7374A67D18A5F6EF8DA61 (Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___c0, const RuntimeMethod* method);
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector4__ctor_mBAB87E6221CF38FDA7BE868992CE92476B0EDFC0 (Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * Component_GetComponent_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_mB090F51A34716700C0F4F1B08F9330C6F503DB9E (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m129DEF8A66683189ED44B21496135824743EF617_gshared)(__this, method);
}
// UnityEngine.Shader UnityEngine.Shader::Find(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * Shader_Find_m755654AA68D1C663A3E20A10E00CDC10F96C962B (String_t* ___name0, const RuntimeMethod* method);
// System.Void UnityEngine.Material::.ctor(UnityEngine.Shader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material__ctor_m81E76B5C1316004F25D4FE9CEC0E78A7428DABA8 (Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * __this, Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___shader0, const RuntimeMethod* method);
// System.Int32 UnityEngine.Camera::get_pixelWidth()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Camera_get_pixelWidth_m67EC53853580E35527F32D6EA002FE21C234172E (Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Mathf::RoundToInt(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Mathf_RoundToInt_m0EAD8BD38FCB72FA1D8A04E96337C820EC83F041 (float ___f0, const RuntimeMethod* method);
// System.Int32 UnityEngine.Camera::get_pixelHeight()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Camera_get_pixelHeight_m38879ACBA6B21C25E83AB07FA37A8E5EB7A51B05 (Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Mathf::Min(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Mathf_Min_m1A2CC204E361AE13C329B6535165179798D3313A (int32_t ___a0, int32_t ___b1, const RuntimeMethod* method);
// System.Single SleekRender.SleekRenderPostProcess::GetCurrentAspect(UnityEngine.Camera)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float SleekRenderPostProcess_GetCurrentAspect_mFA278408C602D5213CB17EC938C37D070544DC79 (SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94 * __this, Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___mainCamera0, const RuntimeMethod* method);
// UnityEngine.RenderTexture SleekRender.SleekRenderPostProcess::CreateTransientRenderTexture(System.String,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * SleekRenderPostProcess_CreateTransientRenderTexture_m5896F414C30F00DE545EF638447324CC3D1DE67F (SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94 * __this, String_t* ___textureName0, int32_t ___width1, int32_t ___height2, const RuntimeMethod* method);
// System.Void UnityEngine.Material::SetTexture(System.Int32,UnityEngine.Texture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material_SetTexture_m4FFF0B403A64253B83534701104F017840142ACA (Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * __this, int32_t ___nameID0, Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___value1, const RuntimeMethod* method);
// UnityEngine.Mesh SleekRender.SleekRenderPostProcess::CreateScreenSpaceQuadMesh()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * SleekRenderPostProcess_CreateScreenSpaceQuadMesh_m2D59ABE6B083BEF9A9DE056AF5725199DD9FCB0C (SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.RenderTexture::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RenderTexture__ctor_m0FF5DDAB599ED301091CF23D4C76691D8EC70CA5 (RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * __this, int32_t ___width0, int32_t ___height1, int32_t ___depth2, int32_t ___format3, const RuntimeMethod* method);
// System.Void UnityEngine.Object::set_name(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_set_name_m538711B144CDE30F929376BCF72D0DC8F85D0826 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Texture::set_filterMode(UnityEngine.FilterMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture_set_filterMode_mB9AC927A527EFE95771B9B438E2CFB9EDA84AF01 (Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Texture::set_wrapMode(UnityEngine.TextureWrapMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture_set_wrapMode_m85E9A995D5947B59FE13A7311E891F3DEDEBBCEC (Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * __this, int32_t ___value0, const RuntimeMethod* method);
// UnityEngine.Rendering.GraphicsDeviceType UnityEngine.SystemInfo::get_graphicsDeviceType()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t SystemInfo_get_graphicsDeviceType_m675AD9D5FA869DF9E71FAEC03F39E8AE8DEBA8D0 (const RuntimeMethod* method);
// System.String UnityEngine.SystemInfo::get_graphicsDeviceName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* SystemInfo_get_graphicsDeviceName_mD9D09CDF695610D3355F28EAD9EE61F7870F52E8 (const RuntimeMethod* method);
// System.Boolean System.String::Contains(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_Contains_m4488034AF8CB3EEA9A205EB8A1F25D438FF8704B (String_t* __this, String_t* ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.SystemInfo::SupportsRenderTextureFormat(UnityEngine.RenderTextureFormat)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SystemInfo_SupportsRenderTextureFormat_m74D259714A97501D28951CA48298D9F0AE3B5907 (int32_t ___format0, const RuntimeMethod* method);
// System.Int32 UnityEngine.QualitySettings::get_antiAliasing()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t QualitySettings_get_antiAliasing_m28EE8A60C753C1D160BB393D92D98CC0E1776B16 (const RuntimeMethod* method);
// System.Void UnityEngine.RenderTexture::set_antiAliasing(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RenderTexture_set_antiAliasing_mBE37447FA23E0D57731C1456165E03303EC9B559 (RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void SleekRender.SleekRenderPostProcess::DestroyImmediateIfNotNull(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SleekRenderPostProcess_DestroyImmediateIfNotNull_m0E4B29743210245443999A680294C1DDBF0140DC (SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94 * __this, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___obj0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___x0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___y1, const RuntimeMethod* method);
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_DestroyImmediate_mF6F4415EF22249D6E650FAA40E403283F19B7446 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___obj0, const RuntimeMethod* method);
// System.Void SleekRender.SleekRenderPostProcess::SetActiveRenderTextureAndClear(UnityEngine.RenderTexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SleekRenderPostProcess_SetActiveRenderTextureAndClear_m01E22DC5E4C5579AA8732CA0F67E01F39E4AC89F (RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ___destination0, const RuntimeMethod* method);
// System.Void SleekRender.SleekRenderPostProcess::DrawFullscreenQuad(UnityEngine.Texture,UnityEngine.Material,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SleekRenderPostProcess_DrawFullscreenQuad_m77F12B59A976F77BFC5A8BF270AD5E5D30408DD0 (SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94 * __this, Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___source0, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material1, int32_t ___materialPass2, const RuntimeMethod* method);
// System.Void UnityEngine.RenderTexture::set_active(UnityEngine.RenderTexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RenderTexture_set_active_m992E25C701DEFC8042B31022EA45F02A787A84F1 (RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.GL::Clear(System.Boolean,System.Boolean,UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GL_Clear_mBC8B714C794457D52A5343F40399BBDF57BA978A (bool ___clearDepth0, bool ___clearColor1, Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___backgroundColor2, const RuntimeMethod* method);
// System.Boolean UnityEngine.Material::SetPass(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Material_SetPass_m4BE0A8FCBF158C83522AA2F69118A2FE33683918 (Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * __this, int32_t ___pass0, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_identity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  Matrix4x4_get_identity_mA0CECDE2A5E85CF014375084624F3770B5B7B79B (const RuntimeMethod* method);
// System.Void UnityEngine.Graphics::DrawMeshNow(UnityEngine.Mesh,UnityEngine.Matrix4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Graphics_DrawMeshNow_m65719727D0812E6140C9DC6AF4A7BDBB72B69FD0 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___mesh0, Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___matrix1, const RuntimeMethod* method);
// System.Single UnityEngine.Camera::get_aspect()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Camera_get_aspect_m2ADA7982754920C3B58B4DB664801D6F2416E0C6 (Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___x0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___y1, const RuntimeMethod* method);
// !!0 UnityEngine.ScriptableObject::CreateInstance<SleekRender.SleekRenderSettings>()
inline SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142 * ScriptableObject_CreateInstance_TisSleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142_mF36470B34704D69B08AB64E76E7987E6BF28E215 (const RuntimeMethod* method)
{
	return ((  SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142 * (*) (const RuntimeMethod*))ScriptableObject_CreateInstance_TisRuntimeObject_m7A8F75139352BA04C2EEC1D72D430FAC94C753DE_gshared)(method);
}
// System.Void UnityEngine.Mesh::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh__ctor_m3AEBC82AB71D4F9498F6E254174BEBA8372834B4 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * __this, float ___x0, float ___y1, const RuntimeMethod* method);
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Color__ctor_mC9AEEB3931D5B8C37483A884DD8EB40DC8946369 (Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * __this, float ___r0, float ___g1, float ___b2, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeHelpers::InitializeArray(System.Array,System.RuntimeFieldHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeHelpers_InitializeArray_m29F50CDFEEE0AB868200291366253DD4737BC76A (RuntimeArray * ___array0, RuntimeFieldHandle_t844BDF00E8E6FE69D9AEAA7657F09018B864F4EF  ___fldHandle1, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::set_vertices(UnityEngine.Vector3[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh_set_vertices_mC1406AE08BC3495F3B0E29B53BACC9FD7BA685C6 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::set_uv(UnityEngine.Vector2[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh_set_uv_m56E4B52315669FBDA89DC9C550AC89EEE8A4E7C8 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::set_triangles(System.Int32[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh_set_triangles_m143A1C262BADCFACE43587EBA2CDC6EBEB5DFAED (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::set_colors(UnityEngine.Color[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh_set_colors_m704D0EF58B7AED0D64AE4763EA375638FB08E026 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::UploadMeshData(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mesh_UploadMeshData_m809A98624475785C493269B72EC6C41B556759A1 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, bool ___markNoLongerReadable0, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97 (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Shader::PropertyToID(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Shader_PropertyToID_m831E5B48743620DB9E3E3DD15A8DEA483981DD45 (String_t* ___name0, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_white()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  Color_get_white_mE7F3AC4FF0D6F35E48049C73116A222CBE96D905 (const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  Color_get_clear_m419239BDAEB3D3C4B4291BF2C6EF09A7D7D81360 (const RuntimeMethod* method);
// UnityEngine.Color32 UnityEngine.Color32::op_Implicit(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  Color32_op_Implicit_m52B034473369A651C8952BD916A2AB193E0E5B30 (Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___c0, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_black()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  Color_get_black_mEB3C91F45F8AA7E4842238DFCC578BB322723DAF (const RuntimeMethod* method);
// System.Void UnityEngine.ScriptableObject::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScriptableObject__ctor_m6E2B3821A4A361556FC12E9B1C71E1D5DC002C5B (ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Collider>()
inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * Component_GetComponent_TisCollider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF_m6B8E8E0E0AF6B08652B81B7950FC5AF63EAD40C6 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m129DEF8A66683189ED44B21496135824743EF617_gshared)(__this, method);
}
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.SphereCollider::get_center()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  SphereCollider_get_center_mE7AD1AC46974FF23EEA621B872E2962E52A1DB00 (SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___a0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___b1, const RuntimeMethod* method);
// System.Single UnityEngine.SphereCollider::get_radius()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float SphereCollider_get_radius_m255804173C17314FD9538AE45C4A46D4882BC094 (SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * __this, const RuntimeMethod* method);
// UnityEngine.Collider[] UnityEngine.Physics::OverlapSphere(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* Physics_OverlapSphere_m354A92672F7A6DE59EF1285D02D62247F46A5D84 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position0, float ___radius1, const RuntimeMethod* method);
// UnityEngine.Rigidbody UnityEngine.Collider::get_attachedRigidbody()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * Collider_get_attachedRigidbody_m9E3C688EAE2F6A76C9AC14968D96769D9A71B1E8 (Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::InverseTransformPoint(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_InverseTransformPoint_mB6E3145F20B531B4A781C194BAC43A8255C96C47 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::MoveTowards(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_MoveTowards_mA288BB5AA73DDA9CA76EDC11F339BAFDA1E4FF45 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___current0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___target1, float ___maxDistanceDelta2, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::get_magnitude()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector3_get_magnitude_m9A750659B60C5FE0C30438A7F9681775D5DB1274 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::InverseLerp(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_InverseLerp_m7054CDF25056E9B27D2467F91C95D628508F1F31 (float ___a0, float ___b1, float ___value2, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___a0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___b1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_get_normalized_mE20796F1D2D36244FACD4D14DADB245BE579849B (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___a0, float ___d1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::TransformPoint(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_TransformPoint_m363B3A9E2C3A9A52F4B872CF34F476D87CCC8CEC (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::Lerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_Lerp_m5BA75496B803820CC64079383956D73C6FD4A8A1 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___a0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___b1, float ___t2, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::AddForceAtPosition(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_AddForceAtPosition_m3A5DCFC3E79923C9D8E32A54BC4AAA1E48EEAD6C (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___force0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position1, const RuntimeMethod* method);
// System.Void UnityEngine.SphereCollider::set_radius(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SphereCollider_set_radius_m3161573A2D89F495F4B79E16C52B905C0F9AD699 (SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.SphereCollider::set_center(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SphereCollider_set_center_m325070F5252B4A2EA567B653CAE3285F101FA3EE (SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_up()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_get_up_m6309EBC4E42D6D0B3D28056BD23D0331275306F7 (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_UnaryNegation(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_op_UnaryNegation_m2AFBBF22801F9BCA5A4EBE642A29F433FE1339C2 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___a0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_right()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_get_right_m6DD9559CA0C75BBA42D9140021C4C2A9AAA9B3F5 (const RuntimeMethod* method);
// System.Void UnityEngine.Gizmos::set_color(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Gizmos_set_color_mFA6C199DF05FF557AEF662222CA60EC25DF54F28 (Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___value0, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Transform_get_rotation_m3AB90A67403249AECCA5E02BC70FCE8C90FE9FB9 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Quaternion_op_Multiply_mD5999DE317D808808B72E58E7A978C4C0995879C (Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___rotation0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___point1, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::AngleAxis(System.Single,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Quaternion_AngleAxis_m07DACF59F0403451DABB9BC991C53EE3301E88B0 (float ___angle0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___axis1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_get_forward_m3E2E192B3302130098738C308FA1EE1439449D0D (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::TransformDirection(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_TransformDirection_m85FC1D7E1322E94F65DA59AEF3B1166850B183EF (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction0, const RuntimeMethod* method);
// System.Void UnityEngine.Gizmos::DrawLine(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Gizmos_DrawLine_m9515D59D2536571F4906A3C54E613A3986DFD892 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___from0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___to1, const RuntimeMethod* method);
// System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>d__4::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartU3Ed__4__ctor_mBDD727783F54367BFE2CED7C675B839450955B8C (U3CStartU3Ed__4_t945A707CD8C01DA1C37AE2DD9D0A904C80F58139 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Quaternion_get_identity_m548B37D80F2DEE60E41D1F09BF6889B557BE1A64 (const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<UnityEngine.Transform>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * Object_Instantiate_TisTransform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_m8BCC51A95F42E99B9033022D8BF0EABA04FED8F0 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___original0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position1, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___rotation2, const RuntimeMethod* method)
{
	return ((  Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * (*) (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 , Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 , const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_m352D452C728667C9C76C942525CDE26444568ECD_gshared)(___original0, ___position1, ___rotation2, method);
}
// System.Void UnityEngine.Transform::set_parent(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_parent_m65B8E4660B2C554069C57A957D9E55FECA7AA73E (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___value0, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityStandardAssets.Effects.ParticleSystemMultiplier>()
inline ParticleSystemMultiplier_t8EBFEF83EBA48DD7F6517ABFFBE2CD051CB2C748 * Component_GetComponent_TisParticleSystemMultiplier_t8EBFEF83EBA48DD7F6517ABFFBE2CD051CB2C748_m77EDAEF51740299BC986B0DA36AB7282BB8DDD45 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  ParticleSystemMultiplier_t8EBFEF83EBA48DD7F6517ABFFBE2CD051CB2C748 * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m129DEF8A66683189ED44B21496135824743EF617_gshared)(__this, method);
}
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Random_Range_mD0C8F37FF3CAB1D87AAA6C45130BD59626BD6780 (int32_t ___min0, int32_t ___max1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Random::get_insideUnitSphere()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Random_get_insideUnitSphere_m10033DFB85B1A21CE44201CB0E421F27B77A868F (const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Random::get_rotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Random_get_rotation_mFEB212A2058746E0E8F544D8C2201663861BA77A (const RuntimeMethod* method);
// System.Void UnityEngine.Ray::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Ray__ctor_m695D219349B8AA4C82F96C55A27D384C07736F6B (Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___origin0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Collider::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Collider_Raycast_mF79828BB10A356336B673595777F7C6941E3F42F (Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * __this, Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  ___ray0, RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * ___hitInfo1, float ___maxDistance2, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_point()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  RaycastHit_get_point_m0E564B2A72C7A744B889AE9D596F3EFA55059001 (RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_normal()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  RaycastHit_get_normal_mF736A6D09D98D63AB7E5BF10F38AEBFC177A1D94 (RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * __this, const RuntimeMethod* method);
// System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris::AddFire(UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExplosionFireAndDebris_AddFire_m781F3E400896C4F2E2B4A24FAEFE52D01ED26C0B (ExplosionFireAndDebris_tACCFCC1184BB9A8C3A0B39D99FFC831E76F9EECB * __this, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___t0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___pos1, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___normal2, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Random::get_onUnitSphere()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Random_get_onUnitSphere_mBF4707ADEABB994E7B5B80305205A621B809E7E6 (const RuntimeMethod* method);
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_Raycast_mE1590EE4E2DC950A9FC2437E98EE8CD2EC2DEE67 (Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  ___ray0, RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * ___hitInfo1, float ___maxDistance2, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_mA121DE1CAC8F25277DEB489DC7771209D91CAE33 (NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 * __this, const RuntimeMethod* method);
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.ParticleSystem>()
inline ParticleSystemU5BU5D_t58EE604F685D8CBA8EFC9353456969F5A1B2FBB9* Component_GetComponentsInChildren_TisParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D_mD5AEF673286CB17B689F765BB71E83509548DC41 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  ParticleSystemU5BU5D_t58EE604F685D8CBA8EFC9353456969F5A1B2FBB9* (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponentsInChildren_TisRuntimeObject_m6D4C38C330FCFD3C323B34031A7E877A5E2D453A_gshared)(__this, method);
}
// UnityEngine.ParticleSystem/EmissionModule UnityEngine.ParticleSystem::get_emission()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR EmissionModule_t35028C3DE5EFDCE49E8A9732460617A56BD1D3F1  ParticleSystem_get_emission_mA1204EAF07A6C6B3F65B45295797A1FFF64D343C (ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/EmissionModule::set_enabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EmissionModule_set_enabled_m3896B441BDE0F0752A6D113012B20D5D31B16D36 (EmissionModule_t35028C3DE5EFDCE49E8A9732460617A56BD1D3F1 * __this, bool ___value0, const RuntimeMethod* method);
// System.Single UnityEngine.Random::get_value()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Random_get_value_mC998749E08291DD42CF31C026FAC4F14F746831C (const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Light>()
inline Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C * Component_GetComponent_TisLight_tFDE490EADBC7E080F74CA804929513AF07C31A6C_m1DCED5DB1934151FC68A8E7CAECF7986359D7107 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m129DEF8A66683189ED44B21496135824743EF617_gshared)(__this, method);
}
// System.Single UnityEngine.Time::get_time()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_time_m7863349C8845BBA36629A2B3F8EF1C3BEA350FD8 (const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::PerlinNoise(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_PerlinNoise_mA36E513B2931F17ACBF26D928674D3DDEDF5C810 (float ___x0, float ___y1, const RuntimeMethod* method);
// System.Void UnityEngine.Light::set_intensity(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Light_set_intensity_mE209975C840F1D887B4207C390DB5A2EF15A763C (Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Behaviour_set_enabled_m9755D3B17D7022D23D1E4C618BD9A6B66A5ADC6B (Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8 * __this, bool ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetMouseButton(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetMouseButton_m43C68DE93C7D990E875BA53C4DEC9CA6230C8B79 (int32_t ___button0, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E (const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Lerp(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Lerp_m9A74C5A0C37D0CDF45EE66E7774D12A9B93B1364 (float ___a0, float ___b1, float ___t2, const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetKeyDown(UnityEngine.KeyCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetKeyDown_mEA57896808B6F484B12CD0AEEB83390A3CFCDBDC (int32_t ___key0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Renderer::get_enabled()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Renderer_get_enabled_m40E07BB15DA58D2EF6F6796C6778163107DD7E1B (Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Renderer::set_enabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Renderer_set_enabled_m0933766657F2685BAAE3340B0A984C0E63925303 (Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * __this, bool ___value0, const RuntimeMethod* method);
// UnityEngine.ParticleSystem/MainModule UnityEngine.ParticleSystem::get_main()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MainModule_t99C675667E0A363368324132DFA34B27FFEE6FC7  ParticleSystem_get_main_m360B0AA57C71DE0358B6B07133C68B5FD88C742F (ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * __this, const RuntimeMethod* method);
// UnityEngine.ParticleSystem/MinMaxCurve UnityEngine.ParticleSystem/MinMaxCurve::op_Implicit(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MinMaxCurve_tDB335EDEBEBD4CFA753081D7C3A2FE2EECFA6D71  MinMaxCurve_op_Implicit_m998EE9F8D83B9545F63E2DFA304E99620F0F707F (float ___constant0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::set_startSpeed(UnityEngine.ParticleSystem/MinMaxCurve)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_startSpeed_m32CD8968F2B5572112DE22CC7B6E2B502222B4AE (MainModule_t99C675667E0A363368324132DFA34B27FFEE6FC7 * __this, MinMaxCurve_tDB335EDEBEBD4CFA753081D7C3A2FE2EECFA6D71  ___value0, const RuntimeMethod* method);
// System.Single UnityEngine.ParticleSystem/MainModule::get_startSizeMultiplier()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_startSizeMultiplier_m8E241099DB1E4ECE4827E9507415C80012AEA312 (MainModule_t99C675667E0A363368324132DFA34B27FFEE6FC7 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::set_startSizeMultiplier(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_startSizeMultiplier_m6375840B2CADFF82321D7B52103FADC5AD144B8C (MainModule_t99C675667E0A363368324132DFA34B27FFEE6FC7 * __this, float ___value0, const RuntimeMethod* method);
// System.Single UnityEngine.ParticleSystem/MainModule::get_startSpeedMultiplier()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_startSpeedMultiplier_m22326AF745786C1748F6B165361CCC26917F214A (MainModule_t99C675667E0A363368324132DFA34B27FFEE6FC7 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::set_startSpeedMultiplier(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_startSpeedMultiplier_m14E157EFF4BD78D8FDA701BE2BD0B398EF6F2453 (MainModule_t99C675667E0A363368324132DFA34B27FFEE6FC7 * __this, float ___value0, const RuntimeMethod* method);
// System.Single UnityEngine.ParticleSystem/MainModule::get_startLifetimeMultiplier()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_startLifetimeMultiplier_m0D425E7689C0B99C5B9E8F8C4AE205600C1EA529 (MainModule_t99C675667E0A363368324132DFA34B27FFEE6FC7 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::set_startLifetimeMultiplier(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_startLifetimeMultiplier_mCD094E55BD574ECF3A8F5D616A2287130B6EC67B (MainModule_t99C675667E0A363368324132DFA34B27FFEE6FC7 * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_Clear_mE11D3D0E23B1C5AAFF1F432E278ED91F1D929FEE (ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem::Play()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_Play_m5BC5E6B56FCF639CAD5DF41B51DC05A0B444212F (ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.AudioSource>()
inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * Component_GetComponent_TisAudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C_m04C8E98F2393C77979C9D8F6DE1D98343EF025E8 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m129DEF8A66683189ED44B21496135824743EF617_gshared)(__this, method);
}
// System.Void UnityEngine.AudioSource::set_clip(UnityEngine.AudioClip)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AudioSource_set_clip_mF574231E0B749E0167CAF9E4FCBA06BAA0F9ED9B (AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * __this, AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AudioSource::Play()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AudioSource_Play_m0BA206481892AA4AF7DB2900A0B0805076516164 (AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.ParticleSystem>()
inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * Component_GetComponent_TisParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D_mA6EBF435A59643B674086A2F309F6A4DCB263452 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m129DEF8A66683189ED44B21496135824743EF617_gshared)(__this, method);
}
// System.Int32 UnityEngine.ParticlePhysicsExtensions::GetCollisionEvents(UnityEngine.ParticleSystem,UnityEngine.GameObject,System.Collections.Generic.List`1<UnityEngine.ParticleCollisionEvent>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ParticlePhysicsExtensions_GetCollisionEvents_m11DDE18328B0E4B9766D1581DFCB0E67BCFD097C (ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___ps0, GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___go1, List_1_t2762C811E470D336E31761384C6E5382164DA4C7 * ___collisionEvents2, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<UnityEngine.ParticleCollisionEvent>::get_Item(System.Int32)
inline ParticleCollisionEvent_t7F4D7D5ACF8521671549D9328D4E2DDE480D8D47  List_1_get_Item_m095B750642DEE222CA241EE9D2392CA6E200FA8D_inline (List_1_t2762C811E470D336E31761384C6E5382164DA4C7 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  ParticleCollisionEvent_t7F4D7D5ACF8521671549D9328D4E2DDE480D8D47  (*) (List_1_t2762C811E470D336E31761384C6E5382164DA4C7 *, int32_t, const RuntimeMethod*))List_1_get_Item_m095B750642DEE222CA241EE9D2392CA6E200FA8D_gshared_inline)(__this, ___index0, method);
}
// UnityEngine.Component UnityEngine.ParticleCollisionEvent::get_colliderComponent()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * ParticleCollisionEvent_get_colliderComponent_mE4A94CAC6A2875588395DCB76FB7978A72796B82 (ParticleCollisionEvent_t7F4D7D5ACF8521671549D9328D4E2DDE480D8D47 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * Component_GetComponent_TisRigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5_m3F58A77E3F62D9C80D7B49BA04E3D4809264FD5C (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m129DEF8A66683189ED44B21496135824743EF617_gshared)(__this, method);
}
// UnityEngine.Vector3 UnityEngine.ParticleCollisionEvent::get_velocity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ParticleCollisionEvent_get_velocity_m60BCDF44F8F345C3534705CBA2BD5E69346021A3 (ParticleCollisionEvent_t7F4D7D5ACF8521671549D9328D4E2DDE480D8D47 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::AddForce(UnityEngine.Vector3,UnityEngine.ForceMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_AddForce_mD64ACF772614FE36CFD8A477A07A407B35DF1A54 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___force0, int32_t ___mode1, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::BroadcastMessage(System.String,UnityEngine.SendMessageOptions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_BroadcastMessage_mABB308EA1B7F641BFC60E091A75E5E053D23A8B5 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, String_t* ___methodName0, int32_t ___options1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.ParticleCollisionEvent>::.ctor()
inline void List_1__ctor_m377714278A7FAB4D7514D181355D61B01B3427B0 (List_1_t2762C811E470D336E31761384C6E5382164DA4C7 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t2762C811E470D336E31761384C6E5382164DA4C7 *, const RuntimeMethod*))List_1__ctor_m377714278A7FAB4D7514D181355D61B01B3427B0_gshared)(__this, method);
}
// System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowArgumentOutOfRangeException_mBA2AF20A35144E0C43CD721A22EAC9FCA15D6550 (const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SleekRender.SleekRenderPostProcess::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SleekRenderPostProcess_OnEnable_m9BE5D8F5EC9B759E760480EA5A11AB9403A37534 (SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94 * __this, const RuntimeMethod* method)
{
	{
		SleekRenderPostProcess_CreateDefaultSettingsIfNoneLinked_mFAFCBC9B80CB0CAE18DB94C102CD3D0FAEFA83A9(__this, /*hidden argument*/NULL);
		SleekRenderPostProcess_CreateResources_m88233905ED6473C25EA3C8A4B3757879C201D6DA(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SleekRender.SleekRenderPostProcess::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SleekRenderPostProcess_OnDisable_m890AF19CE2A19B82FE057B9AF69803EA1171B332 (SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94 * __this, const RuntimeMethod* method)
{
	{
		SleekRenderPostProcess_ReleaseResources_mDFBCB6AF7DB534D25370ED70E8C21A4D8E85F165(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SleekRender.SleekRenderPostProcess::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SleekRenderPostProcess_OnRenderImage_m226B046B38C23DD3FBB020A757EE7161AF37004E (SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94 * __this, RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ___source0, RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ___target1, const RuntimeMethod* method)
{
	{
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_0 = ___source0;
		SleekRenderPostProcess_ApplyPostProcess_m7228BB4785430B68E38460258D93189559260835(__this, L_0, /*hidden argument*/NULL);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_1 = ___source0;
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_2 = ___target1;
		SleekRenderPostProcess_Compose_m76162AF19A38CE8168DB6ED4B2E89E56110D02BA(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SleekRender.SleekRenderPostProcess::ApplyPostProcess(UnityEngine.RenderTexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SleekRenderPostProcess_ApplyPostProcess_m7228BB4785430B68E38460258D93189559260835 (SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94 * __this, RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ___source0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142 * L_0 = __this->get_settings_4();
		NullCheck(L_0);
		bool L_1 = L_0->get_bloomEnabled_5();
		V_0 = L_1;
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_2 = ___source0;
		SleekRenderPostProcess_Downsample_m939DB30198E9BB4BEFF3884A283D450C8704EE96(__this, L_2, /*hidden argument*/NULL);
		bool L_3 = V_0;
		SleekRenderPostProcess_Bloom_m38FA596CFD5659B6F02915F17199F0DF0ADE16FC(__this, L_3, /*hidden argument*/NULL);
		bool L_4 = V_0;
		SleekRenderPostProcess_Precompose_mE5C8A6B77C24F3B90CDFC17D60895195BDD20965(__this, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SleekRender.SleekRenderPostProcess::Downsample(UnityEngine.RenderTexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SleekRenderPostProcess_Downsample_m939DB30198E9BB4BEFF3884A283D450C8704EE96 (SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94 * __this, RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ___source0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SleekRenderPostProcess_Downsample_m939DB30198E9BB4BEFF3884A283D450C8704EE96_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142 * L_0 = __this->get_settings_4();
		NullCheck(L_0);
		float L_1 = L_0->get_bloomThreshold_6();
		V_0 = ((float)((float)(1.0f)/(float)((float)il2cpp_codegen_subtract((float)(1.0f), (float)L_1))));
		SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142 * L_2 = __this->get_settings_4();
		NullCheck(L_2);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = L_2->get_bloomLumaVector_13();
		V_1 = L_3;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = V_1;
		float L_5 = L_4.get_x_2();
		float L_6 = V_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = V_1;
		float L_8 = L_7.get_y_3();
		float L_9 = V_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_10 = V_1;
		float L_11 = L_10.get_z_4();
		float L_12 = V_0;
		SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142 * L_13 = __this->get_settings_4();
		NullCheck(L_13);
		float L_14 = L_13->get_bloomThreshold_6();
		float L_15 = V_0;
		Vector4__ctor_m545458525879607A5392A10B175D0C19B2BC715D((Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E *)(&V_2), ((float)il2cpp_codegen_multiply((float)L_5, (float)L_6)), ((float)il2cpp_codegen_multiply((float)L_8, (float)L_9)), ((float)il2cpp_codegen_multiply((float)L_11, (float)L_12)), ((float)il2cpp_codegen_multiply((float)((-L_14)), (float)L_15)), /*hidden argument*/NULL);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_16 = __this->get__downsampleMaterial_5();
		IL2CPP_RUNTIME_CLASS_INIT(Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_il2cpp_TypeInfo_var);
		int32_t L_17 = ((Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_il2cpp_TypeInfo_var))->get__LuminanceConst_0();
		Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  L_18 = V_2;
		NullCheck(L_16);
		Material_SetVector_m95B7CB07B91F004B4DD9DB5DFA5146472737B8EA(L_16, L_17, L_18, /*hidden argument*/NULL);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_19 = ___source0;
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_20 = __this->get__downsampledBrightpassTexture_10();
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_21 = __this->get__downsampleMaterial_5();
		SleekRenderPostProcess_Blit_mEDE1D6060C22F747323C93F8ABECC7C879413CBE(__this, L_19, L_20, L_21, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SleekRender.SleekRenderPostProcess::Bloom(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SleekRenderPostProcess_Bloom_m38FA596CFD5659B6F02915F17199F0DF0ADE16FC (SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94 * __this, bool ___isBloomEnabled0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___isBloomEnabled0;
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_1 = __this->get__downsampledBrightpassTexture_10();
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_2 = __this->get__brightPassBlurTexture_11();
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_3 = __this->get__horizontalBlurMaterial_6();
		SleekRenderPostProcess_Blit_mEDE1D6060C22F747323C93F8ABECC7C879413CBE(__this, L_1, L_2, L_3, 0, /*hidden argument*/NULL);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_4 = __this->get__brightPassBlurTexture_11();
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_5 = __this->get__verticalBlurTexture_13();
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_6 = __this->get__verticalBlurMaterial_7();
		SleekRenderPostProcess_Blit_mEDE1D6060C22F747323C93F8ABECC7C879413CBE(__this, L_4, L_5, L_6, 0, /*hidden argument*/NULL);
	}

IL_0035:
	{
		return;
	}
}
// System.Void SleekRender.SleekRenderPostProcess::Precompose(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SleekRenderPostProcess_Precompose_mE5C8A6B77C24F3B90CDFC17D60895195BDD20965 (SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94 * __this, bool ___isBloomEnabled0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SleekRenderPostProcess_Precompose_mE5C8A6B77C24F3B90CDFC17D60895195BDD20965_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  V_4;
	memset((&V_4), 0, sizeof(V_4));
	{
		SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142 * L_0 = __this->get_settings_4();
		NullCheck(L_0);
		bool L_1 = L_0->get_vignetteEnabled_18();
		V_0 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_0030;
		}
	}
	{
		bool L_3 = __this->get__isVignetteAlreadyEnabled_21();
		if (L_3)
		{
			goto IL_0030;
		}
	}
	{
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_4 = __this->get__preComposeMaterial_8();
		NullCheck(L_4);
		Material_EnableKeyword_m7466758182CBBC40134C9048CDF682DF46F32FA9(L_4, _stringLiteral6574DEA2D7262D7E5060CA7786488B77C79BA130, /*hidden argument*/NULL);
		__this->set__isVignetteAlreadyEnabled_21((bool)1);
		goto IL_0052;
	}

IL_0030:
	{
		bool L_5 = V_0;
		if (L_5)
		{
			goto IL_0052;
		}
	}
	{
		bool L_6 = __this->get__isVignetteAlreadyEnabled_21();
		if (!L_6)
		{
			goto IL_0052;
		}
	}
	{
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_7 = __this->get__preComposeMaterial_8();
		NullCheck(L_7);
		Material_DisableKeyword_m2ACBFC5D28ED46FF2CF5532F00D702FF62C02ED3(L_7, _stringLiteral6574DEA2D7262D7E5060CA7786488B77C79BA130, /*hidden argument*/NULL);
		__this->set__isVignetteAlreadyEnabled_21((bool)0);
	}

IL_0052:
	{
		bool L_8 = V_0;
		if (!L_8)
		{
			goto IL_00f6;
		}
	}
	{
		SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142 * L_9 = __this->get_settings_4();
		NullCheck(L_9);
		float L_10 = L_9->get_vignetteBeginRadius_19();
		float L_11 = L_10;
		float L_12 = L_11;
		V_1 = ((float)il2cpp_codegen_multiply((float)L_12, (float)L_12));
		SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142 * L_13 = __this->get_settings_4();
		NullCheck(L_13);
		float L_14 = L_13->get_vignetteExpandRadius_20();
		V_2 = ((float)il2cpp_codegen_add((float)L_11, (float)L_14));
		float L_15 = V_2;
		float L_16 = V_1;
		V_3 = ((float)((float)(1.0f)/(float)((float)il2cpp_codegen_subtract((float)L_15, (float)L_16))));
		SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142 * L_17 = __this->get_settings_4();
		NullCheck(L_17);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_18 = L_17->get_vignetteColor_21();
		V_4 = L_18;
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_19 = __this->get__preComposeMaterial_8();
		IL2CPP_RUNTIME_CLASS_INIT(Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_il2cpp_TypeInfo_var);
		int32_t L_20 = ((Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_il2cpp_TypeInfo_var))->get__VignetteShape_8();
		float L_21 = V_3;
		float L_22 = V_3;
		float L_23 = V_3;
		float L_24 = V_1;
		Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  L_25;
		memset((&L_25), 0, sizeof(L_25));
		Vector4__ctor_mFADAC87CAF1F57AD3FE715E6930D78BF7D303D81((&L_25), ((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(4.0f), (float)L_21)), (float)L_22)), ((float)il2cpp_codegen_multiply((float)((-L_23)), (float)L_24)), /*hidden argument*/NULL);
		NullCheck(L_19);
		Material_SetVector_m95B7CB07B91F004B4DD9DB5DFA5146472737B8EA(L_19, L_20, L_25, /*hidden argument*/NULL);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_26 = __this->get__preComposeMaterial_8();
		int32_t L_27 = ((Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_il2cpp_TypeInfo_var))->get__VignetteColor_9();
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_28 = V_4;
		float L_29 = L_28.get_r_0();
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_30 = V_4;
		float L_31 = L_30.get_a_3();
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_32 = V_4;
		float L_33 = L_32.get_g_1();
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_34 = V_4;
		float L_35 = L_34.get_a_3();
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_36 = V_4;
		float L_37 = L_36.get_b_2();
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_38 = V_4;
		float L_39 = L_38.get_a_3();
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_40 = V_4;
		float L_41 = L_40.get_a_3();
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_42;
		memset((&L_42), 0, sizeof(L_42));
		Color__ctor_m20DF490CEB364C4FC36D7EE392640DF5B7420D7C((&L_42), ((float)il2cpp_codegen_multiply((float)L_29, (float)L_31)), ((float)il2cpp_codegen_multiply((float)L_33, (float)L_35)), ((float)il2cpp_codegen_multiply((float)L_37, (float)L_39)), L_41, /*hidden argument*/NULL);
		NullCheck(L_26);
		Material_SetColor_m48FBB701F6177B367EDFAC6BE896D183EF640725(L_26, L_27, L_42, /*hidden argument*/NULL);
	}

IL_00f6:
	{
		bool L_43 = ___isBloomEnabled0;
		if (!L_43)
		{
			goto IL_0150;
		}
	}
	{
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_44 = __this->get__preComposeMaterial_8();
		IL2CPP_RUNTIME_CLASS_INIT(Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_il2cpp_TypeInfo_var);
		int32_t L_45 = ((Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_il2cpp_TypeInfo_var))->get__BloomIntencity_1();
		SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142 * L_46 = __this->get_settings_4();
		NullCheck(L_46);
		float L_47 = L_46->get_bloomIntensity_7();
		NullCheck(L_44);
		Material_SetFloat_mC2FDDF0798373DEE6BBA9B9FFFE03EC3CFB9BF47(L_44, L_45, L_47, /*hidden argument*/NULL);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_48 = __this->get__preComposeMaterial_8();
		int32_t L_49 = ((Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_il2cpp_TypeInfo_var))->get__BloomTint_2();
		SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142 * L_50 = __this->get_settings_4();
		NullCheck(L_50);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_51 = L_50->get_bloomTint_8();
		NullCheck(L_48);
		Material_SetColor_m48FBB701F6177B367EDFAC6BE896D183EF640725(L_48, L_49, L_51, /*hidden argument*/NULL);
		bool L_52 = __this->get__isBloomAlreadyEnabled_20();
		if (L_52)
		{
			goto IL_016f;
		}
	}
	{
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_53 = __this->get__preComposeMaterial_8();
		NullCheck(L_53);
		Material_EnableKeyword_m7466758182CBBC40134C9048CDF682DF46F32FA9(L_53, _stringLiteral8C3FB1B5EB4296D3AF847E3A575580A61D698AF4, /*hidden argument*/NULL);
		__this->set__isBloomAlreadyEnabled_20((bool)1);
		goto IL_016f;
	}

IL_0150:
	{
		bool L_54 = __this->get__isBloomAlreadyEnabled_20();
		if (!L_54)
		{
			goto IL_016f;
		}
	}
	{
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_55 = __this->get__preComposeMaterial_8();
		NullCheck(L_55);
		Material_DisableKeyword_m2ACBFC5D28ED46FF2CF5532F00D702FF62C02ED3(L_55, _stringLiteral8C3FB1B5EB4296D3AF847E3A575580A61D698AF4, /*hidden argument*/NULL);
		__this->set__isBloomAlreadyEnabled_20((bool)0);
	}

IL_016f:
	{
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_56 = __this->get__downsampledBrightpassTexture_10();
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_57 = __this->get__preComposeTexture_14();
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_58 = __this->get__preComposeMaterial_8();
		SleekRenderPostProcess_Blit_mEDE1D6060C22F747323C93F8ABECC7C879413CBE(__this, L_56, L_57, L_58, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SleekRender.SleekRenderPostProcess::Compose(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SleekRenderPostProcess_Compose_m76162AF19A38CE8168DB6ED4B2E89E56110D02BA (SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94 * __this, RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ___source0, RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ___target1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SleekRenderPostProcess_Compose_m76162AF19A38CE8168DB6ED4B2E89E56110D02BA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  V_0;
	memset((&V_0), 0, sizeof(V_0));
	float V_1 = 0.0f;
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  V_2;
	memset((&V_2), 0, sizeof(V_2));
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	{
		SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142 * L_0 = __this->get_settings_4();
		NullCheck(L_0);
		Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  L_1 = L_0->get_colorize_16();
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_2 = Color32_op_Implicit_mA89CAD76E78975F51DF7374A67D18A5F6EF8DA61(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_3 = V_0;
		float L_4 = L_3.get_a_3();
		V_1 = L_4;
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_5 = V_0;
		float L_6 = L_5.get_r_0();
		float L_7 = V_1;
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_8 = V_0;
		float L_9 = L_8.get_g_1();
		float L_10 = V_1;
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_11 = V_0;
		float L_12 = L_11.get_b_2();
		float L_13 = V_1;
		float L_14 = V_1;
		Color__ctor_m20DF490CEB364C4FC36D7EE392640DF5B7420D7C((Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 *)(&V_2), ((float)il2cpp_codegen_multiply((float)L_6, (float)L_7)), ((float)il2cpp_codegen_multiply((float)L_9, (float)L_10)), ((float)il2cpp_codegen_multiply((float)L_12, (float)L_13)), ((float)il2cpp_codegen_subtract((float)(1.0f), (float)L_14)), /*hidden argument*/NULL);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_15 = __this->get__composeMaterial_9();
		IL2CPP_RUNTIME_CLASS_INIT(Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_il2cpp_TypeInfo_var);
		int32_t L_16 = ((Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_il2cpp_TypeInfo_var))->get__Colorize_7();
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_17 = V_2;
		NullCheck(L_15);
		Material_SetColor_m48FBB701F6177B367EDFAC6BE896D183EF640725(L_15, L_16, L_17, /*hidden argument*/NULL);
		SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142 * L_18 = __this->get_settings_4();
		NullCheck(L_18);
		bool L_19 = L_18->get_colorizeEnabled_15();
		if (!L_19)
		{
			goto IL_007d;
		}
	}
	{
		bool L_20 = __this->get__isColorizeAlreadyEnabled_19();
		if (L_20)
		{
			goto IL_007d;
		}
	}
	{
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_21 = __this->get__composeMaterial_9();
		NullCheck(L_21);
		Material_EnableKeyword_m7466758182CBBC40134C9048CDF682DF46F32FA9(L_21, _stringLiteral4CA0AAB8D31E2D73CA72A054532184CDBCD32F53, /*hidden argument*/NULL);
		__this->set__isColorizeAlreadyEnabled_19((bool)1);
		goto IL_00a9;
	}

IL_007d:
	{
		SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142 * L_22 = __this->get_settings_4();
		NullCheck(L_22);
		bool L_23 = L_22->get_colorizeEnabled_15();
		if (L_23)
		{
			goto IL_00a9;
		}
	}
	{
		bool L_24 = __this->get__isColorizeAlreadyEnabled_19();
		if (!L_24)
		{
			goto IL_00a9;
		}
	}
	{
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_25 = __this->get__composeMaterial_9();
		NullCheck(L_25);
		Material_DisableKeyword_m2ACBFC5D28ED46FF2CF5532F00D702FF62C02ED3(L_25, _stringLiteral4CA0AAB8D31E2D73CA72A054532184CDBCD32F53, /*hidden argument*/NULL);
		__this->set__isColorizeAlreadyEnabled_19((bool)0);
	}

IL_00a9:
	{
		SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142 * L_26 = __this->get_settings_4();
		NullCheck(L_26);
		float L_27 = L_26->get_contrast_24();
		V_3 = ((float)il2cpp_codegen_add((float)L_27, (float)(1.0f)));
		SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142 * L_28 = __this->get_settings_4();
		NullCheck(L_28);
		float L_29 = L_28->get_brightness_25();
		V_4 = ((float)((float)((float)il2cpp_codegen_add((float)L_29, (float)(1.0f)))/(float)(2.0f)));
		float L_30 = V_3;
		float L_31 = V_4;
		V_5 = ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)(-0.5f), (float)((float)il2cpp_codegen_add((float)L_30, (float)(1.0f))))), (float)((float)il2cpp_codegen_multiply((float)L_31, (float)(2.0f)))));
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_32 = __this->get__composeMaterial_9();
		IL2CPP_RUNTIME_CLASS_INIT(Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_il2cpp_TypeInfo_var);
		int32_t L_33 = ((Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_il2cpp_TypeInfo_var))->get__BrightnessContrast_10();
		float L_34 = V_3;
		float L_35 = V_4;
		float L_36 = V_5;
		Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  L_37;
		memset((&L_37), 0, sizeof(L_37));
		Vector4__ctor_mBAB87E6221CF38FDA7BE868992CE92476B0EDFC0((&L_37), L_34, L_35, L_36, /*hidden argument*/NULL);
		NullCheck(L_32);
		Material_SetVector_m95B7CB07B91F004B4DD9DB5DFA5146472737B8EA(L_32, L_33, L_37, /*hidden argument*/NULL);
		SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142 * L_38 = __this->get_settings_4();
		NullCheck(L_38);
		bool L_39 = L_38->get_brightnessContrastEnabled_23();
		if (!L_39)
		{
			goto IL_0134;
		}
	}
	{
		bool L_40 = __this->get__isContrastAndBrightnessAlreadyEnabled_23();
		if (L_40)
		{
			goto IL_0134;
		}
	}
	{
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_41 = __this->get__composeMaterial_9();
		NullCheck(L_41);
		Material_EnableKeyword_m7466758182CBBC40134C9048CDF682DF46F32FA9(L_41, _stringLiteralD53BF922AC5FC0414F5AFDC2F09EDC5F766B3C86, /*hidden argument*/NULL);
		__this->set__isContrastAndBrightnessAlreadyEnabled_23((bool)1);
		goto IL_0160;
	}

IL_0134:
	{
		SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142 * L_42 = __this->get_settings_4();
		NullCheck(L_42);
		bool L_43 = L_42->get_brightnessContrastEnabled_23();
		if (L_43)
		{
			goto IL_0160;
		}
	}
	{
		bool L_44 = __this->get__isContrastAndBrightnessAlreadyEnabled_23();
		if (!L_44)
		{
			goto IL_0160;
		}
	}
	{
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_45 = __this->get__composeMaterial_9();
		NullCheck(L_45);
		Material_DisableKeyword_m2ACBFC5D28ED46FF2CF5532F00D702FF62C02ED3(L_45, _stringLiteralD53BF922AC5FC0414F5AFDC2F09EDC5F766B3C86, /*hidden argument*/NULL);
		__this->set__isContrastAndBrightnessAlreadyEnabled_23((bool)0);
	}

IL_0160:
	{
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_46 = ___source0;
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_47 = ___target1;
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_48 = __this->get__composeMaterial_9();
		SleekRenderPostProcess_Blit_mEDE1D6060C22F747323C93F8ABECC7C879413CBE(__this, L_46, L_47, L_48, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SleekRender.SleekRenderPostProcess::CreateResources()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SleekRenderPostProcess_CreateResources_m88233905ED6473C25EA3C8A4B3757879C201D6DA (SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SleekRenderPostProcess_CreateResources_m88233905ED6473C25EA3C8A4B3757879C201D6DA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * V_0 = NULL;
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * V_1 = NULL;
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * V_2 = NULL;
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * V_3 = NULL;
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * V_4 = NULL;
	int32_t V_5 = 0;
	float V_6 = 0.0f;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	float V_11 = 0.0f;
	float V_12 = 0.0f;
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  V_13;
	memset((&V_13), 0, sizeof(V_13));
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  V_14;
	memset((&V_14), 0, sizeof(V_14));
	int32_t G_B2_0 = 0;
	int32_t G_B1_0 = 0;
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	{
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_0 = Component_GetComponent_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_mB090F51A34716700C0F4F1B08F9330C6F503DB9E(__this, /*hidden argument*/Component_GetComponent_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_mB090F51A34716700C0F4F1B08F9330C6F503DB9E_RuntimeMethod_var);
		__this->set__mainCamera_15(L_0);
		Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * L_1 = Shader_Find_m755654AA68D1C663A3E20A10E00CDC10F96C962B(_stringLiteral745D7F7582276839A0CDACA165D0880010C3B0CB, /*hidden argument*/NULL);
		V_0 = L_1;
		Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * L_2 = Shader_Find_m755654AA68D1C663A3E20A10E00CDC10F96C962B(_stringLiteral77831B254E3A6816BFF569221912AD410B829E3B, /*hidden argument*/NULL);
		V_1 = L_2;
		Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * L_3 = Shader_Find_m755654AA68D1C663A3E20A10E00CDC10F96C962B(_stringLiteral49509FA907B8AD7BE4933B9CB7CBCD7B386D7165, /*hidden argument*/NULL);
		V_2 = L_3;
		Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * L_4 = Shader_Find_m755654AA68D1C663A3E20A10E00CDC10F96C962B(_stringLiteral8D30A1632F91DD50F6A0FEC47EAA8ADB8D922D60, /*hidden argument*/NULL);
		V_3 = L_4;
		Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * L_5 = Shader_Find_m755654AA68D1C663A3E20A10E00CDC10F96C962B(_stringLiteral07C17D8026434E7F6CC9CE35B102F6869FB838FB, /*hidden argument*/NULL);
		V_4 = L_5;
		Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * L_6 = V_0;
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_7 = (Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *)il2cpp_codegen_object_new(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598_il2cpp_TypeInfo_var);
		Material__ctor_m81E76B5C1316004F25D4FE9CEC0E78A7428DABA8(L_7, L_6, /*hidden argument*/NULL);
		__this->set__downsampleMaterial_5(L_7);
		Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * L_8 = V_1;
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_9 = (Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *)il2cpp_codegen_object_new(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598_il2cpp_TypeInfo_var);
		Material__ctor_m81E76B5C1316004F25D4FE9CEC0E78A7428DABA8(L_9, L_8, /*hidden argument*/NULL);
		__this->set__horizontalBlurMaterial_6(L_9);
		Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * L_10 = V_2;
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_11 = (Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *)il2cpp_codegen_object_new(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598_il2cpp_TypeInfo_var);
		Material__ctor_m81E76B5C1316004F25D4FE9CEC0E78A7428DABA8(L_11, L_10, /*hidden argument*/NULL);
		__this->set__verticalBlurMaterial_7(L_11);
		Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * L_12 = V_4;
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_13 = (Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *)il2cpp_codegen_object_new(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598_il2cpp_TypeInfo_var);
		Material__ctor_m81E76B5C1316004F25D4FE9CEC0E78A7428DABA8(L_13, L_12, /*hidden argument*/NULL);
		__this->set__preComposeMaterial_8(L_13);
		Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * L_14 = V_3;
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_15 = (Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *)il2cpp_codegen_object_new(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598_il2cpp_TypeInfo_var);
		Material__ctor_m81E76B5C1316004F25D4FE9CEC0E78A7428DABA8(L_15, L_14, /*hidden argument*/NULL);
		__this->set__composeMaterial_9(L_15);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_16 = __this->get__mainCamera_15();
		NullCheck(L_16);
		int32_t L_17 = Camera_get_pixelWidth_m67EC53853580E35527F32D6EA002FE21C234172E(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		int32_t L_18 = Mathf_RoundToInt_m0EAD8BD38FCB72FA1D8A04E96337C820EC83F041((((float)((float)L_17))), /*hidden argument*/NULL);
		__this->set__currentCameraPixelWidth_17(L_18);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_19 = __this->get__mainCamera_15();
		NullCheck(L_19);
		int32_t L_20 = Camera_get_pixelHeight_m38879ACBA6B21C25E83AB07FA37A8E5EB7A51B05(L_19, /*hidden argument*/NULL);
		int32_t L_21 = Mathf_RoundToInt_m0EAD8BD38FCB72FA1D8A04E96337C820EC83F041((((float)((float)L_20))), /*hidden argument*/NULL);
		__this->set__currentCameraPixelHeight_18(L_21);
		int32_t L_22 = __this->get__currentCameraPixelWidth_17();
		int32_t L_23 = __this->get__currentCameraPixelHeight_18();
		V_5 = L_23;
		int32_t L_24 = V_5;
		int32_t L_25 = Mathf_Min_m1A2CC204E361AE13C329B6535165179798D3313A(L_24, ((int32_t)720), /*hidden argument*/NULL);
		int32_t L_26 = V_5;
		V_6 = ((float)((float)(((float)((float)L_25)))/(float)(((float)((float)L_26)))));
		SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142 * L_27 = __this->get_settings_4();
		NullCheck(L_27);
		int32_t L_28 = L_27->get_bloomTextureHeight_11();
		V_7 = L_28;
		SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142 * L_29 = __this->get_settings_4();
		NullCheck(L_29);
		bool L_30 = L_29->get_preserveAspectRatio_9();
		G_B1_0 = L_22;
		if (L_30)
		{
			G_B2_0 = L_22;
			goto IL_00f7;
		}
	}
	{
		SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142 * L_31 = __this->get_settings_4();
		NullCheck(L_31);
		int32_t L_32 = L_31->get_bloomTextureWidth_10();
		G_B3_0 = L_32;
		G_B3_1 = G_B1_0;
		goto IL_010c;
	}

IL_00f7:
	{
		int32_t L_33 = V_7;
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_34 = __this->get__mainCamera_15();
		float L_35 = SleekRenderPostProcess_GetCurrentAspect_mFA278408C602D5213CB17EC938C37D070544DC79(__this, L_34, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		int32_t L_36 = Mathf_RoundToInt_m0EAD8BD38FCB72FA1D8A04E96337C820EC83F041(((float)il2cpp_codegen_multiply((float)(((float)((float)L_33))), (float)L_35)), /*hidden argument*/NULL);
		G_B3_0 = L_36;
		G_B3_1 = G_B2_0;
	}

IL_010c:
	{
		V_8 = G_B3_0;
		float L_37 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		int32_t L_38 = Mathf_RoundToInt_m0EAD8BD38FCB72FA1D8A04E96337C820EC83F041(((float)((float)((float)il2cpp_codegen_multiply((float)(((float)((float)G_B3_1))), (float)L_37))/(float)(5.0f))), /*hidden argument*/NULL);
		V_9 = L_38;
		int32_t L_39 = V_5;
		float L_40 = V_6;
		int32_t L_41 = Mathf_RoundToInt_m0EAD8BD38FCB72FA1D8A04E96337C820EC83F041(((float)((float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_39))), (float)L_40))/(float)(5.0f))), /*hidden argument*/NULL);
		V_10 = L_41;
		int32_t L_42 = V_9;
		int32_t L_43 = V_10;
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_44 = SleekRenderPostProcess_CreateTransientRenderTexture_m5896F414C30F00DE545EF638447324CC3D1DE67F(__this, _stringLiteral6CCAA9D4E4A7F56B1FEDDFBAC85B689B84422E0E, L_42, L_43, /*hidden argument*/NULL);
		__this->set__downsampledBrightpassTexture_10(L_44);
		int32_t L_45 = V_8;
		int32_t L_46 = V_7;
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_47 = SleekRenderPostProcess_CreateTransientRenderTexture_m5896F414C30F00DE545EF638447324CC3D1DE67F(__this, _stringLiteralF234EE02A8E22EC8F4118B8395D4992E644FE853, L_45, L_46, /*hidden argument*/NULL);
		__this->set__brightPassBlurTexture_11(L_47);
		int32_t L_48 = V_8;
		int32_t L_49 = V_7;
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_50 = SleekRenderPostProcess_CreateTransientRenderTexture_m5896F414C30F00DE545EF638447324CC3D1DE67F(__this, _stringLiteralF6DC242E6953FE7235423EAAA611B8770BA13F2C, L_48, L_49, /*hidden argument*/NULL);
		__this->set__horizontalBlurTexture_12(L_50);
		int32_t L_51 = V_8;
		int32_t L_52 = V_7;
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_53 = SleekRenderPostProcess_CreateTransientRenderTexture_m5896F414C30F00DE545EF638447324CC3D1DE67F(__this, _stringLiteral8042151345307136E9BF2C874EF9884446C75512, L_51, L_52, /*hidden argument*/NULL);
		__this->set__verticalBlurTexture_13(L_53);
		int32_t L_54 = V_9;
		int32_t L_55 = V_10;
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_56 = SleekRenderPostProcess_CreateTransientRenderTexture_m5896F414C30F00DE545EF638447324CC3D1DE67F(__this, _stringLiteral3DEF32960CD3BA7431114E6BD565613B2E9B64BF, L_54, L_55, /*hidden argument*/NULL);
		__this->set__preComposeTexture_14(L_56);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_57 = __this->get__verticalBlurMaterial_7();
		IL2CPP_RUNTIME_CLASS_INIT(Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_il2cpp_TypeInfo_var);
		int32_t L_58 = ((Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_il2cpp_TypeInfo_var))->get__MainTex_3();
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_59 = __this->get__downsampledBrightpassTexture_10();
		NullCheck(L_57);
		Material_SetTexture_m4FFF0B403A64253B83534701104F017840142ACA(L_57, L_58, L_59, /*hidden argument*/NULL);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_60 = __this->get__verticalBlurMaterial_7();
		int32_t L_61 = ((Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_il2cpp_TypeInfo_var))->get__BloomTex_4();
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_62 = __this->get__horizontalBlurTexture_12();
		NullCheck(L_60);
		Material_SetTexture_m4FFF0B403A64253B83534701104F017840142ACA(L_60, L_61, L_62, /*hidden argument*/NULL);
		int32_t L_63 = V_8;
		V_11 = ((float)((float)(1.0f)/(float)(((float)((float)L_63)))));
		int32_t L_64 = V_7;
		V_12 = ((float)((float)(1.0f)/(float)(((float)((float)L_64)))));
		float L_65 = V_11;
		float L_66 = V_12;
		Vector4__ctor_mFADAC87CAF1F57AD3FE715E6930D78BF7D303D81((Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E *)(&V_13), L_65, L_66, /*hidden argument*/NULL);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_67 = __this->get__verticalBlurMaterial_7();
		int32_t L_68 = ((Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_il2cpp_TypeInfo_var))->get__TexelSize_6();
		Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  L_69 = V_13;
		NullCheck(L_67);
		Material_SetVector_m95B7CB07B91F004B4DD9DB5DFA5146472737B8EA(L_67, L_68, L_69, /*hidden argument*/NULL);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_70 = __this->get__horizontalBlurMaterial_6();
		int32_t L_71 = ((Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_il2cpp_TypeInfo_var))->get__TexelSize_6();
		Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  L_72 = V_13;
		NullCheck(L_70);
		Material_SetVector_m95B7CB07B91F004B4DD9DB5DFA5146472737B8EA(L_70, L_71, L_72, /*hidden argument*/NULL);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_73 = __this->get__preComposeMaterial_8();
		int32_t L_74 = ((Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_il2cpp_TypeInfo_var))->get__BloomTex_4();
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_75 = __this->get__verticalBlurTexture_13();
		NullCheck(L_73);
		Material_SetTexture_m4FFF0B403A64253B83534701104F017840142ACA(L_73, L_74, L_75, /*hidden argument*/NULL);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_76 = __this->get__downsampledBrightpassTexture_10();
		NullCheck(L_76);
		int32_t L_77 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_76);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_78 = __this->get__downsampledBrightpassTexture_10();
		NullCheck(L_78);
		int32_t L_79 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_78);
		Vector4__ctor_mFADAC87CAF1F57AD3FE715E6930D78BF7D303D81((Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E *)(&V_14), ((float)((float)(1.0f)/(float)(((float)((float)L_77))))), ((float)((float)(1.0f)/(float)(((float)((float)L_79))))), /*hidden argument*/NULL);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_80 = __this->get__downsampleMaterial_5();
		int32_t L_81 = ((Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_il2cpp_TypeInfo_var))->get__TexelSize_6();
		Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  L_82 = V_14;
		NullCheck(L_80);
		Material_SetVector_m95B7CB07B91F004B4DD9DB5DFA5146472737B8EA(L_80, L_81, L_82, /*hidden argument*/NULL);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_83 = __this->get__composeMaterial_9();
		int32_t L_84 = ((Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_il2cpp_TypeInfo_var))->get__PreComposeTex_5();
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_85 = __this->get__preComposeTexture_14();
		NullCheck(L_83);
		Material_SetTexture_m4FFF0B403A64253B83534701104F017840142ACA(L_83, L_84, L_85, /*hidden argument*/NULL);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_86 = __this->get__composeMaterial_9();
		int32_t L_87 = ((Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_il2cpp_TypeInfo_var))->get__LuminanceConst_0();
		Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  L_88;
		memset((&L_88), 0, sizeof(L_88));
		Vector4__ctor_m545458525879607A5392A10B175D0C19B2BC715D((&L_88), (0.2126f), (0.7152f), (0.0722f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_86);
		Material_SetVector_m95B7CB07B91F004B4DD9DB5DFA5146472737B8EA(L_86, L_87, L_88, /*hidden argument*/NULL);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_89 = SleekRenderPostProcess_CreateScreenSpaceQuadMesh_m2D59ABE6B083BEF9A9DE056AF5725199DD9FCB0C(__this, /*hidden argument*/NULL);
		__this->set__fullscreenQuadMesh_16(L_89);
		__this->set__isColorizeAlreadyEnabled_19((bool)0);
		__this->set__isBloomAlreadyEnabled_20((bool)0);
		__this->set__isVignetteAlreadyEnabled_21((bool)0);
		__this->set__isContrastAndBrightnessAlreadyEnabled_23((bool)0);
		return;
	}
}
// UnityEngine.RenderTexture SleekRender.SleekRenderPostProcess::CreateTransientRenderTexture(System.String,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * SleekRenderPostProcess_CreateTransientRenderTexture_m5896F414C30F00DE545EF638447324CC3D1DE67F (SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94 * __this, String_t* ___textureName0, int32_t ___width1, int32_t ___height2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SleekRenderPostProcess_CreateTransientRenderTexture_m5896F414C30F00DE545EF638447324CC3D1DE67F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___width1;
		int32_t L_1 = ___height2;
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_2 = (RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 *)il2cpp_codegen_object_new(RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6_il2cpp_TypeInfo_var);
		RenderTexture__ctor_m0FF5DDAB599ED301091CF23D4C76691D8EC70CA5(L_2, L_0, L_1, 0, 0, /*hidden argument*/NULL);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_3 = L_2;
		String_t* L_4 = ___textureName0;
		NullCheck(L_3);
		Object_set_name_m538711B144CDE30F929376BCF72D0DC8F85D0826(L_3, L_4, /*hidden argument*/NULL);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_5 = L_3;
		NullCheck(L_5);
		Texture_set_filterMode_mB9AC927A527EFE95771B9B438E2CFB9EDA84AF01(L_5, 1, /*hidden argument*/NULL);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_6 = L_5;
		NullCheck(L_6);
		Texture_set_wrapMode_m85E9A995D5947B59FE13A7311E891F3DEDEBBCEC(L_6, 1, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.RenderTexture SleekRender.SleekRenderPostProcess::CreateMainRenderTexture(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * SleekRenderPostProcess_CreateMainRenderTexture_mA21F6ABA9A57B0BD2A2ACBCCEBD12CA09A16838D (SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94 * __this, int32_t ___width0, int32_t ___height1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SleekRenderPostProcess_CreateMainRenderTexture_mA21F6ABA9A57B0BD2A2ACBCCEBD12CA09A16838D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * G_B4_0 = NULL;
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * G_B4_1 = NULL;
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * G_B3_0 = NULL;
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * G_B3_1 = NULL;
	int32_t G_B5_0 = 0;
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * G_B5_1 = NULL;
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * G_B5_2 = NULL;
	{
		int32_t L_0 = SystemInfo_get_graphicsDeviceType_m675AD9D5FA869DF9E71FAEC03F39E8AE8DEBA8D0(/*hidden argument*/NULL);
		String_t* L_1 = SystemInfo_get_graphicsDeviceName_mD9D09CDF695610D3355F28EAD9EE61F7870F52E8(/*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = String_Contains_m4488034AF8CB3EEA9A205EB8A1F25D438FF8704B(L_1, _stringLiteral088472395B2697109252AA04C2F12D53CF051D62, /*hidden argument*/NULL);
		V_0 = L_2;
		bool L_3 = SystemInfo_SupportsRenderTextureFormat_m74D259714A97501D28951CA48298D9F0AE3B5907(4, /*hidden argument*/NULL);
		V_1 = (bool)((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
		V_2 = 4;
		bool L_4 = V_0;
		bool L_5 = V_1;
		if (!((int32_t)((int32_t)((int32_t)((int32_t)((((int32_t)L_0) == ((int32_t)((int32_t)16)))? 1 : 0)|(int32_t)L_4))|(int32_t)L_5)))
		{
			goto IL_002d;
		}
	}
	{
		V_2 = 0;
	}

IL_002d:
	{
		int32_t L_6 = ___width0;
		int32_t L_7 = ___height1;
		int32_t L_8 = V_2;
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_9 = (RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 *)il2cpp_codegen_object_new(RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6_il2cpp_TypeInfo_var);
		RenderTexture__ctor_m0FF5DDAB599ED301091CF23D4C76691D8EC70CA5(L_9, L_6, L_7, ((int32_t)16), L_8, /*hidden argument*/NULL);
		int32_t L_10 = QualitySettings_get_antiAliasing_m28EE8A60C753C1D160BB393D92D98CC0E1776B16(/*hidden argument*/NULL);
		V_3 = L_10;
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_11 = L_9;
		int32_t L_12 = V_3;
		G_B3_0 = L_11;
		G_B3_1 = L_11;
		if (!L_12)
		{
			G_B4_0 = L_11;
			G_B4_1 = L_11;
			goto IL_0044;
		}
	}
	{
		int32_t L_13 = V_3;
		G_B5_0 = L_13;
		G_B5_1 = G_B3_0;
		G_B5_2 = G_B3_1;
		goto IL_0045;
	}

IL_0044:
	{
		G_B5_0 = 1;
		G_B5_1 = G_B4_0;
		G_B5_2 = G_B4_1;
	}

IL_0045:
	{
		NullCheck(G_B5_1);
		RenderTexture_set_antiAliasing_mBE37447FA23E0D57731C1456165E03303EC9B559(G_B5_1, G_B5_0, /*hidden argument*/NULL);
		return G_B5_2;
	}
}
// System.Void SleekRender.SleekRenderPostProcess::ReleaseResources()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SleekRenderPostProcess_ReleaseResources_mDFBCB6AF7DB534D25370ED70E8C21A4D8E85F165 (SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94 * __this, const RuntimeMethod* method)
{
	{
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_0 = __this->get__downsampleMaterial_5();
		SleekRenderPostProcess_DestroyImmediateIfNotNull_m0E4B29743210245443999A680294C1DDBF0140DC(__this, L_0, /*hidden argument*/NULL);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_1 = __this->get__horizontalBlurMaterial_6();
		SleekRenderPostProcess_DestroyImmediateIfNotNull_m0E4B29743210245443999A680294C1DDBF0140DC(__this, L_1, /*hidden argument*/NULL);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_2 = __this->get__verticalBlurMaterial_7();
		SleekRenderPostProcess_DestroyImmediateIfNotNull_m0E4B29743210245443999A680294C1DDBF0140DC(__this, L_2, /*hidden argument*/NULL);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_3 = __this->get__preComposeMaterial_8();
		SleekRenderPostProcess_DestroyImmediateIfNotNull_m0E4B29743210245443999A680294C1DDBF0140DC(__this, L_3, /*hidden argument*/NULL);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_4 = __this->get__composeMaterial_9();
		SleekRenderPostProcess_DestroyImmediateIfNotNull_m0E4B29743210245443999A680294C1DDBF0140DC(__this, L_4, /*hidden argument*/NULL);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_5 = __this->get__downsampledBrightpassTexture_10();
		SleekRenderPostProcess_DestroyImmediateIfNotNull_m0E4B29743210245443999A680294C1DDBF0140DC(__this, L_5, /*hidden argument*/NULL);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_6 = __this->get__brightPassBlurTexture_11();
		SleekRenderPostProcess_DestroyImmediateIfNotNull_m0E4B29743210245443999A680294C1DDBF0140DC(__this, L_6, /*hidden argument*/NULL);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_7 = __this->get__horizontalBlurTexture_12();
		SleekRenderPostProcess_DestroyImmediateIfNotNull_m0E4B29743210245443999A680294C1DDBF0140DC(__this, L_7, /*hidden argument*/NULL);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_8 = __this->get__verticalBlurTexture_13();
		SleekRenderPostProcess_DestroyImmediateIfNotNull_m0E4B29743210245443999A680294C1DDBF0140DC(__this, L_8, /*hidden argument*/NULL);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_9 = __this->get__preComposeTexture_14();
		SleekRenderPostProcess_DestroyImmediateIfNotNull_m0E4B29743210245443999A680294C1DDBF0140DC(__this, L_9, /*hidden argument*/NULL);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_10 = __this->get__fullscreenQuadMesh_16();
		SleekRenderPostProcess_DestroyImmediateIfNotNull_m0E4B29743210245443999A680294C1DDBF0140DC(__this, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SleekRender.SleekRenderPostProcess::DestroyImmediateIfNotNull(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SleekRenderPostProcess_DestroyImmediateIfNotNull_m0E4B29743210245443999A680294C1DDBF0140DC (SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94 * __this, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SleekRenderPostProcess_DestroyImmediateIfNotNull_m0E4B29743210245443999A680294C1DDBF0140DC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_0 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000f;
		}
	}
	{
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_2 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_mF6F4415EF22249D6E650FAA40E403283F19B7446(L_2, /*hidden argument*/NULL);
	}

IL_000f:
	{
		return;
	}
}
// System.Void SleekRender.SleekRenderPostProcess::Blit(UnityEngine.Texture,UnityEngine.RenderTexture,UnityEngine.Material,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SleekRenderPostProcess_Blit_mEDE1D6060C22F747323C93F8ABECC7C879413CBE (SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94 * __this, Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___source0, RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ___destination1, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material2, int32_t ___materialPass3, const RuntimeMethod* method)
{
	{
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_0 = ___destination1;
		SleekRenderPostProcess_SetActiveRenderTextureAndClear_m01E22DC5E4C5579AA8732CA0F67E01F39E4AC89F(L_0, /*hidden argument*/NULL);
		Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * L_1 = ___source0;
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_2 = ___material2;
		int32_t L_3 = ___materialPass3;
		SleekRenderPostProcess_DrawFullscreenQuad_m77F12B59A976F77BFC5A8BF270AD5E5D30408DD0(__this, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SleekRender.SleekRenderPostProcess::SetActiveRenderTextureAndClear(UnityEngine.RenderTexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SleekRenderPostProcess_SetActiveRenderTextureAndClear_m01E22DC5E4C5579AA8732CA0F67E01F39E4AC89F (RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ___destination0, const RuntimeMethod* method)
{
	{
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_0 = ___destination0;
		RenderTexture_set_active_m992E25C701DEFC8042B31022EA45F02A787A84F1(L_0, /*hidden argument*/NULL);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_1;
		memset((&L_1), 0, sizeof(L_1));
		Color__ctor_m20DF490CEB364C4FC36D7EE392640DF5B7420D7C((&L_1), (1.0f), (0.75f), (0.5f), (0.8f), /*hidden argument*/NULL);
		GL_Clear_mBC8B714C794457D52A5343F40399BBDF57BA978A((bool)1, (bool)1, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SleekRender.SleekRenderPostProcess::DrawFullscreenQuad(UnityEngine.Texture,UnityEngine.Material,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SleekRenderPostProcess_DrawFullscreenQuad_m77F12B59A976F77BFC5A8BF270AD5E5D30408DD0 (SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94 * __this, Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___source0, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material1, int32_t ___materialPass2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SleekRenderPostProcess_DrawFullscreenQuad_m77F12B59A976F77BFC5A8BF270AD5E5D30408DD0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_0 = ___material1;
		IL2CPP_RUNTIME_CLASS_INIT(Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_il2cpp_TypeInfo_var);
		int32_t L_1 = ((Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_il2cpp_TypeInfo_var))->get__MainTex_3();
		Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * L_2 = ___source0;
		NullCheck(L_0);
		Material_SetTexture_m4FFF0B403A64253B83534701104F017840142ACA(L_0, L_1, L_2, /*hidden argument*/NULL);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_3 = ___material1;
		int32_t L_4 = ___materialPass2;
		NullCheck(L_3);
		Material_SetPass_m4BE0A8FCBF158C83522AA2F69118A2FE33683918(L_3, L_4, /*hidden argument*/NULL);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_5 = __this->get__fullscreenQuadMesh_16();
		IL2CPP_RUNTIME_CLASS_INIT(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_il2cpp_TypeInfo_var);
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_6 = Matrix4x4_get_identity_mA0CECDE2A5E85CF014375084624F3770B5B7B79B(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t6FB7A5D4561F3AB3C34BF334BB0BD8061BE763B1_il2cpp_TypeInfo_var);
		Graphics_DrawMeshNow_m65719727D0812E6140C9DC6AF4A7BDBB72B69FD0(L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SleekRender.SleekRenderPostProcess::CheckScreenSizeAndRecreateTexturesIfNeeded(UnityEngine.Camera)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SleekRenderPostProcess_CheckScreenSizeAndRecreateTexturesIfNeeded_m8FE59E4480A6A09382775B46448C5784B79029E4 (SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94 * __this, Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___mainCamera0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SleekRenderPostProcess_CheckScreenSizeAndRecreateTexturesIfNeeded_m8FE59E4480A6A09382775B46448C5784B79029E4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	int32_t G_B5_0 = 0;
	int32_t G_B4_0 = 0;
	int32_t G_B9_0 = 0;
	int32_t G_B6_0 = 0;
	int32_t G_B7_0 = 0;
	int32_t G_B8_0 = 0;
	int32_t G_B11_0 = 0;
	int32_t G_B10_0 = 0;
	int32_t G_B13_0 = 0;
	int32_t G_B14_0 = 0;
	int32_t G_B12_0 = 0;
	{
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_0 = ___mainCamera0;
		NullCheck(L_0);
		int32_t L_1 = Camera_get_pixelWidth_m67EC53853580E35527F32D6EA002FE21C234172E(L_0, /*hidden argument*/NULL);
		int32_t L_2 = __this->get__currentCameraPixelWidth_17();
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0021;
		}
	}
	{
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_3 = ___mainCamera0;
		NullCheck(L_3);
		int32_t L_4 = Camera_get_pixelHeight_m38879ACBA6B21C25E83AB07FA37A8E5EB7A51B05(L_3, /*hidden argument*/NULL);
		int32_t L_5 = __this->get__currentCameraPixelHeight_18();
		G_B3_0 = ((((int32_t)((((int32_t)L_4) == ((int32_t)L_5))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 1;
	}

IL_0022:
	{
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_6 = __this->get__horizontalBlurTexture_12();
		NullCheck(L_6);
		int32_t L_7 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_6);
		SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142 * L_8 = __this->get_settings_4();
		NullCheck(L_8);
		int32_t L_9 = L_8->get_bloomTextureHeight_11();
		V_0 = (bool)((((int32_t)((((int32_t)L_7) == ((int32_t)L_9))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142 * L_10 = __this->get_settings_4();
		NullCheck(L_10);
		bool L_11 = L_10->get_preserveAspectRatio_9();
		G_B4_0 = G_B3_0;
		if (L_11)
		{
			G_B5_0 = G_B3_0;
			goto IL_0069;
		}
	}
	{
		bool L_12 = V_0;
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_13 = __this->get__horizontalBlurTexture_12();
		NullCheck(L_13);
		int32_t L_14 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_13);
		SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142 * L_15 = __this->get_settings_4();
		NullCheck(L_15);
		int32_t L_16 = L_15->get_bloomTextureWidth_10();
		V_0 = (bool)((int32_t)((int32_t)L_12|(int32_t)((((int32_t)((((int32_t)L_14) == ((int32_t)L_16))? 1 : 0)) == ((int32_t)0))? 1 : 0)));
		G_B5_0 = G_B4_0;
	}

IL_0069:
	{
		bool L_17 = V_0;
		G_B6_0 = G_B5_0;
		if (L_17)
		{
			G_B9_0 = G_B5_0;
			goto IL_00a1;
		}
	}
	{
		SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142 * L_18 = __this->get_settings_4();
		NullCheck(L_18);
		bool L_19 = L_18->get_preserveAspectRatio_9();
		G_B7_0 = G_B6_0;
		if (!L_19)
		{
			G_B9_0 = G_B6_0;
			goto IL_00a1;
		}
	}
	{
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_20 = __this->get__horizontalBlurTexture_12();
		NullCheck(L_20);
		int32_t L_21 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_20);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_22 = __this->get__horizontalBlurTexture_12();
		NullCheck(L_22);
		int32_t L_23 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_22);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_24 = ___mainCamera0;
		float L_25 = SleekRenderPostProcess_GetCurrentAspect_mFA278408C602D5213CB17EC938C37D070544DC79(__this, L_24, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		int32_t L_26 = Mathf_RoundToInt_m0EAD8BD38FCB72FA1D8A04E96337C820EC83F041(((float)il2cpp_codegen_multiply((float)(((float)((float)L_23))), (float)L_25)), /*hidden argument*/NULL);
		G_B8_0 = G_B7_0;
		if ((((int32_t)L_21) == ((int32_t)L_26)))
		{
			G_B9_0 = G_B7_0;
			goto IL_00a1;
		}
	}
	{
		V_0 = (bool)1;
		G_B9_0 = G_B8_0;
	}

IL_00a1:
	{
		SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142 * L_27 = __this->get_settings_4();
		NullCheck(L_27);
		bool L_28 = L_27->get_preserveAspectRatio_9();
		G_B10_0 = G_B9_0;
		if (!L_28)
		{
			G_B11_0 = G_B9_0;
			goto IL_00b6;
		}
	}
	{
		bool L_29 = __this->get__isAlreadyPreservingAspectRatio_22();
		G_B11_0 = G_B10_0;
		if (!L_29)
		{
			G_B13_0 = G_B10_0;
			goto IL_00cb;
		}
	}

IL_00b6:
	{
		SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142 * L_30 = __this->get_settings_4();
		NullCheck(L_30);
		bool L_31 = L_30->get_preserveAspectRatio_9();
		G_B12_0 = G_B11_0;
		if (L_31)
		{
			G_B14_0 = G_B11_0;
			goto IL_00de;
		}
	}
	{
		bool L_32 = __this->get__isAlreadyPreservingAspectRatio_22();
		G_B13_0 = G_B12_0;
		if (!L_32)
		{
			G_B14_0 = G_B12_0;
			goto IL_00de;
		}
	}

IL_00cb:
	{
		SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142 * L_33 = __this->get_settings_4();
		NullCheck(L_33);
		bool L_34 = L_33->get_preserveAspectRatio_9();
		__this->set__isAlreadyPreservingAspectRatio_22(L_34);
		V_0 = (bool)1;
		G_B14_0 = G_B13_0;
	}

IL_00de:
	{
		bool L_35 = V_0;
		if (!((int32_t)((int32_t)G_B14_0|(int32_t)L_35)))
		{
			goto IL_00ee;
		}
	}
	{
		SleekRenderPostProcess_ReleaseResources_mDFBCB6AF7DB534D25370ED70E8C21A4D8E85F165(__this, /*hidden argument*/NULL);
		SleekRenderPostProcess_CreateResources_m88233905ED6473C25EA3C8A4B3757879C201D6DA(__this, /*hidden argument*/NULL);
	}

IL_00ee:
	{
		return;
	}
}
// System.Single SleekRender.SleekRenderPostProcess::GetCurrentAspect(UnityEngine.Camera)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float SleekRenderPostProcess_GetCurrentAspect_mFA278408C602D5213CB17EC938C37D070544DC79 (SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94 * __this, Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___mainCamera0, const RuntimeMethod* method)
{
	{
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_0 = ___mainCamera0;
		NullCheck(L_0);
		float L_1 = Camera_get_aspect_m2ADA7982754920C3B58B4DB664801D6F2416E0C6(L_0, /*hidden argument*/NULL);
		return ((float)il2cpp_codegen_multiply((float)L_1, (float)(0.7f)));
	}
}
// System.Void SleekRender.SleekRenderPostProcess::CreateDefaultSettingsIfNoneLinked()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SleekRenderPostProcess_CreateDefaultSettingsIfNoneLinked_mFAFCBC9B80CB0CAE18DB94C102CD3D0FAEFA83A9 (SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SleekRenderPostProcess_CreateDefaultSettingsIfNoneLinked_mFAFCBC9B80CB0CAE18DB94C102CD3D0FAEFA83A9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142 * L_0 = __this->get_settings_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142 * L_2 = ScriptableObject_CreateInstance_TisSleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142_mF36470B34704D69B08AB64E76E7987E6BF28E215(/*hidden argument*/ScriptableObject_CreateInstance_TisSleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142_mF36470B34704D69B08AB64E76E7987E6BF28E215_RuntimeMethod_var);
		__this->set_settings_4(L_2);
		SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142 * L_3 = __this->get_settings_4();
		NullCheck(L_3);
		Object_set_name_m538711B144CDE30F929376BCF72D0DC8F85D0826(L_3, _stringLiteral7F4FC3F227306B55CD4604D3B2D0C6753FAB7410, /*hidden argument*/NULL);
	}

IL_0029:
	{
		return;
	}
}
// UnityEngine.Mesh SleekRender.SleekRenderPostProcess::CreateScreenSpaceQuadMesh()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * SleekRenderPostProcess_CreateScreenSpaceQuadMesh_m2D59ABE6B083BEF9A9DE056AF5725199DD9FCB0C (SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SleekRenderPostProcess_CreateScreenSpaceQuadMesh_m2D59ABE6B083BEF9A9DE056AF5725199DD9FCB0C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * V_0 = NULL;
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* V_1 = NULL;
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* V_2 = NULL;
	ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* V_3 = NULL;
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* V_4 = NULL;
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_0 = (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C *)il2cpp_codegen_object_new(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C_il2cpp_TypeInfo_var);
		Mesh__ctor_m3AEBC82AB71D4F9498F6E254174BEBA8372834B4(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_1 = (Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*)(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*)SZArrayNew(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28_il2cpp_TypeInfo_var, (uint32_t)4);
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_2 = L_1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3;
		memset((&L_3), 0, sizeof(L_3));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_3), (-1.0f), (-1.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 )L_3);
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_4 = L_2;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_5;
		memset((&L_5), 0, sizeof(L_5));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_5), (-1.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 )L_5);
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_6 = L_4;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7;
		memset((&L_7), 0, sizeof(L_7));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_7), (1.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 )L_7);
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_8 = L_6;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_9), (1.0f), (-1.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_8);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(3), (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 )L_9);
		V_1 = L_8;
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_10 = (Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6*)(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6*)SZArrayNew(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6_il2cpp_TypeInfo_var, (uint32_t)4);
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_11 = L_10;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((&L_12), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_11);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D )L_12);
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_13 = L_11;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_14;
		memset((&L_14), 0, sizeof(L_14));
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((&L_14), (0.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_13);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(1), (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D )L_14);
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_15 = L_13;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_16;
		memset((&L_16), 0, sizeof(L_16));
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((&L_16), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_15);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(2), (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D )L_16);
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_17 = L_15;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_18;
		memset((&L_18), 0, sizeof(L_18));
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((&L_18), (1.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_17);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(3), (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D )L_18);
		V_2 = L_17;
		ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* L_19 = (ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399*)(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399*)SZArrayNew(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399_il2cpp_TypeInfo_var, (uint32_t)4);
		ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* L_20 = L_19;
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_21;
		memset((&L_21), 0, sizeof(L_21));
		Color__ctor_mC9AEEB3931D5B8C37483A884DD8EB40DC8946369((&L_21), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_20);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 )L_21);
		ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* L_22 = L_20;
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_23;
		memset((&L_23), 0, sizeof(L_23));
		Color__ctor_mC9AEEB3931D5B8C37483A884DD8EB40DC8946369((&L_23), (0.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_22);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(1), (Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 )L_23);
		ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* L_24 = L_22;
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_25;
		memset((&L_25), 0, sizeof(L_25));
		Color__ctor_mC9AEEB3931D5B8C37483A884DD8EB40DC8946369((&L_25), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_24);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(2), (Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 )L_25);
		ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* L_26 = L_24;
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_27;
		memset((&L_27), 0, sizeof(L_27));
		Color__ctor_mC9AEEB3931D5B8C37483A884DD8EB40DC8946369((&L_27), (1.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_26);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(3), (Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 )L_27);
		V_3 = L_26;
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_28 = (Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83*)(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83*)SZArrayNew(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83_il2cpp_TypeInfo_var, (uint32_t)6);
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_29 = L_28;
		RuntimeFieldHandle_t844BDF00E8E6FE69D9AEAA7657F09018B864F4EF  L_30 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_t1462DEA80E3C0F6D30E594F33C4F5283DF75F28F____23447D2BC3FF6984AB09F575BC63CFE460337394_0_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_m29F50CDFEEE0AB868200291366253DD4737BC76A((RuntimeArray *)(RuntimeArray *)L_29, L_30, /*hidden argument*/NULL);
		V_4 = L_29;
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_31 = V_0;
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_32 = V_1;
		NullCheck(L_31);
		Mesh_set_vertices_mC1406AE08BC3495F3B0E29B53BACC9FD7BA685C6(L_31, L_32, /*hidden argument*/NULL);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_33 = V_0;
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_34 = V_2;
		NullCheck(L_33);
		Mesh_set_uv_m56E4B52315669FBDA89DC9C550AC89EEE8A4E7C8(L_33, L_34, /*hidden argument*/NULL);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_35 = V_0;
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_36 = V_4;
		NullCheck(L_35);
		Mesh_set_triangles_m143A1C262BADCFACE43587EBA2CDC6EBEB5DFAED(L_35, L_36, /*hidden argument*/NULL);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_37 = V_0;
		ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* L_38 = V_3;
		NullCheck(L_37);
		Mesh_set_colors_m704D0EF58B7AED0D64AE4763EA375638FB08E026(L_37, L_38, /*hidden argument*/NULL);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_39 = V_0;
		NullCheck(L_39);
		Mesh_UploadMeshData_m809A98624475785C493269B72EC6C41B556759A1(L_39, (bool)1, /*hidden argument*/NULL);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_40 = V_0;
		return L_40;
	}
}
// System.Void SleekRender.SleekRenderPostProcess::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SleekRenderPostProcess__ctor_m0F4ABD1F2FB792A238BC58F55A68C3472FEF81F8 (SleekRenderPostProcess_t5586C12A2E2F055B4268C984432EABF5030E9D94 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SleekRender.SleekRenderPostProcess_Uniforms::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Uniforms__cctor_m7573D90B8FF2BCE1B1C8A02911B76BB8C140F972 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Uniforms__cctor_m7573D90B8FF2BCE1B1C8A02911B76BB8C140F972_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = Shader_PropertyToID_m831E5B48743620DB9E3E3DD15A8DEA483981DD45(_stringLiteralCBE31FA694878583D3DEB5EDC1C5A8F38AB53857, /*hidden argument*/NULL);
		((Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_il2cpp_TypeInfo_var))->set__LuminanceConst_0(L_0);
		int32_t L_1 = Shader_PropertyToID_m831E5B48743620DB9E3E3DD15A8DEA483981DD45(_stringLiteral30271936C7B32B89A14938BB7CD12CADA14C4986, /*hidden argument*/NULL);
		((Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_il2cpp_TypeInfo_var))->set__BloomIntencity_1(L_1);
		int32_t L_2 = Shader_PropertyToID_m831E5B48743620DB9E3E3DD15A8DEA483981DD45(_stringLiteralE0780C0EF7B1666605E71B814D8AD13B2A650ABE, /*hidden argument*/NULL);
		((Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_il2cpp_TypeInfo_var))->set__BloomTint_2(L_2);
		int32_t L_3 = Shader_PropertyToID_m831E5B48743620DB9E3E3DD15A8DEA483981DD45(_stringLiteralC510EA100EEE1C261FE63B56E1F3390BFB85F481, /*hidden argument*/NULL);
		((Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_il2cpp_TypeInfo_var))->set__MainTex_3(L_3);
		int32_t L_4 = Shader_PropertyToID_m831E5B48743620DB9E3E3DD15A8DEA483981DD45(_stringLiteral445809F402DAF60282A49D8CDDAA3FA7DCD75127, /*hidden argument*/NULL);
		((Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_il2cpp_TypeInfo_var))->set__BloomTex_4(L_4);
		int32_t L_5 = Shader_PropertyToID_m831E5B48743620DB9E3E3DD15A8DEA483981DD45(_stringLiteralA86D57D17CF0F3781405F49FDEF380455D496A5E, /*hidden argument*/NULL);
		((Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_il2cpp_TypeInfo_var))->set__PreComposeTex_5(L_5);
		int32_t L_6 = Shader_PropertyToID_m831E5B48743620DB9E3E3DD15A8DEA483981DD45(_stringLiteral1B1401C3041E707DF5717E7BB94159B6B4F887E8, /*hidden argument*/NULL);
		((Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_il2cpp_TypeInfo_var))->set__TexelSize_6(L_6);
		int32_t L_7 = Shader_PropertyToID_m831E5B48743620DB9E3E3DD15A8DEA483981DD45(_stringLiteralAC523897F9F4D00B244564D81D5C14603DF6A69C, /*hidden argument*/NULL);
		((Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_il2cpp_TypeInfo_var))->set__Colorize_7(L_7);
		int32_t L_8 = Shader_PropertyToID_m831E5B48743620DB9E3E3DD15A8DEA483981DD45(_stringLiteralFEEDCD9FACABFCEDB19D47D699FA78BBEC70CAC1, /*hidden argument*/NULL);
		((Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_il2cpp_TypeInfo_var))->set__VignetteShape_8(L_8);
		int32_t L_9 = Shader_PropertyToID_m831E5B48743620DB9E3E3DD15A8DEA483981DD45(_stringLiteral4609CF47A2EA4974A5D110F1FC15122D047ED134, /*hidden argument*/NULL);
		((Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_il2cpp_TypeInfo_var))->set__VignetteColor_9(L_9);
		int32_t L_10 = Shader_PropertyToID_m831E5B48743620DB9E3E3DD15A8DEA483981DD45(_stringLiteralCD96BB6E91576D4BDBEC8D699ACCA61156855FBA, /*hidden argument*/NULL);
		((Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_StaticFields*)il2cpp_codegen_static_fields_for(Uniforms_t44F2FE157683C642FF0A2F8736A4BEC11AF37290_il2cpp_TypeInfo_var))->set__BrightnessContrast_10(L_10);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SleekRender.SleekRenderSettings::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SleekRenderSettings__ctor_mB71818A23BF0930F3E701629961F83633A636486 (SleekRenderSettings_t78066A768B5BDB0215B3F11F183121BD09ADA142 * __this, const RuntimeMethod* method)
{
	{
		__this->set_bloomEnabled_5((bool)1);
		__this->set_bloomThreshold_6((0.6f));
		__this->set_bloomIntensity_7((2.5f));
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_0 = Color_get_white_mE7F3AC4FF0D6F35E48049C73116A222CBE96D905(/*hidden argument*/NULL);
		__this->set_bloomTint_8(L_0);
		__this->set_bloomTextureWidth_10(((int32_t)128));
		__this->set_bloomTextureHeight_11(((int32_t)128));
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1;
		memset((&L_1), 0, sizeof(L_1));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_1), (0.333333343f), (0.333333343f), (0.333333343f), /*hidden argument*/NULL);
		__this->set_bloomLumaVector_13(L_1);
		__this->set_colorizeExpanded_14((bool)1);
		__this->set_colorizeEnabled_15((bool)1);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_2 = Color_get_clear_m419239BDAEB3D3C4B4291BF2C6EF09A7D7D81360(/*hidden argument*/NULL);
		Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  L_3 = Color32_op_Implicit_m52B034473369A651C8952BD916A2AB193E0E5B30(L_2, /*hidden argument*/NULL);
		__this->set_colorize_16(L_3);
		__this->set_vignetteExpanded_17((bool)1);
		__this->set_vignetteEnabled_18((bool)1);
		__this->set_vignetteBeginRadius_19((0.166f));
		__this->set_vignetteExpandRadius_20((1.34f));
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_4 = Color_get_black_mEB3C91F45F8AA7E4842238DFCC578BB322723DAF(/*hidden argument*/NULL);
		__this->set_vignetteColor_21(L_4);
		__this->set_brightnessContrastEnabled_23((bool)1);
		ScriptableObject__ctor_m6E2B3821A4A361556FC12E9B1C71E1D5DC002C5B(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityStandardAssets.Effects.AfterburnerPhysicsForce::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AfterburnerPhysicsForce_OnEnable_m31ABCDDA8E500224F25244C9D3FE44CBDD45D7CE (AfterburnerPhysicsForce_t753048E9DC6DB55FA797EA775B76014E1E7F204B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AfterburnerPhysicsForce_OnEnable_m31ABCDDA8E500224F25244C9D3FE44CBDD45D7CE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * L_0 = Component_GetComponent_TisCollider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF_m6B8E8E0E0AF6B08652B81B7950FC5AF63EAD40C6(__this, /*hidden argument*/Component_GetComponent_TisCollider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF_m6B8E8E0E0AF6B08652B81B7950FC5AF63EAD40C6_RuntimeMethod_var);
		__this->set_m_Sphere_9(((SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F *)IsInstClass((RuntimeObject*)L_0, SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UnityStandardAssets.Effects.AfterburnerPhysicsForce::FixedUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AfterburnerPhysicsForce_FixedUpdate_m90286B7CDABBED931E9D13E5D879E6EC41BF6E8A (AfterburnerPhysicsForce_t753048E9DC6DB55FA797EA775B76014E1E7F204B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AfterburnerPhysicsForce_FixedUpdate_m90286B7CDABBED931E9D13E5D879E6EC41BF6E8A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_1;
	memset((&V_1), 0, sizeof(V_1));
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_4;
	memset((&V_4), 0, sizeof(V_4));
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_0 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_0, /*hidden argument*/NULL);
		SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * L_2 = __this->get_m_Sphere_9();
		NullCheck(L_2);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = SphereCollider_get_center_mE7AD1AC46974FF23EEA621B872E2962E52A1DB00(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_1, L_3, /*hidden argument*/NULL);
		SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * L_5 = __this->get_m_Sphere_9();
		NullCheck(L_5);
		float L_6 = SphereCollider_get_radius_m255804173C17314FD9538AE45C4A46D4882BC094(L_5, /*hidden argument*/NULL);
		ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* L_7 = Physics_OverlapSphere_m354A92672F7A6DE59EF1285D02D62247F46A5D84(L_4, L_6, /*hidden argument*/NULL);
		__this->set_m_Cols_8(L_7);
		V_0 = 0;
		goto IL_0168;
	}

IL_0038:
	{
		ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* L_8 = __this->get_m_Cols_8();
		int32_t L_9 = V_0;
		NullCheck(L_8);
		int32_t L_10 = L_9;
		Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck(L_11);
		Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * L_12 = Collider_get_attachedRigidbody_m9E3C688EAE2F6A76C9AC14968D96769D9A71B1E8(L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_13 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_12, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0164;
		}
	}
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_14 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* L_15 = __this->get_m_Cols_8();
		int32_t L_16 = V_0;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		NullCheck(L_18);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_19 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_20 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_19, /*hidden argument*/NULL);
		NullCheck(L_14);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_21 = Transform_InverseTransformPoint_mB6E3145F20B531B4A781C194BAC43A8255C96C47(L_14, L_20, /*hidden argument*/NULL);
		V_1 = L_21;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_22 = V_1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_23 = V_1;
		float L_24 = L_23.get_z_4();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_25;
		memset((&L_25), 0, sizeof(L_25));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_25), (0.0f), (0.0f), L_24, /*hidden argument*/NULL);
		float L_26 = __this->get_effectWidth_5();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_27 = Vector3_MoveTowards_mA288BB5AA73DDA9CA76EDC11F339BAFDA1E4FF45(L_22, L_25, ((float)il2cpp_codegen_multiply((float)L_26, (float)(0.5f))), /*hidden argument*/NULL);
		V_1 = L_27;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_28 = V_1;
		float L_29 = L_28.get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_30 = V_1;
		float L_31 = L_30.get_z_4();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_32 = atan2f(L_29, L_31);
		float L_33 = fabsf(((float)il2cpp_codegen_multiply((float)L_32, (float)(57.29578f))));
		V_2 = L_33;
		float L_34 = __this->get_effectDistance_6();
		float L_35 = Vector3_get_magnitude_m9A750659B60C5FE0C30438A7F9681775D5DB1274((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_1), /*hidden argument*/NULL);
		float L_36 = Mathf_InverseLerp_m7054CDF25056E9B27D2467F91C95D628508F1F31(L_34, (0.0f), L_35, /*hidden argument*/NULL);
		V_3 = L_36;
		float L_37 = V_3;
		float L_38 = __this->get_effectAngle_4();
		float L_39 = V_2;
		float L_40 = Mathf_InverseLerp_m7054CDF25056E9B27D2467F91C95D628508F1F31(L_38, (0.0f), L_39, /*hidden argument*/NULL);
		V_3 = ((float)il2cpp_codegen_multiply((float)L_37, (float)L_40));
		ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* L_41 = __this->get_m_Cols_8();
		int32_t L_42 = V_0;
		NullCheck(L_41);
		int32_t L_43 = L_42;
		Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * L_44 = (L_41)->GetAt(static_cast<il2cpp_array_size_t>(L_43));
		NullCheck(L_44);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_45 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_44, /*hidden argument*/NULL);
		NullCheck(L_45);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_46 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_45, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_47 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_47);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_48 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_47, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_49 = Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3(L_46, L_48, /*hidden argument*/NULL);
		V_4 = L_49;
		ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* L_50 = __this->get_m_Cols_8();
		int32_t L_51 = V_0;
		NullCheck(L_50);
		int32_t L_52 = L_51;
		Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * L_53 = (L_50)->GetAt(static_cast<il2cpp_array_size_t>(L_52));
		NullCheck(L_53);
		Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * L_54 = Collider_get_attachedRigidbody_m9E3C688EAE2F6A76C9AC14968D96769D9A71B1E8(L_53, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_55 = Vector3_get_normalized_mE20796F1D2D36244FACD4D14DADB245BE579849B((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_4), /*hidden argument*/NULL);
		float L_56 = __this->get_force_7();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_57 = Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E(L_55, L_56, /*hidden argument*/NULL);
		float L_58 = V_3;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_59 = Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E(L_57, L_58, /*hidden argument*/NULL);
		ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* L_60 = __this->get_m_Cols_8();
		int32_t L_61 = V_0;
		NullCheck(L_60);
		int32_t L_62 = L_61;
		Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * L_63 = (L_60)->GetAt(static_cast<il2cpp_array_size_t>(L_62));
		NullCheck(L_63);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_64 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_63, /*hidden argument*/NULL);
		NullCheck(L_64);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_65 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_64, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_66 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_67 = V_1;
		float L_68 = L_67.get_z_4();
		NullCheck(L_66);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_69 = Transform_TransformPoint_m363B3A9E2C3A9A52F4B872CF34F476D87CCC8CEC(L_66, (0.0f), (0.0f), L_68, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_70 = Vector3_Lerp_m5BA75496B803820CC64079383956D73C6FD4A8A1(L_65, L_69, (0.1f), /*hidden argument*/NULL);
		NullCheck(L_54);
		Rigidbody_AddForceAtPosition_m3A5DCFC3E79923C9D8E32A54BC4AAA1E48EEAD6C(L_54, L_59, L_70, /*hidden argument*/NULL);
	}

IL_0164:
	{
		int32_t L_71 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_71, (int32_t)1));
	}

IL_0168:
	{
		int32_t L_72 = V_0;
		ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* L_73 = __this->get_m_Cols_8();
		NullCheck(L_73);
		if ((((int32_t)L_72) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_73)->max_length)))))))
		{
			goto IL_0038;
		}
	}
	{
		return;
	}
}
// System.Void UnityStandardAssets.Effects.AfterburnerPhysicsForce::OnDrawGizmosSelected()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AfterburnerPhysicsForce_OnDrawGizmosSelected_m8E75C894A4544C40EB1FD3DA8883E3358BAF8AA3 (AfterburnerPhysicsForce_t753048E9DC6DB55FA797EA775B76014E1E7F204B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AfterburnerPhysicsForce_OnDrawGizmosSelected_m8E75C894A4544C40EB1FD3DA8883E3358BAF8AA3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* V_0 = NULL;
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* V_1 = NULL;
	int32_t V_2 = 0;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_3;
	memset((&V_3), 0, sizeof(V_3));
	{
		SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * L_0 = __this->get_m_Sphere_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * L_2 = Component_GetComponent_TisCollider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF_m6B8E8E0E0AF6B08652B81B7950FC5AF63EAD40C6(__this, /*hidden argument*/Component_GetComponent_TisCollider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF_m6B8E8E0E0AF6B08652B81B7950FC5AF63EAD40C6_RuntimeMethod_var);
		__this->set_m_Sphere_9(((SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F *)IsInstClass((RuntimeObject*)L_2, SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F_il2cpp_TypeInfo_var)));
	}

IL_001f:
	{
		SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * L_3 = __this->get_m_Sphere_9();
		float L_4 = __this->get_effectDistance_6();
		NullCheck(L_3);
		SphereCollider_set_radius_m3161573A2D89F495F4B79E16C52B905C0F9AD699(L_3, ((float)il2cpp_codegen_multiply((float)L_4, (float)(0.5f))), /*hidden argument*/NULL);
		SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * L_5 = __this->get_m_Sphere_9();
		float L_6 = __this->get_effectDistance_6();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7;
		memset((&L_7), 0, sizeof(L_7));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_7), (0.0f), (0.0f), ((float)il2cpp_codegen_multiply((float)L_6, (float)(0.5f))), /*hidden argument*/NULL);
		NullCheck(L_5);
		SphereCollider_set_center_m325070F5252B4A2EA567B653CAE3285F101FA3EE(L_5, L_7, /*hidden argument*/NULL);
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_8 = (Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*)(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*)SZArrayNew(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28_il2cpp_TypeInfo_var, (uint32_t)4);
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_9 = L_8;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_10 = Vector3_get_up_m6309EBC4E42D6D0B3D28056BD23D0331275306F7(/*hidden argument*/NULL);
		NullCheck(L_9);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 )L_10);
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_11 = L_9;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_12 = Vector3_get_up_m6309EBC4E42D6D0B3D28056BD23D0331275306F7(/*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_13 = Vector3_op_UnaryNegation_m2AFBBF22801F9BCA5A4EBE642A29F433FE1339C2(L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(1), (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 )L_13);
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_14 = L_11;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_15 = Vector3_get_right_m6DD9559CA0C75BBA42D9140021C4C2A9AAA9B3F5(/*hidden argument*/NULL);
		NullCheck(L_14);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(2), (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 )L_15);
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_16 = L_14;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_17 = Vector3_get_right_m6DD9559CA0C75BBA42D9140021C4C2A9AAA9B3F5(/*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_18 = Vector3_op_UnaryNegation_m2AFBBF22801F9BCA5A4EBE642A29F433FE1339C2(L_17, /*hidden argument*/NULL);
		NullCheck(L_16);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(3), (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 )L_18);
		V_0 = L_16;
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_19 = (Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*)(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28*)SZArrayNew(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28_il2cpp_TypeInfo_var, (uint32_t)4);
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_20 = L_19;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_21 = Vector3_get_right_m6DD9559CA0C75BBA42D9140021C4C2A9AAA9B3F5(/*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_22 = Vector3_op_UnaryNegation_m2AFBBF22801F9BCA5A4EBE642A29F433FE1339C2(L_21, /*hidden argument*/NULL);
		NullCheck(L_20);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 )L_22);
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_23 = L_20;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_24 = Vector3_get_right_m6DD9559CA0C75BBA42D9140021C4C2A9AAA9B3F5(/*hidden argument*/NULL);
		NullCheck(L_23);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(1), (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 )L_24);
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_25 = L_23;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_26 = Vector3_get_up_m6309EBC4E42D6D0B3D28056BD23D0331275306F7(/*hidden argument*/NULL);
		NullCheck(L_25);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(2), (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 )L_26);
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_27 = L_25;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_28 = Vector3_get_up_m6309EBC4E42D6D0B3D28056BD23D0331275306F7(/*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_29 = Vector3_op_UnaryNegation_m2AFBBF22801F9BCA5A4EBE642A29F433FE1339C2(L_28, /*hidden argument*/NULL);
		NullCheck(L_27);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(3), (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 )L_29);
		V_1 = L_27;
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_30;
		memset((&L_30), 0, sizeof(L_30));
		Color__ctor_m20DF490CEB364C4FC36D7EE392640DF5B7420D7C((&L_30), (0.0f), (1.0f), (0.0f), (0.5f), /*hidden argument*/NULL);
		Gizmos_set_color_mFA6C199DF05FF557AEF662222CA60EC25DF54F28(L_30, /*hidden argument*/NULL);
		V_2 = 0;
		goto IL_0191;
	}

IL_0103:
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_31 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_31);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_32 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_31, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_33 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_33);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_34 = Transform_get_rotation_m3AB90A67403249AECCA5E02BC70FCE8C90FE9FB9(L_33, /*hidden argument*/NULL);
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_35 = V_0;
		int32_t L_36 = V_2;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_39 = Quaternion_op_Multiply_mD5999DE317D808808B72E58E7A978C4C0995879C(L_34, L_38, /*hidden argument*/NULL);
		float L_40 = __this->get_effectWidth_5();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_41 = Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E(L_39, L_40, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_42 = Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E(L_41, (0.5f), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_43 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_32, L_42, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_44 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		float L_45 = __this->get_effectAngle_4();
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_46 = V_1;
		int32_t L_47 = V_2;
		NullCheck(L_46);
		int32_t L_48 = L_47;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_49 = (L_46)->GetAt(static_cast<il2cpp_array_size_t>(L_48));
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_50 = Quaternion_AngleAxis_m07DACF59F0403451DABB9BC991C53EE3301E88B0(L_45, L_49, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_51 = Vector3_get_forward_m3E2E192B3302130098738C308FA1EE1439449D0D(/*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_52 = Quaternion_op_Multiply_mD5999DE317D808808B72E58E7A978C4C0995879C(L_50, L_51, /*hidden argument*/NULL);
		NullCheck(L_44);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_53 = Transform_TransformDirection_m85FC1D7E1322E94F65DA59AEF3B1166850B183EF(L_44, L_52, /*hidden argument*/NULL);
		V_3 = L_53;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_54 = L_43;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_55 = V_3;
		SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * L_56 = __this->get_m_Sphere_9();
		NullCheck(L_56);
		float L_57 = SphereCollider_get_radius_m255804173C17314FD9538AE45C4A46D4882BC094(L_56, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_58 = Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E(L_55, L_57, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_59 = Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E(L_58, (2.0f), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_60 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_54, L_59, /*hidden argument*/NULL);
		Gizmos_DrawLine_m9515D59D2536571F4906A3C54E613A3986DFD892(L_54, L_60, /*hidden argument*/NULL);
		int32_t L_61 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_61, (int32_t)1));
	}

IL_0191:
	{
		int32_t L_62 = V_2;
		if ((((int32_t)L_62) < ((int32_t)4)))
		{
			goto IL_0103;
		}
	}
	{
		return;
	}
}
// System.Void UnityStandardAssets.Effects.AfterburnerPhysicsForce::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AfterburnerPhysicsForce__ctor_mE4F415134C355B0C418241BA9F4BE2DAB65BF1A0 (AfterburnerPhysicsForce_t753048E9DC6DB55FA797EA775B76014E1E7F204B * __this, const RuntimeMethod* method)
{
	{
		__this->set_effectAngle_4((15.0f));
		__this->set_effectWidth_5((1.0f));
		__this->set_effectDistance_6((10.0f));
		__this->set_force_7((10.0f));
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Collections.IEnumerator UnityStandardAssets.Effects.ExplosionFireAndDebris::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ExplosionFireAndDebris_Start_m85654B2EA9348AE14094A4CDCA1ECD714E4CBC22 (ExplosionFireAndDebris_tACCFCC1184BB9A8C3A0B39D99FFC831E76F9EECB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ExplosionFireAndDebris_Start_m85654B2EA9348AE14094A4CDCA1ECD714E4CBC22_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CStartU3Ed__4_t945A707CD8C01DA1C37AE2DD9D0A904C80F58139 * L_0 = (U3CStartU3Ed__4_t945A707CD8C01DA1C37AE2DD9D0A904C80F58139 *)il2cpp_codegen_object_new(U3CStartU3Ed__4_t945A707CD8C01DA1C37AE2DD9D0A904C80F58139_il2cpp_TypeInfo_var);
		U3CStartU3Ed__4__ctor_mBDD727783F54367BFE2CED7C675B839450955B8C(L_0, 0, /*hidden argument*/NULL);
		U3CStartU3Ed__4_t945A707CD8C01DA1C37AE2DD9D0A904C80F58139 * L_1 = L_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_2(__this);
		return L_1;
	}
}
// System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris::AddFire(UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExplosionFireAndDebris_AddFire_m781F3E400896C4F2E2B4A24FAEFE52D01ED26C0B (ExplosionFireAndDebris_tACCFCC1184BB9A8C3A0B39D99FFC831E76F9EECB * __this, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___t0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___pos1, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___normal2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ExplosionFireAndDebris_AddFire_m781F3E400896C4F2E2B4A24FAEFE52D01ED26C0B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = ___pos1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = ___normal2;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E(L_1, (0.5f), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_0, L_2, /*hidden argument*/NULL);
		___pos1 = L_3;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_4 = __this->get_firePrefab_5();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_5 = ___pos1;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_6 = Quaternion_get_identity_m548B37D80F2DEE60E41D1F09BF6889B557BE1A64(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_7 = Object_Instantiate_TisTransform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_m8BCC51A95F42E99B9033022D8BF0EABA04FED8F0(L_4, L_5, L_6, /*hidden argument*/Object_Instantiate_TisTransform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_m8BCC51A95F42E99B9033022D8BF0EABA04FED8F0_RuntimeMethod_var);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_8 = ___t0;
		NullCheck(L_7);
		Transform_set_parent_m65B8E4660B2C554069C57A957D9E55FECA7AA73E(L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExplosionFireAndDebris__ctor_m6E9B3DD674A9E1DF3B85ECF3E618F7F29B5EB97E (ExplosionFireAndDebris_tACCFCC1184BB9A8C3A0B39D99FFC831E76F9EECB * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris_<Start>d__4::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartU3Ed__4__ctor_mBDD727783F54367BFE2CED7C675B839450955B8C (U3CStartU3Ed__4_t945A707CD8C01DA1C37AE2DD9D0A904C80F58139 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris_<Start>d__4::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartU3Ed__4_System_IDisposable_Dispose_m6E7E396A3AE4E0720223F5D8DCB4E8A55ED7BF01 (U3CStartU3Ed__4_t945A707CD8C01DA1C37AE2DD9D0A904C80F58139 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean UnityStandardAssets.Effects.ExplosionFireAndDebris_<Start>d__4::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CStartU3Ed__4_MoveNext_m4846C74D5F4A4FF39164BCFD48198D976AA0F953 (U3CStartU3Ed__4_t945A707CD8C01DA1C37AE2DD9D0A904C80F58139 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ed__4_MoveNext_m4846C74D5F4A4FF39164BCFD48198D976AA0F953_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	ExplosionFireAndDebris_tACCFCC1184BB9A8C3A0B39D99FFC831E76F9EECB * V_1 = NULL;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	int32_t V_4 = 0;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_5;
	memset((&V_5), 0, sizeof(V_5));
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  V_6;
	memset((&V_6), 0, sizeof(V_6));
	ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* V_7 = NULL;
	int32_t V_8 = 0;
	Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * V_9 = NULL;
	RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3  V_10;
	memset((&V_10), 0, sizeof(V_10));
	Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  V_11;
	memset((&V_11), 0, sizeof(V_11));
	RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3  V_12;
	memset((&V_12), 0, sizeof(V_12));
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		ExplosionFireAndDebris_tACCFCC1184BB9A8C3A0B39D99FFC831E76F9EECB * L_1 = __this->get_U3CU3E4__this_2();
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_00b2;
		}
	}
	{
		return (bool)0;
	}

IL_001a:
	{
		__this->set_U3CU3E1__state_0((-1));
		ExplosionFireAndDebris_tACCFCC1184BB9A8C3A0B39D99FFC831E76F9EECB * L_4 = V_1;
		NullCheck(L_4);
		ParticleSystemMultiplier_t8EBFEF83EBA48DD7F6517ABFFBE2CD051CB2C748 * L_5 = Component_GetComponent_TisParticleSystemMultiplier_t8EBFEF83EBA48DD7F6517ABFFBE2CD051CB2C748_m77EDAEF51740299BC986B0DA36AB7282BB8DDD45(L_4, /*hidden argument*/Component_GetComponent_TisParticleSystemMultiplier_t8EBFEF83EBA48DD7F6517ABFFBE2CD051CB2C748_m77EDAEF51740299BC986B0DA36AB7282BB8DDD45_RuntimeMethod_var);
		NullCheck(L_5);
		float L_6 = L_5->get_multiplier_4();
		__this->set_U3CmultiplierU3E5__2_3(L_6);
		V_4 = 0;
		goto IL_008f;
	}

IL_0037:
	{
		ExplosionFireAndDebris_tACCFCC1184BB9A8C3A0B39D99FFC831E76F9EECB * L_7 = V_1;
		NullCheck(L_7);
		TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* L_8 = L_7->get_debrisPrefabs_4();
		ExplosionFireAndDebris_tACCFCC1184BB9A8C3A0B39D99FFC831E76F9EECB * L_9 = V_1;
		NullCheck(L_9);
		TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* L_10 = L_9->get_debrisPrefabs_4();
		NullCheck(L_10);
		int32_t L_11 = Random_Range_mD0C8F37FF3CAB1D87AAA6C45130BD59626BD6780(0, (((int32_t)((int32_t)(((RuntimeArray*)L_10)->max_length)))), /*hidden argument*/NULL);
		NullCheck(L_8);
		int32_t L_12 = L_11;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_13 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		ExplosionFireAndDebris_tACCFCC1184BB9A8C3A0B39D99FFC831E76F9EECB * L_14 = V_1;
		NullCheck(L_14);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_15 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_16 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_15, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_17 = Random_get_insideUnitSphere_m10033DFB85B1A21CE44201CB0E421F27B77A868F(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_18 = Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E(L_17, (3.0f), /*hidden argument*/NULL);
		float L_19 = __this->get_U3CmultiplierU3E5__2_3();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_20 = Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E(L_18, L_19, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_21 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_16, L_20, /*hidden argument*/NULL);
		V_5 = L_21;
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_22 = Random_get_rotation_mFEB212A2058746E0E8F544D8C2201663861BA77A(/*hidden argument*/NULL);
		V_6 = L_22;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_23 = V_5;
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_24 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Instantiate_TisTransform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_m8BCC51A95F42E99B9033022D8BF0EABA04FED8F0(L_13, L_23, L_24, /*hidden argument*/Object_Instantiate_TisTransform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_m8BCC51A95F42E99B9033022D8BF0EABA04FED8F0_RuntimeMethod_var);
		int32_t L_25 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_25, (int32_t)1));
	}

IL_008f:
	{
		int32_t L_26 = V_4;
		ExplosionFireAndDebris_tACCFCC1184BB9A8C3A0B39D99FFC831E76F9EECB * L_27 = V_1;
		NullCheck(L_27);
		int32_t L_28 = L_27->get_numDebrisPieces_6();
		float L_29 = __this->get_U3CmultiplierU3E5__2_3();
		if ((((float)(((float)((float)L_26)))) < ((float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_28))), (float)L_29)))))
		{
			goto IL_0037;
		}
	}
	{
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_00b2:
	{
		__this->set_U3CU3E1__state_0((-1));
		float L_30 = __this->get_U3CmultiplierU3E5__2_3();
		V_2 = ((float)il2cpp_codegen_multiply((float)(10.0f), (float)L_30));
		ExplosionFireAndDebris_tACCFCC1184BB9A8C3A0B39D99FFC831E76F9EECB * L_31 = V_1;
		NullCheck(L_31);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_32 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_31, /*hidden argument*/NULL);
		NullCheck(L_32);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_33 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_32, /*hidden argument*/NULL);
		float L_34 = V_2;
		ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* L_35 = Physics_OverlapSphere_m354A92672F7A6DE59EF1285D02D62247F46A5D84(L_33, L_34, /*hidden argument*/NULL);
		V_7 = L_35;
		V_8 = 0;
		goto IL_0159;
	}

IL_00de:
	{
		ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* L_36 = V_7;
		int32_t L_37 = V_8;
		NullCheck(L_36);
		int32_t L_38 = L_37;
		Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * L_39 = (L_36)->GetAt(static_cast<il2cpp_array_size_t>(L_38));
		V_9 = L_39;
		ExplosionFireAndDebris_tACCFCC1184BB9A8C3A0B39D99FFC831E76F9EECB * L_40 = V_1;
		NullCheck(L_40);
		int32_t L_41 = L_40->get_numFires_7();
		if ((((int32_t)L_41) <= ((int32_t)0)))
		{
			goto IL_0153;
		}
	}
	{
		ExplosionFireAndDebris_tACCFCC1184BB9A8C3A0B39D99FFC831E76F9EECB * L_42 = V_1;
		NullCheck(L_42);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_43 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_42, /*hidden argument*/NULL);
		NullCheck(L_43);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_44 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_43, /*hidden argument*/NULL);
		Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * L_45 = V_9;
		NullCheck(L_45);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_46 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_45, /*hidden argument*/NULL);
		NullCheck(L_46);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_47 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_46, /*hidden argument*/NULL);
		ExplosionFireAndDebris_tACCFCC1184BB9A8C3A0B39D99FFC831E76F9EECB * L_48 = V_1;
		NullCheck(L_48);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_49 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_48, /*hidden argument*/NULL);
		NullCheck(L_49);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_50 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_49, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_51 = Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3(L_47, L_50, /*hidden argument*/NULL);
		Ray__ctor_m695D219349B8AA4C82F96C55A27D384C07736F6B((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&V_11), L_44, L_51, /*hidden argument*/NULL);
		Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * L_52 = V_9;
		Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  L_53 = V_11;
		float L_54 = V_2;
		NullCheck(L_52);
		bool L_55 = Collider_Raycast_mF79828BB10A356336B673595777F7C6941E3F42F(L_52, L_53, (RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 *)(&V_10), L_54, /*hidden argument*/NULL);
		if (!L_55)
		{
			goto IL_0153;
		}
	}
	{
		ExplosionFireAndDebris_tACCFCC1184BB9A8C3A0B39D99FFC831E76F9EECB * L_56 = V_1;
		Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * L_57 = V_9;
		NullCheck(L_57);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_58 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_57, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_59 = RaycastHit_get_point_m0E564B2A72C7A744B889AE9D596F3EFA55059001((RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 *)(&V_10), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_60 = RaycastHit_get_normal_mF736A6D09D98D63AB7E5BF10F38AEBFC177A1D94((RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 *)(&V_10), /*hidden argument*/NULL);
		NullCheck(L_56);
		ExplosionFireAndDebris_AddFire_m781F3E400896C4F2E2B4A24FAEFE52D01ED26C0B(L_56, L_58, L_59, L_60, /*hidden argument*/NULL);
		ExplosionFireAndDebris_tACCFCC1184BB9A8C3A0B39D99FFC831E76F9EECB * L_61 = V_1;
		ExplosionFireAndDebris_tACCFCC1184BB9A8C3A0B39D99FFC831E76F9EECB * L_62 = V_1;
		NullCheck(L_62);
		int32_t L_63 = L_62->get_numFires_7();
		NullCheck(L_61);
		L_61->set_numFires_7(((int32_t)il2cpp_codegen_subtract((int32_t)L_63, (int32_t)1)));
	}

IL_0153:
	{
		int32_t L_64 = V_8;
		V_8 = ((int32_t)il2cpp_codegen_add((int32_t)L_64, (int32_t)1));
	}

IL_0159:
	{
		int32_t L_65 = V_8;
		ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* L_66 = V_7;
		NullCheck(L_66);
		if ((((int32_t)L_65) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_66)->max_length)))))))
		{
			goto IL_00de;
		}
	}
	{
		V_3 = (0.0f);
		goto IL_01c2;
	}

IL_016c:
	{
		ExplosionFireAndDebris_tACCFCC1184BB9A8C3A0B39D99FFC831E76F9EECB * L_67 = V_1;
		NullCheck(L_67);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_68 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_67, /*hidden argument*/NULL);
		NullCheck(L_68);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_69 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_68, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_70 = Vector3_get_up_m6309EBC4E42D6D0B3D28056BD23D0331275306F7(/*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_71 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_69, L_70, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_72 = Random_get_onUnitSphere_mBF4707ADEABB994E7B5B80305205A621B809E7E6(/*hidden argument*/NULL);
		Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  L_73;
		memset((&L_73), 0, sizeof(L_73));
		Ray__ctor_m695D219349B8AA4C82F96C55A27D384C07736F6B((&L_73), L_71, L_72, /*hidden argument*/NULL);
		float L_74 = V_3;
		bool L_75 = Physics_Raycast_mE1590EE4E2DC950A9FC2437E98EE8CD2EC2DEE67(L_73, (RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 *)(&V_12), L_74, /*hidden argument*/NULL);
		if (!L_75)
		{
			goto IL_01b8;
		}
	}
	{
		ExplosionFireAndDebris_tACCFCC1184BB9A8C3A0B39D99FFC831E76F9EECB * L_76 = V_1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_77 = RaycastHit_get_point_m0E564B2A72C7A744B889AE9D596F3EFA55059001((RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 *)(&V_12), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_78 = RaycastHit_get_normal_mF736A6D09D98D63AB7E5BF10F38AEBFC177A1D94((RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 *)(&V_12), /*hidden argument*/NULL);
		NullCheck(L_76);
		ExplosionFireAndDebris_AddFire_m781F3E400896C4F2E2B4A24FAEFE52D01ED26C0B(L_76, (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *)NULL, L_77, L_78, /*hidden argument*/NULL);
		ExplosionFireAndDebris_tACCFCC1184BB9A8C3A0B39D99FFC831E76F9EECB * L_79 = V_1;
		ExplosionFireAndDebris_tACCFCC1184BB9A8C3A0B39D99FFC831E76F9EECB * L_80 = V_1;
		NullCheck(L_80);
		int32_t L_81 = L_80->get_numFires_7();
		NullCheck(L_79);
		L_79->set_numFires_7(((int32_t)il2cpp_codegen_subtract((int32_t)L_81, (int32_t)1)));
	}

IL_01b8:
	{
		float L_82 = V_3;
		float L_83 = V_2;
		V_3 = ((float)il2cpp_codegen_add((float)L_82, (float)((float)il2cpp_codegen_multiply((float)L_83, (float)(0.1f)))));
	}

IL_01c2:
	{
		ExplosionFireAndDebris_tACCFCC1184BB9A8C3A0B39D99FFC831E76F9EECB * L_84 = V_1;
		NullCheck(L_84);
		int32_t L_85 = L_84->get_numFires_7();
		if ((((int32_t)L_85) <= ((int32_t)0)))
		{
			goto IL_01cf;
		}
	}
	{
		float L_86 = V_3;
		float L_87 = V_2;
		if ((((float)L_86) < ((float)L_87)))
		{
			goto IL_016c;
		}
	}

IL_01cf:
	{
		return (bool)0;
	}
}
// System.Object UnityStandardAssets.Effects.ExplosionFireAndDebris_<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m134C857EC6C5EEB674DF4B22AE43D9CBD4D3596F (U3CStartU3Ed__4_t945A707CD8C01DA1C37AE2DD9D0A904C80F58139 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris_<Start>d__4::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m886FA065B7C64EA3A746502C9F6BF104B9801F07 (U3CStartU3Ed__4_t945A707CD8C01DA1C37AE2DD9D0A904C80F58139 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m886FA065B7C64EA3A746502C9F6BF104B9801F07_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 * L_0 = (NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 *)il2cpp_codegen_object_new(NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_mA121DE1CAC8F25277DEB489DC7771209D91CAE33(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m886FA065B7C64EA3A746502C9F6BF104B9801F07_RuntimeMethod_var);
	}
}
// System.Object UnityStandardAssets.Effects.ExplosionFireAndDebris_<Start>d__4::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mEA0D5C2C7930B6E16648C869029DC54290E095E2 (U3CStartU3Ed__4_t945A707CD8C01DA1C37AE2DD9D0A904C80F58139 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityStandardAssets.Effects.ExtinguishableParticleSystem::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtinguishableParticleSystem_Start_m442E9165E311C7DC5AE322C5CF8B8D6A2325DC39 (ExtinguishableParticleSystem_tD91348BD8771EB84F5369DA844F390BE915ADB64 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ExtinguishableParticleSystem_Start_m442E9165E311C7DC5AE322C5CF8B8D6A2325DC39_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ParticleSystemU5BU5D_t58EE604F685D8CBA8EFC9353456969F5A1B2FBB9* L_0 = Component_GetComponentsInChildren_TisParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D_mD5AEF673286CB17B689F765BB71E83509548DC41(__this, /*hidden argument*/Component_GetComponentsInChildren_TisParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D_mD5AEF673286CB17B689F765BB71E83509548DC41_RuntimeMethod_var);
		__this->set_m_Systems_5(L_0);
		return;
	}
}
// System.Void UnityStandardAssets.Effects.ExtinguishableParticleSystem::Extinguish()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtinguishableParticleSystem_Extinguish_m2E9C8C61E7E4E1083A3059582EE9F68BB0D8EFA8 (ExtinguishableParticleSystem_tD91348BD8771EB84F5369DA844F390BE915ADB64 * __this, const RuntimeMethod* method)
{
	ParticleSystemU5BU5D_t58EE604F685D8CBA8EFC9353456969F5A1B2FBB9* V_0 = NULL;
	int32_t V_1 = 0;
	EmissionModule_t35028C3DE5EFDCE49E8A9732460617A56BD1D3F1  V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		ParticleSystemU5BU5D_t58EE604F685D8CBA8EFC9353456969F5A1B2FBB9* L_0 = __this->get_m_Systems_5();
		V_0 = L_0;
		V_1 = 0;
		goto IL_0020;
	}

IL_000b:
	{
		ParticleSystemU5BU5D_t58EE604F685D8CBA8EFC9353456969F5A1B2FBB9* L_1 = V_0;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		NullCheck(L_4);
		EmissionModule_t35028C3DE5EFDCE49E8A9732460617A56BD1D3F1  L_5 = ParticleSystem_get_emission_mA1204EAF07A6C6B3F65B45295797A1FFF64D343C(L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		EmissionModule_set_enabled_m3896B441BDE0F0752A6D113012B20D5D31B16D36((EmissionModule_t35028C3DE5EFDCE49E8A9732460617A56BD1D3F1 *)(&V_2), (bool)0, /*hidden argument*/NULL);
		int32_t L_6 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_1;
		ParticleSystemU5BU5D_t58EE604F685D8CBA8EFC9353456969F5A1B2FBB9* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_8)->max_length)))))))
		{
			goto IL_000b;
		}
	}
	{
		return;
	}
}
// System.Void UnityStandardAssets.Effects.ExtinguishableParticleSystem::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtinguishableParticleSystem__ctor_m66F7FA6921D145A9EF55397A60FCC4CA574DA068 (ExtinguishableParticleSystem_tD91348BD8771EB84F5369DA844F390BE915ADB64 * __this, const RuntimeMethod* method)
{
	{
		__this->set_multiplier_4((1.0f));
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityStandardAssets.Effects.FireLight::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FireLight_Start_mBC43BC2A9EDAD47A5D43C96C04E1AECC4405B81D (FireLight_tBDF3DBBD1B6288E05ECBDCDB787FBB8ABD8572C5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FireLight_Start_mBC43BC2A9EDAD47A5D43C96C04E1AECC4405B81D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = Random_get_value_mC998749E08291DD42CF31C026FAC4F14F746831C(/*hidden argument*/NULL);
		__this->set_m_Rnd_4(((float)il2cpp_codegen_multiply((float)L_0, (float)(100.0f))));
		Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C * L_1 = Component_GetComponent_TisLight_tFDE490EADBC7E080F74CA804929513AF07C31A6C_m1DCED5DB1934151FC68A8E7CAECF7986359D7107(__this, /*hidden argument*/Component_GetComponent_TisLight_tFDE490EADBC7E080F74CA804929513AF07C31A6C_m1DCED5DB1934151FC68A8E7CAECF7986359D7107_RuntimeMethod_var);
		__this->set_m_Light_6(L_1);
		return;
	}
}
// System.Void UnityStandardAssets.Effects.FireLight::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FireLight_Update_mE6D64BEF14D90624104BC092626B738B75D8D0F7 (FireLight_tBDF3DBBD1B6288E05ECBDCDB787FBB8ABD8572C5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FireLight_Update_mE6D64BEF14D90624104BC092626B738B75D8D0F7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		bool L_0 = __this->get_m_Burning_5();
		if (!L_0)
		{
			goto IL_0120;
		}
	}
	{
		Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C * L_1 = __this->get_m_Light_6();
		float L_2 = __this->get_m_Rnd_4();
		float L_3 = Time_get_time_m7863349C8845BBA36629A2B3F8EF1C3BEA350FD8(/*hidden argument*/NULL);
		float L_4 = __this->get_m_Rnd_4();
		float L_5 = Time_get_time_m7863349C8845BBA36629A2B3F8EF1C3BEA350FD8(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_6 = Mathf_PerlinNoise_mA36E513B2931F17ACBF26D928674D3DDEDF5C810(((float)il2cpp_codegen_add((float)L_2, (float)L_3)), ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)L_4, (float)(1.0f))), (float)((float)il2cpp_codegen_multiply((float)L_5, (float)(1.0f))))), /*hidden argument*/NULL);
		NullCheck(L_1);
		Light_set_intensity_mE209975C840F1D887B4207C390DB5A2EF15A763C(L_1, ((float)il2cpp_codegen_multiply((float)(2.0f), (float)L_6)), /*hidden argument*/NULL);
		float L_7 = __this->get_m_Rnd_4();
		float L_8 = Time_get_time_m7863349C8845BBA36629A2B3F8EF1C3BEA350FD8(/*hidden argument*/NULL);
		float L_9 = __this->get_m_Rnd_4();
		float L_10 = Time_get_time_m7863349C8845BBA36629A2B3F8EF1C3BEA350FD8(/*hidden argument*/NULL);
		float L_11 = Mathf_PerlinNoise_mA36E513B2931F17ACBF26D928674D3DDEDF5C810(((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)L_7, (float)(0.0f))), (float)((float)il2cpp_codegen_multiply((float)L_8, (float)(2.0f))))), ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)L_9, (float)(1.0f))), (float)((float)il2cpp_codegen_multiply((float)L_10, (float)(2.0f))))), /*hidden argument*/NULL);
		V_0 = ((float)il2cpp_codegen_subtract((float)L_11, (float)(0.5f)));
		float L_12 = __this->get_m_Rnd_4();
		float L_13 = Time_get_time_m7863349C8845BBA36629A2B3F8EF1C3BEA350FD8(/*hidden argument*/NULL);
		float L_14 = __this->get_m_Rnd_4();
		float L_15 = Time_get_time_m7863349C8845BBA36629A2B3F8EF1C3BEA350FD8(/*hidden argument*/NULL);
		float L_16 = Mathf_PerlinNoise_mA36E513B2931F17ACBF26D928674D3DDEDF5C810(((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)L_12, (float)(2.0f))), (float)((float)il2cpp_codegen_multiply((float)L_13, (float)(2.0f))))), ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)L_14, (float)(3.0f))), (float)((float)il2cpp_codegen_multiply((float)L_15, (float)(2.0f))))), /*hidden argument*/NULL);
		V_1 = ((float)il2cpp_codegen_subtract((float)L_16, (float)(0.5f)));
		float L_17 = __this->get_m_Rnd_4();
		float L_18 = Time_get_time_m7863349C8845BBA36629A2B3F8EF1C3BEA350FD8(/*hidden argument*/NULL);
		float L_19 = __this->get_m_Rnd_4();
		float L_20 = Time_get_time_m7863349C8845BBA36629A2B3F8EF1C3BEA350FD8(/*hidden argument*/NULL);
		float L_21 = Mathf_PerlinNoise_mA36E513B2931F17ACBF26D928674D3DDEDF5C810(((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)L_17, (float)(4.0f))), (float)((float)il2cpp_codegen_multiply((float)L_18, (float)(2.0f))))), ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)L_19, (float)(5.0f))), (float)((float)il2cpp_codegen_multiply((float)L_20, (float)(2.0f))))), /*hidden argument*/NULL);
		V_2 = ((float)il2cpp_codegen_subtract((float)L_21, (float)(0.5f)));
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_22 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_23 = Vector3_get_up_m6309EBC4E42D6D0B3D28056BD23D0331275306F7(/*hidden argument*/NULL);
		float L_24 = V_0;
		float L_25 = V_1;
		float L_26 = V_2;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_27;
		memset((&L_27), 0, sizeof(L_27));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_27), L_24, L_25, L_26, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_28 = Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E(L_27, (1.0f), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_29 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_23, L_28, /*hidden argument*/NULL);
		NullCheck(L_22);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_22, L_29, /*hidden argument*/NULL);
	}

IL_0120:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Effects.FireLight::Extinguish()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FireLight_Extinguish_m96CC75996F151C74E4D46556599FDF4F39AF6776 (FireLight_tBDF3DBBD1B6288E05ECBDCDB787FBB8ABD8572C5 * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_Burning_5((bool)0);
		Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C * L_0 = __this->get_m_Light_6();
		NullCheck(L_0);
		Behaviour_set_enabled_m9755D3B17D7022D23D1E4C618BD9A6B66A5ADC6B(L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Effects.FireLight::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FireLight__ctor_mFAEAECD04029DD5E24A331FEE4B5C43FFD5B6FBE (FireLight_tBDF3DBBD1B6288E05ECBDCDB787FBB8ABD8572C5 * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_Burning_5((bool)1);
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityStandardAssets.Effects.Hose::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Hose_Update_mF09247CA136AF92D5586DD9C95823C7E9A6AE939 (Hose_t411AEC20AB5A70AEFCF03FCB2418F0C8EB14C2D2 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Hose_Update_mF09247CA136AF92D5586DD9C95823C7E9A6AE939_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ParticleSystemU5BU5D_t58EE604F685D8CBA8EFC9353456969F5A1B2FBB9* V_0 = NULL;
	int32_t V_1 = 0;
	MainModule_t99C675667E0A363368324132DFA34B27FFEE6FC7  V_2;
	memset((&V_2), 0, sizeof(V_2));
	EmissionModule_t35028C3DE5EFDCE49E8A9732460617A56BD1D3F1  V_3;
	memset((&V_3), 0, sizeof(V_3));
	float G_B2_0 = 0.0f;
	Hose_t411AEC20AB5A70AEFCF03FCB2418F0C8EB14C2D2 * G_B2_1 = NULL;
	float G_B1_0 = 0.0f;
	Hose_t411AEC20AB5A70AEFCF03FCB2418F0C8EB14C2D2 * G_B1_1 = NULL;
	float G_B3_0 = 0.0f;
	float G_B3_1 = 0.0f;
	Hose_t411AEC20AB5A70AEFCF03FCB2418F0C8EB14C2D2 * G_B3_2 = NULL;
	{
		float L_0 = __this->get_m_Power_9();
		bool L_1 = Input_GetMouseButton_m43C68DE93C7D990E875BA53C4DEC9CA6230C8B79(0, /*hidden argument*/NULL);
		G_B1_0 = L_0;
		G_B1_1 = __this;
		if (L_1)
		{
			G_B2_0 = L_0;
			G_B2_1 = __this;
			goto IL_0017;
		}
	}
	{
		float L_2 = __this->get_minPower_5();
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_001d;
	}

IL_0017:
	{
		float L_3 = __this->get_maxPower_4();
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_001d:
	{
		float L_4 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		float L_5 = __this->get_changeSpeed_6();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_6 = Mathf_Lerp_m9A74C5A0C37D0CDF45EE66E7774D12A9B93B1364(G_B3_1, G_B3_0, ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), /*hidden argument*/NULL);
		NullCheck(G_B3_2);
		G_B3_2->set_m_Power_9(L_6);
		bool L_7 = Input_GetKeyDown_mEA57896808B6F484B12CD0AEEB83390A3CFCDBDC(((int32_t)49), /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0055;
		}
	}
	{
		Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * L_8 = __this->get_systemRenderer_8();
		Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * L_9 = __this->get_systemRenderer_8();
		NullCheck(L_9);
		bool L_10 = Renderer_get_enabled_m40E07BB15DA58D2EF6F6796C6778163107DD7E1B(L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		Renderer_set_enabled_m0933766657F2685BAAE3340B0A984C0E63925303(L_8, (bool)((((int32_t)L_10) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
	}

IL_0055:
	{
		ParticleSystemU5BU5D_t58EE604F685D8CBA8EFC9353456969F5A1B2FBB9* L_11 = __this->get_hoseWaterSystems_7();
		V_0 = L_11;
		V_1 = 0;
		goto IL_00a1;
	}

IL_0060:
	{
		ParticleSystemU5BU5D_t58EE604F685D8CBA8EFC9353456969F5A1B2FBB9* L_12 = V_0;
		int32_t L_13 = V_1;
		NullCheck(L_12);
		int32_t L_14 = L_13;
		ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * L_16 = L_15;
		NullCheck(L_16);
		MainModule_t99C675667E0A363368324132DFA34B27FFEE6FC7  L_17 = ParticleSystem_get_main_m360B0AA57C71DE0358B6B07133C68B5FD88C742F(L_16, /*hidden argument*/NULL);
		V_2 = L_17;
		float L_18 = __this->get_m_Power_9();
		MinMaxCurve_tDB335EDEBEBD4CFA753081D7C3A2FE2EECFA6D71  L_19 = MinMaxCurve_op_Implicit_m998EE9F8D83B9545F63E2DFA304E99620F0F707F(L_18, /*hidden argument*/NULL);
		MainModule_set_startSpeed_m32CD8968F2B5572112DE22CC7B6E2B502222B4AE((MainModule_t99C675667E0A363368324132DFA34B27FFEE6FC7 *)(&V_2), L_19, /*hidden argument*/NULL);
		NullCheck(L_16);
		EmissionModule_t35028C3DE5EFDCE49E8A9732460617A56BD1D3F1  L_20 = ParticleSystem_get_emission_mA1204EAF07A6C6B3F65B45295797A1FFF64D343C(L_16, /*hidden argument*/NULL);
		V_3 = L_20;
		float L_21 = __this->get_m_Power_9();
		float L_22 = __this->get_minPower_5();
		EmissionModule_set_enabled_m3896B441BDE0F0752A6D113012B20D5D31B16D36((EmissionModule_t35028C3DE5EFDCE49E8A9732460617A56BD1D3F1 *)(&V_3), (bool)((((float)L_21) > ((float)((float)il2cpp_codegen_multiply((float)L_22, (float)(1.1f)))))? 1 : 0), /*hidden argument*/NULL);
		int32_t L_23 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_23, (int32_t)1));
	}

IL_00a1:
	{
		int32_t L_24 = V_1;
		ParticleSystemU5BU5D_t58EE604F685D8CBA8EFC9353456969F5A1B2FBB9* L_25 = V_0;
		NullCheck(L_25);
		if ((((int32_t)L_24) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_25)->max_length)))))))
		{
			goto IL_0060;
		}
	}
	{
		return;
	}
}
// System.Void UnityStandardAssets.Effects.Hose::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Hose__ctor_mDB316C87F32CA6A88980009F13FCB35475103094 (Hose_t411AEC20AB5A70AEFCF03FCB2418F0C8EB14C2D2 * __this, const RuntimeMethod* method)
{
	{
		__this->set_maxPower_4((20.0f));
		__this->set_minPower_5((5.0f));
		__this->set_changeSpeed_6((5.0f));
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityStandardAssets.Effects.ParticleSystemMultiplier::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemMultiplier_Start_m9629829804A66FA8E1BA25039F0BCD4447D1785D (ParticleSystemMultiplier_t8EBFEF83EBA48DD7F6517ABFFBE2CD051CB2C748 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ParticleSystemMultiplier_Start_m9629829804A66FA8E1BA25039F0BCD4447D1785D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ParticleSystemU5BU5D_t58EE604F685D8CBA8EFC9353456969F5A1B2FBB9* V_0 = NULL;
	int32_t V_1 = 0;
	MainModule_t99C675667E0A363368324132DFA34B27FFEE6FC7  V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		ParticleSystemU5BU5D_t58EE604F685D8CBA8EFC9353456969F5A1B2FBB9* L_0 = Component_GetComponentsInChildren_TisParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D_mD5AEF673286CB17B689F765BB71E83509548DC41(__this, /*hidden argument*/Component_GetComponentsInChildren_TisParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D_mD5AEF673286CB17B689F765BB71E83509548DC41_RuntimeMethod_var);
		V_0 = L_0;
		V_1 = 0;
		goto IL_006f;
	}

IL_000b:
	{
		ParticleSystemU5BU5D_t58EE604F685D8CBA8EFC9353456969F5A1B2FBB9* L_1 = V_0;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * L_5 = L_4;
		NullCheck(L_5);
		MainModule_t99C675667E0A363368324132DFA34B27FFEE6FC7  L_6 = ParticleSystem_get_main_m360B0AA57C71DE0358B6B07133C68B5FD88C742F(L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		MainModule_t99C675667E0A363368324132DFA34B27FFEE6FC7 * L_7 = (&V_2);
		float L_8 = MainModule_get_startSizeMultiplier_m8E241099DB1E4ECE4827E9507415C80012AEA312((MainModule_t99C675667E0A363368324132DFA34B27FFEE6FC7 *)L_7, /*hidden argument*/NULL);
		float L_9 = __this->get_multiplier_4();
		MainModule_set_startSizeMultiplier_m6375840B2CADFF82321D7B52103FADC5AD144B8C((MainModule_t99C675667E0A363368324132DFA34B27FFEE6FC7 *)L_7, ((float)il2cpp_codegen_multiply((float)L_8, (float)L_9)), /*hidden argument*/NULL);
		MainModule_t99C675667E0A363368324132DFA34B27FFEE6FC7 * L_10 = (&V_2);
		float L_11 = MainModule_get_startSpeedMultiplier_m22326AF745786C1748F6B165361CCC26917F214A((MainModule_t99C675667E0A363368324132DFA34B27FFEE6FC7 *)L_10, /*hidden argument*/NULL);
		float L_12 = __this->get_multiplier_4();
		MainModule_set_startSpeedMultiplier_m14E157EFF4BD78D8FDA701BE2BD0B398EF6F2453((MainModule_t99C675667E0A363368324132DFA34B27FFEE6FC7 *)L_10, ((float)il2cpp_codegen_multiply((float)L_11, (float)L_12)), /*hidden argument*/NULL);
		MainModule_t99C675667E0A363368324132DFA34B27FFEE6FC7 * L_13 = (&V_2);
		float L_14 = MainModule_get_startLifetimeMultiplier_m0D425E7689C0B99C5B9E8F8C4AE205600C1EA529((MainModule_t99C675667E0A363368324132DFA34B27FFEE6FC7 *)L_13, /*hidden argument*/NULL);
		float L_15 = __this->get_multiplier_4();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_16 = Mathf_Lerp_m9A74C5A0C37D0CDF45EE66E7774D12A9B93B1364(L_15, (1.0f), (0.5f), /*hidden argument*/NULL);
		MainModule_set_startLifetimeMultiplier_mCD094E55BD574ECF3A8F5D616A2287130B6EC67B((MainModule_t99C675667E0A363368324132DFA34B27FFEE6FC7 *)L_13, ((float)il2cpp_codegen_multiply((float)L_14, (float)L_16)), /*hidden argument*/NULL);
		ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * L_17 = L_5;
		NullCheck(L_17);
		ParticleSystem_Clear_mE11D3D0E23B1C5AAFF1F432E278ED91F1D929FEE(L_17, /*hidden argument*/NULL);
		NullCheck(L_17);
		ParticleSystem_Play_m5BC5E6B56FCF639CAD5DF41B51DC05A0B444212F(L_17, /*hidden argument*/NULL);
		int32_t L_18 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_18, (int32_t)1));
	}

IL_006f:
	{
		int32_t L_19 = V_1;
		ParticleSystemU5BU5D_t58EE604F685D8CBA8EFC9353456969F5A1B2FBB9* L_20 = V_0;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_20)->max_length)))))))
		{
			goto IL_000b;
		}
	}
	{
		return;
	}
}
// System.Void UnityStandardAssets.Effects.ParticleSystemMultiplier::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemMultiplier__ctor_mFA383D5A7F8469462B1E9A5EDB01811D52DC0CFC (ParticleSystemMultiplier_t8EBFEF83EBA48DD7F6517ABFFBE2CD051CB2C748 * __this, const RuntimeMethod* method)
{
	{
		__this->set_multiplier_4((1.0f));
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityStandardAssets.Effects.SmokeParticles::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SmokeParticles_Start_m1A4CBEB9EDF7F792FCF7F96D9435B6C0B0A94DD6 (SmokeParticles_t556E08200E5E25126D51BFF297C7522998E3D793 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmokeParticles_Start_m1A4CBEB9EDF7F792FCF7F96D9435B6C0B0A94DD6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * L_0 = Component_GetComponent_TisAudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C_m04C8E98F2393C77979C9D8F6DE1D98343EF025E8(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C_m04C8E98F2393C77979C9D8F6DE1D98343EF025E8_RuntimeMethod_var);
		AudioClipU5BU5D_t03931BD44BC339329210676E452B8ECD3EC171C2* L_1 = __this->get_extinguishSounds_4();
		AudioClipU5BU5D_t03931BD44BC339329210676E452B8ECD3EC171C2* L_2 = __this->get_extinguishSounds_4();
		NullCheck(L_2);
		int32_t L_3 = Random_Range_mD0C8F37FF3CAB1D87AAA6C45130BD59626BD6780(0, (((int32_t)((int32_t)(((RuntimeArray*)L_2)->max_length)))), /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_4 = L_3;
		AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * L_5 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck(L_0);
		AudioSource_set_clip_mF574231E0B749E0167CAF9E4FCBA06BAA0F9ED9B(L_0, L_5, /*hidden argument*/NULL);
		AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * L_6 = Component_GetComponent_TisAudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C_m04C8E98F2393C77979C9D8F6DE1D98343EF025E8(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C_m04C8E98F2393C77979C9D8F6DE1D98343EF025E8_RuntimeMethod_var);
		NullCheck(L_6);
		AudioSource_Play_m0BA206481892AA4AF7DB2900A0B0805076516164(L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Effects.SmokeParticles::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SmokeParticles__ctor_m6FF23CF3C3FFDCE57F75C6DA08F84101A4953F0C (SmokeParticles_t556E08200E5E25126D51BFF297C7522998E3D793 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityStandardAssets.Effects.WaterHoseParticles::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaterHoseParticles_Start_m6CFC5FEBF60CBD00CDE17E0A1EEC12163EC9A792 (WaterHoseParticles_t4C0EC93AEAF5B1623A7F04AF7D912D6B6438E92B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaterHoseParticles_Start_m6CFC5FEBF60CBD00CDE17E0A1EEC12163EC9A792_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * L_0 = Component_GetComponent_TisParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D_mA6EBF435A59643B674086A2F309F6A4DCB263452(__this, /*hidden argument*/Component_GetComponent_TisParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D_mA6EBF435A59643B674086A2F309F6A4DCB263452_RuntimeMethod_var);
		__this->set_m_ParticleSystem_7(L_0);
		return;
	}
}
// System.Void UnityStandardAssets.Effects.WaterHoseParticles::OnParticleCollision(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaterHoseParticles_OnParticleCollision_m8E8399822017058F9AE64E3D9F7B47208553ED70 (WaterHoseParticles_t4C0EC93AEAF5B1623A7F04AF7D912D6B6438E92B * __this, GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaterHoseParticles_OnParticleCollision_m8E8399822017058F9AE64E3D9F7B47208553ED70_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * V_2 = NULL;
	ParticleCollisionEvent_t7F4D7D5ACF8521671549D9328D4E2DDE480D8D47  V_3;
	memset((&V_3), 0, sizeof(V_3));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_4;
	memset((&V_4), 0, sizeof(V_4));
	{
		ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * L_0 = __this->get_m_ParticleSystem_7();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = ___other0;
		List_1_t2762C811E470D336E31761384C6E5382164DA4C7 * L_2 = __this->get_m_CollisionEvents_6();
		int32_t L_3 = ParticlePhysicsExtensions_GetCollisionEvents_m11DDE18328B0E4B9766D1581DFCB0E67BCFD097C(L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0090;
	}

IL_0017:
	{
		float L_4 = Time_get_time_m7863349C8845BBA36629A2B3F8EF1C3BEA350FD8(/*hidden argument*/NULL);
		float L_5 = ((WaterHoseParticles_t4C0EC93AEAF5B1623A7F04AF7D912D6B6438E92B_StaticFields*)il2cpp_codegen_static_fields_for(WaterHoseParticles_t4C0EC93AEAF5B1623A7F04AF7D912D6B6438E92B_il2cpp_TypeInfo_var))->get_lastSoundTime_4();
		if ((!(((float)L_4) > ((float)((float)il2cpp_codegen_add((float)L_5, (float)(0.2f)))))))
		{
			goto IL_0033;
		}
	}
	{
		float L_6 = Time_get_time_m7863349C8845BBA36629A2B3F8EF1C3BEA350FD8(/*hidden argument*/NULL);
		((WaterHoseParticles_t4C0EC93AEAF5B1623A7F04AF7D912D6B6438E92B_StaticFields*)il2cpp_codegen_static_fields_for(WaterHoseParticles_t4C0EC93AEAF5B1623A7F04AF7D912D6B6438E92B_il2cpp_TypeInfo_var))->set_lastSoundTime_4(L_6);
	}

IL_0033:
	{
		List_1_t2762C811E470D336E31761384C6E5382164DA4C7 * L_7 = __this->get_m_CollisionEvents_6();
		int32_t L_8 = V_1;
		NullCheck(L_7);
		ParticleCollisionEvent_t7F4D7D5ACF8521671549D9328D4E2DDE480D8D47  L_9 = List_1_get_Item_m095B750642DEE222CA241EE9D2392CA6E200FA8D_inline(L_7, L_8, /*hidden argument*/List_1_get_Item_m095B750642DEE222CA241EE9D2392CA6E200FA8D_RuntimeMethod_var);
		V_3 = L_9;
		Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * L_10 = ParticleCollisionEvent_get_colliderComponent_mE4A94CAC6A2875588395DCB76FB7978A72796B82((ParticleCollisionEvent_t7F4D7D5ACF8521671549D9328D4E2DDE480D8D47 *)(&V_3), /*hidden argument*/NULL);
		NullCheck(L_10);
		Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * L_11 = Component_GetComponent_TisRigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5_m3F58A77E3F62D9C80D7B49BA04E3D4809264FD5C(L_10, /*hidden argument*/Component_GetComponent_TisRigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5_m3F58A77E3F62D9C80D7B49BA04E3D4809264FD5C_RuntimeMethod_var);
		V_2 = L_11;
		Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * L_12 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_13 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_12, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0080;
		}
	}
	{
		List_1_t2762C811E470D336E31761384C6E5382164DA4C7 * L_14 = __this->get_m_CollisionEvents_6();
		int32_t L_15 = V_1;
		NullCheck(L_14);
		ParticleCollisionEvent_t7F4D7D5ACF8521671549D9328D4E2DDE480D8D47  L_16 = List_1_get_Item_m095B750642DEE222CA241EE9D2392CA6E200FA8D_inline(L_14, L_15, /*hidden argument*/List_1_get_Item_m095B750642DEE222CA241EE9D2392CA6E200FA8D_RuntimeMethod_var);
		V_3 = L_16;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_17 = ParticleCollisionEvent_get_velocity_m60BCDF44F8F345C3534705CBA2BD5E69346021A3((ParticleCollisionEvent_t7F4D7D5ACF8521671549D9328D4E2DDE480D8D47 *)(&V_3), /*hidden argument*/NULL);
		V_4 = L_17;
		Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * L_18 = V_2;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_19 = V_4;
		float L_20 = __this->get_force_5();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_21 = Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E(L_19, L_20, /*hidden argument*/NULL);
		NullCheck(L_18);
		Rigidbody_AddForce_mD64ACF772614FE36CFD8A477A07A407B35DF1A54(L_18, L_21, 1, /*hidden argument*/NULL);
	}

IL_0080:
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_22 = ___other0;
		NullCheck(L_22);
		GameObject_BroadcastMessage_mABB308EA1B7F641BFC60E091A75E5E053D23A8B5(L_22, _stringLiteral4796C2FBF2E7B58822864E6AA158FDAF94586F93, 1, /*hidden argument*/NULL);
		int32_t L_23 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_23, (int32_t)1));
	}

IL_0090:
	{
		int32_t L_24 = V_1;
		int32_t L_25 = V_0;
		if ((((int32_t)L_24) < ((int32_t)L_25)))
		{
			goto IL_0017;
		}
	}
	{
		return;
	}
}
// System.Void UnityStandardAssets.Effects.WaterHoseParticles::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaterHoseParticles__ctor_m9E63BF105FF8511932CBF7FAA1C664713744DCA3 (WaterHoseParticles_t4C0EC93AEAF5B1623A7F04AF7D912D6B6438E92B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaterHoseParticles__ctor_m9E63BF105FF8511932CBF7FAA1C664713744DCA3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_force_5((1.0f));
		List_1_t2762C811E470D336E31761384C6E5382164DA4C7 * L_0 = (List_1_t2762C811E470D336E31761384C6E5382164DA4C7 *)il2cpp_codegen_object_new(List_1_t2762C811E470D336E31761384C6E5382164DA4C7_il2cpp_TypeInfo_var);
		List_1__ctor_m377714278A7FAB4D7514D181355D61B01B3427B0(L_0, /*hidden argument*/List_1__ctor_m377714278A7FAB4D7514D181355D61B01B3427B0_RuntimeMethod_var);
		__this->set_m_CollisionEvents_6(L_0);
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR ParticleCollisionEvent_t7F4D7D5ACF8521671549D9328D4E2DDE480D8D47  List_1_get_Item_m095B750642DEE222CA241EE9D2392CA6E200FA8D_gshared_inline (List_1_t2762C811E470D336E31761384C6E5382164DA4C7 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_mBA2AF20A35144E0C43CD721A22EAC9FCA15D6550(/*hidden argument*/NULL);
	}

IL_000e:
	{
		ParticleCollisionEventU5BU5D_t771CB4A499CC97F3340C673D227DD1F5969EB9B9* L_2 = (ParticleCollisionEventU5BU5D_t771CB4A499CC97F3340C673D227DD1F5969EB9B9*)__this->get__items_1();
		int32_t L_3 = ___index0;
		ParticleCollisionEvent_t7F4D7D5ACF8521671549D9328D4E2DDE480D8D47  L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((ParticleCollisionEventU5BU5D_t771CB4A499CC97F3340C673D227DD1F5969EB9B9*)L_2, (int32_t)L_3);
		return L_4;
	}
}
