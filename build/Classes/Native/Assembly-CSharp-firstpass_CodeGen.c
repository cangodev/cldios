﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void UnityStandardAssets.Effects.AfterburnerPhysicsForce::OnEnable()
extern void AfterburnerPhysicsForce_OnEnable_m31ABCDDA8E500224F25244C9D3FE44CBDD45D7CE ();
// 0x00000002 System.Void UnityStandardAssets.Effects.AfterburnerPhysicsForce::FixedUpdate()
extern void AfterburnerPhysicsForce_FixedUpdate_m90286B7CDABBED931E9D13E5D879E6EC41BF6E8A ();
// 0x00000003 System.Void UnityStandardAssets.Effects.AfterburnerPhysicsForce::OnDrawGizmosSelected()
extern void AfterburnerPhysicsForce_OnDrawGizmosSelected_m8E75C894A4544C40EB1FD3DA8883E3358BAF8AA3 ();
// 0x00000004 System.Void UnityStandardAssets.Effects.AfterburnerPhysicsForce::.ctor()
extern void AfterburnerPhysicsForce__ctor_mE4F415134C355B0C418241BA9F4BE2DAB65BF1A0 ();
// 0x00000005 System.Collections.IEnumerator UnityStandardAssets.Effects.ExplosionFireAndDebris::Start()
extern void ExplosionFireAndDebris_Start_m85654B2EA9348AE14094A4CDCA1ECD714E4CBC22 ();
// 0x00000006 System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris::AddFire(UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Vector3)
extern void ExplosionFireAndDebris_AddFire_m781F3E400896C4F2E2B4A24FAEFE52D01ED26C0B ();
// 0x00000007 System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris::.ctor()
extern void ExplosionFireAndDebris__ctor_m6E9B3DD674A9E1DF3B85ECF3E618F7F29B5EB97E ();
// 0x00000008 System.Void UnityStandardAssets.Effects.ExtinguishableParticleSystem::Start()
extern void ExtinguishableParticleSystem_Start_m442E9165E311C7DC5AE322C5CF8B8D6A2325DC39 ();
// 0x00000009 System.Void UnityStandardAssets.Effects.ExtinguishableParticleSystem::Extinguish()
extern void ExtinguishableParticleSystem_Extinguish_m2E9C8C61E7E4E1083A3059582EE9F68BB0D8EFA8 ();
// 0x0000000A System.Void UnityStandardAssets.Effects.ExtinguishableParticleSystem::.ctor()
extern void ExtinguishableParticleSystem__ctor_m66F7FA6921D145A9EF55397A60FCC4CA574DA068 ();
// 0x0000000B System.Void UnityStandardAssets.Effects.FireLight::Start()
extern void FireLight_Start_mBC43BC2A9EDAD47A5D43C96C04E1AECC4405B81D ();
// 0x0000000C System.Void UnityStandardAssets.Effects.FireLight::Update()
extern void FireLight_Update_mE6D64BEF14D90624104BC092626B738B75D8D0F7 ();
// 0x0000000D System.Void UnityStandardAssets.Effects.FireLight::Extinguish()
extern void FireLight_Extinguish_m96CC75996F151C74E4D46556599FDF4F39AF6776 ();
// 0x0000000E System.Void UnityStandardAssets.Effects.FireLight::.ctor()
extern void FireLight__ctor_mFAEAECD04029DD5E24A331FEE4B5C43FFD5B6FBE ();
// 0x0000000F System.Void UnityStandardAssets.Effects.Hose::Update()
extern void Hose_Update_mF09247CA136AF92D5586DD9C95823C7E9A6AE939 ();
// 0x00000010 System.Void UnityStandardAssets.Effects.Hose::.ctor()
extern void Hose__ctor_mDB316C87F32CA6A88980009F13FCB35475103094 ();
// 0x00000011 System.Void UnityStandardAssets.Effects.ParticleSystemMultiplier::Start()
extern void ParticleSystemMultiplier_Start_m9629829804A66FA8E1BA25039F0BCD4447D1785D ();
// 0x00000012 System.Void UnityStandardAssets.Effects.ParticleSystemMultiplier::.ctor()
extern void ParticleSystemMultiplier__ctor_mFA383D5A7F8469462B1E9A5EDB01811D52DC0CFC ();
// 0x00000013 System.Void UnityStandardAssets.Effects.SmokeParticles::Start()
extern void SmokeParticles_Start_m1A4CBEB9EDF7F792FCF7F96D9435B6C0B0A94DD6 ();
// 0x00000014 System.Void UnityStandardAssets.Effects.SmokeParticles::.ctor()
extern void SmokeParticles__ctor_m6FF23CF3C3FFDCE57F75C6DA08F84101A4953F0C ();
// 0x00000015 System.Void UnityStandardAssets.Effects.WaterHoseParticles::Start()
extern void WaterHoseParticles_Start_m6CFC5FEBF60CBD00CDE17E0A1EEC12163EC9A792 ();
// 0x00000016 System.Void UnityStandardAssets.Effects.WaterHoseParticles::OnParticleCollision(UnityEngine.GameObject)
extern void WaterHoseParticles_OnParticleCollision_m8E8399822017058F9AE64E3D9F7B47208553ED70 ();
// 0x00000017 System.Void UnityStandardAssets.Effects.WaterHoseParticles::.ctor()
extern void WaterHoseParticles__ctor_m9E63BF105FF8511932CBF7FAA1C664713744DCA3 ();
// 0x00000018 System.Void SleekRender.SleekRenderPostProcess::OnEnable()
extern void SleekRenderPostProcess_OnEnable_m9BE5D8F5EC9B759E760480EA5A11AB9403A37534 ();
// 0x00000019 System.Void SleekRender.SleekRenderPostProcess::OnDisable()
extern void SleekRenderPostProcess_OnDisable_m890AF19CE2A19B82FE057B9AF69803EA1171B332 ();
// 0x0000001A System.Void SleekRender.SleekRenderPostProcess::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void SleekRenderPostProcess_OnRenderImage_m226B046B38C23DD3FBB020A757EE7161AF37004E ();
// 0x0000001B System.Void SleekRender.SleekRenderPostProcess::ApplyPostProcess(UnityEngine.RenderTexture)
extern void SleekRenderPostProcess_ApplyPostProcess_m7228BB4785430B68E38460258D93189559260835 ();
// 0x0000001C System.Void SleekRender.SleekRenderPostProcess::Downsample(UnityEngine.RenderTexture)
extern void SleekRenderPostProcess_Downsample_m939DB30198E9BB4BEFF3884A283D450C8704EE96 ();
// 0x0000001D System.Void SleekRender.SleekRenderPostProcess::Bloom(System.Boolean)
extern void SleekRenderPostProcess_Bloom_m38FA596CFD5659B6F02915F17199F0DF0ADE16FC ();
// 0x0000001E System.Void SleekRender.SleekRenderPostProcess::Precompose(System.Boolean)
extern void SleekRenderPostProcess_Precompose_mE5C8A6B77C24F3B90CDFC17D60895195BDD20965 ();
// 0x0000001F System.Void SleekRender.SleekRenderPostProcess::Compose(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void SleekRenderPostProcess_Compose_m76162AF19A38CE8168DB6ED4B2E89E56110D02BA ();
// 0x00000020 System.Void SleekRender.SleekRenderPostProcess::CreateResources()
extern void SleekRenderPostProcess_CreateResources_m88233905ED6473C25EA3C8A4B3757879C201D6DA ();
// 0x00000021 UnityEngine.RenderTexture SleekRender.SleekRenderPostProcess::CreateTransientRenderTexture(System.String,System.Int32,System.Int32)
extern void SleekRenderPostProcess_CreateTransientRenderTexture_m5896F414C30F00DE545EF638447324CC3D1DE67F ();
// 0x00000022 UnityEngine.RenderTexture SleekRender.SleekRenderPostProcess::CreateMainRenderTexture(System.Int32,System.Int32)
extern void SleekRenderPostProcess_CreateMainRenderTexture_mA21F6ABA9A57B0BD2A2ACBCCEBD12CA09A16838D ();
// 0x00000023 System.Void SleekRender.SleekRenderPostProcess::ReleaseResources()
extern void SleekRenderPostProcess_ReleaseResources_mDFBCB6AF7DB534D25370ED70E8C21A4D8E85F165 ();
// 0x00000024 System.Void SleekRender.SleekRenderPostProcess::DestroyImmediateIfNotNull(UnityEngine.Object)
extern void SleekRenderPostProcess_DestroyImmediateIfNotNull_m0E4B29743210245443999A680294C1DDBF0140DC ();
// 0x00000025 System.Void SleekRender.SleekRenderPostProcess::Blit(UnityEngine.Texture,UnityEngine.RenderTexture,UnityEngine.Material,System.Int32)
extern void SleekRenderPostProcess_Blit_mEDE1D6060C22F747323C93F8ABECC7C879413CBE ();
// 0x00000026 System.Void SleekRender.SleekRenderPostProcess::SetActiveRenderTextureAndClear(UnityEngine.RenderTexture)
extern void SleekRenderPostProcess_SetActiveRenderTextureAndClear_m01E22DC5E4C5579AA8732CA0F67E01F39E4AC89F ();
// 0x00000027 System.Void SleekRender.SleekRenderPostProcess::DrawFullscreenQuad(UnityEngine.Texture,UnityEngine.Material,System.Int32)
extern void SleekRenderPostProcess_DrawFullscreenQuad_m77F12B59A976F77BFC5A8BF270AD5E5D30408DD0 ();
// 0x00000028 System.Void SleekRender.SleekRenderPostProcess::CheckScreenSizeAndRecreateTexturesIfNeeded(UnityEngine.Camera)
extern void SleekRenderPostProcess_CheckScreenSizeAndRecreateTexturesIfNeeded_m8FE59E4480A6A09382775B46448C5784B79029E4 ();
// 0x00000029 System.Single SleekRender.SleekRenderPostProcess::GetCurrentAspect(UnityEngine.Camera)
extern void SleekRenderPostProcess_GetCurrentAspect_mFA278408C602D5213CB17EC938C37D070544DC79 ();
// 0x0000002A System.Void SleekRender.SleekRenderPostProcess::CreateDefaultSettingsIfNoneLinked()
extern void SleekRenderPostProcess_CreateDefaultSettingsIfNoneLinked_mFAFCBC9B80CB0CAE18DB94C102CD3D0FAEFA83A9 ();
// 0x0000002B UnityEngine.Mesh SleekRender.SleekRenderPostProcess::CreateScreenSpaceQuadMesh()
extern void SleekRenderPostProcess_CreateScreenSpaceQuadMesh_m2D59ABE6B083BEF9A9DE056AF5725199DD9FCB0C ();
// 0x0000002C System.Void SleekRender.SleekRenderPostProcess::.ctor()
extern void SleekRenderPostProcess__ctor_m0F4ABD1F2FB792A238BC58F55A68C3472FEF81F8 ();
// 0x0000002D System.Void SleekRender.SleekRenderSettings::.ctor()
extern void SleekRenderSettings__ctor_mB71818A23BF0930F3E701629961F83633A636486 ();
// 0x0000002E System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris_<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_mBDD727783F54367BFE2CED7C675B839450955B8C ();
// 0x0000002F System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris_<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_m6E7E396A3AE4E0720223F5D8DCB4E8A55ED7BF01 ();
// 0x00000030 System.Boolean UnityStandardAssets.Effects.ExplosionFireAndDebris_<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_m4846C74D5F4A4FF39164BCFD48198D976AA0F953 ();
// 0x00000031 System.Object UnityStandardAssets.Effects.ExplosionFireAndDebris_<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m134C857EC6C5EEB674DF4B22AE43D9CBD4D3596F ();
// 0x00000032 System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris_<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m886FA065B7C64EA3A746502C9F6BF104B9801F07 ();
// 0x00000033 System.Object UnityStandardAssets.Effects.ExplosionFireAndDebris_<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mEA0D5C2C7930B6E16648C869029DC54290E095E2 ();
// 0x00000034 System.Void SleekRender.SleekRenderPostProcess_Uniforms::.cctor()
extern void Uniforms__cctor_m7573D90B8FF2BCE1B1C8A02911B76BB8C140F972 ();
static Il2CppMethodPointer s_methodPointers[52] = 
{
	AfterburnerPhysicsForce_OnEnable_m31ABCDDA8E500224F25244C9D3FE44CBDD45D7CE,
	AfterburnerPhysicsForce_FixedUpdate_m90286B7CDABBED931E9D13E5D879E6EC41BF6E8A,
	AfterburnerPhysicsForce_OnDrawGizmosSelected_m8E75C894A4544C40EB1FD3DA8883E3358BAF8AA3,
	AfterburnerPhysicsForce__ctor_mE4F415134C355B0C418241BA9F4BE2DAB65BF1A0,
	ExplosionFireAndDebris_Start_m85654B2EA9348AE14094A4CDCA1ECD714E4CBC22,
	ExplosionFireAndDebris_AddFire_m781F3E400896C4F2E2B4A24FAEFE52D01ED26C0B,
	ExplosionFireAndDebris__ctor_m6E9B3DD674A9E1DF3B85ECF3E618F7F29B5EB97E,
	ExtinguishableParticleSystem_Start_m442E9165E311C7DC5AE322C5CF8B8D6A2325DC39,
	ExtinguishableParticleSystem_Extinguish_m2E9C8C61E7E4E1083A3059582EE9F68BB0D8EFA8,
	ExtinguishableParticleSystem__ctor_m66F7FA6921D145A9EF55397A60FCC4CA574DA068,
	FireLight_Start_mBC43BC2A9EDAD47A5D43C96C04E1AECC4405B81D,
	FireLight_Update_mE6D64BEF14D90624104BC092626B738B75D8D0F7,
	FireLight_Extinguish_m96CC75996F151C74E4D46556599FDF4F39AF6776,
	FireLight__ctor_mFAEAECD04029DD5E24A331FEE4B5C43FFD5B6FBE,
	Hose_Update_mF09247CA136AF92D5586DD9C95823C7E9A6AE939,
	Hose__ctor_mDB316C87F32CA6A88980009F13FCB35475103094,
	ParticleSystemMultiplier_Start_m9629829804A66FA8E1BA25039F0BCD4447D1785D,
	ParticleSystemMultiplier__ctor_mFA383D5A7F8469462B1E9A5EDB01811D52DC0CFC,
	SmokeParticles_Start_m1A4CBEB9EDF7F792FCF7F96D9435B6C0B0A94DD6,
	SmokeParticles__ctor_m6FF23CF3C3FFDCE57F75C6DA08F84101A4953F0C,
	WaterHoseParticles_Start_m6CFC5FEBF60CBD00CDE17E0A1EEC12163EC9A792,
	WaterHoseParticles_OnParticleCollision_m8E8399822017058F9AE64E3D9F7B47208553ED70,
	WaterHoseParticles__ctor_m9E63BF105FF8511932CBF7FAA1C664713744DCA3,
	SleekRenderPostProcess_OnEnable_m9BE5D8F5EC9B759E760480EA5A11AB9403A37534,
	SleekRenderPostProcess_OnDisable_m890AF19CE2A19B82FE057B9AF69803EA1171B332,
	SleekRenderPostProcess_OnRenderImage_m226B046B38C23DD3FBB020A757EE7161AF37004E,
	SleekRenderPostProcess_ApplyPostProcess_m7228BB4785430B68E38460258D93189559260835,
	SleekRenderPostProcess_Downsample_m939DB30198E9BB4BEFF3884A283D450C8704EE96,
	SleekRenderPostProcess_Bloom_m38FA596CFD5659B6F02915F17199F0DF0ADE16FC,
	SleekRenderPostProcess_Precompose_mE5C8A6B77C24F3B90CDFC17D60895195BDD20965,
	SleekRenderPostProcess_Compose_m76162AF19A38CE8168DB6ED4B2E89E56110D02BA,
	SleekRenderPostProcess_CreateResources_m88233905ED6473C25EA3C8A4B3757879C201D6DA,
	SleekRenderPostProcess_CreateTransientRenderTexture_m5896F414C30F00DE545EF638447324CC3D1DE67F,
	SleekRenderPostProcess_CreateMainRenderTexture_mA21F6ABA9A57B0BD2A2ACBCCEBD12CA09A16838D,
	SleekRenderPostProcess_ReleaseResources_mDFBCB6AF7DB534D25370ED70E8C21A4D8E85F165,
	SleekRenderPostProcess_DestroyImmediateIfNotNull_m0E4B29743210245443999A680294C1DDBF0140DC,
	SleekRenderPostProcess_Blit_mEDE1D6060C22F747323C93F8ABECC7C879413CBE,
	SleekRenderPostProcess_SetActiveRenderTextureAndClear_m01E22DC5E4C5579AA8732CA0F67E01F39E4AC89F,
	SleekRenderPostProcess_DrawFullscreenQuad_m77F12B59A976F77BFC5A8BF270AD5E5D30408DD0,
	SleekRenderPostProcess_CheckScreenSizeAndRecreateTexturesIfNeeded_m8FE59E4480A6A09382775B46448C5784B79029E4,
	SleekRenderPostProcess_GetCurrentAspect_mFA278408C602D5213CB17EC938C37D070544DC79,
	SleekRenderPostProcess_CreateDefaultSettingsIfNoneLinked_mFAFCBC9B80CB0CAE18DB94C102CD3D0FAEFA83A9,
	SleekRenderPostProcess_CreateScreenSpaceQuadMesh_m2D59ABE6B083BEF9A9DE056AF5725199DD9FCB0C,
	SleekRenderPostProcess__ctor_m0F4ABD1F2FB792A238BC58F55A68C3472FEF81F8,
	SleekRenderSettings__ctor_mB71818A23BF0930F3E701629961F83633A636486,
	U3CStartU3Ed__4__ctor_mBDD727783F54367BFE2CED7C675B839450955B8C,
	U3CStartU3Ed__4_System_IDisposable_Dispose_m6E7E396A3AE4E0720223F5D8DCB4E8A55ED7BF01,
	U3CStartU3Ed__4_MoveNext_m4846C74D5F4A4FF39164BCFD48198D976AA0F953,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m134C857EC6C5EEB674DF4B22AE43D9CBD4D3596F,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m886FA065B7C64EA3A746502C9F6BF104B9801F07,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mEA0D5C2C7930B6E16648C869029DC54290E095E2,
	Uniforms__cctor_m7573D90B8FF2BCE1B1C8A02911B76BB8C140F972,
};
static const int32_t s_InvokerIndices[52] = 
{
	13,
	13,
	13,
	13,
	14,
	1686,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	4,
	13,
	13,
	13,
	23,
	4,
	4,
	44,
	44,
	23,
	13,
	82,
	148,
	13,
	4,
	510,
	30,
	585,
	4,
	181,
	13,
	14,
	13,
	13,
	9,
	13,
	17,
	14,
	13,
	14,
	8,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpU2DfirstpassCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpU2DfirstpassCodeGenModule = 
{
	"Assembly-CSharp-firstpass.dll",
	52,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
