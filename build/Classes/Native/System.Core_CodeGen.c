﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 ();
// 0x00000002 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 ();
// 0x00000003 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000004 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000005 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000006 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000007 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000008 System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000009 System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x0000000A TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x0000000B System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x0000000C System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x0000000D System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x0000000E System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x0000000F System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000010 System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000011 System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000012 System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x00000013 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000014 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x00000015 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x00000016 System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x00000017 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000018 System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000019 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x0000001A System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x0000001B System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000001C System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000001D System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x0000001E System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x0000001F System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000020 System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x00000021 System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x00000022 System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x00000023 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x00000024 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000025 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
// 0x00000026 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000027 System.Void System.Collections.Generic.HashSet`1::CopyFrom(System.Collections.Generic.HashSet`1<T>)
// 0x00000028 System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x00000029 System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x0000002A System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x0000002B System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x0000002C System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x0000002D System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x0000002E System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x0000002F System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x00000030 System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000031 System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000032 System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000033 System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x00000034 System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x00000035 System.Void System.Collections.Generic.HashSet`1::UnionWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000036 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x00000037 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x00000038 System.Collections.Generic.IEqualityComparer`1<T> System.Collections.Generic.HashSet`1::get_Comparer()
// 0x00000039 System.Void System.Collections.Generic.HashSet`1::TrimExcess()
// 0x0000003A System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x0000003B System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x0000003C System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x0000003D System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x0000003E System.Void System.Collections.Generic.HashSet`1::AddValue(System.Int32,System.Int32,T)
// 0x0000003F System.Boolean System.Collections.Generic.HashSet`1::AreEqualityComparersEqual(System.Collections.Generic.HashSet`1<T>,System.Collections.Generic.HashSet`1<T>)
// 0x00000040 System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x00000041 System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x00000042 System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x00000043 System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x00000044 T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x00000045 System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x00000046 System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[70] = 
{
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[70] = 
{
	0,
	19,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[13] = 
{
	{ 0x02000004, { 24, 4 } },
	{ 0x02000005, { 28, 9 } },
	{ 0x02000006, { 37, 7 } },
	{ 0x02000007, { 44, 10 } },
	{ 0x02000008, { 54, 1 } },
	{ 0x02000009, { 55, 34 } },
	{ 0x0200000B, { 89, 2 } },
	{ 0x06000003, { 0, 10 } },
	{ 0x06000004, { 10, 5 } },
	{ 0x06000005, { 15, 3 } },
	{ 0x06000006, { 18, 1 } },
	{ 0x06000007, { 19, 3 } },
	{ 0x06000008, { 22, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[91] = 
{
	{ (Il2CppRGCTXDataType)2, 14281 },
	{ (Il2CppRGCTXDataType)3, 10386 },
	{ (Il2CppRGCTXDataType)2, 14282 },
	{ (Il2CppRGCTXDataType)2, 14283 },
	{ (Il2CppRGCTXDataType)3, 10387 },
	{ (Il2CppRGCTXDataType)2, 14284 },
	{ (Il2CppRGCTXDataType)2, 14285 },
	{ (Il2CppRGCTXDataType)3, 10388 },
	{ (Il2CppRGCTXDataType)2, 14286 },
	{ (Il2CppRGCTXDataType)3, 10389 },
	{ (Il2CppRGCTXDataType)2, 14287 },
	{ (Il2CppRGCTXDataType)3, 10390 },
	{ (Il2CppRGCTXDataType)3, 10391 },
	{ (Il2CppRGCTXDataType)2, 10793 },
	{ (Il2CppRGCTXDataType)3, 10392 },
	{ (Il2CppRGCTXDataType)2, 10795 },
	{ (Il2CppRGCTXDataType)2, 14288 },
	{ (Il2CppRGCTXDataType)3, 10393 },
	{ (Il2CppRGCTXDataType)2, 10798 },
	{ (Il2CppRGCTXDataType)2, 10800 },
	{ (Il2CppRGCTXDataType)2, 14289 },
	{ (Il2CppRGCTXDataType)3, 10394 },
	{ (Il2CppRGCTXDataType)2, 14290 },
	{ (Il2CppRGCTXDataType)2, 10803 },
	{ (Il2CppRGCTXDataType)3, 10395 },
	{ (Il2CppRGCTXDataType)3, 10396 },
	{ (Il2CppRGCTXDataType)2, 10807 },
	{ (Il2CppRGCTXDataType)3, 10397 },
	{ (Il2CppRGCTXDataType)3, 10398 },
	{ (Il2CppRGCTXDataType)2, 10816 },
	{ (Il2CppRGCTXDataType)2, 14291 },
	{ (Il2CppRGCTXDataType)3, 10399 },
	{ (Il2CppRGCTXDataType)3, 10400 },
	{ (Il2CppRGCTXDataType)2, 10818 },
	{ (Il2CppRGCTXDataType)2, 14203 },
	{ (Il2CppRGCTXDataType)3, 10401 },
	{ (Il2CppRGCTXDataType)3, 10402 },
	{ (Il2CppRGCTXDataType)3, 10403 },
	{ (Il2CppRGCTXDataType)2, 10825 },
	{ (Il2CppRGCTXDataType)2, 14292 },
	{ (Il2CppRGCTXDataType)3, 10404 },
	{ (Il2CppRGCTXDataType)3, 10405 },
	{ (Il2CppRGCTXDataType)3, 10031 },
	{ (Il2CppRGCTXDataType)3, 10406 },
	{ (Il2CppRGCTXDataType)3, 10407 },
	{ (Il2CppRGCTXDataType)2, 10834 },
	{ (Il2CppRGCTXDataType)2, 14293 },
	{ (Il2CppRGCTXDataType)3, 10408 },
	{ (Il2CppRGCTXDataType)3, 10409 },
	{ (Il2CppRGCTXDataType)3, 10410 },
	{ (Il2CppRGCTXDataType)3, 10411 },
	{ (Il2CppRGCTXDataType)3, 10412 },
	{ (Il2CppRGCTXDataType)3, 10037 },
	{ (Il2CppRGCTXDataType)3, 10413 },
	{ (Il2CppRGCTXDataType)3, 10414 },
	{ (Il2CppRGCTXDataType)3, 10415 },
	{ (Il2CppRGCTXDataType)2, 14294 },
	{ (Il2CppRGCTXDataType)3, 10416 },
	{ (Il2CppRGCTXDataType)3, 10417 },
	{ (Il2CppRGCTXDataType)2, 10851 },
	{ (Il2CppRGCTXDataType)3, 10418 },
	{ (Il2CppRGCTXDataType)2, 10851 },
	{ (Il2CppRGCTXDataType)3, 10419 },
	{ (Il2CppRGCTXDataType)2, 10868 },
	{ (Il2CppRGCTXDataType)3, 10420 },
	{ (Il2CppRGCTXDataType)3, 10421 },
	{ (Il2CppRGCTXDataType)3, 10422 },
	{ (Il2CppRGCTXDataType)2, 14295 },
	{ (Il2CppRGCTXDataType)3, 10423 },
	{ (Il2CppRGCTXDataType)3, 10424 },
	{ (Il2CppRGCTXDataType)3, 10425 },
	{ (Il2CppRGCTXDataType)2, 10848 },
	{ (Il2CppRGCTXDataType)3, 10426 },
	{ (Il2CppRGCTXDataType)3, 10427 },
	{ (Il2CppRGCTXDataType)2, 10853 },
	{ (Il2CppRGCTXDataType)3, 10428 },
	{ (Il2CppRGCTXDataType)1, 14296 },
	{ (Il2CppRGCTXDataType)2, 10852 },
	{ (Il2CppRGCTXDataType)3, 10429 },
	{ (Il2CppRGCTXDataType)1, 10852 },
	{ (Il2CppRGCTXDataType)1, 10848 },
	{ (Il2CppRGCTXDataType)2, 14295 },
	{ (Il2CppRGCTXDataType)2, 10852 },
	{ (Il2CppRGCTXDataType)2, 10850 },
	{ (Il2CppRGCTXDataType)2, 10854 },
	{ (Il2CppRGCTXDataType)3, 10430 },
	{ (Il2CppRGCTXDataType)3, 10431 },
	{ (Il2CppRGCTXDataType)3, 10432 },
	{ (Il2CppRGCTXDataType)2, 10849 },
	{ (Il2CppRGCTXDataType)3, 10433 },
	{ (Il2CppRGCTXDataType)2, 10864 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	70,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	13,
	s_rgctxIndices,
	91,
	s_rgctxValues,
	NULL,
};
