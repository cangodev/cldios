﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.AnimationClip
struct AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE;
// UnityEngine.Animator
struct Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A;
// UnityEngine.AudioClip
struct AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051;
// UnityEngine.AudioSource
struct AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C;
// UnityEngine.Behaviour
struct Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8;
// UnityEngine.Collider2D
struct Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379;
// UnityEngine.Component
struct Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F;
// UnityEngine.TextAsset
struct TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E;
// UnityEngine.TextMesh
struct TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// vrash
struct vrash_tF08459DC43F95DF43201EA6383AD9FCB6A2CEF75;
// vrash2
struct vrash2_t3A7B3C3FF7F1F1760684D8542856E35CD2F2F7A6;
// vrashkrug
struct vrashkrug_t2DABFFE7C1A58EE05150CA30272EE7D5A95F1B24;
// x2tip
struct x2tip_t8849FCBC472E6674196F256CC8051DE414072152;
// yes
struct yes_t36BDD09D45583CF07DAA0274CDCE0985E722F5E6;
// zabratmoney
struct zabratmoney_t7DDABFB3031D063938927397F1B078B66BA485CC;
// zaltip
struct zaltip_t9E4D625BB000CD131ACEE4ABCC45F34F66419F87;

IL2CPP_EXTERN_C RuntimeClass* Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* nesgor_tAFC06E7C3AD54556038BA9DE502122E4C3D43533_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* start_tAE134A10E53E820C269A6C9B80C302397F2612BC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* x2tip_t8849FCBC472E6674196F256CC8051DE414072152_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* zabratmoney_t7DDABFB3031D063938927397F1B078B66BA485CC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral2C117556D3F8F65E48D75C404BCFDAC4F9F4AD77;
IL2CPP_EXTERN_C String_t* _stringLiteral4345CB1FA27885A8FBFE7C0C830A592CC76A552B;
IL2CPP_EXTERN_C String_t* _stringLiteral446BB1BC637F6C370F33E67ADC71039B469F50AF;
IL2CPP_EXTERN_C String_t* _stringLiteral46659F20834DBA92057B5B6BC8830AA83C63408C;
IL2CPP_EXTERN_C String_t* _stringLiteral4AEB195CD69ED93520B9B4129636264E0CDC0153;
IL2CPP_EXTERN_C String_t* _stringLiteral75EBCB361C656225206BB0191DF63CB1D38CBBAB;
IL2CPP_EXTERN_C String_t* _stringLiteralEECABAE13529439E62DE1A0D81E07B14F9A348C9;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisAnimator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A_m9904EA7E80165F7771F8AB3967F417D7C2B09996_RuntimeMethod_var;
IL2CPP_EXTERN_C const uint32_t x2tip_FixedUpdate_mFFE5693DFAD7BF8EE35C6A8FCBBAB80B46CD29AD_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t x2tip_OnMouseDown_mDB3FBD8B1FC9A31ADBDDBA5FE4B45456BB3F980B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t x2tip_Start_mAB78F0E29058BDF05E96A1F0408977B3CBB6F992_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t yes_OnMouseDown_mBD3EAE21B5A9137CB7BE5D3A84A7425E9D927627_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t zabratmoney_OnMouseDown_m1220E6EE6FB81C86A773225E7C608BBF78D30EA6_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t zaltip_FixedUpdate_m83E52C147D3345C561AACF32B0A9EFA8F6E789BE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t zaltip_Naznach_m32DAA727AB6914925B335901C65BF7325FD7B0DC_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t zaltip_OnMouseDown_mBCD4C20E5E7F3FBC38CF0CB3D676AC4B8C837356_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t zaltip_ShowRewardedAd_mB5E223BE079E093762E376AEED55F8D5D71F30D8_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t zaltip_Start_mF003BE496B7C2E221441F6312AADB74C1AAE755D_MetadataUsageId;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

struct Il2CppArrayBounds;

// System.Array


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.SceneManagement.LoadSceneMode
struct  LoadSceneMode_t75F0B96794398942671B8315D2A9AC25C40A22D5 
{
public:
	// System.Int32 UnityEngine.SceneManagement.LoadSceneMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LoadSceneMode_t75F0B96794398942671B8315D2A9AC25C40A22D5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.GameObject
struct  GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.TextMesh
struct  TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Transform
struct  Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Animator
struct  Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};


// UnityEngine.AudioBehaviour
struct  AudioBehaviour_tC612EC4E17A648A5C568621F3FBF1DBD773C71C7  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};


// UnityEngine.Collider2D
struct  Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};


// UnityEngine.AudioSource
struct  AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C  : public AudioBehaviour_tC612EC4E17A648A5C568621F3FBF1DBD773C71C7
{
public:

public:
};


// generator
struct  generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.TextAsset generator::ez
	TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * ___ez_4;
	// UnityEngine.TextAsset generator::med
	TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * ___med_5;
	// UnityEngine.TextAsset generator::hard
	TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * ___hard_6;
	// UnityEngine.TextAsset generator::final
	TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * ___final_7;
	// UnityEngine.TextAsset generator::ezeng
	TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * ___ezeng_8;
	// UnityEngine.TextAsset generator::medeng
	TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * ___medeng_9;
	// System.Int32 generator::chislostrok
	int32_t ___chislostrok_10;
	// System.String generator::randomquest
	String_t* ___randomquest_11;
	// System.String[] generator::numberQuest
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___numberQuest_12;
	// System.String[] generator::engquests
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___engquests_13;
	// System.String[] generator::vars
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___vars_14;
	// System.String generator::questsam
	String_t* ___questsam_15;
	// UnityEngine.GameObject generator::polotno
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___polotno_16;
	// System.String generator::As
	String_t* ___As_17;
	// System.String generator::Bs
	String_t* ___Bs_18;
	// System.String generator::Cs
	String_t* ___Cs_19;
	// System.String generator::Ds
	String_t* ___Ds_20;
	// UnityEngine.TextMesh generator::question
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___question_21;
	// UnityEngine.TextMesh generator::A
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___A_22;
	// UnityEngine.TextMesh generator::B
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___B_23;
	// UnityEngine.TextMesh generator::C
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___C_24;
	// UnityEngine.TextMesh generator::D
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___D_25;
	// System.String generator::chosenstroka
	String_t* ___chosenstroka_27;
	// System.String generator::questionitogo
	String_t* ___questionitogo_28;
	// System.Boolean generator::tomove
	bool ___tomove_29;
	// System.Int32 generator::numberofstroka
	int32_t ___numberofstroka_31;
	// UnityEngine.GameObject generator::odinp
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___odinp_32;
	// UnityEngine.GameObject generator::shestc
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___shestc_33;
	// System.Single generator::counter
	float ___counter_34;
	// System.String generator::previous
	String_t* ___previous_36;
	// System.String generator::seichas
	String_t* ___seichas_37;
	// UnityEngine.AudioSource generator::fivewin
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___fivewin_38;
	// UnityEngine.GameObject generator::ten
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___ten_40;
	// System.Boolean generator::mmm
	bool ___mmm_41;
	// System.Int32 generator::kakoytru
	int32_t ___kakoytru_42;
	// System.Boolean generator::isPaused
	bool ___isPaused_43;
	// System.Boolean generator::ekbabay
	bool ___ekbabay_44;
	// UnityEngine.Animator generator::das
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___das_45;
	// System.Single generator::counter9
	float ___counter9_46;
	// System.Single generator::counter91
	float ___counter91_47;
	// System.Boolean generator::shit
	bool ___shit_48;
	// UnityEngine.Material generator::lgts
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___lgts_49;
	// UnityEngine.Material generator::lgts2
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___lgts2_50;
	// UnityEngine.Material generator::lgts3
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___lgts3_51;
	// UnityEngine.Material generator::lgts4
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___lgts4_52;
	// UnityEngine.Material generator::lgts5
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___lgts5_53;
	// System.Boolean generator::fuck
	bool ___fuck_54;
	// System.Int32 generator::ss
	int32_t ___ss_55;

public:
	inline static int32_t get_offset_of_ez_4() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___ez_4)); }
	inline TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * get_ez_4() const { return ___ez_4; }
	inline TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E ** get_address_of_ez_4() { return &___ez_4; }
	inline void set_ez_4(TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * value)
	{
		___ez_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ez_4), (void*)value);
	}

	inline static int32_t get_offset_of_med_5() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___med_5)); }
	inline TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * get_med_5() const { return ___med_5; }
	inline TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E ** get_address_of_med_5() { return &___med_5; }
	inline void set_med_5(TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * value)
	{
		___med_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___med_5), (void*)value);
	}

	inline static int32_t get_offset_of_hard_6() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___hard_6)); }
	inline TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * get_hard_6() const { return ___hard_6; }
	inline TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E ** get_address_of_hard_6() { return &___hard_6; }
	inline void set_hard_6(TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * value)
	{
		___hard_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hard_6), (void*)value);
	}

	inline static int32_t get_offset_of_final_7() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___final_7)); }
	inline TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * get_final_7() const { return ___final_7; }
	inline TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E ** get_address_of_final_7() { return &___final_7; }
	inline void set_final_7(TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * value)
	{
		___final_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___final_7), (void*)value);
	}

	inline static int32_t get_offset_of_ezeng_8() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___ezeng_8)); }
	inline TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * get_ezeng_8() const { return ___ezeng_8; }
	inline TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E ** get_address_of_ezeng_8() { return &___ezeng_8; }
	inline void set_ezeng_8(TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * value)
	{
		___ezeng_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ezeng_8), (void*)value);
	}

	inline static int32_t get_offset_of_medeng_9() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___medeng_9)); }
	inline TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * get_medeng_9() const { return ___medeng_9; }
	inline TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E ** get_address_of_medeng_9() { return &___medeng_9; }
	inline void set_medeng_9(TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * value)
	{
		___medeng_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___medeng_9), (void*)value);
	}

	inline static int32_t get_offset_of_chislostrok_10() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___chislostrok_10)); }
	inline int32_t get_chislostrok_10() const { return ___chislostrok_10; }
	inline int32_t* get_address_of_chislostrok_10() { return &___chislostrok_10; }
	inline void set_chislostrok_10(int32_t value)
	{
		___chislostrok_10 = value;
	}

	inline static int32_t get_offset_of_randomquest_11() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___randomquest_11)); }
	inline String_t* get_randomquest_11() const { return ___randomquest_11; }
	inline String_t** get_address_of_randomquest_11() { return &___randomquest_11; }
	inline void set_randomquest_11(String_t* value)
	{
		___randomquest_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___randomquest_11), (void*)value);
	}

	inline static int32_t get_offset_of_numberQuest_12() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___numberQuest_12)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_numberQuest_12() const { return ___numberQuest_12; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_numberQuest_12() { return &___numberQuest_12; }
	inline void set_numberQuest_12(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___numberQuest_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___numberQuest_12), (void*)value);
	}

	inline static int32_t get_offset_of_engquests_13() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___engquests_13)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_engquests_13() const { return ___engquests_13; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_engquests_13() { return &___engquests_13; }
	inline void set_engquests_13(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___engquests_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___engquests_13), (void*)value);
	}

	inline static int32_t get_offset_of_vars_14() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___vars_14)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_vars_14() const { return ___vars_14; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_vars_14() { return &___vars_14; }
	inline void set_vars_14(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___vars_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___vars_14), (void*)value);
	}

	inline static int32_t get_offset_of_questsam_15() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___questsam_15)); }
	inline String_t* get_questsam_15() const { return ___questsam_15; }
	inline String_t** get_address_of_questsam_15() { return &___questsam_15; }
	inline void set_questsam_15(String_t* value)
	{
		___questsam_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___questsam_15), (void*)value);
	}

	inline static int32_t get_offset_of_polotno_16() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___polotno_16)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_polotno_16() const { return ___polotno_16; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_polotno_16() { return &___polotno_16; }
	inline void set_polotno_16(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___polotno_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___polotno_16), (void*)value);
	}

	inline static int32_t get_offset_of_As_17() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___As_17)); }
	inline String_t* get_As_17() const { return ___As_17; }
	inline String_t** get_address_of_As_17() { return &___As_17; }
	inline void set_As_17(String_t* value)
	{
		___As_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___As_17), (void*)value);
	}

	inline static int32_t get_offset_of_Bs_18() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___Bs_18)); }
	inline String_t* get_Bs_18() const { return ___Bs_18; }
	inline String_t** get_address_of_Bs_18() { return &___Bs_18; }
	inline void set_Bs_18(String_t* value)
	{
		___Bs_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Bs_18), (void*)value);
	}

	inline static int32_t get_offset_of_Cs_19() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___Cs_19)); }
	inline String_t* get_Cs_19() const { return ___Cs_19; }
	inline String_t** get_address_of_Cs_19() { return &___Cs_19; }
	inline void set_Cs_19(String_t* value)
	{
		___Cs_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Cs_19), (void*)value);
	}

	inline static int32_t get_offset_of_Ds_20() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___Ds_20)); }
	inline String_t* get_Ds_20() const { return ___Ds_20; }
	inline String_t** get_address_of_Ds_20() { return &___Ds_20; }
	inline void set_Ds_20(String_t* value)
	{
		___Ds_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Ds_20), (void*)value);
	}

	inline static int32_t get_offset_of_question_21() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___question_21)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_question_21() const { return ___question_21; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_question_21() { return &___question_21; }
	inline void set_question_21(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___question_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___question_21), (void*)value);
	}

	inline static int32_t get_offset_of_A_22() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___A_22)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_A_22() const { return ___A_22; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_A_22() { return &___A_22; }
	inline void set_A_22(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___A_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___A_22), (void*)value);
	}

	inline static int32_t get_offset_of_B_23() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___B_23)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_B_23() const { return ___B_23; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_B_23() { return &___B_23; }
	inline void set_B_23(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___B_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___B_23), (void*)value);
	}

	inline static int32_t get_offset_of_C_24() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___C_24)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_C_24() const { return ___C_24; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_C_24() { return &___C_24; }
	inline void set_C_24(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___C_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___C_24), (void*)value);
	}

	inline static int32_t get_offset_of_D_25() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___D_25)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_D_25() const { return ___D_25; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_D_25() { return &___D_25; }
	inline void set_D_25(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___D_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___D_25), (void*)value);
	}

	inline static int32_t get_offset_of_chosenstroka_27() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___chosenstroka_27)); }
	inline String_t* get_chosenstroka_27() const { return ___chosenstroka_27; }
	inline String_t** get_address_of_chosenstroka_27() { return &___chosenstroka_27; }
	inline void set_chosenstroka_27(String_t* value)
	{
		___chosenstroka_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___chosenstroka_27), (void*)value);
	}

	inline static int32_t get_offset_of_questionitogo_28() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___questionitogo_28)); }
	inline String_t* get_questionitogo_28() const { return ___questionitogo_28; }
	inline String_t** get_address_of_questionitogo_28() { return &___questionitogo_28; }
	inline void set_questionitogo_28(String_t* value)
	{
		___questionitogo_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___questionitogo_28), (void*)value);
	}

	inline static int32_t get_offset_of_tomove_29() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___tomove_29)); }
	inline bool get_tomove_29() const { return ___tomove_29; }
	inline bool* get_address_of_tomove_29() { return &___tomove_29; }
	inline void set_tomove_29(bool value)
	{
		___tomove_29 = value;
	}

	inline static int32_t get_offset_of_numberofstroka_31() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___numberofstroka_31)); }
	inline int32_t get_numberofstroka_31() const { return ___numberofstroka_31; }
	inline int32_t* get_address_of_numberofstroka_31() { return &___numberofstroka_31; }
	inline void set_numberofstroka_31(int32_t value)
	{
		___numberofstroka_31 = value;
	}

	inline static int32_t get_offset_of_odinp_32() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___odinp_32)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_odinp_32() const { return ___odinp_32; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_odinp_32() { return &___odinp_32; }
	inline void set_odinp_32(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___odinp_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___odinp_32), (void*)value);
	}

	inline static int32_t get_offset_of_shestc_33() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___shestc_33)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_shestc_33() const { return ___shestc_33; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_shestc_33() { return &___shestc_33; }
	inline void set_shestc_33(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___shestc_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___shestc_33), (void*)value);
	}

	inline static int32_t get_offset_of_counter_34() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___counter_34)); }
	inline float get_counter_34() const { return ___counter_34; }
	inline float* get_address_of_counter_34() { return &___counter_34; }
	inline void set_counter_34(float value)
	{
		___counter_34 = value;
	}

	inline static int32_t get_offset_of_previous_36() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___previous_36)); }
	inline String_t* get_previous_36() const { return ___previous_36; }
	inline String_t** get_address_of_previous_36() { return &___previous_36; }
	inline void set_previous_36(String_t* value)
	{
		___previous_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___previous_36), (void*)value);
	}

	inline static int32_t get_offset_of_seichas_37() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___seichas_37)); }
	inline String_t* get_seichas_37() const { return ___seichas_37; }
	inline String_t** get_address_of_seichas_37() { return &___seichas_37; }
	inline void set_seichas_37(String_t* value)
	{
		___seichas_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___seichas_37), (void*)value);
	}

	inline static int32_t get_offset_of_fivewin_38() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___fivewin_38)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_fivewin_38() const { return ___fivewin_38; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_fivewin_38() { return &___fivewin_38; }
	inline void set_fivewin_38(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___fivewin_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fivewin_38), (void*)value);
	}

	inline static int32_t get_offset_of_ten_40() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___ten_40)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_ten_40() const { return ___ten_40; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_ten_40() { return &___ten_40; }
	inline void set_ten_40(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___ten_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ten_40), (void*)value);
	}

	inline static int32_t get_offset_of_mmm_41() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___mmm_41)); }
	inline bool get_mmm_41() const { return ___mmm_41; }
	inline bool* get_address_of_mmm_41() { return &___mmm_41; }
	inline void set_mmm_41(bool value)
	{
		___mmm_41 = value;
	}

	inline static int32_t get_offset_of_kakoytru_42() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___kakoytru_42)); }
	inline int32_t get_kakoytru_42() const { return ___kakoytru_42; }
	inline int32_t* get_address_of_kakoytru_42() { return &___kakoytru_42; }
	inline void set_kakoytru_42(int32_t value)
	{
		___kakoytru_42 = value;
	}

	inline static int32_t get_offset_of_isPaused_43() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___isPaused_43)); }
	inline bool get_isPaused_43() const { return ___isPaused_43; }
	inline bool* get_address_of_isPaused_43() { return &___isPaused_43; }
	inline void set_isPaused_43(bool value)
	{
		___isPaused_43 = value;
	}

	inline static int32_t get_offset_of_ekbabay_44() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___ekbabay_44)); }
	inline bool get_ekbabay_44() const { return ___ekbabay_44; }
	inline bool* get_address_of_ekbabay_44() { return &___ekbabay_44; }
	inline void set_ekbabay_44(bool value)
	{
		___ekbabay_44 = value;
	}

	inline static int32_t get_offset_of_das_45() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___das_45)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_das_45() const { return ___das_45; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_das_45() { return &___das_45; }
	inline void set_das_45(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___das_45 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___das_45), (void*)value);
	}

	inline static int32_t get_offset_of_counter9_46() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___counter9_46)); }
	inline float get_counter9_46() const { return ___counter9_46; }
	inline float* get_address_of_counter9_46() { return &___counter9_46; }
	inline void set_counter9_46(float value)
	{
		___counter9_46 = value;
	}

	inline static int32_t get_offset_of_counter91_47() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___counter91_47)); }
	inline float get_counter91_47() const { return ___counter91_47; }
	inline float* get_address_of_counter91_47() { return &___counter91_47; }
	inline void set_counter91_47(float value)
	{
		___counter91_47 = value;
	}

	inline static int32_t get_offset_of_shit_48() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___shit_48)); }
	inline bool get_shit_48() const { return ___shit_48; }
	inline bool* get_address_of_shit_48() { return &___shit_48; }
	inline void set_shit_48(bool value)
	{
		___shit_48 = value;
	}

	inline static int32_t get_offset_of_lgts_49() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___lgts_49)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_lgts_49() const { return ___lgts_49; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_lgts_49() { return &___lgts_49; }
	inline void set_lgts_49(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___lgts_49 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lgts_49), (void*)value);
	}

	inline static int32_t get_offset_of_lgts2_50() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___lgts2_50)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_lgts2_50() const { return ___lgts2_50; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_lgts2_50() { return &___lgts2_50; }
	inline void set_lgts2_50(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___lgts2_50 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lgts2_50), (void*)value);
	}

	inline static int32_t get_offset_of_lgts3_51() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___lgts3_51)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_lgts3_51() const { return ___lgts3_51; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_lgts3_51() { return &___lgts3_51; }
	inline void set_lgts3_51(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___lgts3_51 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lgts3_51), (void*)value);
	}

	inline static int32_t get_offset_of_lgts4_52() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___lgts4_52)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_lgts4_52() const { return ___lgts4_52; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_lgts4_52() { return &___lgts4_52; }
	inline void set_lgts4_52(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___lgts4_52 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lgts4_52), (void*)value);
	}

	inline static int32_t get_offset_of_lgts5_53() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___lgts5_53)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_lgts5_53() const { return ___lgts5_53; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_lgts5_53() { return &___lgts5_53; }
	inline void set_lgts5_53(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___lgts5_53 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lgts5_53), (void*)value);
	}

	inline static int32_t get_offset_of_fuck_54() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___fuck_54)); }
	inline bool get_fuck_54() const { return ___fuck_54; }
	inline bool* get_address_of_fuck_54() { return &___fuck_54; }
	inline void set_fuck_54(bool value)
	{
		___fuck_54 = value;
	}

	inline static int32_t get_offset_of_ss_55() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB, ___ss_55)); }
	inline int32_t get_ss_55() const { return ___ss_55; }
	inline int32_t* get_address_of_ss_55() { return &___ss_55; }
	inline void set_ss_55(int32_t value)
	{
		___ss_55 = value;
	}
};

struct generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_StaticFields
{
public:
	// System.Int32 generator::questpoporyad
	int32_t ___questpoporyad_26;
	// System.Int32 generator::whichtrue
	int32_t ___whichtrue_30;
	// System.Single generator::kakoycount
	float ___kakoycount_35;
	// System.Boolean generator::podskazused
	bool ___podskazused_39;

public:
	inline static int32_t get_offset_of_questpoporyad_26() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_StaticFields, ___questpoporyad_26)); }
	inline int32_t get_questpoporyad_26() const { return ___questpoporyad_26; }
	inline int32_t* get_address_of_questpoporyad_26() { return &___questpoporyad_26; }
	inline void set_questpoporyad_26(int32_t value)
	{
		___questpoporyad_26 = value;
	}

	inline static int32_t get_offset_of_whichtrue_30() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_StaticFields, ___whichtrue_30)); }
	inline int32_t get_whichtrue_30() const { return ___whichtrue_30; }
	inline int32_t* get_address_of_whichtrue_30() { return &___whichtrue_30; }
	inline void set_whichtrue_30(int32_t value)
	{
		___whichtrue_30 = value;
	}

	inline static int32_t get_offset_of_kakoycount_35() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_StaticFields, ___kakoycount_35)); }
	inline float get_kakoycount_35() const { return ___kakoycount_35; }
	inline float* get_address_of_kakoycount_35() { return &___kakoycount_35; }
	inline void set_kakoycount_35(float value)
	{
		___kakoycount_35 = value;
	}

	inline static int32_t get_offset_of_podskazused_39() { return static_cast<int32_t>(offsetof(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_StaticFields, ___podskazused_39)); }
	inline bool get_podskazused_39() const { return ___podskazused_39; }
	inline bool* get_address_of_podskazused_39() { return &___podskazused_39; }
	inline void set_podskazused_39(bool value)
	{
		___podskazused_39 = value;
	}
};


// nesgor
struct  nesgor_tAFC06E7C3AD54556038BA9DE502122E4C3D43533  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject nesgor::strelka
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___strelka_4;

public:
	inline static int32_t get_offset_of_strelka_4() { return static_cast<int32_t>(offsetof(nesgor_tAFC06E7C3AD54556038BA9DE502122E4C3D43533, ___strelka_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_strelka_4() const { return ___strelka_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_strelka_4() { return &___strelka_4; }
	inline void set_strelka_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___strelka_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___strelka_4), (void*)value);
	}
};

struct nesgor_tAFC06E7C3AD54556038BA9DE502122E4C3D43533_StaticFields
{
public:
	// System.Int32 nesgor::nesgorkakaya
	int32_t ___nesgorkakaya_5;
	// System.Boolean nesgor::classicda
	bool ___classicda_6;

public:
	inline static int32_t get_offset_of_nesgorkakaya_5() { return static_cast<int32_t>(offsetof(nesgor_tAFC06E7C3AD54556038BA9DE502122E4C3D43533_StaticFields, ___nesgorkakaya_5)); }
	inline int32_t get_nesgorkakaya_5() const { return ___nesgorkakaya_5; }
	inline int32_t* get_address_of_nesgorkakaya_5() { return &___nesgorkakaya_5; }
	inline void set_nesgorkakaya_5(int32_t value)
	{
		___nesgorkakaya_5 = value;
	}

	inline static int32_t get_offset_of_classicda_6() { return static_cast<int32_t>(offsetof(nesgor_tAFC06E7C3AD54556038BA9DE502122E4C3D43533_StaticFields, ___classicda_6)); }
	inline bool get_classicda_6() const { return ___classicda_6; }
	inline bool* get_address_of_classicda_6() { return &___classicda_6; }
	inline void set_classicda_6(bool value)
	{
		___classicda_6 = value;
	}
};


// onclick
struct  onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.AudioSource onclick::mixer
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___mixer_4;
	// UnityEngine.AudioClip onclick::submit
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___submit_5;
	// UnityEngine.AudioClip onclick::onefivewrong
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___onefivewrong_6;
	// UnityEngine.AudioClip onclick::onefivenice
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___onefivenice_7;
	// UnityEngine.AudioClip onclick::sixchnice
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___sixchnice_8;
	// UnityEngine.AudioClip onclick::sixchwrong
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___sixchwrong_9;
	// UnityEngine.AudioClip onclick::elevenchnice
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___elevenchnice_10;
	// UnityEngine.AudioClip onclick::elevenchwrong
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___elevenchwrong_11;
	// UnityEngine.GameObject onclick::chosenA
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___chosenA_12;
	// UnityEngine.GameObject onclick::chosenB
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___chosenB_13;
	// UnityEngine.GameObject onclick::chosenC
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___chosenC_14;
	// UnityEngine.GameObject onclick::chosenD
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___chosenD_15;
	// UnityEngine.GameObject onclick::chosen
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___chosen_16;
	// UnityEngine.GameObject onclick::asks
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___asks_17;
	// UnityEngine.AnimationClip onclick::fast
	AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE * ___fast_18;
	// System.Boolean onclick::rightorwhat
	bool ___rightorwhat_19;
	// UnityEngine.GameObject onclick::shortsht
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___shortsht_20;
	// UnityEngine.TextMesh onclick::tottext
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___tottext_21;
	// UnityEngine.GameObject onclick::chosenactually
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___chosenactually_22;
	// UnityEngine.Material onclick::one
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___one_23;
	// UnityEngine.Material onclick::two
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___two_24;
	// UnityEngine.SpriteRenderer onclick::three
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ___three_25;
	// System.Boolean onclick::otv
	bool ___otv_26;
	// System.Single onclick::counter2
	float ___counter2_28;
	// System.Boolean onclick::lose
	bool ___lose_30;
	// UnityEngine.GameObject onclick::tride
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___tride_32;
	// UnityEngine.AudioSource onclick::odnp
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___odnp_33;
	// UnityEngine.AudioSource onclick::sixblin
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___sixblin_34;
	// UnityEngine.AudioSource onclick::tenblin
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___tenblin_35;
	// System.Boolean onclick::muted
	bool ___muted_36;
	// UnityEngine.Animator onclick::cam
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___cam_37;
	// UnityEngine.GameObject onclick::tofind
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___tofind_39;
	// UnityEngine.GameObject onclick::fuck
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___fuck_40;
	// UnityEngine.GameObject onclick::suck
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___suck_41;
	// UnityEngine.Material onclick::lgts
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___lgts_42;
	// UnityEngine.Material onclick::lgts2
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___lgts2_43;
	// UnityEngine.Material onclick::lgts3
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___lgts3_44;
	// UnityEngine.Material onclick::lgts4
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___lgts4_45;
	// System.Boolean onclick::holyfuck
	bool ___holyfuck_46;
	// UnityEngine.Material onclick::lgts5
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___lgts5_47;
	// System.Single onclick::counter9
	float ___counter9_48;
	// UnityEngine.GameObject onclick::winms
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___winms_49;
	// System.Boolean onclick::bobo
	bool ___bobo_50;
	// System.Single onclick::tillend
	float ___tillend_51;

public:
	inline static int32_t get_offset_of_mixer_4() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D, ___mixer_4)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_mixer_4() const { return ___mixer_4; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_mixer_4() { return &___mixer_4; }
	inline void set_mixer_4(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___mixer_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mixer_4), (void*)value);
	}

	inline static int32_t get_offset_of_submit_5() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D, ___submit_5)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_submit_5() const { return ___submit_5; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_submit_5() { return &___submit_5; }
	inline void set_submit_5(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___submit_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___submit_5), (void*)value);
	}

	inline static int32_t get_offset_of_onefivewrong_6() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D, ___onefivewrong_6)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_onefivewrong_6() const { return ___onefivewrong_6; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_onefivewrong_6() { return &___onefivewrong_6; }
	inline void set_onefivewrong_6(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___onefivewrong_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onefivewrong_6), (void*)value);
	}

	inline static int32_t get_offset_of_onefivenice_7() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D, ___onefivenice_7)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_onefivenice_7() const { return ___onefivenice_7; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_onefivenice_7() { return &___onefivenice_7; }
	inline void set_onefivenice_7(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___onefivenice_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onefivenice_7), (void*)value);
	}

	inline static int32_t get_offset_of_sixchnice_8() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D, ___sixchnice_8)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_sixchnice_8() const { return ___sixchnice_8; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_sixchnice_8() { return &___sixchnice_8; }
	inline void set_sixchnice_8(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___sixchnice_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sixchnice_8), (void*)value);
	}

	inline static int32_t get_offset_of_sixchwrong_9() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D, ___sixchwrong_9)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_sixchwrong_9() const { return ___sixchwrong_9; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_sixchwrong_9() { return &___sixchwrong_9; }
	inline void set_sixchwrong_9(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___sixchwrong_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sixchwrong_9), (void*)value);
	}

	inline static int32_t get_offset_of_elevenchnice_10() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D, ___elevenchnice_10)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_elevenchnice_10() const { return ___elevenchnice_10; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_elevenchnice_10() { return &___elevenchnice_10; }
	inline void set_elevenchnice_10(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___elevenchnice_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___elevenchnice_10), (void*)value);
	}

	inline static int32_t get_offset_of_elevenchwrong_11() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D, ___elevenchwrong_11)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_elevenchwrong_11() const { return ___elevenchwrong_11; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_elevenchwrong_11() { return &___elevenchwrong_11; }
	inline void set_elevenchwrong_11(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___elevenchwrong_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___elevenchwrong_11), (void*)value);
	}

	inline static int32_t get_offset_of_chosenA_12() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D, ___chosenA_12)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_chosenA_12() const { return ___chosenA_12; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_chosenA_12() { return &___chosenA_12; }
	inline void set_chosenA_12(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___chosenA_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___chosenA_12), (void*)value);
	}

	inline static int32_t get_offset_of_chosenB_13() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D, ___chosenB_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_chosenB_13() const { return ___chosenB_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_chosenB_13() { return &___chosenB_13; }
	inline void set_chosenB_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___chosenB_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___chosenB_13), (void*)value);
	}

	inline static int32_t get_offset_of_chosenC_14() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D, ___chosenC_14)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_chosenC_14() const { return ___chosenC_14; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_chosenC_14() { return &___chosenC_14; }
	inline void set_chosenC_14(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___chosenC_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___chosenC_14), (void*)value);
	}

	inline static int32_t get_offset_of_chosenD_15() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D, ___chosenD_15)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_chosenD_15() const { return ___chosenD_15; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_chosenD_15() { return &___chosenD_15; }
	inline void set_chosenD_15(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___chosenD_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___chosenD_15), (void*)value);
	}

	inline static int32_t get_offset_of_chosen_16() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D, ___chosen_16)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_chosen_16() const { return ___chosen_16; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_chosen_16() { return &___chosen_16; }
	inline void set_chosen_16(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___chosen_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___chosen_16), (void*)value);
	}

	inline static int32_t get_offset_of_asks_17() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D, ___asks_17)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_asks_17() const { return ___asks_17; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_asks_17() { return &___asks_17; }
	inline void set_asks_17(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___asks_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___asks_17), (void*)value);
	}

	inline static int32_t get_offset_of_fast_18() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D, ___fast_18)); }
	inline AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE * get_fast_18() const { return ___fast_18; }
	inline AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE ** get_address_of_fast_18() { return &___fast_18; }
	inline void set_fast_18(AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE * value)
	{
		___fast_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fast_18), (void*)value);
	}

	inline static int32_t get_offset_of_rightorwhat_19() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D, ___rightorwhat_19)); }
	inline bool get_rightorwhat_19() const { return ___rightorwhat_19; }
	inline bool* get_address_of_rightorwhat_19() { return &___rightorwhat_19; }
	inline void set_rightorwhat_19(bool value)
	{
		___rightorwhat_19 = value;
	}

	inline static int32_t get_offset_of_shortsht_20() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D, ___shortsht_20)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_shortsht_20() const { return ___shortsht_20; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_shortsht_20() { return &___shortsht_20; }
	inline void set_shortsht_20(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___shortsht_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___shortsht_20), (void*)value);
	}

	inline static int32_t get_offset_of_tottext_21() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D, ___tottext_21)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_tottext_21() const { return ___tottext_21; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_tottext_21() { return &___tottext_21; }
	inline void set_tottext_21(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___tottext_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tottext_21), (void*)value);
	}

	inline static int32_t get_offset_of_chosenactually_22() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D, ___chosenactually_22)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_chosenactually_22() const { return ___chosenactually_22; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_chosenactually_22() { return &___chosenactually_22; }
	inline void set_chosenactually_22(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___chosenactually_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___chosenactually_22), (void*)value);
	}

	inline static int32_t get_offset_of_one_23() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D, ___one_23)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_one_23() const { return ___one_23; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_one_23() { return &___one_23; }
	inline void set_one_23(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___one_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___one_23), (void*)value);
	}

	inline static int32_t get_offset_of_two_24() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D, ___two_24)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_two_24() const { return ___two_24; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_two_24() { return &___two_24; }
	inline void set_two_24(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___two_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___two_24), (void*)value);
	}

	inline static int32_t get_offset_of_three_25() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D, ___three_25)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get_three_25() const { return ___three_25; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of_three_25() { return &___three_25; }
	inline void set_three_25(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		___three_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___three_25), (void*)value);
	}

	inline static int32_t get_offset_of_otv_26() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D, ___otv_26)); }
	inline bool get_otv_26() const { return ___otv_26; }
	inline bool* get_address_of_otv_26() { return &___otv_26; }
	inline void set_otv_26(bool value)
	{
		___otv_26 = value;
	}

	inline static int32_t get_offset_of_counter2_28() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D, ___counter2_28)); }
	inline float get_counter2_28() const { return ___counter2_28; }
	inline float* get_address_of_counter2_28() { return &___counter2_28; }
	inline void set_counter2_28(float value)
	{
		___counter2_28 = value;
	}

	inline static int32_t get_offset_of_lose_30() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D, ___lose_30)); }
	inline bool get_lose_30() const { return ___lose_30; }
	inline bool* get_address_of_lose_30() { return &___lose_30; }
	inline void set_lose_30(bool value)
	{
		___lose_30 = value;
	}

	inline static int32_t get_offset_of_tride_32() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D, ___tride_32)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_tride_32() const { return ___tride_32; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_tride_32() { return &___tride_32; }
	inline void set_tride_32(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___tride_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tride_32), (void*)value);
	}

	inline static int32_t get_offset_of_odnp_33() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D, ___odnp_33)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_odnp_33() const { return ___odnp_33; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_odnp_33() { return &___odnp_33; }
	inline void set_odnp_33(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___odnp_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___odnp_33), (void*)value);
	}

	inline static int32_t get_offset_of_sixblin_34() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D, ___sixblin_34)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_sixblin_34() const { return ___sixblin_34; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_sixblin_34() { return &___sixblin_34; }
	inline void set_sixblin_34(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___sixblin_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sixblin_34), (void*)value);
	}

	inline static int32_t get_offset_of_tenblin_35() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D, ___tenblin_35)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_tenblin_35() const { return ___tenblin_35; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_tenblin_35() { return &___tenblin_35; }
	inline void set_tenblin_35(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___tenblin_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tenblin_35), (void*)value);
	}

	inline static int32_t get_offset_of_muted_36() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D, ___muted_36)); }
	inline bool get_muted_36() const { return ___muted_36; }
	inline bool* get_address_of_muted_36() { return &___muted_36; }
	inline void set_muted_36(bool value)
	{
		___muted_36 = value;
	}

	inline static int32_t get_offset_of_cam_37() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D, ___cam_37)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_cam_37() const { return ___cam_37; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_cam_37() { return &___cam_37; }
	inline void set_cam_37(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___cam_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cam_37), (void*)value);
	}

	inline static int32_t get_offset_of_tofind_39() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D, ___tofind_39)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_tofind_39() const { return ___tofind_39; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_tofind_39() { return &___tofind_39; }
	inline void set_tofind_39(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___tofind_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tofind_39), (void*)value);
	}

	inline static int32_t get_offset_of_fuck_40() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D, ___fuck_40)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_fuck_40() const { return ___fuck_40; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_fuck_40() { return &___fuck_40; }
	inline void set_fuck_40(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___fuck_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fuck_40), (void*)value);
	}

	inline static int32_t get_offset_of_suck_41() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D, ___suck_41)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_suck_41() const { return ___suck_41; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_suck_41() { return &___suck_41; }
	inline void set_suck_41(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___suck_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___suck_41), (void*)value);
	}

	inline static int32_t get_offset_of_lgts_42() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D, ___lgts_42)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_lgts_42() const { return ___lgts_42; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_lgts_42() { return &___lgts_42; }
	inline void set_lgts_42(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___lgts_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lgts_42), (void*)value);
	}

	inline static int32_t get_offset_of_lgts2_43() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D, ___lgts2_43)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_lgts2_43() const { return ___lgts2_43; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_lgts2_43() { return &___lgts2_43; }
	inline void set_lgts2_43(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___lgts2_43 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lgts2_43), (void*)value);
	}

	inline static int32_t get_offset_of_lgts3_44() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D, ___lgts3_44)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_lgts3_44() const { return ___lgts3_44; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_lgts3_44() { return &___lgts3_44; }
	inline void set_lgts3_44(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___lgts3_44 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lgts3_44), (void*)value);
	}

	inline static int32_t get_offset_of_lgts4_45() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D, ___lgts4_45)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_lgts4_45() const { return ___lgts4_45; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_lgts4_45() { return &___lgts4_45; }
	inline void set_lgts4_45(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___lgts4_45 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lgts4_45), (void*)value);
	}

	inline static int32_t get_offset_of_holyfuck_46() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D, ___holyfuck_46)); }
	inline bool get_holyfuck_46() const { return ___holyfuck_46; }
	inline bool* get_address_of_holyfuck_46() { return &___holyfuck_46; }
	inline void set_holyfuck_46(bool value)
	{
		___holyfuck_46 = value;
	}

	inline static int32_t get_offset_of_lgts5_47() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D, ___lgts5_47)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_lgts5_47() const { return ___lgts5_47; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_lgts5_47() { return &___lgts5_47; }
	inline void set_lgts5_47(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___lgts5_47 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lgts5_47), (void*)value);
	}

	inline static int32_t get_offset_of_counter9_48() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D, ___counter9_48)); }
	inline float get_counter9_48() const { return ___counter9_48; }
	inline float* get_address_of_counter9_48() { return &___counter9_48; }
	inline void set_counter9_48(float value)
	{
		___counter9_48 = value;
	}

	inline static int32_t get_offset_of_winms_49() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D, ___winms_49)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_winms_49() const { return ___winms_49; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_winms_49() { return &___winms_49; }
	inline void set_winms_49(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___winms_49 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___winms_49), (void*)value);
	}

	inline static int32_t get_offset_of_bobo_50() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D, ___bobo_50)); }
	inline bool get_bobo_50() const { return ___bobo_50; }
	inline bool* get_address_of_bobo_50() { return &___bobo_50; }
	inline void set_bobo_50(bool value)
	{
		___bobo_50 = value;
	}

	inline static int32_t get_offset_of_tillend_51() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D, ___tillend_51)); }
	inline float get_tillend_51() const { return ___tillend_51; }
	inline float* get_address_of_tillend_51() { return &___tillend_51; }
	inline void set_tillend_51(float value)
	{
		___tillend_51 = value;
	}
};

struct onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D_StaticFields
{
public:
	// System.Boolean onclick::already
	bool ___already_27;
	// System.Boolean onclick::dlya50
	bool ___dlya50_29;
	// System.Boolean onclick::x2used
	bool ___x2used_31;
	// System.Boolean onclick::fff
	bool ___fff_38;
	// System.Single onclick::raznic
	float ___raznic_52;
	// System.Boolean onclick::shitat
	bool ___shitat_53;
	// System.Boolean onclick::shitat2
	bool ___shitat2_54;

public:
	inline static int32_t get_offset_of_already_27() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D_StaticFields, ___already_27)); }
	inline bool get_already_27() const { return ___already_27; }
	inline bool* get_address_of_already_27() { return &___already_27; }
	inline void set_already_27(bool value)
	{
		___already_27 = value;
	}

	inline static int32_t get_offset_of_dlya50_29() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D_StaticFields, ___dlya50_29)); }
	inline bool get_dlya50_29() const { return ___dlya50_29; }
	inline bool* get_address_of_dlya50_29() { return &___dlya50_29; }
	inline void set_dlya50_29(bool value)
	{
		___dlya50_29 = value;
	}

	inline static int32_t get_offset_of_x2used_31() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D_StaticFields, ___x2used_31)); }
	inline bool get_x2used_31() const { return ___x2used_31; }
	inline bool* get_address_of_x2used_31() { return &___x2used_31; }
	inline void set_x2used_31(bool value)
	{
		___x2used_31 = value;
	}

	inline static int32_t get_offset_of_fff_38() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D_StaticFields, ___fff_38)); }
	inline bool get_fff_38() const { return ___fff_38; }
	inline bool* get_address_of_fff_38() { return &___fff_38; }
	inline void set_fff_38(bool value)
	{
		___fff_38 = value;
	}

	inline static int32_t get_offset_of_raznic_52() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D_StaticFields, ___raznic_52)); }
	inline float get_raznic_52() const { return ___raznic_52; }
	inline float* get_address_of_raznic_52() { return &___raznic_52; }
	inline void set_raznic_52(float value)
	{
		___raznic_52 = value;
	}

	inline static int32_t get_offset_of_shitat_53() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D_StaticFields, ___shitat_53)); }
	inline bool get_shitat_53() const { return ___shitat_53; }
	inline bool* get_address_of_shitat_53() { return &___shitat_53; }
	inline void set_shitat_53(bool value)
	{
		___shitat_53 = value;
	}

	inline static int32_t get_offset_of_shitat2_54() { return static_cast<int32_t>(offsetof(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D_StaticFields, ___shitat2_54)); }
	inline bool get_shitat2_54() const { return ___shitat2_54; }
	inline bool* get_address_of_shitat2_54() { return &___shitat2_54; }
	inline void set_shitat2_54(bool value)
	{
		___shitat2_54 = value;
	}
};


// score
struct  score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_StaticFields
{
public:
	// System.Int32 score::schet
	int32_t ___schet_4;

public:
	inline static int32_t get_offset_of_schet_4() { return static_cast<int32_t>(offsetof(score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_StaticFields, ___schet_4)); }
	inline int32_t get_schet_4() const { return ___schet_4; }
	inline int32_t* get_address_of_schet_4() { return &___schet_4; }
	inline void set_schet_4(int32_t value)
	{
		___schet_4 = value;
	}
};


// start
struct  start_tAE134A10E53E820C269A6C9B80C302397F2612BC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject start::fon1
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___fon1_4;
	// UnityEngine.GameObject start::fon2
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___fon2_5;
	// UnityEngine.AudioSource start::ft
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___ft_6;
	// UnityEngine.AudioSource start::intriga
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___intriga_7;
	// UnityEngine.GameObject start::asks
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___asks_8;
	// UnityEngine.Vector3 start::velocity
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___velocity_9;
	// UnityEngine.GameObject start::fonodin
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___fonodin_10;
	// UnityEngine.Animator start::goaway
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___goaway_11;
	// UnityEngine.GameObject start::toedit
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___toedit_12;
	// UnityEngine.Animator start::introcamera
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___introcamera_13;
	// UnityEngine.GameObject start::highcamera
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___highcamera_14;
	// System.Single start::cnt2
	float ___cnt2_15;
	// System.Boolean start::aaaaa
	bool ___aaaaa_16;
	// UnityEngine.GameObject start::camcontrol
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___camcontrol_17;
	// UnityEngine.Animator start::p1
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___p1_18;
	// UnityEngine.Animator start::p2
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___p2_19;
	// UnityEngine.Animator start::p3
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___p3_20;
	// UnityEngine.Animator start::p4
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___p4_21;
	// UnityEngine.Animator start::p5
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___p5_22;
	// UnityEngine.Animator start::p6
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___p6_23;
	// UnityEngine.Material start::projs
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___projs_24;
	// UnityEngine.Material start::pupirki
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___pupirki_25;
	// UnityEngine.GameObject start::txt1
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___txt1_26;
	// UnityEngine.GameObject start::passhole
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___passhole_27;
	// UnityEngine.GameObject start::sasshole
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___sasshole_28;
	// UnityEngine.Material start::op
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___op_30;
	// UnityEngine.Material start::opq
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___opq_31;
	// UnityEngine.Material start::opqe
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___opqe_32;
	// UnityEngine.Material start::awplego
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___awplego_33;
	// System.Single start::countersuka
	float ___countersuka_34;
	// UnityEngine.GameObject start::tttru
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___tttru_35;
	// UnityEngine.GameObject start::txt2
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___txt2_36;
	// UnityEngine.GameObject start::rrrrr
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___rrrrr_37;
	// UnityEngine.GameObject start::bebebe
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___bebebe_38;
	// UnityEngine.GameObject start::textska
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___textska_39;
	// UnityEngine.AudioSource start::aplodiki
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___aplodiki_40;

public:
	inline static int32_t get_offset_of_fon1_4() { return static_cast<int32_t>(offsetof(start_tAE134A10E53E820C269A6C9B80C302397F2612BC, ___fon1_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_fon1_4() const { return ___fon1_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_fon1_4() { return &___fon1_4; }
	inline void set_fon1_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___fon1_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fon1_4), (void*)value);
	}

	inline static int32_t get_offset_of_fon2_5() { return static_cast<int32_t>(offsetof(start_tAE134A10E53E820C269A6C9B80C302397F2612BC, ___fon2_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_fon2_5() const { return ___fon2_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_fon2_5() { return &___fon2_5; }
	inline void set_fon2_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___fon2_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fon2_5), (void*)value);
	}

	inline static int32_t get_offset_of_ft_6() { return static_cast<int32_t>(offsetof(start_tAE134A10E53E820C269A6C9B80C302397F2612BC, ___ft_6)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_ft_6() const { return ___ft_6; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_ft_6() { return &___ft_6; }
	inline void set_ft_6(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___ft_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ft_6), (void*)value);
	}

	inline static int32_t get_offset_of_intriga_7() { return static_cast<int32_t>(offsetof(start_tAE134A10E53E820C269A6C9B80C302397F2612BC, ___intriga_7)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_intriga_7() const { return ___intriga_7; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_intriga_7() { return &___intriga_7; }
	inline void set_intriga_7(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___intriga_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___intriga_7), (void*)value);
	}

	inline static int32_t get_offset_of_asks_8() { return static_cast<int32_t>(offsetof(start_tAE134A10E53E820C269A6C9B80C302397F2612BC, ___asks_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_asks_8() const { return ___asks_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_asks_8() { return &___asks_8; }
	inline void set_asks_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___asks_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___asks_8), (void*)value);
	}

	inline static int32_t get_offset_of_velocity_9() { return static_cast<int32_t>(offsetof(start_tAE134A10E53E820C269A6C9B80C302397F2612BC, ___velocity_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_velocity_9() const { return ___velocity_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_velocity_9() { return &___velocity_9; }
	inline void set_velocity_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___velocity_9 = value;
	}

	inline static int32_t get_offset_of_fonodin_10() { return static_cast<int32_t>(offsetof(start_tAE134A10E53E820C269A6C9B80C302397F2612BC, ___fonodin_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_fonodin_10() const { return ___fonodin_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_fonodin_10() { return &___fonodin_10; }
	inline void set_fonodin_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___fonodin_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fonodin_10), (void*)value);
	}

	inline static int32_t get_offset_of_goaway_11() { return static_cast<int32_t>(offsetof(start_tAE134A10E53E820C269A6C9B80C302397F2612BC, ___goaway_11)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_goaway_11() const { return ___goaway_11; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_goaway_11() { return &___goaway_11; }
	inline void set_goaway_11(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___goaway_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___goaway_11), (void*)value);
	}

	inline static int32_t get_offset_of_toedit_12() { return static_cast<int32_t>(offsetof(start_tAE134A10E53E820C269A6C9B80C302397F2612BC, ___toedit_12)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_toedit_12() const { return ___toedit_12; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_toedit_12() { return &___toedit_12; }
	inline void set_toedit_12(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___toedit_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___toedit_12), (void*)value);
	}

	inline static int32_t get_offset_of_introcamera_13() { return static_cast<int32_t>(offsetof(start_tAE134A10E53E820C269A6C9B80C302397F2612BC, ___introcamera_13)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_introcamera_13() const { return ___introcamera_13; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_introcamera_13() { return &___introcamera_13; }
	inline void set_introcamera_13(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___introcamera_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___introcamera_13), (void*)value);
	}

	inline static int32_t get_offset_of_highcamera_14() { return static_cast<int32_t>(offsetof(start_tAE134A10E53E820C269A6C9B80C302397F2612BC, ___highcamera_14)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_highcamera_14() const { return ___highcamera_14; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_highcamera_14() { return &___highcamera_14; }
	inline void set_highcamera_14(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___highcamera_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___highcamera_14), (void*)value);
	}

	inline static int32_t get_offset_of_cnt2_15() { return static_cast<int32_t>(offsetof(start_tAE134A10E53E820C269A6C9B80C302397F2612BC, ___cnt2_15)); }
	inline float get_cnt2_15() const { return ___cnt2_15; }
	inline float* get_address_of_cnt2_15() { return &___cnt2_15; }
	inline void set_cnt2_15(float value)
	{
		___cnt2_15 = value;
	}

	inline static int32_t get_offset_of_aaaaa_16() { return static_cast<int32_t>(offsetof(start_tAE134A10E53E820C269A6C9B80C302397F2612BC, ___aaaaa_16)); }
	inline bool get_aaaaa_16() const { return ___aaaaa_16; }
	inline bool* get_address_of_aaaaa_16() { return &___aaaaa_16; }
	inline void set_aaaaa_16(bool value)
	{
		___aaaaa_16 = value;
	}

	inline static int32_t get_offset_of_camcontrol_17() { return static_cast<int32_t>(offsetof(start_tAE134A10E53E820C269A6C9B80C302397F2612BC, ___camcontrol_17)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_camcontrol_17() const { return ___camcontrol_17; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_camcontrol_17() { return &___camcontrol_17; }
	inline void set_camcontrol_17(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___camcontrol_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___camcontrol_17), (void*)value);
	}

	inline static int32_t get_offset_of_p1_18() { return static_cast<int32_t>(offsetof(start_tAE134A10E53E820C269A6C9B80C302397F2612BC, ___p1_18)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_p1_18() const { return ___p1_18; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_p1_18() { return &___p1_18; }
	inline void set_p1_18(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___p1_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___p1_18), (void*)value);
	}

	inline static int32_t get_offset_of_p2_19() { return static_cast<int32_t>(offsetof(start_tAE134A10E53E820C269A6C9B80C302397F2612BC, ___p2_19)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_p2_19() const { return ___p2_19; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_p2_19() { return &___p2_19; }
	inline void set_p2_19(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___p2_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___p2_19), (void*)value);
	}

	inline static int32_t get_offset_of_p3_20() { return static_cast<int32_t>(offsetof(start_tAE134A10E53E820C269A6C9B80C302397F2612BC, ___p3_20)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_p3_20() const { return ___p3_20; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_p3_20() { return &___p3_20; }
	inline void set_p3_20(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___p3_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___p3_20), (void*)value);
	}

	inline static int32_t get_offset_of_p4_21() { return static_cast<int32_t>(offsetof(start_tAE134A10E53E820C269A6C9B80C302397F2612BC, ___p4_21)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_p4_21() const { return ___p4_21; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_p4_21() { return &___p4_21; }
	inline void set_p4_21(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___p4_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___p4_21), (void*)value);
	}

	inline static int32_t get_offset_of_p5_22() { return static_cast<int32_t>(offsetof(start_tAE134A10E53E820C269A6C9B80C302397F2612BC, ___p5_22)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_p5_22() const { return ___p5_22; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_p5_22() { return &___p5_22; }
	inline void set_p5_22(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___p5_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___p5_22), (void*)value);
	}

	inline static int32_t get_offset_of_p6_23() { return static_cast<int32_t>(offsetof(start_tAE134A10E53E820C269A6C9B80C302397F2612BC, ___p6_23)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_p6_23() const { return ___p6_23; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_p6_23() { return &___p6_23; }
	inline void set_p6_23(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___p6_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___p6_23), (void*)value);
	}

	inline static int32_t get_offset_of_projs_24() { return static_cast<int32_t>(offsetof(start_tAE134A10E53E820C269A6C9B80C302397F2612BC, ___projs_24)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_projs_24() const { return ___projs_24; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_projs_24() { return &___projs_24; }
	inline void set_projs_24(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___projs_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___projs_24), (void*)value);
	}

	inline static int32_t get_offset_of_pupirki_25() { return static_cast<int32_t>(offsetof(start_tAE134A10E53E820C269A6C9B80C302397F2612BC, ___pupirki_25)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_pupirki_25() const { return ___pupirki_25; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_pupirki_25() { return &___pupirki_25; }
	inline void set_pupirki_25(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___pupirki_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pupirki_25), (void*)value);
	}

	inline static int32_t get_offset_of_txt1_26() { return static_cast<int32_t>(offsetof(start_tAE134A10E53E820C269A6C9B80C302397F2612BC, ___txt1_26)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_txt1_26() const { return ___txt1_26; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_txt1_26() { return &___txt1_26; }
	inline void set_txt1_26(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___txt1_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___txt1_26), (void*)value);
	}

	inline static int32_t get_offset_of_passhole_27() { return static_cast<int32_t>(offsetof(start_tAE134A10E53E820C269A6C9B80C302397F2612BC, ___passhole_27)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_passhole_27() const { return ___passhole_27; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_passhole_27() { return &___passhole_27; }
	inline void set_passhole_27(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___passhole_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___passhole_27), (void*)value);
	}

	inline static int32_t get_offset_of_sasshole_28() { return static_cast<int32_t>(offsetof(start_tAE134A10E53E820C269A6C9B80C302397F2612BC, ___sasshole_28)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_sasshole_28() const { return ___sasshole_28; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_sasshole_28() { return &___sasshole_28; }
	inline void set_sasshole_28(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___sasshole_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sasshole_28), (void*)value);
	}

	inline static int32_t get_offset_of_op_30() { return static_cast<int32_t>(offsetof(start_tAE134A10E53E820C269A6C9B80C302397F2612BC, ___op_30)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_op_30() const { return ___op_30; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_op_30() { return &___op_30; }
	inline void set_op_30(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___op_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___op_30), (void*)value);
	}

	inline static int32_t get_offset_of_opq_31() { return static_cast<int32_t>(offsetof(start_tAE134A10E53E820C269A6C9B80C302397F2612BC, ___opq_31)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_opq_31() const { return ___opq_31; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_opq_31() { return &___opq_31; }
	inline void set_opq_31(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___opq_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___opq_31), (void*)value);
	}

	inline static int32_t get_offset_of_opqe_32() { return static_cast<int32_t>(offsetof(start_tAE134A10E53E820C269A6C9B80C302397F2612BC, ___opqe_32)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_opqe_32() const { return ___opqe_32; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_opqe_32() { return &___opqe_32; }
	inline void set_opqe_32(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___opqe_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___opqe_32), (void*)value);
	}

	inline static int32_t get_offset_of_awplego_33() { return static_cast<int32_t>(offsetof(start_tAE134A10E53E820C269A6C9B80C302397F2612BC, ___awplego_33)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_awplego_33() const { return ___awplego_33; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_awplego_33() { return &___awplego_33; }
	inline void set_awplego_33(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___awplego_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___awplego_33), (void*)value);
	}

	inline static int32_t get_offset_of_countersuka_34() { return static_cast<int32_t>(offsetof(start_tAE134A10E53E820C269A6C9B80C302397F2612BC, ___countersuka_34)); }
	inline float get_countersuka_34() const { return ___countersuka_34; }
	inline float* get_address_of_countersuka_34() { return &___countersuka_34; }
	inline void set_countersuka_34(float value)
	{
		___countersuka_34 = value;
	}

	inline static int32_t get_offset_of_tttru_35() { return static_cast<int32_t>(offsetof(start_tAE134A10E53E820C269A6C9B80C302397F2612BC, ___tttru_35)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_tttru_35() const { return ___tttru_35; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_tttru_35() { return &___tttru_35; }
	inline void set_tttru_35(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___tttru_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tttru_35), (void*)value);
	}

	inline static int32_t get_offset_of_txt2_36() { return static_cast<int32_t>(offsetof(start_tAE134A10E53E820C269A6C9B80C302397F2612BC, ___txt2_36)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_txt2_36() const { return ___txt2_36; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_txt2_36() { return &___txt2_36; }
	inline void set_txt2_36(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___txt2_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___txt2_36), (void*)value);
	}

	inline static int32_t get_offset_of_rrrrr_37() { return static_cast<int32_t>(offsetof(start_tAE134A10E53E820C269A6C9B80C302397F2612BC, ___rrrrr_37)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_rrrrr_37() const { return ___rrrrr_37; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_rrrrr_37() { return &___rrrrr_37; }
	inline void set_rrrrr_37(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___rrrrr_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rrrrr_37), (void*)value);
	}

	inline static int32_t get_offset_of_bebebe_38() { return static_cast<int32_t>(offsetof(start_tAE134A10E53E820C269A6C9B80C302397F2612BC, ___bebebe_38)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_bebebe_38() const { return ___bebebe_38; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_bebebe_38() { return &___bebebe_38; }
	inline void set_bebebe_38(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___bebebe_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bebebe_38), (void*)value);
	}

	inline static int32_t get_offset_of_textska_39() { return static_cast<int32_t>(offsetof(start_tAE134A10E53E820C269A6C9B80C302397F2612BC, ___textska_39)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_textska_39() const { return ___textska_39; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_textska_39() { return &___textska_39; }
	inline void set_textska_39(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___textska_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___textska_39), (void*)value);
	}

	inline static int32_t get_offset_of_aplodiki_40() { return static_cast<int32_t>(offsetof(start_tAE134A10E53E820C269A6C9B80C302397F2612BC, ___aplodiki_40)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_aplodiki_40() const { return ___aplodiki_40; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_aplodiki_40() { return &___aplodiki_40; }
	inline void set_aplodiki_40(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___aplodiki_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___aplodiki_40), (void*)value);
	}
};

struct start_tAE134A10E53E820C269A6C9B80C302397F2612BC_StaticFields
{
public:
	// System.Boolean start::SSEC
	bool ___SSEC_29;

public:
	inline static int32_t get_offset_of_SSEC_29() { return static_cast<int32_t>(offsetof(start_tAE134A10E53E820C269A6C9B80C302397F2612BC_StaticFields, ___SSEC_29)); }
	inline bool get_SSEC_29() const { return ___SSEC_29; }
	inline bool* get_address_of_SSEC_29() { return &___SSEC_29; }
	inline void set_SSEC_29(bool value)
	{
		___SSEC_29 = value;
	}
};


// vrash
struct  vrash_tF08459DC43F95DF43201EA6383AD9FCB6A2CEF75  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};


// vrash2
struct  vrash2_t3A7B3C3FF7F1F1760684D8542856E35CD2F2F7A6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};


// vrashkrug
struct  vrashkrug_t2DABFFE7C1A58EE05150CA30272EE7D5A95F1B24  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};


// x2tip
struct  x2tip_t8849FCBC472E6674196F256CC8051DE414072152  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.AudioSource x2tip::music
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___music_4;
	// UnityEngine.GameObject x2tip::tride
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___tride_5;
	// UnityEngine.AudioSource x2tip::one
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___one_6;
	// UnityEngine.AudioSource x2tip::five
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___five_7;
	// UnityEngine.AudioSource x2tip::ten
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___ten_8;
	// System.Boolean x2tip::used
	bool ___used_9;

public:
	inline static int32_t get_offset_of_music_4() { return static_cast<int32_t>(offsetof(x2tip_t8849FCBC472E6674196F256CC8051DE414072152, ___music_4)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_music_4() const { return ___music_4; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_music_4() { return &___music_4; }
	inline void set_music_4(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___music_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___music_4), (void*)value);
	}

	inline static int32_t get_offset_of_tride_5() { return static_cast<int32_t>(offsetof(x2tip_t8849FCBC472E6674196F256CC8051DE414072152, ___tride_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_tride_5() const { return ___tride_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_tride_5() { return &___tride_5; }
	inline void set_tride_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___tride_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tride_5), (void*)value);
	}

	inline static int32_t get_offset_of_one_6() { return static_cast<int32_t>(offsetof(x2tip_t8849FCBC472E6674196F256CC8051DE414072152, ___one_6)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_one_6() const { return ___one_6; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_one_6() { return &___one_6; }
	inline void set_one_6(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___one_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___one_6), (void*)value);
	}

	inline static int32_t get_offset_of_five_7() { return static_cast<int32_t>(offsetof(x2tip_t8849FCBC472E6674196F256CC8051DE414072152, ___five_7)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_five_7() const { return ___five_7; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_five_7() { return &___five_7; }
	inline void set_five_7(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___five_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___five_7), (void*)value);
	}

	inline static int32_t get_offset_of_ten_8() { return static_cast<int32_t>(offsetof(x2tip_t8849FCBC472E6674196F256CC8051DE414072152, ___ten_8)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_ten_8() const { return ___ten_8; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_ten_8() { return &___ten_8; }
	inline void set_ten_8(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___ten_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ten_8), (void*)value);
	}

	inline static int32_t get_offset_of_used_9() { return static_cast<int32_t>(offsetof(x2tip_t8849FCBC472E6674196F256CC8051DE414072152, ___used_9)); }
	inline bool get_used_9() const { return ___used_9; }
	inline bool* get_address_of_used_9() { return &___used_9; }
	inline void set_used_9(bool value)
	{
		___used_9 = value;
	}
};

struct x2tip_t8849FCBC472E6674196F256CC8051DE414072152_StaticFields
{
public:
	// System.Boolean x2tip::poraout
	bool ___poraout_10;

public:
	inline static int32_t get_offset_of_poraout_10() { return static_cast<int32_t>(offsetof(x2tip_t8849FCBC472E6674196F256CC8051DE414072152_StaticFields, ___poraout_10)); }
	inline bool get_poraout_10() const { return ___poraout_10; }
	inline bool* get_address_of_poraout_10() { return &___poraout_10; }
	inline void set_poraout_10(bool value)
	{
		___poraout_10 = value;
	}
};


// yes
struct  yes_t36BDD09D45583CF07DAA0274CDCE0985E722F5E6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};


// zabratmoney
struct  zabratmoney_t7DDABFB3031D063938927397F1B078B66BA485CC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject zabratmoney::a
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___a_4;
	// UnityEngine.GameObject zabratmoney::b
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___b_5;
	// UnityEngine.GameObject zabratmoney::c
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___c_6;
	// UnityEngine.GameObject zabratmoney::d
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___d_7;
	// UnityEngine.GameObject zabratmoney::opa
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___opa_9;

public:
	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(zabratmoney_t7DDABFB3031D063938927397F1B078B66BA485CC, ___a_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_a_4() const { return ___a_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___a_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___a_4), (void*)value);
	}

	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(zabratmoney_t7DDABFB3031D063938927397F1B078B66BA485CC, ___b_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_b_5() const { return ___b_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___b_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___b_5), (void*)value);
	}

	inline static int32_t get_offset_of_c_6() { return static_cast<int32_t>(offsetof(zabratmoney_t7DDABFB3031D063938927397F1B078B66BA485CC, ___c_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_c_6() const { return ___c_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_c_6() { return &___c_6; }
	inline void set_c_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___c_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___c_6), (void*)value);
	}

	inline static int32_t get_offset_of_d_7() { return static_cast<int32_t>(offsetof(zabratmoney_t7DDABFB3031D063938927397F1B078B66BA485CC, ___d_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_d_7() const { return ___d_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_d_7() { return &___d_7; }
	inline void set_d_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___d_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___d_7), (void*)value);
	}

	inline static int32_t get_offset_of_opa_9() { return static_cast<int32_t>(offsetof(zabratmoney_t7DDABFB3031D063938927397F1B078B66BA485CC, ___opa_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_opa_9() const { return ___opa_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_opa_9() { return &___opa_9; }
	inline void set_opa_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___opa_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___opa_9), (void*)value);
	}
};

struct zabratmoney_t7DDABFB3031D063938927397F1B078B66BA485CC_StaticFields
{
public:
	// System.Boolean zabratmoney::alr
	bool ___alr_8;

public:
	inline static int32_t get_offset_of_alr_8() { return static_cast<int32_t>(offsetof(zabratmoney_t7DDABFB3031D063938927397F1B078B66BA485CC_StaticFields, ___alr_8)); }
	inline bool get_alr_8() const { return ___alr_8; }
	inline bool* get_address_of_alr_8() { return &___alr_8; }
	inline void set_alr_8(bool value)
	{
		___alr_8 = value;
	}
};


// zaltip
struct  zaltip_t9E4D625BB000CD131ACEE4ABCC45F34F66419F87  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.AudioSource zaltip::tudin
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___tudin_4;
	// UnityEngine.GameObject zaltip::zalfon
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___zalfon_5;
	// UnityEngine.GameObject zaltip::p1
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___p1_6;
	// UnityEngine.GameObject zaltip::p2
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___p2_7;
	// UnityEngine.GameObject zaltip::p3
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___p3_8;
	// UnityEngine.GameObject zaltip::p4
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___p4_9;
	// UnityEngine.GameObject zaltip::p5
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___p5_10;
	// UnityEngine.GameObject zaltip::p6
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___p6_11;
	// UnityEngine.GameObject zaltip::firstp
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___firstp_12;
	// UnityEngine.GameObject zaltip::secp
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___secp_13;
	// UnityEngine.GameObject zaltip::thirdp
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___thirdp_14;
	// UnityEngine.GameObject zaltip::fourp
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___fourp_15;
	// UnityEngine.Animator zaltip::firsta
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___firsta_16;
	// UnityEngine.Animator zaltip::seca
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___seca_17;
	// UnityEngine.Animator zaltip::thirda
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___thirda_18;
	// UnityEngine.Animator zaltip::foura
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___foura_19;
	// UnityEngine.TextMesh zaltip::a
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___a_20;
	// UnityEngine.TextMesh zaltip::b
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___b_21;
	// UnityEngine.TextMesh zaltip::c
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___c_22;
	// UnityEngine.TextMesh zaltip::d
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___d_23;
	// UnityEngine.Collider2D zaltip::ad
	Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * ___ad_24;
	// System.Boolean zaltip::used
	bool ___used_25;
	// System.Single zaltip::counter
	float ___counter_26;
	// System.Int32 zaltip::sevenperc
	int32_t ___sevenperc_27;

public:
	inline static int32_t get_offset_of_tudin_4() { return static_cast<int32_t>(offsetof(zaltip_t9E4D625BB000CD131ACEE4ABCC45F34F66419F87, ___tudin_4)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_tudin_4() const { return ___tudin_4; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_tudin_4() { return &___tudin_4; }
	inline void set_tudin_4(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___tudin_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tudin_4), (void*)value);
	}

	inline static int32_t get_offset_of_zalfon_5() { return static_cast<int32_t>(offsetof(zaltip_t9E4D625BB000CD131ACEE4ABCC45F34F66419F87, ___zalfon_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_zalfon_5() const { return ___zalfon_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_zalfon_5() { return &___zalfon_5; }
	inline void set_zalfon_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___zalfon_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___zalfon_5), (void*)value);
	}

	inline static int32_t get_offset_of_p1_6() { return static_cast<int32_t>(offsetof(zaltip_t9E4D625BB000CD131ACEE4ABCC45F34F66419F87, ___p1_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_p1_6() const { return ___p1_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_p1_6() { return &___p1_6; }
	inline void set_p1_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___p1_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___p1_6), (void*)value);
	}

	inline static int32_t get_offset_of_p2_7() { return static_cast<int32_t>(offsetof(zaltip_t9E4D625BB000CD131ACEE4ABCC45F34F66419F87, ___p2_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_p2_7() const { return ___p2_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_p2_7() { return &___p2_7; }
	inline void set_p2_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___p2_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___p2_7), (void*)value);
	}

	inline static int32_t get_offset_of_p3_8() { return static_cast<int32_t>(offsetof(zaltip_t9E4D625BB000CD131ACEE4ABCC45F34F66419F87, ___p3_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_p3_8() const { return ___p3_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_p3_8() { return &___p3_8; }
	inline void set_p3_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___p3_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___p3_8), (void*)value);
	}

	inline static int32_t get_offset_of_p4_9() { return static_cast<int32_t>(offsetof(zaltip_t9E4D625BB000CD131ACEE4ABCC45F34F66419F87, ___p4_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_p4_9() const { return ___p4_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_p4_9() { return &___p4_9; }
	inline void set_p4_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___p4_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___p4_9), (void*)value);
	}

	inline static int32_t get_offset_of_p5_10() { return static_cast<int32_t>(offsetof(zaltip_t9E4D625BB000CD131ACEE4ABCC45F34F66419F87, ___p5_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_p5_10() const { return ___p5_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_p5_10() { return &___p5_10; }
	inline void set_p5_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___p5_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___p5_10), (void*)value);
	}

	inline static int32_t get_offset_of_p6_11() { return static_cast<int32_t>(offsetof(zaltip_t9E4D625BB000CD131ACEE4ABCC45F34F66419F87, ___p6_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_p6_11() const { return ___p6_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_p6_11() { return &___p6_11; }
	inline void set_p6_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___p6_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___p6_11), (void*)value);
	}

	inline static int32_t get_offset_of_firstp_12() { return static_cast<int32_t>(offsetof(zaltip_t9E4D625BB000CD131ACEE4ABCC45F34F66419F87, ___firstp_12)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_firstp_12() const { return ___firstp_12; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_firstp_12() { return &___firstp_12; }
	inline void set_firstp_12(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___firstp_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___firstp_12), (void*)value);
	}

	inline static int32_t get_offset_of_secp_13() { return static_cast<int32_t>(offsetof(zaltip_t9E4D625BB000CD131ACEE4ABCC45F34F66419F87, ___secp_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_secp_13() const { return ___secp_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_secp_13() { return &___secp_13; }
	inline void set_secp_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___secp_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___secp_13), (void*)value);
	}

	inline static int32_t get_offset_of_thirdp_14() { return static_cast<int32_t>(offsetof(zaltip_t9E4D625BB000CD131ACEE4ABCC45F34F66419F87, ___thirdp_14)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_thirdp_14() const { return ___thirdp_14; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_thirdp_14() { return &___thirdp_14; }
	inline void set_thirdp_14(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___thirdp_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___thirdp_14), (void*)value);
	}

	inline static int32_t get_offset_of_fourp_15() { return static_cast<int32_t>(offsetof(zaltip_t9E4D625BB000CD131ACEE4ABCC45F34F66419F87, ___fourp_15)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_fourp_15() const { return ___fourp_15; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_fourp_15() { return &___fourp_15; }
	inline void set_fourp_15(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___fourp_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fourp_15), (void*)value);
	}

	inline static int32_t get_offset_of_firsta_16() { return static_cast<int32_t>(offsetof(zaltip_t9E4D625BB000CD131ACEE4ABCC45F34F66419F87, ___firsta_16)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_firsta_16() const { return ___firsta_16; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_firsta_16() { return &___firsta_16; }
	inline void set_firsta_16(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___firsta_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___firsta_16), (void*)value);
	}

	inline static int32_t get_offset_of_seca_17() { return static_cast<int32_t>(offsetof(zaltip_t9E4D625BB000CD131ACEE4ABCC45F34F66419F87, ___seca_17)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_seca_17() const { return ___seca_17; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_seca_17() { return &___seca_17; }
	inline void set_seca_17(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___seca_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___seca_17), (void*)value);
	}

	inline static int32_t get_offset_of_thirda_18() { return static_cast<int32_t>(offsetof(zaltip_t9E4D625BB000CD131ACEE4ABCC45F34F66419F87, ___thirda_18)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_thirda_18() const { return ___thirda_18; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_thirda_18() { return &___thirda_18; }
	inline void set_thirda_18(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___thirda_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___thirda_18), (void*)value);
	}

	inline static int32_t get_offset_of_foura_19() { return static_cast<int32_t>(offsetof(zaltip_t9E4D625BB000CD131ACEE4ABCC45F34F66419F87, ___foura_19)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_foura_19() const { return ___foura_19; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_foura_19() { return &___foura_19; }
	inline void set_foura_19(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___foura_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___foura_19), (void*)value);
	}

	inline static int32_t get_offset_of_a_20() { return static_cast<int32_t>(offsetof(zaltip_t9E4D625BB000CD131ACEE4ABCC45F34F66419F87, ___a_20)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_a_20() const { return ___a_20; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_a_20() { return &___a_20; }
	inline void set_a_20(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___a_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___a_20), (void*)value);
	}

	inline static int32_t get_offset_of_b_21() { return static_cast<int32_t>(offsetof(zaltip_t9E4D625BB000CD131ACEE4ABCC45F34F66419F87, ___b_21)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_b_21() const { return ___b_21; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_b_21() { return &___b_21; }
	inline void set_b_21(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___b_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___b_21), (void*)value);
	}

	inline static int32_t get_offset_of_c_22() { return static_cast<int32_t>(offsetof(zaltip_t9E4D625BB000CD131ACEE4ABCC45F34F66419F87, ___c_22)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_c_22() const { return ___c_22; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_c_22() { return &___c_22; }
	inline void set_c_22(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___c_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___c_22), (void*)value);
	}

	inline static int32_t get_offset_of_d_23() { return static_cast<int32_t>(offsetof(zaltip_t9E4D625BB000CD131ACEE4ABCC45F34F66419F87, ___d_23)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_d_23() const { return ___d_23; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_d_23() { return &___d_23; }
	inline void set_d_23(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___d_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___d_23), (void*)value);
	}

	inline static int32_t get_offset_of_ad_24() { return static_cast<int32_t>(offsetof(zaltip_t9E4D625BB000CD131ACEE4ABCC45F34F66419F87, ___ad_24)); }
	inline Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * get_ad_24() const { return ___ad_24; }
	inline Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 ** get_address_of_ad_24() { return &___ad_24; }
	inline void set_ad_24(Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * value)
	{
		___ad_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ad_24), (void*)value);
	}

	inline static int32_t get_offset_of_used_25() { return static_cast<int32_t>(offsetof(zaltip_t9E4D625BB000CD131ACEE4ABCC45F34F66419F87, ___used_25)); }
	inline bool get_used_25() const { return ___used_25; }
	inline bool* get_address_of_used_25() { return &___used_25; }
	inline void set_used_25(bool value)
	{
		___used_25 = value;
	}

	inline static int32_t get_offset_of_counter_26() { return static_cast<int32_t>(offsetof(zaltip_t9E4D625BB000CD131ACEE4ABCC45F34F66419F87, ___counter_26)); }
	inline float get_counter_26() const { return ___counter_26; }
	inline float* get_address_of_counter_26() { return &___counter_26; }
	inline void set_counter_26(float value)
	{
		___counter_26 = value;
	}

	inline static int32_t get_offset_of_sevenperc_27() { return static_cast<int32_t>(offsetof(zaltip_t9E4D625BB000CD131ACEE4ABCC45F34F66419F87, ___sevenperc_27)); }
	inline int32_t get_sevenperc_27() const { return ___sevenperc_27; }
	inline int32_t* get_address_of_sevenperc_27() { return &___sevenperc_27; }
	inline void set_sevenperc_27(int32_t value)
	{
		___sevenperc_27 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif


// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_mD4382B2843BA9A61A01A8F9D7B9813D060F9C9CA_gshared (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);

// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::Rotate(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_Rotate_mEEA80F3DA5A4C93611D7165DF54763E578477EF9 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, float ___xAngle0, float ___yAngle1, float ___zAngle2, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97 (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AudioSource::set_mute(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AudioSource_set_mute_m04D579849D7D37D6CC39DE31DB928176B2A9C2CF (AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AudioSource::Stop()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AudioSource_Stop_m488F7AA7F7067DE3EC92CEE3413E86C2E5940200 (AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Animator>()
inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * GameObject_GetComponent_TisAnimator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A_m9904EA7E80165F7771F8AB3967F417D7C2B09996 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mD4382B2843BA9A61A01A8F9D7B9813D060F9C9CA_gshared)(__this, method);
}
// System.Void UnityEngine.Animator::Play(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Animator_Play_m254CA699DF64E86856EC8C54D1A739E3C0A49793 (Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * __this, String_t* ___stateName0, const RuntimeMethod* method);
// System.Void UnityEngine.AudioSource::Play()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AudioSource_Play_m0BA206481892AA4AF7DB2900A0B0805076516164 (AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String,UnityEngine.SceneManagement.LoadSceneMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SceneManager_LoadScene_m9721867D46BC827D58271AD235267B0B0865F115 (String_t* ___sceneName0, int32_t ___mode1, const RuntimeMethod* method);
// System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PlayerPrefs_GetInt_m318D2B42E0FCAF179BF86D6C2353B38A58089BAD (String_t* ___key0, const RuntimeMethod* method);
// System.Void UnityEngine.PlayerPrefs::SetInt(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerPrefs_SetInt_mBF4101DF829B4738CCC293E1C2D173AEE45EFE62 (String_t* ___key0, int32_t ___value1, const RuntimeMethod* method);
// System.Void AppodealAds.Unity.Api.Appodeal::initialize(System.String,System.Int32,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Appodeal_initialize_m891EAFF98230D571C1779B75EE6CFECD08122D44 (String_t* ___appKey0, int32_t ___adTypes1, bool ___hasConsent2, const RuntimeMethod* method);
// System.Boolean AppodealAds.Unity.Api.Appodeal::isLoaded(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Appodeal_isLoaded_m8F9485BCC1B4B7D802865FBEB0255C2316BACD0A (int32_t ___adTypes0, const RuntimeMethod* method);
// System.Boolean AppodealAds.Unity.Api.Appodeal::show(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Appodeal_show_m154AD7C996073738388B655CEDE1E99F891FF694 (int32_t ___adTypes0, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::Log(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E (const RuntimeMethod* method);
// System.Void zaltip::Naznach()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void zaltip_Naznach_m32DAA727AB6914925B335901C65BF7325FD7B0DC (zaltip_t9E4D625BB000CD131ACEE4ABCC45F34F66419F87 * __this, const RuntimeMethod* method);
// System.String UnityEngine.Object::get_name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * __this, const RuntimeMethod* method);
// System.Boolean System.String::op_Equality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// System.Void zaltip::ShowRewardedAd()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void zaltip_ShowRewardedAd_mB5E223BE079E093762E376AEED55F8D5D71F30D8 (zaltip_t9E4D625BB000CD131ACEE4ABCC45F34F66419F87 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Behaviour_set_enabled_m9755D3B17D7022D23D1E4C618BD9A6B66A5ADC6B (Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::set_speed(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Animator_set_speed_mEA558D196D84684744A642A56AFBF22F16448813 (Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * __this, float ___value0, const RuntimeMethod* method);
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Random_Range_mD0C8F37FF3CAB1D87AAA6C45130BD59626BD6780 (int32_t ___min0, int32_t ___max1, const RuntimeMethod* method);
// System.String System.Int32::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Int32_ToString_m1863896DE712BF97C031D55B12E1583F1982DC02 (int32_t* __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method);
// System.Void UnityEngine.TextMesh::set_text(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextMesh_set_text_m64242AB987CF285F432E7AED38F24FF855E9B220 (TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * __this, String_t* ___value0, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void vrash::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vrash_Start_mC5EB8C7343105D4B1C4888F2D32F8BFE09C93E78 (vrash_tF08459DC43F95DF43201EA6383AD9FCB6A2CEF75 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void vrash::FixedUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vrash_FixedUpdate_m36E5542C127F196A3590B44DAA1E9A4B105B4EAB (vrash_tF08459DC43F95DF43201EA6383AD9FCB6A2CEF75 * __this, const RuntimeMethod* method)
{
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_1 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_Rotate_mEEA80F3DA5A4C93611D7165DF54763E578477EF9(L_1, (0.0f), (0.0f), (0.1f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void vrash::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vrash__ctor_mAD1DCBF5812CC0926BEF1DC97495A811D7F05126 (vrash_tF08459DC43F95DF43201EA6383AD9FCB6A2CEF75 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void vrash2::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vrash2_Start_mB5D8563B40D329550A67408E88F45EE8569AEE99 (vrash2_t3A7B3C3FF7F1F1760684D8542856E35CD2F2F7A6 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void vrash2::FixedUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vrash2_FixedUpdate_mA3EE1D437535E86442C096B4BE39C757C74A261A (vrash2_t3A7B3C3FF7F1F1760684D8542856E35CD2F2F7A6 * __this, const RuntimeMethod* method)
{
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_1 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_Rotate_mEEA80F3DA5A4C93611D7165DF54763E578477EF9(L_1, (0.0f), (0.0f), (-0.2f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void vrash2::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vrash2__ctor_m4F87DA475B4893AFEC7D023448B2F9E6687D63AE (vrash2_t3A7B3C3FF7F1F1760684D8542856E35CD2F2F7A6 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void vrashkrug::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vrashkrug_Start_m131F263B0BB6CE453081E504AE4C17810A5F32AC (vrashkrug_t2DABFFE7C1A58EE05150CA30272EE7D5A95F1B24 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void vrashkrug::FixedUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vrashkrug_FixedUpdate_mE399B99AC54B860D55DA26928A7BA67FFFCB926A (vrashkrug_t2DABFFE7C1A58EE05150CA30272EE7D5A95F1B24 * __this, const RuntimeMethod* method)
{
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_1 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_Rotate_mEEA80F3DA5A4C93611D7165DF54763E578477EF9(L_1, (0.0f), (0.12f), (0.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void vrashkrug::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vrashkrug__ctor_m7224864BB2D8543C23B1F04A8676EF5B8D95F0F7 (vrashkrug_t2DABFFE7C1A58EE05150CA30272EE7D5A95F1B24 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void x2tip::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void x2tip_Start_mAB78F0E29058BDF05E96A1F0408977B3CBB6F992 (x2tip_t8849FCBC472E6674196F256CC8051DE414072152 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (x2tip_Start_mAB78F0E29058BDF05E96A1F0408977B3CBB6F992_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ((nesgor_tAFC06E7C3AD54556038BA9DE502122E4C3D43533_StaticFields*)il2cpp_codegen_static_fields_for(nesgor_tAFC06E7C3AD54556038BA9DE502122E4C3D43533_il2cpp_TypeInfo_var))->get_classicda_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_1, (bool)0, /*hidden argument*/NULL);
	}

IL_0013:
	{
		return;
	}
}
// System.Void x2tip::FixedUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void x2tip_FixedUpdate_mFFE5693DFAD7BF8EE35C6A8FCBBAB80B46CD29AD (x2tip_t8849FCBC472E6674196F256CC8051DE414072152 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (x2tip_FixedUpdate_mFFE5693DFAD7BF8EE35C6A8FCBBAB80B46CD29AD_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ((x2tip_t8849FCBC472E6674196F256CC8051DE414072152_StaticFields*)il2cpp_codegen_static_fields_for(x2tip_t8849FCBC472E6674196F256CC8051DE414072152_il2cpp_TypeInfo_var))->get_poraout_10();
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * L_1 = __this->get_one_6();
		NullCheck(L_1);
		AudioSource_set_mute_m04D579849D7D37D6CC39DE31DB928176B2A9C2CF(L_1, (bool)0, /*hidden argument*/NULL);
		AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * L_2 = __this->get_five_7();
		NullCheck(L_2);
		AudioSource_set_mute_m04D579849D7D37D6CC39DE31DB928176B2A9C2CF(L_2, (bool)0, /*hidden argument*/NULL);
		AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * L_3 = __this->get_ten_8();
		NullCheck(L_3);
		AudioSource_set_mute_m04D579849D7D37D6CC39DE31DB928176B2A9C2CF(L_3, (bool)0, /*hidden argument*/NULL);
		AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * L_4 = __this->get_music_4();
		NullCheck(L_4);
		AudioSource_Stop_m488F7AA7F7067DE3EC92CEE3413E86C2E5940200(L_4, /*hidden argument*/NULL);
	}

IL_0036:
	{
		return;
	}
}
// System.Void x2tip::OnMouseDown()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void x2tip_OnMouseDown_mDB3FBD8B1FC9A31ADBDDBA5FE4B45456BB3F980B (x2tip_t8849FCBC472E6674196F256CC8051DE414072152 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (x2tip_OnMouseDown_mDB3FBD8B1FC9A31ADBDDBA5FE4B45456BB3F980B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ((generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_StaticFields*)il2cpp_codegen_static_fields_for(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_il2cpp_TypeInfo_var))->get_podskazused_39();
		if (L_0)
		{
			goto IL_0079;
		}
	}
	{
		bool L_1 = ((onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D_StaticFields*)il2cpp_codegen_static_fields_for(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D_il2cpp_TypeInfo_var))->get_already_27();
		if (L_1)
		{
			goto IL_0079;
		}
	}
	{
		bool L_2 = __this->get_used_9();
		if (L_2)
		{
			goto IL_0079;
		}
	}
	{
		((generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_StaticFields*)il2cpp_codegen_static_fields_for(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_il2cpp_TypeInfo_var))->set_podskazused_39((bool)1);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_4 = GameObject_GetComponent_TisAnimator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A_m9904EA7E80165F7771F8AB3967F417D7C2B09996(L_3, /*hidden argument*/GameObject_GetComponent_TisAnimator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A_m9904EA7E80165F7771F8AB3967F417D7C2B09996_RuntimeMethod_var);
		NullCheck(L_4);
		Animator_Play_m254CA699DF64E86856EC8C54D1A739E3C0A49793(L_4, _stringLiteralEECABAE13529439E62DE1A0D81E07B14F9A348C9, /*hidden argument*/NULL);
		__this->set_used_9((bool)1);
		AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * L_5 = __this->get_music_4();
		NullCheck(L_5);
		AudioSource_Play_m0BA206481892AA4AF7DB2900A0B0805076516164(L_5, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_6 = __this->get_tride_5();
		NullCheck(L_6);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_6, (bool)1, /*hidden argument*/NULL);
		AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * L_7 = __this->get_one_6();
		NullCheck(L_7);
		AudioSource_set_mute_m04D579849D7D37D6CC39DE31DB928176B2A9C2CF(L_7, (bool)1, /*hidden argument*/NULL);
		AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * L_8 = __this->get_five_7();
		NullCheck(L_8);
		AudioSource_set_mute_m04D579849D7D37D6CC39DE31DB928176B2A9C2CF(L_8, (bool)1, /*hidden argument*/NULL);
		AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * L_9 = __this->get_ten_8();
		NullCheck(L_9);
		AudioSource_set_mute_m04D579849D7D37D6CC39DE31DB928176B2A9C2CF(L_9, (bool)1, /*hidden argument*/NULL);
		((onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D_StaticFields*)il2cpp_codegen_static_fields_for(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D_il2cpp_TypeInfo_var))->set_x2used_31((bool)1);
	}

IL_0079:
	{
		return;
	}
}
// System.Void x2tip::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void x2tip__ctor_m083D3F2F77DEB7C0D43389ABADB32BA51792E8E3 (x2tip_t8849FCBC472E6674196F256CC8051DE414072152 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void yes::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void yes_Start_m2FFF5C7A490BA9768E43B268210104F0E371EC25 (yes_t36BDD09D45583CF07DAA0274CDCE0985E722F5E6 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void yes::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void yes_Update_m9D7619DFEABD9B64A6C1C35C4F187410F07E40CF (yes_t36BDD09D45583CF07DAA0274CDCE0985E722F5E6 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void yes::OnMouseDown()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void yes_OnMouseDown_mBD3EAE21B5A9137CB7BE5D3A84A7425E9D927627 (yes_t36BDD09D45583CF07DAA0274CDCE0985E722F5E6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (yes_OnMouseDown_mBD3EAE21B5A9137CB7BE5D3A84A7425E9D927627_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ((generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_StaticFields*)il2cpp_codegen_static_fields_for(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_il2cpp_TypeInfo_var))->get_questpoporyad_26();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_000e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_il2cpp_TypeInfo_var);
		((score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_StaticFields*)il2cpp_codegen_static_fields_for(score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_il2cpp_TypeInfo_var))->set_schet_4(0);
	}

IL_000e:
	{
		int32_t L_1 = ((generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_StaticFields*)il2cpp_codegen_static_fields_for(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_il2cpp_TypeInfo_var))->get_questpoporyad_26();
		if ((!(((uint32_t)L_1) == ((uint32_t)2))))
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_il2cpp_TypeInfo_var);
		((score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_StaticFields*)il2cpp_codegen_static_fields_for(score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_il2cpp_TypeInfo_var))->set_schet_4(((int32_t)500));
	}

IL_0020:
	{
		int32_t L_2 = ((generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_StaticFields*)il2cpp_codegen_static_fields_for(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_il2cpp_TypeInfo_var))->get_questpoporyad_26();
		if ((!(((uint32_t)L_2) == ((uint32_t)3))))
		{
			goto IL_0032;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_il2cpp_TypeInfo_var);
		((score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_StaticFields*)il2cpp_codegen_static_fields_for(score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_il2cpp_TypeInfo_var))->set_schet_4(((int32_t)1000));
	}

IL_0032:
	{
		int32_t L_3 = ((generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_StaticFields*)il2cpp_codegen_static_fields_for(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_il2cpp_TypeInfo_var))->get_questpoporyad_26();
		if ((!(((uint32_t)L_3) == ((uint32_t)4))))
		{
			goto IL_0044;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_il2cpp_TypeInfo_var);
		((score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_StaticFields*)il2cpp_codegen_static_fields_for(score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_il2cpp_TypeInfo_var))->set_schet_4(((int32_t)2000));
	}

IL_0044:
	{
		int32_t L_4 = ((generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_StaticFields*)il2cpp_codegen_static_fields_for(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_il2cpp_TypeInfo_var))->get_questpoporyad_26();
		if ((!(((uint32_t)L_4) == ((uint32_t)5))))
		{
			goto IL_0056;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_il2cpp_TypeInfo_var);
		((score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_StaticFields*)il2cpp_codegen_static_fields_for(score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_il2cpp_TypeInfo_var))->set_schet_4(((int32_t)3000));
	}

IL_0056:
	{
		int32_t L_5 = ((generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_StaticFields*)il2cpp_codegen_static_fields_for(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_il2cpp_TypeInfo_var))->get_questpoporyad_26();
		if ((!(((uint32_t)L_5) == ((uint32_t)6))))
		{
			goto IL_0068;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_il2cpp_TypeInfo_var);
		((score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_StaticFields*)il2cpp_codegen_static_fields_for(score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_il2cpp_TypeInfo_var))->set_schet_4(((int32_t)5000));
	}

IL_0068:
	{
		int32_t L_6 = ((generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_StaticFields*)il2cpp_codegen_static_fields_for(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_il2cpp_TypeInfo_var))->get_questpoporyad_26();
		if ((!(((uint32_t)L_6) == ((uint32_t)7))))
		{
			goto IL_007a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_il2cpp_TypeInfo_var);
		((score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_StaticFields*)il2cpp_codegen_static_fields_for(score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_il2cpp_TypeInfo_var))->set_schet_4(((int32_t)10000));
	}

IL_007a:
	{
		int32_t L_7 = ((generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_StaticFields*)il2cpp_codegen_static_fields_for(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_il2cpp_TypeInfo_var))->get_questpoporyad_26();
		if ((!(((uint32_t)L_7) == ((uint32_t)8))))
		{
			goto IL_008c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_il2cpp_TypeInfo_var);
		((score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_StaticFields*)il2cpp_codegen_static_fields_for(score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_il2cpp_TypeInfo_var))->set_schet_4(((int32_t)15000));
	}

IL_008c:
	{
		int32_t L_8 = ((generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_StaticFields*)il2cpp_codegen_static_fields_for(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_il2cpp_TypeInfo_var))->get_questpoporyad_26();
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)9)))))
		{
			goto IL_009f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_il2cpp_TypeInfo_var);
		((score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_StaticFields*)il2cpp_codegen_static_fields_for(score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_il2cpp_TypeInfo_var))->set_schet_4(((int32_t)25000));
	}

IL_009f:
	{
		int32_t L_9 = ((generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_StaticFields*)il2cpp_codegen_static_fields_for(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_il2cpp_TypeInfo_var))->get_questpoporyad_26();
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_00b2;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_il2cpp_TypeInfo_var);
		((score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_StaticFields*)il2cpp_codegen_static_fields_for(score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_il2cpp_TypeInfo_var))->set_schet_4(((int32_t)50000));
	}

IL_00b2:
	{
		int32_t L_10 = ((generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_StaticFields*)il2cpp_codegen_static_fields_for(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_il2cpp_TypeInfo_var))->get_questpoporyad_26();
		if ((!(((uint32_t)L_10) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_00c5;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_il2cpp_TypeInfo_var);
		((score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_StaticFields*)il2cpp_codegen_static_fields_for(score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_il2cpp_TypeInfo_var))->set_schet_4(((int32_t)100000));
	}

IL_00c5:
	{
		int32_t L_11 = ((generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_StaticFields*)il2cpp_codegen_static_fields_for(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_il2cpp_TypeInfo_var))->get_questpoporyad_26();
		if ((!(((uint32_t)L_11) == ((uint32_t)((int32_t)12)))))
		{
			goto IL_00d8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_il2cpp_TypeInfo_var);
		((score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_StaticFields*)il2cpp_codegen_static_fields_for(score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_il2cpp_TypeInfo_var))->set_schet_4(((int32_t)200000));
	}

IL_00d8:
	{
		int32_t L_12 = ((generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_StaticFields*)il2cpp_codegen_static_fields_for(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_il2cpp_TypeInfo_var))->get_questpoporyad_26();
		if ((!(((uint32_t)L_12) == ((uint32_t)((int32_t)13)))))
		{
			goto IL_00eb;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_il2cpp_TypeInfo_var);
		((score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_StaticFields*)il2cpp_codegen_static_fields_for(score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_il2cpp_TypeInfo_var))->set_schet_4(((int32_t)400000));
	}

IL_00eb:
	{
		int32_t L_13 = ((generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_StaticFields*)il2cpp_codegen_static_fields_for(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_il2cpp_TypeInfo_var))->get_questpoporyad_26();
		if ((!(((uint32_t)L_13) == ((uint32_t)((int32_t)14)))))
		{
			goto IL_00fe;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_il2cpp_TypeInfo_var);
		((score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_StaticFields*)il2cpp_codegen_static_fields_for(score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_il2cpp_TypeInfo_var))->set_schet_4(((int32_t)800000));
	}

IL_00fe:
	{
		int32_t L_14 = ((generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_StaticFields*)il2cpp_codegen_static_fields_for(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_il2cpp_TypeInfo_var))->get_questpoporyad_26();
		if ((!(((uint32_t)L_14) == ((uint32_t)((int32_t)15)))))
		{
			goto IL_0111;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_il2cpp_TypeInfo_var);
		((score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_StaticFields*)il2cpp_codegen_static_fields_for(score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_il2cpp_TypeInfo_var))->set_schet_4(((int32_t)1500000));
	}

IL_0111:
	{
		((zabratmoney_t7DDABFB3031D063938927397F1B078B66BA485CC_StaticFields*)il2cpp_codegen_static_fields_for(zabratmoney_t7DDABFB3031D063938927397F1B078B66BA485CC_il2cpp_TypeInfo_var))->set_alr_8((bool)0);
		((generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_StaticFields*)il2cpp_codegen_static_fields_for(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_il2cpp_TypeInfo_var))->set_podskazused_39((bool)0);
		bool L_15 = ((start_tAE134A10E53E820C269A6C9B80C302397F2612BC_StaticFields*)il2cpp_codegen_static_fields_for(start_tAE134A10E53E820C269A6C9B80C302397F2612BC_il2cpp_TypeInfo_var))->get_SSEC_29();
		if (!L_15)
		{
			goto IL_0131;
		}
	}
	{
		SceneManager_LoadScene_m9721867D46BC827D58271AD235267B0B0865F115(_stringLiteral2C117556D3F8F65E48D75C404BCFDAC4F9F4AD77, 0, /*hidden argument*/NULL);
		goto IL_013c;
	}

IL_0131:
	{
		SceneManager_LoadScene_m9721867D46BC827D58271AD235267B0B0865F115(_stringLiteral2C117556D3F8F65E48D75C404BCFDAC4F9F4AD77, 0, /*hidden argument*/NULL);
	}

IL_013c:
	{
		int32_t L_16 = PlayerPrefs_GetInt_m318D2B42E0FCAF179BF86D6C2353B38A58089BAD(_stringLiteral75EBCB361C656225206BB0191DF63CB1D38CBBAB, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_il2cpp_TypeInfo_var);
		int32_t L_17 = ((score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_StaticFields*)il2cpp_codegen_static_fields_for(score_t945E9DA2894DF953DA8EA6E5EAA3E0AA69F9B1B2_il2cpp_TypeInfo_var))->get_schet_4();
		PlayerPrefs_SetInt_mBF4101DF829B4738CCC293E1C2D173AEE45EFE62(_stringLiteral75EBCB361C656225206BB0191DF63CB1D38CBBAB, ((int32_t)il2cpp_codegen_add((int32_t)L_16, (int32_t)L_17)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void yes::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void yes__ctor_m38D6295FE568A2CC7B20B3F1E1E1D51743C41D17 (yes_t36BDD09D45583CF07DAA0274CDCE0985E722F5E6 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void zabratmoney::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void zabratmoney_Start_m8AE46FFBCF2E2E2E5235BFA9152568DEF9446832 (zabratmoney_t7DDABFB3031D063938927397F1B078B66BA485CC * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void zabratmoney::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void zabratmoney_Update_mD7F53A577260CC0D172FA63D659541376463A1E4 (zabratmoney_t7DDABFB3031D063938927397F1B078B66BA485CC * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void zabratmoney::OnMouseDown()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void zabratmoney_OnMouseDown_m1220E6EE6FB81C86A773225E7C608BBF78D30EA6 (zabratmoney_t7DDABFB3031D063938927397F1B078B66BA485CC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (zabratmoney_OnMouseDown_m1220E6EE6FB81C86A773225E7C608BBF78D30EA6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ((zabratmoney_t7DDABFB3031D063938927397F1B078B66BA485CC_StaticFields*)il2cpp_codegen_static_fields_for(zabratmoney_t7DDABFB3031D063938927397F1B078B66BA485CC_il2cpp_TypeInfo_var))->get_alr_8();
		if (L_0)
		{
			goto IL_005d;
		}
	}
	{
		bool L_1 = ((onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D_StaticFields*)il2cpp_codegen_static_fields_for(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D_il2cpp_TypeInfo_var))->get_already_27();
		if (L_1)
		{
			goto IL_005d;
		}
	}
	{
		bool L_2 = ((generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_StaticFields*)il2cpp_codegen_static_fields_for(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_il2cpp_TypeInfo_var))->get_podskazused_39();
		if (L_2)
		{
			goto IL_005d;
		}
	}
	{
		((generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_StaticFields*)il2cpp_codegen_static_fields_for(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_il2cpp_TypeInfo_var))->set_podskazused_39((bool)1);
		((zabratmoney_t7DDABFB3031D063938927397F1B078B66BA485CC_StaticFields*)il2cpp_codegen_static_fields_for(zabratmoney_t7DDABFB3031D063938927397F1B078B66BA485CC_il2cpp_TypeInfo_var))->set_alr_8((bool)1);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = __this->get_a_4();
		NullCheck(L_3);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_3, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_4 = __this->get_b_5();
		NullCheck(L_4);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_4, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_5 = __this->get_c_6();
		NullCheck(L_5);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_5, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_6 = __this->get_d_7();
		NullCheck(L_6);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_6, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_7 = __this->get_opa_9();
		NullCheck(L_7);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_7, (bool)1, /*hidden argument*/NULL);
	}

IL_005d:
	{
		return;
	}
}
// System.Void zabratmoney::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void zabratmoney__ctor_m4B52AA36C4EBB005FE0B6B4C739E84D83FA2B44D (zabratmoney_t7DDABFB3031D063938927397F1B078B66BA485CC * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void zaltip::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void zaltip_Start_mF003BE496B7C2E221441F6312AADB74C1AAE755D (zaltip_t9E4D625BB000CD131ACEE4ABCC45F34F66419F87 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (zaltip_Start_mF003BE496B7C2E221441F6312AADB74C1AAE755D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Appodeal_initialize_m891EAFF98230D571C1779B75EE6CFECD08122D44(_stringLiteral446BB1BC637F6C370F33E67ADC71039B469F50AF, ((int32_t)256), (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void zaltip::ShowRewardedAd()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void zaltip_ShowRewardedAd_mB5E223BE079E093762E376AEED55F8D5D71F30D8 (zaltip_t9E4D625BB000CD131ACEE4ABCC45F34F66419F87 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (zaltip_ShowRewardedAd_mB5E223BE079E093762E376AEED55F8D5D71F30D8_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = Appodeal_isLoaded_m8F9485BCC1B4B7D802865FBEB0255C2316BACD0A(((int32_t)256), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_00a5;
		}
	}
	{
		Appodeal_show_m154AD7C996073738388B655CEDE1E99F891FF694(((int32_t)256), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(_stringLiteral46659F20834DBA92057B5B6BC8830AA83C63408C, /*hidden argument*/NULL);
		((generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_StaticFields*)il2cpp_codegen_static_fields_for(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_il2cpp_TypeInfo_var))->set_podskazused_39((bool)1);
		__this->set_used_25((bool)1);
		AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * L_1 = __this->get_tudin_4();
		NullCheck(L_1);
		AudioSource_Play_m0BA206481892AA4AF7DB2900A0B0805076516164(L_1, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_2 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_3 = GameObject_GetComponent_TisAnimator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A_m9904EA7E80165F7771F8AB3967F417D7C2B09996(L_2, /*hidden argument*/GameObject_GetComponent_TisAnimator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A_m9904EA7E80165F7771F8AB3967F417D7C2B09996_RuntimeMethod_var);
		NullCheck(L_3);
		Animator_Play_m254CA699DF64E86856EC8C54D1A739E3C0A49793(L_3, _stringLiteralEECABAE13529439E62DE1A0D81E07B14F9A348C9, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_4 = __this->get_zalfon_5();
		NullCheck(L_4);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_4, (bool)1, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_5 = __this->get_p1_6();
		NullCheck(L_5);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_5, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_6 = __this->get_p2_7();
		NullCheck(L_6);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_6, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_7 = __this->get_p3_8();
		NullCheck(L_7);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_7, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_8 = __this->get_p4_9();
		NullCheck(L_8);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_8, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_9 = __this->get_p5_10();
		NullCheck(L_9);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_9, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_10 = __this->get_p6_11();
		NullCheck(L_10);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_10, (bool)0, /*hidden argument*/NULL);
	}

IL_00a5:
	{
		return;
	}
}
// System.Void zaltip::FixedUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void zaltip_FixedUpdate_m83E52C147D3345C561AACF32B0A9EFA8F6E789BE (zaltip_t9E4D625BB000CD131ACEE4ABCC45F34F66419F87 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (zaltip_FixedUpdate_m83E52C147D3345C561AACF32B0A9EFA8F6E789BE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ((start_tAE134A10E53E820C269A6C9B80C302397F2612BC_StaticFields*)il2cpp_codegen_static_fields_for(start_tAE134A10E53E820C269A6C9B80C302397F2612BC_il2cpp_TypeInfo_var))->get_SSEC_29();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_1, (bool)0, /*hidden argument*/NULL);
	}

IL_0013:
	{
		bool L_2 = __this->get_used_25();
		if (!L_2)
		{
			goto IL_0047;
		}
	}
	{
		float L_3 = __this->get_counter_26();
		float L_4 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		__this->set_counter_26(((float)il2cpp_codegen_subtract((float)L_3, (float)L_4)));
		float L_5 = __this->get_counter_26();
		if ((!(((float)L_5) < ((float)(0.0f)))))
		{
			goto IL_0047;
		}
	}
	{
		zaltip_Naznach_m32DAA727AB6914925B335901C65BF7325FD7B0DC(__this, /*hidden argument*/NULL);
		__this->set_used_25((bool)0);
	}

IL_0047:
	{
		return;
	}
}
// System.Void zaltip::OnMouseDown()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void zaltip_OnMouseDown_mBCD4C20E5E7F3FBC38CF0CB3D676AC4B8C837356 (zaltip_t9E4D625BB000CD131ACEE4ABCC45F34F66419F87 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (zaltip_OnMouseDown_mBCD4C20E5E7F3FBC38CF0CB3D676AC4B8C837356_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ((generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_StaticFields*)il2cpp_codegen_static_fields_for(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_il2cpp_TypeInfo_var))->get_podskazused_39();
		if (L_0)
		{
			goto IL_00ca;
		}
	}
	{
		bool L_1 = ((onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D_StaticFields*)il2cpp_codegen_static_fields_for(onclick_tCF244B7C348F6912B45B6930BE4EC3EC5EA7FE5D_il2cpp_TypeInfo_var))->get_already_27();
		if (L_1)
		{
			goto IL_00ca;
		}
	}
	{
		bool L_2 = __this->get_used_25();
		if (L_2)
		{
			goto IL_00ca;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		String_t* L_4 = Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE(L_3, /*hidden argument*/NULL);
		bool L_5 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_4, _stringLiteral4AEB195CD69ED93520B9B4129636264E0CDC0153, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003d;
		}
	}
	{
		zaltip_ShowRewardedAd_mB5E223BE079E093762E376AEED55F8D5D71F30D8(__this, /*hidden argument*/NULL);
		return;
	}

IL_003d:
	{
		((generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_StaticFields*)il2cpp_codegen_static_fields_for(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_il2cpp_TypeInfo_var))->set_podskazused_39((bool)1);
		__this->set_used_25((bool)1);
		AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * L_6 = __this->get_tudin_4();
		NullCheck(L_6);
		AudioSource_Play_m0BA206481892AA4AF7DB2900A0B0805076516164(L_6, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_7 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_8 = GameObject_GetComponent_TisAnimator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A_m9904EA7E80165F7771F8AB3967F417D7C2B09996(L_7, /*hidden argument*/GameObject_GetComponent_TisAnimator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A_m9904EA7E80165F7771F8AB3967F417D7C2B09996_RuntimeMethod_var);
		NullCheck(L_8);
		Animator_Play_m254CA699DF64E86856EC8C54D1A739E3C0A49793(L_8, _stringLiteralEECABAE13529439E62DE1A0D81E07B14F9A348C9, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_9 = __this->get_zalfon_5();
		NullCheck(L_9);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_9, (bool)1, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_10 = __this->get_p1_6();
		NullCheck(L_10);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_10, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_11 = __this->get_p2_7();
		NullCheck(L_11);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_11, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_12 = __this->get_p3_8();
		NullCheck(L_12);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_12, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_13 = __this->get_p4_9();
		NullCheck(L_13);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_13, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_14 = __this->get_p5_10();
		NullCheck(L_14);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_14, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_15 = __this->get_p6_11();
		NullCheck(L_15);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_15, (bool)0, /*hidden argument*/NULL);
		Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * L_16 = __this->get_ad_24();
		NullCheck(L_16);
		Behaviour_set_enabled_m9755D3B17D7022D23D1E4C618BD9A6B66A5ADC6B(L_16, (bool)1, /*hidden argument*/NULL);
	}

IL_00ca:
	{
		return;
	}
}
// System.Void zaltip::Naznach()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void zaltip_Naznach_m32DAA727AB6914925B335901C65BF7325FD7B0DC (zaltip_t9E4D625BB000CD131ACEE4ABCC45F34F66419F87 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (zaltip_Naznach_m32DAA727AB6914925B335901C65BF7325FD7B0DC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_0 = __this->get_firsta_16();
		NullCheck(L_0);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_2 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_1, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3;
		memset((&L_3), 0, sizeof(L_3));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_3), (1.22f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_2, L_3, /*hidden argument*/NULL);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_4 = __this->get_seca_17();
		NullCheck(L_4);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_5 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_6 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_5, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7;
		memset((&L_7), 0, sizeof(L_7));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_7), (1.22f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_6, L_7, /*hidden argument*/NULL);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_8 = __this->get_thirda_18();
		NullCheck(L_8);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_9 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_10 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_9, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_11;
		memset((&L_11), 0, sizeof(L_11));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_11), (1.22f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_10, L_11, /*hidden argument*/NULL);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_12 = __this->get_foura_19();
		NullCheck(L_12);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_13 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_14 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_13, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_15;
		memset((&L_15), 0, sizeof(L_15));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_15), (1.22f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_14);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_14, L_15, /*hidden argument*/NULL);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_16 = __this->get_firsta_16();
		NullCheck(L_16);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_17 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_18 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_17, /*hidden argument*/NULL);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_19 = __this->get_firsta_16();
		NullCheck(L_19);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_20 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_21 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_22 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_21, /*hidden argument*/NULL);
		float L_23 = L_22.get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_24;
		memset((&L_24), 0, sizeof(L_24));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_24), L_23, (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_18);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_18, L_24, /*hidden argument*/NULL);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_25 = __this->get_seca_17();
		NullCheck(L_25);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_26 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_25, /*hidden argument*/NULL);
		NullCheck(L_26);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_27 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_26, /*hidden argument*/NULL);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_28 = __this->get_seca_17();
		NullCheck(L_28);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_29 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_30 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_31 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_30, /*hidden argument*/NULL);
		float L_32 = L_31.get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_33;
		memset((&L_33), 0, sizeof(L_33));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_33), L_32, (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_27);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_27, L_33, /*hidden argument*/NULL);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_34 = __this->get_thirda_18();
		NullCheck(L_34);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_35 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_34, /*hidden argument*/NULL);
		NullCheck(L_35);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_36 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_35, /*hidden argument*/NULL);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_37 = __this->get_thirda_18();
		NullCheck(L_37);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_38 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_37, /*hidden argument*/NULL);
		NullCheck(L_38);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_39 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_38, /*hidden argument*/NULL);
		NullCheck(L_39);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_40 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_39, /*hidden argument*/NULL);
		float L_41 = L_40.get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_42;
		memset((&L_42), 0, sizeof(L_42));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_42), L_41, (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_36);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_36, L_42, /*hidden argument*/NULL);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_43 = __this->get_foura_19();
		NullCheck(L_43);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_44 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_43, /*hidden argument*/NULL);
		NullCheck(L_44);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_45 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_44, /*hidden argument*/NULL);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_46 = __this->get_foura_19();
		NullCheck(L_46);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_47 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_46, /*hidden argument*/NULL);
		NullCheck(L_47);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_48 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_47, /*hidden argument*/NULL);
		NullCheck(L_48);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_49 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_48, /*hidden argument*/NULL);
		float L_50 = L_49.get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_51;
		memset((&L_51), 0, sizeof(L_51));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_51), L_50, (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_45);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_45, L_51, /*hidden argument*/NULL);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_52 = __this->get_firsta_16();
		NullCheck(L_52);
		Animator_set_speed_mEA558D196D84684744A642A56AFBF22F16448813(L_52, (0.0f), /*hidden argument*/NULL);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_53 = __this->get_seca_17();
		NullCheck(L_53);
		Animator_set_speed_mEA558D196D84684744A642A56AFBF22F16448813(L_53, (0.0f), /*hidden argument*/NULL);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_54 = __this->get_thirda_18();
		NullCheck(L_54);
		Animator_set_speed_mEA558D196D84684744A642A56AFBF22F16448813(L_54, (0.0f), /*hidden argument*/NULL);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_55 = __this->get_foura_19();
		NullCheck(L_55);
		Animator_set_speed_mEA558D196D84684744A642A56AFBF22F16448813(L_55, (0.0f), /*hidden argument*/NULL);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_56 = __this->get_firsta_16();
		NullCheck(L_56);
		Behaviour_set_enabled_m9755D3B17D7022D23D1E4C618BD9A6B66A5ADC6B(L_56, (bool)0, /*hidden argument*/NULL);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_57 = __this->get_seca_17();
		NullCheck(L_57);
		Behaviour_set_enabled_m9755D3B17D7022D23D1E4C618BD9A6B66A5ADC6B(L_57, (bool)0, /*hidden argument*/NULL);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_58 = __this->get_thirda_18();
		NullCheck(L_58);
		Behaviour_set_enabled_m9755D3B17D7022D23D1E4C618BD9A6B66A5ADC6B(L_58, (bool)0, /*hidden argument*/NULL);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_59 = __this->get_foura_19();
		NullCheck(L_59);
		Behaviour_set_enabled_m9755D3B17D7022D23D1E4C618BD9A6B66A5ADC6B(L_59, (bool)0, /*hidden argument*/NULL);
		int32_t L_60 = Random_Range_mD0C8F37FF3CAB1D87AAA6C45130BD59626BD6780(((int32_t)42), ((int32_t)98), /*hidden argument*/NULL);
		V_0 = L_60;
		int32_t L_61 = V_0;
		int32_t L_62 = Random_Range_mD0C8F37FF3CAB1D87AAA6C45130BD59626BD6780(0, ((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)100), (int32_t)L_61)), /*hidden argument*/NULL);
		V_1 = L_62;
		int32_t L_63 = V_1;
		int32_t L_64 = V_0;
		int32_t L_65 = Random_Range_mD0C8F37FF3CAB1D87AAA6C45130BD59626BD6780(0, ((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)100), (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_63, (int32_t)L_64)))), /*hidden argument*/NULL);
		V_2 = L_65;
		int32_t L_66 = V_2;
		int32_t L_67 = V_1;
		int32_t L_68 = V_0;
		V_3 = ((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)100), (int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_66, (int32_t)L_67)), (int32_t)L_68))));
		int32_t L_69 = ((generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_StaticFields*)il2cpp_codegen_static_fields_for(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_il2cpp_TypeInfo_var))->get_whichtrue_30();
		if ((!(((uint32_t)L_69) == ((uint32_t)1))))
		{
			goto IL_045e;
		}
	}
	{
		TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * L_70 = __this->get_a_20();
		String_t* L_71 = Int32_ToString_m1863896DE712BF97C031D55B12E1583F1982DC02((int32_t*)(&V_0), /*hidden argument*/NULL);
		String_t* L_72 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(L_71, _stringLiteral4345CB1FA27885A8FBFE7C0C830A592CC76A552B, /*hidden argument*/NULL);
		NullCheck(L_70);
		TextMesh_set_text_m64242AB987CF285F432E7AED38F24FF855E9B220(L_70, L_72, /*hidden argument*/NULL);
		TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * L_73 = __this->get_b_21();
		String_t* L_74 = Int32_ToString_m1863896DE712BF97C031D55B12E1583F1982DC02((int32_t*)(&V_1), /*hidden argument*/NULL);
		String_t* L_75 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(L_74, _stringLiteral4345CB1FA27885A8FBFE7C0C830A592CC76A552B, /*hidden argument*/NULL);
		NullCheck(L_73);
		TextMesh_set_text_m64242AB987CF285F432E7AED38F24FF855E9B220(L_73, L_75, /*hidden argument*/NULL);
		TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * L_76 = __this->get_c_22();
		String_t* L_77 = Int32_ToString_m1863896DE712BF97C031D55B12E1583F1982DC02((int32_t*)(&V_2), /*hidden argument*/NULL);
		String_t* L_78 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(L_77, _stringLiteral4345CB1FA27885A8FBFE7C0C830A592CC76A552B, /*hidden argument*/NULL);
		NullCheck(L_76);
		TextMesh_set_text_m64242AB987CF285F432E7AED38F24FF855E9B220(L_76, L_78, /*hidden argument*/NULL);
		TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * L_79 = __this->get_d_23();
		String_t* L_80 = Int32_ToString_m1863896DE712BF97C031D55B12E1583F1982DC02((int32_t*)(&V_3), /*hidden argument*/NULL);
		String_t* L_81 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(L_80, _stringLiteral4345CB1FA27885A8FBFE7C0C830A592CC76A552B, /*hidden argument*/NULL);
		NullCheck(L_79);
		TextMesh_set_text_m64242AB987CF285F432E7AED38F24FF855E9B220(L_79, L_81, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_82 = __this->get_firstp_12();
		NullCheck(L_82);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_83 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_82, /*hidden argument*/NULL);
		int32_t L_84 = V_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_85;
		memset((&L_85), 0, sizeof(L_85));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_85), (1.0f), ((float)il2cpp_codegen_multiply((float)(((float)((float)L_84))), (float)(0.01f))), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_83);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_83, L_85, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_86 = __this->get_firstp_12();
		NullCheck(L_86);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_87 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_86, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_88 = __this->get_firstp_12();
		NullCheck(L_88);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_89 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_88, /*hidden argument*/NULL);
		NullCheck(L_89);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_90 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_89, /*hidden argument*/NULL);
		float L_91 = L_90.get_x_2();
		int32_t L_92 = V_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_93;
		memset((&L_93), 0, sizeof(L_93));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_93), L_91, ((-((float)il2cpp_codegen_multiply((float)(0.31f), (float)((float)il2cpp_codegen_subtract((float)(10.0f), (float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_92))), (float)(0.1f))))))))), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_87);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_87, L_93, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_94 = __this->get_secp_13();
		NullCheck(L_94);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_95 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_94, /*hidden argument*/NULL);
		int32_t L_96 = V_1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_97;
		memset((&L_97), 0, sizeof(L_97));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_97), (1.0f), ((float)il2cpp_codegen_multiply((float)(((float)((float)L_96))), (float)(0.01f))), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_95);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_95, L_97, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_98 = __this->get_secp_13();
		NullCheck(L_98);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_99 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_98, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_100 = __this->get_secp_13();
		NullCheck(L_100);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_101 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_100, /*hidden argument*/NULL);
		NullCheck(L_101);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_102 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_101, /*hidden argument*/NULL);
		float L_103 = L_102.get_x_2();
		int32_t L_104 = V_1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_105;
		memset((&L_105), 0, sizeof(L_105));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_105), L_103, ((-((float)il2cpp_codegen_multiply((float)(0.31f), (float)((float)il2cpp_codegen_subtract((float)(10.0f), (float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_104))), (float)(0.1f))))))))), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_99);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_99, L_105, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_106 = __this->get_thirdp_14();
		NullCheck(L_106);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_107 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_106, /*hidden argument*/NULL);
		int32_t L_108 = V_2;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_109;
		memset((&L_109), 0, sizeof(L_109));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_109), (1.0f), ((float)il2cpp_codegen_multiply((float)(((float)((float)L_108))), (float)(0.01f))), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_107);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_107, L_109, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_110 = __this->get_thirdp_14();
		NullCheck(L_110);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_111 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_110, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_112 = __this->get_thirdp_14();
		NullCheck(L_112);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_113 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_112, /*hidden argument*/NULL);
		NullCheck(L_113);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_114 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_113, /*hidden argument*/NULL);
		float L_115 = L_114.get_x_2();
		int32_t L_116 = V_2;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_117;
		memset((&L_117), 0, sizeof(L_117));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_117), L_115, ((-((float)il2cpp_codegen_multiply((float)(0.31f), (float)((float)il2cpp_codegen_subtract((float)(10.0f), (float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_116))), (float)(0.1f))))))))), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_111);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_111, L_117, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_118 = __this->get_fourp_15();
		NullCheck(L_118);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_119 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_118, /*hidden argument*/NULL);
		int32_t L_120 = V_3;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_121;
		memset((&L_121), 0, sizeof(L_121));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_121), (1.0f), ((float)il2cpp_codegen_multiply((float)(((float)((float)L_120))), (float)(0.01f))), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_119);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_119, L_121, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_122 = __this->get_fourp_15();
		NullCheck(L_122);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_123 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_122, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_124 = __this->get_fourp_15();
		NullCheck(L_124);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_125 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_124, /*hidden argument*/NULL);
		NullCheck(L_125);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_126 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_125, /*hidden argument*/NULL);
		float L_127 = L_126.get_x_2();
		int32_t L_128 = V_3;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_129;
		memset((&L_129), 0, sizeof(L_129));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_129), L_127, ((-((float)il2cpp_codegen_multiply((float)(0.31f), (float)((float)il2cpp_codegen_subtract((float)(10.0f), (float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_128))), (float)(0.1f))))))))), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_123);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_123, L_129, /*hidden argument*/NULL);
	}

IL_045e:
	{
		int32_t L_130 = ((generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_StaticFields*)il2cpp_codegen_static_fields_for(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_il2cpp_TypeInfo_var))->get_whichtrue_30();
		if ((!(((uint32_t)L_130) == ((uint32_t)2))))
		{
			goto IL_0685;
		}
	}
	{
		TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * L_131 = __this->get_a_20();
		String_t* L_132 = Int32_ToString_m1863896DE712BF97C031D55B12E1583F1982DC02((int32_t*)(&V_1), /*hidden argument*/NULL);
		String_t* L_133 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(L_132, _stringLiteral4345CB1FA27885A8FBFE7C0C830A592CC76A552B, /*hidden argument*/NULL);
		NullCheck(L_131);
		TextMesh_set_text_m64242AB987CF285F432E7AED38F24FF855E9B220(L_131, L_133, /*hidden argument*/NULL);
		TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * L_134 = __this->get_b_21();
		String_t* L_135 = Int32_ToString_m1863896DE712BF97C031D55B12E1583F1982DC02((int32_t*)(&V_0), /*hidden argument*/NULL);
		String_t* L_136 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(L_135, _stringLiteral4345CB1FA27885A8FBFE7C0C830A592CC76A552B, /*hidden argument*/NULL);
		NullCheck(L_134);
		TextMesh_set_text_m64242AB987CF285F432E7AED38F24FF855E9B220(L_134, L_136, /*hidden argument*/NULL);
		TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * L_137 = __this->get_c_22();
		String_t* L_138 = Int32_ToString_m1863896DE712BF97C031D55B12E1583F1982DC02((int32_t*)(&V_2), /*hidden argument*/NULL);
		String_t* L_139 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(L_138, _stringLiteral4345CB1FA27885A8FBFE7C0C830A592CC76A552B, /*hidden argument*/NULL);
		NullCheck(L_137);
		TextMesh_set_text_m64242AB987CF285F432E7AED38F24FF855E9B220(L_137, L_139, /*hidden argument*/NULL);
		TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * L_140 = __this->get_d_23();
		String_t* L_141 = Int32_ToString_m1863896DE712BF97C031D55B12E1583F1982DC02((int32_t*)(&V_3), /*hidden argument*/NULL);
		String_t* L_142 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(L_141, _stringLiteral4345CB1FA27885A8FBFE7C0C830A592CC76A552B, /*hidden argument*/NULL);
		NullCheck(L_140);
		TextMesh_set_text_m64242AB987CF285F432E7AED38F24FF855E9B220(L_140, L_142, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_143 = __this->get_firstp_12();
		NullCheck(L_143);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_144 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_143, /*hidden argument*/NULL);
		int32_t L_145 = V_1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_146;
		memset((&L_146), 0, sizeof(L_146));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_146), (1.0f), ((float)il2cpp_codegen_multiply((float)(((float)((float)L_145))), (float)(0.01f))), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_144);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_144, L_146, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_147 = __this->get_firstp_12();
		NullCheck(L_147);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_148 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_147, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_149 = __this->get_firstp_12();
		NullCheck(L_149);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_150 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_149, /*hidden argument*/NULL);
		NullCheck(L_150);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_151 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_150, /*hidden argument*/NULL);
		float L_152 = L_151.get_x_2();
		int32_t L_153 = V_1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_154;
		memset((&L_154), 0, sizeof(L_154));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_154), L_152, ((-((float)il2cpp_codegen_multiply((float)(0.31f), (float)((float)il2cpp_codegen_subtract((float)(10.0f), (float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_153))), (float)(0.1f))))))))), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_148);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_148, L_154, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_155 = __this->get_secp_13();
		NullCheck(L_155);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_156 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_155, /*hidden argument*/NULL);
		int32_t L_157 = V_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_158;
		memset((&L_158), 0, sizeof(L_158));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_158), (1.0f), ((float)il2cpp_codegen_multiply((float)(((float)((float)L_157))), (float)(0.01f))), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_156);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_156, L_158, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_159 = __this->get_secp_13();
		NullCheck(L_159);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_160 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_159, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_161 = __this->get_secp_13();
		NullCheck(L_161);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_162 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_161, /*hidden argument*/NULL);
		NullCheck(L_162);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_163 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_162, /*hidden argument*/NULL);
		float L_164 = L_163.get_x_2();
		int32_t L_165 = V_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_166;
		memset((&L_166), 0, sizeof(L_166));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_166), L_164, ((-((float)il2cpp_codegen_multiply((float)(0.31f), (float)((float)il2cpp_codegen_subtract((float)(10.0f), (float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_165))), (float)(0.1f))))))))), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_160);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_160, L_166, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_167 = __this->get_thirdp_14();
		NullCheck(L_167);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_168 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_167, /*hidden argument*/NULL);
		int32_t L_169 = V_2;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_170;
		memset((&L_170), 0, sizeof(L_170));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_170), (1.0f), ((float)il2cpp_codegen_multiply((float)(((float)((float)L_169))), (float)(0.01f))), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_168);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_168, L_170, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_171 = __this->get_thirdp_14();
		NullCheck(L_171);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_172 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_171, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_173 = __this->get_thirdp_14();
		NullCheck(L_173);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_174 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_173, /*hidden argument*/NULL);
		NullCheck(L_174);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_175 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_174, /*hidden argument*/NULL);
		float L_176 = L_175.get_x_2();
		int32_t L_177 = V_2;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_178;
		memset((&L_178), 0, sizeof(L_178));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_178), L_176, ((-((float)il2cpp_codegen_multiply((float)(0.31f), (float)((float)il2cpp_codegen_subtract((float)(10.0f), (float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_177))), (float)(0.1f))))))))), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_172);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_172, L_178, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_179 = __this->get_fourp_15();
		NullCheck(L_179);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_180 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_179, /*hidden argument*/NULL);
		int32_t L_181 = V_3;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_182;
		memset((&L_182), 0, sizeof(L_182));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_182), (1.0f), ((float)il2cpp_codegen_multiply((float)(((float)((float)L_181))), (float)(0.01f))), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_180);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_180, L_182, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_183 = __this->get_fourp_15();
		NullCheck(L_183);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_184 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_183, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_185 = __this->get_fourp_15();
		NullCheck(L_185);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_186 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_185, /*hidden argument*/NULL);
		NullCheck(L_186);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_187 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_186, /*hidden argument*/NULL);
		float L_188 = L_187.get_x_2();
		int32_t L_189 = V_3;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_190;
		memset((&L_190), 0, sizeof(L_190));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_190), L_188, ((-((float)il2cpp_codegen_multiply((float)(0.31f), (float)((float)il2cpp_codegen_subtract((float)(10.0f), (float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_189))), (float)(0.1f))))))))), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_184);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_184, L_190, /*hidden argument*/NULL);
	}

IL_0685:
	{
		int32_t L_191 = ((generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_StaticFields*)il2cpp_codegen_static_fields_for(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_il2cpp_TypeInfo_var))->get_whichtrue_30();
		if ((!(((uint32_t)L_191) == ((uint32_t)3))))
		{
			goto IL_08ac;
		}
	}
	{
		TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * L_192 = __this->get_a_20();
		String_t* L_193 = Int32_ToString_m1863896DE712BF97C031D55B12E1583F1982DC02((int32_t*)(&V_2), /*hidden argument*/NULL);
		String_t* L_194 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(L_193, _stringLiteral4345CB1FA27885A8FBFE7C0C830A592CC76A552B, /*hidden argument*/NULL);
		NullCheck(L_192);
		TextMesh_set_text_m64242AB987CF285F432E7AED38F24FF855E9B220(L_192, L_194, /*hidden argument*/NULL);
		TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * L_195 = __this->get_b_21();
		String_t* L_196 = Int32_ToString_m1863896DE712BF97C031D55B12E1583F1982DC02((int32_t*)(&V_1), /*hidden argument*/NULL);
		String_t* L_197 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(L_196, _stringLiteral4345CB1FA27885A8FBFE7C0C830A592CC76A552B, /*hidden argument*/NULL);
		NullCheck(L_195);
		TextMesh_set_text_m64242AB987CF285F432E7AED38F24FF855E9B220(L_195, L_197, /*hidden argument*/NULL);
		TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * L_198 = __this->get_c_22();
		String_t* L_199 = Int32_ToString_m1863896DE712BF97C031D55B12E1583F1982DC02((int32_t*)(&V_0), /*hidden argument*/NULL);
		String_t* L_200 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(L_199, _stringLiteral4345CB1FA27885A8FBFE7C0C830A592CC76A552B, /*hidden argument*/NULL);
		NullCheck(L_198);
		TextMesh_set_text_m64242AB987CF285F432E7AED38F24FF855E9B220(L_198, L_200, /*hidden argument*/NULL);
		TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * L_201 = __this->get_d_23();
		String_t* L_202 = Int32_ToString_m1863896DE712BF97C031D55B12E1583F1982DC02((int32_t*)(&V_3), /*hidden argument*/NULL);
		String_t* L_203 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(L_202, _stringLiteral4345CB1FA27885A8FBFE7C0C830A592CC76A552B, /*hidden argument*/NULL);
		NullCheck(L_201);
		TextMesh_set_text_m64242AB987CF285F432E7AED38F24FF855E9B220(L_201, L_203, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_204 = __this->get_firstp_12();
		NullCheck(L_204);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_205 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_204, /*hidden argument*/NULL);
		int32_t L_206 = V_2;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_207;
		memset((&L_207), 0, sizeof(L_207));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_207), (1.0f), ((float)il2cpp_codegen_multiply((float)(((float)((float)L_206))), (float)(0.01f))), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_205);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_205, L_207, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_208 = __this->get_firstp_12();
		NullCheck(L_208);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_209 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_208, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_210 = __this->get_firstp_12();
		NullCheck(L_210);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_211 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_210, /*hidden argument*/NULL);
		NullCheck(L_211);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_212 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_211, /*hidden argument*/NULL);
		float L_213 = L_212.get_x_2();
		int32_t L_214 = V_2;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_215;
		memset((&L_215), 0, sizeof(L_215));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_215), L_213, ((-((float)il2cpp_codegen_multiply((float)(0.31f), (float)((float)il2cpp_codegen_subtract((float)(10.0f), (float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_214))), (float)(0.1f))))))))), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_209);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_209, L_215, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_216 = __this->get_secp_13();
		NullCheck(L_216);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_217 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_216, /*hidden argument*/NULL);
		int32_t L_218 = V_1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_219;
		memset((&L_219), 0, sizeof(L_219));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_219), (1.0f), ((float)il2cpp_codegen_multiply((float)(((float)((float)L_218))), (float)(0.01f))), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_217);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_217, L_219, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_220 = __this->get_secp_13();
		NullCheck(L_220);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_221 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_220, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_222 = __this->get_secp_13();
		NullCheck(L_222);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_223 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_222, /*hidden argument*/NULL);
		NullCheck(L_223);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_224 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_223, /*hidden argument*/NULL);
		float L_225 = L_224.get_x_2();
		int32_t L_226 = V_1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_227;
		memset((&L_227), 0, sizeof(L_227));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_227), L_225, ((-((float)il2cpp_codegen_multiply((float)(0.31f), (float)((float)il2cpp_codegen_subtract((float)(10.0f), (float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_226))), (float)(0.1f))))))))), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_221);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_221, L_227, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_228 = __this->get_thirdp_14();
		NullCheck(L_228);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_229 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_228, /*hidden argument*/NULL);
		int32_t L_230 = V_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_231;
		memset((&L_231), 0, sizeof(L_231));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_231), (1.0f), ((float)il2cpp_codegen_multiply((float)(((float)((float)L_230))), (float)(0.01f))), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_229);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_229, L_231, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_232 = __this->get_thirdp_14();
		NullCheck(L_232);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_233 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_232, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_234 = __this->get_thirdp_14();
		NullCheck(L_234);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_235 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_234, /*hidden argument*/NULL);
		NullCheck(L_235);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_236 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_235, /*hidden argument*/NULL);
		float L_237 = L_236.get_x_2();
		int32_t L_238 = V_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_239;
		memset((&L_239), 0, sizeof(L_239));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_239), L_237, ((-((float)il2cpp_codegen_multiply((float)(0.31f), (float)((float)il2cpp_codegen_subtract((float)(10.0f), (float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_238))), (float)(0.1f))))))))), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_233);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_233, L_239, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_240 = __this->get_fourp_15();
		NullCheck(L_240);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_241 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_240, /*hidden argument*/NULL);
		int32_t L_242 = V_3;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_243;
		memset((&L_243), 0, sizeof(L_243));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_243), (1.0f), ((float)il2cpp_codegen_multiply((float)(((float)((float)L_242))), (float)(0.01f))), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_241);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_241, L_243, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_244 = __this->get_fourp_15();
		NullCheck(L_244);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_245 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_244, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_246 = __this->get_fourp_15();
		NullCheck(L_246);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_247 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_246, /*hidden argument*/NULL);
		NullCheck(L_247);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_248 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_247, /*hidden argument*/NULL);
		float L_249 = L_248.get_x_2();
		int32_t L_250 = V_3;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_251;
		memset((&L_251), 0, sizeof(L_251));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_251), L_249, ((-((float)il2cpp_codegen_multiply((float)(0.31f), (float)((float)il2cpp_codegen_subtract((float)(10.0f), (float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_250))), (float)(0.1f))))))))), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_245);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_245, L_251, /*hidden argument*/NULL);
	}

IL_08ac:
	{
		int32_t L_252 = ((generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_StaticFields*)il2cpp_codegen_static_fields_for(generator_tA4E623ECFF944EA52CA3C5FB479C6772EE2CEEEB_il2cpp_TypeInfo_var))->get_whichtrue_30();
		if ((!(((uint32_t)L_252) == ((uint32_t)4))))
		{
			goto IL_0ad3;
		}
	}
	{
		TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * L_253 = __this->get_a_20();
		String_t* L_254 = Int32_ToString_m1863896DE712BF97C031D55B12E1583F1982DC02((int32_t*)(&V_3), /*hidden argument*/NULL);
		String_t* L_255 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(L_254, _stringLiteral4345CB1FA27885A8FBFE7C0C830A592CC76A552B, /*hidden argument*/NULL);
		NullCheck(L_253);
		TextMesh_set_text_m64242AB987CF285F432E7AED38F24FF855E9B220(L_253, L_255, /*hidden argument*/NULL);
		TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * L_256 = __this->get_b_21();
		String_t* L_257 = Int32_ToString_m1863896DE712BF97C031D55B12E1583F1982DC02((int32_t*)(&V_1), /*hidden argument*/NULL);
		String_t* L_258 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(L_257, _stringLiteral4345CB1FA27885A8FBFE7C0C830A592CC76A552B, /*hidden argument*/NULL);
		NullCheck(L_256);
		TextMesh_set_text_m64242AB987CF285F432E7AED38F24FF855E9B220(L_256, L_258, /*hidden argument*/NULL);
		TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * L_259 = __this->get_c_22();
		String_t* L_260 = Int32_ToString_m1863896DE712BF97C031D55B12E1583F1982DC02((int32_t*)(&V_2), /*hidden argument*/NULL);
		String_t* L_261 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(L_260, _stringLiteral4345CB1FA27885A8FBFE7C0C830A592CC76A552B, /*hidden argument*/NULL);
		NullCheck(L_259);
		TextMesh_set_text_m64242AB987CF285F432E7AED38F24FF855E9B220(L_259, L_261, /*hidden argument*/NULL);
		TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * L_262 = __this->get_d_23();
		String_t* L_263 = Int32_ToString_m1863896DE712BF97C031D55B12E1583F1982DC02((int32_t*)(&V_0), /*hidden argument*/NULL);
		String_t* L_264 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(L_263, _stringLiteral4345CB1FA27885A8FBFE7C0C830A592CC76A552B, /*hidden argument*/NULL);
		NullCheck(L_262);
		TextMesh_set_text_m64242AB987CF285F432E7AED38F24FF855E9B220(L_262, L_264, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_265 = __this->get_firstp_12();
		NullCheck(L_265);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_266 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_265, /*hidden argument*/NULL);
		int32_t L_267 = V_3;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_268;
		memset((&L_268), 0, sizeof(L_268));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_268), (1.0f), ((float)il2cpp_codegen_multiply((float)(((float)((float)L_267))), (float)(0.01f))), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_266);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_266, L_268, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_269 = __this->get_firstp_12();
		NullCheck(L_269);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_270 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_269, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_271 = __this->get_firstp_12();
		NullCheck(L_271);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_272 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_271, /*hidden argument*/NULL);
		NullCheck(L_272);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_273 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_272, /*hidden argument*/NULL);
		float L_274 = L_273.get_x_2();
		int32_t L_275 = V_3;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_276;
		memset((&L_276), 0, sizeof(L_276));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_276), L_274, ((-((float)il2cpp_codegen_multiply((float)(0.31f), (float)((float)il2cpp_codegen_subtract((float)(10.0f), (float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_275))), (float)(0.1f))))))))), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_270);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_270, L_276, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_277 = __this->get_secp_13();
		NullCheck(L_277);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_278 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_277, /*hidden argument*/NULL);
		int32_t L_279 = V_1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_280;
		memset((&L_280), 0, sizeof(L_280));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_280), (1.0f), ((float)il2cpp_codegen_multiply((float)(((float)((float)L_279))), (float)(0.01f))), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_278);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_278, L_280, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_281 = __this->get_secp_13();
		NullCheck(L_281);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_282 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_281, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_283 = __this->get_secp_13();
		NullCheck(L_283);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_284 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_283, /*hidden argument*/NULL);
		NullCheck(L_284);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_285 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_284, /*hidden argument*/NULL);
		float L_286 = L_285.get_x_2();
		int32_t L_287 = V_1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_288;
		memset((&L_288), 0, sizeof(L_288));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_288), L_286, ((-((float)il2cpp_codegen_multiply((float)(0.31f), (float)((float)il2cpp_codegen_subtract((float)(10.0f), (float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_287))), (float)(0.1f))))))))), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_282);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_282, L_288, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_289 = __this->get_thirdp_14();
		NullCheck(L_289);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_290 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_289, /*hidden argument*/NULL);
		int32_t L_291 = V_2;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_292;
		memset((&L_292), 0, sizeof(L_292));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_292), (1.0f), ((float)il2cpp_codegen_multiply((float)(((float)((float)L_291))), (float)(0.01f))), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_290);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_290, L_292, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_293 = __this->get_thirdp_14();
		NullCheck(L_293);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_294 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_293, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_295 = __this->get_thirdp_14();
		NullCheck(L_295);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_296 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_295, /*hidden argument*/NULL);
		NullCheck(L_296);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_297 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_296, /*hidden argument*/NULL);
		float L_298 = L_297.get_x_2();
		int32_t L_299 = V_2;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_300;
		memset((&L_300), 0, sizeof(L_300));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_300), L_298, ((-((float)il2cpp_codegen_multiply((float)(0.31f), (float)((float)il2cpp_codegen_subtract((float)(10.0f), (float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_299))), (float)(0.1f))))))))), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_294);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_294, L_300, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_301 = __this->get_fourp_15();
		NullCheck(L_301);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_302 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_301, /*hidden argument*/NULL);
		int32_t L_303 = V_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_304;
		memset((&L_304), 0, sizeof(L_304));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_304), (1.0f), ((float)il2cpp_codegen_multiply((float)(((float)((float)L_303))), (float)(0.01f))), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_302);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_302, L_304, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_305 = __this->get_fourp_15();
		NullCheck(L_305);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_306 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_305, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_307 = __this->get_fourp_15();
		NullCheck(L_307);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_308 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_307, /*hidden argument*/NULL);
		NullCheck(L_308);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_309 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_308, /*hidden argument*/NULL);
		float L_310 = L_309.get_x_2();
		int32_t L_311 = V_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_312;
		memset((&L_312), 0, sizeof(L_312));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_312), L_310, ((-((float)il2cpp_codegen_multiply((float)(0.31f), (float)((float)il2cpp_codegen_subtract((float)(10.0f), (float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_311))), (float)(0.1f))))))))), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_306);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_306, L_312, /*hidden argument*/NULL);
	}

IL_0ad3:
	{
		return;
	}
}
// System.Void zaltip::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void zaltip__ctor_m03972C1473ABCD6C0F445A82B6ACD2D0F3262DFD (zaltip_t9E4D625BB000CD131ACEE4ABCC45F34F66419F87 * __this, const RuntimeMethod* method)
{
	{
		__this->set_counter_26((4.0f));
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
